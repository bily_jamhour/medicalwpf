﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace MD_Controls
{
    public class Md_Controls
    {
        static public void ShowException(string sFunction, string sModuleID, Exception ex, string sDescription = "")
        {
            string sMsg = "";
            string sAux = "";
            string sExType = "";

            if (ex != null)
            {
                sMsg = ex.Message.Split('#')[0].ToString();
                sExType = ex.GetType().Name;
            }
            else
            {
                sExType = sModuleID;
            }

            sAux = "[" + sModuleID + " - " + sFunction + "]";
            if (sDescription != "")
                sAux += "\r\n" + sDescription;

            if (sMsg != "")
                sAux += "\r\n\r\n" + sMsg;


            RadMessageBox.Show(sAux + "\r\n\r\nConsulte archivo Log.", sExType, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
        }
    }
}
