﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MD_Controls
{
    public partial class InputNumeric : UserControl
    {
        public InputNumeric()
        {
            InitializeComponent();
        }
        private int iNumeric = -1;
        public void Clear()
        {
            txtNumeric.Text = "";
        }
        private void radTextBoxControl1_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            iNumeric = -1;
            int.TryParse(txtNumeric.Text, out iNumeric);
            if (iNumeric == 0)
            {
                txtNumeric.TextChanging -= radTextBoxControl1_TextChanging;
                txtNumeric.Text = "";
                txtNumeric.TextChanging += radTextBoxControl1_TextChanging;
            }
        }
        public Int32 Value()
        {
            radTextBoxControl1_TextChanging(this, null);
            return (iNumeric == -1) ? 0 : iNumeric;
        }
        public void SetTitle(string sTitle)
        {
            lblNumeric.Text = sTitle;
        }

        public void SetValue(string sNumber)
        {
            int iNumber = 0;
            int.TryParse(sNumber, out iNumber);
            txtNumeric.Text = iNumber.ToString();

        }

        private void txtNumeric_Leave(object sender, EventArgs e)
        {
            radTextBoxControl1_TextChanging(sender, null);
        }
    }
}
