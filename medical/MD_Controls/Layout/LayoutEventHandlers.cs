﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;

namespace MD_Controls.Layout
{
    public class LayoutGetGridListEventParameter : EventArgs
    {
        public List<RadGridView> GridViews { get; set; }
        public bool Handled { get; set; }

        public LayoutGetGridListEventParameter()
            : base()
        {
            GridViews = new List<RadGridView>();
        }
    }

    public class LayoutSerializeEventParameter : EventArgs
    {
        public LayoutContainerObject Elements { get; set; }

        public LayoutSerializeEventParameter()
        {
            Elements = new LayoutContainerObject();
        }
    }

    public delegate void PrintOrExportEventHandler(LayoutControl.ExportModeEnum mode, LayoutControl.ExportSelectionEnum selection);

    public delegate void GetGridsEventHandler(object sender, LayoutGetGridListEventParameter e);

    public delegate void SerializationEventHandler(object sender, LayoutSerializeEventParameter e);

    [Serializable]
    public class LayoutContainerObject
    {
        public List<LayoutElement> LayoutElements { get; set; }

        public LayoutContainerObject() : base()
        {
            LayoutElements = new List<LayoutElement>();
        }
    }

    public class LayoutElement
    {
        public string Name { get; set; }
        public string LayoutInfo { get; set; }
        public string ObjectType { get; set; }
    }

}
