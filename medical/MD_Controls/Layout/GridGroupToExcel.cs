﻿using medical_Data_Lib.Excel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;

namespace MD_Controls.Layout
{

    public class GridGroupToExcel
    {

        private readonly RadGridView _grid;
        private readonly string _sheetTitle;
        private CellPointer _pointer;
        private Stack<object> _groupKeys;
        private Stack<object> _columns;


        public GridGroupToExcel(RadGridView grid, string sheetTitle)
        {
            _grid = grid;
            _sheetTitle = sheetTitle;
        }
        public void Export(string fileName)
        {
            var excel = new ExcelPackage();
            var sheet = excel.Workbook.Worksheets.Add(string.IsNullOrWhiteSpace(_sheetTitle) ? _grid.FindForm().Text : _sheetTitle);
            _pointer = new CellPointer { Sheet = sheet };

            ExportHeaderRow(_grid);
            var currentDataset = _grid.DataSource;
            _grid.DataSource = null;
            _grid.DataSource = currentDataset;

            _columns = new Stack<object>();
            _groupKeys = new Stack<object>();
            foreach (var row in _grid.Groups)
            {
                ExportGroup(row);
            }

            var targetFile = File.Create(fileName);
            excel.SaveAs(targetFile);
            targetFile.Flush();
            targetFile.Close();
        }

        private void ExportGroupedColumnHeaders(RadGridView grid, GroupDescriptor group = null)
        {
            var names = grid.GroupDescriptors.SelectMany(x => x.GroupNames);
            foreach (var name in names)
            {
                if (grid.Columns.Contains(name.PropertyName))
                {
                    _pointer.WriteText(grid.Columns[name.PropertyName].HeaderText);
                }
                else
                {
                    _pointer.WriteObject(name.PropertyName);
                }
            }
        }

        private void ExportHeaderRow(RadGridView grid)
        {
            ExportGroupedColumnHeaders(grid);

            var summaryFields = new List<GridViewSummaryItem>();

            if (grid.MasterTemplate.SummaryRowsTop.Any())
                summaryFields.AddRange(grid.MasterTemplate.SummaryRowsTop.First().ToList());

            if (grid.MasterTemplate.SummaryRowGroupHeaders.Any())
                summaryFields.AddRange(grid.MasterTemplate.SummaryRowGroupHeaders.First().ToList());

            if (grid.MasterTemplate.SummaryRowsBottom.Any())
                summaryFields.AddRange(grid.MasterTemplate.SummaryRowsBottom.First().ToList());

            var headers = new HashSet<string>();
            foreach (var field in summaryFields)
            {
                var key = field.Name + "#" + field.AggregateExpression;
                if (!headers.Contains(key))
                {
                    var title = field.FormatString.Replace(": {0};", "");
                    _pointer.Bold().WriteObject(title);
                    headers.Add(key);
                }
            }
            _pointer.NewLine();
        }

        public void ExportGroup(Group<GridViewRowInfo> group)
        {
            var key = group.Key;
            if (key.GetType().IsArray) key = ((object[])key).FirstOrDefault();

            var columnCount = _columns.Count;
            var keyCount = _groupKeys.Count;

            foreach (var keyItem in (object[])group.Key)
            {
                _groupKeys.Push(keyItem);
            }

            if (group.Groups.Any())
            {
                foreach (var subGroup in group.Groups)
                {
                    ExportGroup(subGroup);
                }
            }
            else
            {
                _pointer.WriteObjects(_groupKeys.Reverse().ToArray());
                var row = group as DataGroup;

                foreach (var summaryRow in row.GroupRow.TopSummaryRows)
                {
                    foreach (var summaryItem in summaryRow.SummaryRowItem)
                    {
                        var id = summaryItem.Name + "#" + summaryItem.AggregateExpression;
                        if (!_columns.Contains(id))
                        {
                            _pointer.WriteObject(summaryItem.Evaluate(row.GroupRow));
                            _columns.Push(id);
                        }
                    }
                }
                foreach (var summaryRow in row.GroupRow.ViewInfo.SummaryRows)
                {
                    foreach (var summaryItem in summaryRow.SummaryRowItem)
                    {
                        var id = summaryItem.Name + "#" + summaryItem.AggregateExpression;
                        if (!_columns.Contains(id))
                        {
                            _pointer.WriteObject(summaryItem.Evaluate(row.GroupRow));
                            _columns.Push(id);
                        }
                    }
                }
                foreach (var summaryRow in row.GroupRow.BottomSummaryRows)
                {
                    foreach (var summaryItem in summaryRow.SummaryRowItem)
                    {
                        var id = summaryItem.Name + "#" + summaryItem.AggregateExpression;
                        if (!_columns.Contains(id))
                        {
                            _pointer.WriteObject(summaryItem.Evaluate(row.GroupRow));
                            _columns.Push(id);
                        }
                    }
                }
                _pointer.NewLine();
            }
            while (_columns.Count > columnCount)
            {
                _columns.Pop();
            }
            while (_groupKeys.Count > keyCount)
            {
                _groupKeys.Pop();
            }
        }

    }
}
