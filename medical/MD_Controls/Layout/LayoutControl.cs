﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using MD_Controls.Grid;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Export;
using Telerik.Data;
using System.IO;

namespace MD_Controls.Layout
{
    public partial class LayoutControl : UserControl
    {
        public const string GridLayoutField = "RadGridViewLayout";
        public const string GridLayoutFieldSummariesTop = "RadGridViewLayout_SummariesTop";
        public const string GridLayoutFieldSummariesBottom = "RadGridViewLayout_SummariesBottom";
        public const string GridLayoutFieldSummariesGroupHeaders = "RadGridViewLayout_SummariesGroupHeaders";
        private const string SummaryRowGroupHeaders = "SummaryRowGroupHeaders";
        private static readonly Type[] SummaryExtraTypes = { typeof(DistinctCountSummaryItem) };
        public bool bSendEmail = false;
        public string sEmail = "";
        public enum ExportBehaviourEnum
        {
            BasicExport = 0, // Grid based export and group export
            None = 1,
            PrintAndExcel = 2,
            JustPrint = 3,
            JustExcel = 4
        }

        [DefaultValue(ExportBehaviourEnum.None)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public ExportBehaviourEnum ExportBehaviour
        {
            get { return _exportBehaviour; }
            set
            {
                _exportBehaviour = value;
                RefreshStatus();
            }
        }
        public void RefreshStatus()
        {
            PnlExportDropdown.Visible = false;
            BtnPrintOrExport.Visible = false;
            BtnPrintOrExport.Enabled = false;

            if (ExportBehaviour == ExportBehaviourEnum.None) return;
            BtnPrintOrExport.Visible = true;
            PnlExportDropdown.Visible = true;

            var grid = GetGridElementToExport();
            if (grid == null) return;
            var hasRows = grid.RowCount > 0;

            BtnPrintOrExport.Enabled = hasRows;
            MnuPrintAll.Enabled = false;
            MnuPrintAll.Visibility = ElementVisibility.Visible;

            MnuPrintSelection.Enabled = false;
            MnuPrintSelection.Visibility = ElementVisibility.Visible;

            /*  if (sIMIConnStr != "")
              {
                  BtnPrintOrExport.Text = "Exportar";
                  BtnPrintOrExport.Items[0].Text = "Exportar";

                  BtnPrintOrExport.Items[1].Visibility = ElementVisibility.Collapsed;
                  BtnPrintOrExport.Items[2].Visibility = ElementVisibility.Collapsed;
                  BtnPrintOrExport.Items[3].Visibility = ElementVisibility.Hidden;
              }*/


            MnuExportGroups.Enabled = false;
            MnuExportGroups.Visibility = ElementVisibility.Visible;

            MnuExportAll.Enabled = false;
            MnuExportAll.Visibility = ElementVisibility.Visible;

            MnuExportSelection.Enabled = false;
            MnuExportSelection.Visibility = ElementVisibility.Visible;

            MnuExportGroups.Visibility = ElementVisibility.Collapsed;
            radMenuSeparatorItem1.Visibility = ElementVisibility.Visible;


            var hasGroups = grid.Groups.Count > 0;
            var hasGroupSummaries = grid.MasterTemplate.SummaryRowGroupHeaders.Count > 0
                                    || grid.MasterTemplate.SummaryRowsTop.Count > 0
                                    || grid.MasterTemplate.SummaryRowsBottom.Count > 0;

            if (ExportBehaviour == ExportBehaviourEnum.BasicExport)
            {
                MnuPrintAll.Visibility = ElementVisibility.Collapsed;
                MnuPrintSelection.Visibility = ElementVisibility.Collapsed;
                MnuExportSelection.Visibility = ElementVisibility.Collapsed;
                radMenuSeparatorItem1.Visibility = ElementVisibility.Collapsed;

                MnuExportGroups.Visibility = ElementVisibility.Visible;
                MnuExportGroups.Enabled = hasGroups && hasGroupSummaries;
                MnuExportAll.Enabled = true;
            }

            if (ExportBehaviour == ExportBehaviourEnum.JustExcel || ExportBehaviour == ExportBehaviourEnum.PrintAndExcel)
            {
                MnuExportGroups.Visibility = ElementVisibility.Visible;
                MnuExportGroups.Enabled = hasGroups && hasGroupSummaries;
                MnuExportAll.Enabled = true;
            }

            if (ExportBehaviour == ExportBehaviourEnum.PrintAndExcel)
            {
                MnuExportSelection.Enabled = true;
            }

            if (ExportBehaviour == ExportBehaviourEnum.JustPrint || ExportBehaviour == ExportBehaviourEnum.PrintAndExcel)
            {
                MnuPrintAll.Enabled = true;
                MnuPrintSelection.Enabled = true;
            }
            //bily comentado con Luis con fecha 15/06/2017, el boton de acumulados, solo sera visible cuando sea exportacion basica
            if (ExportBehaviour != ExportBehaviourEnum.BasicExport)
                MnuExportGroups.Visibility = ElementVisibility.Hidden;
        }

        public enum ExportModeEnum
        {
            Excel,
            Print
        }

        public enum ExportSelectionEnum
        {
            All,
            Selection
        }

        private bool ExcelExportVisualSettings { get; set; }

        public string SheetTitle { get; set; }

        public enum FormIdOriginEnum
        {
            Default = 0,
            FormName = 1,
            FormTag = 2
        }

        private RadGridView _gridView;
        private ExportBehaviourEnum _exportBehaviour = ExportBehaviourEnum.BasicExport;


        /* [Browsable(false)]
         [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
         //public CUserSettings Manager { get; set; }*/

        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public RadGridView GridView
        {
            get { return _gridView; }
            set
            {
                _gridView = value;

                if (_gridView != null)
                {
                    value.XmlSerializationInfo.SerializationMetadata.Add(typeof(GridViewTemplate), SummaryRowGroupHeaders, DesignerSerializationVisibilityAttribute.Content);
                    GridView.DataBindingComplete += GridView_DataBindingComplete;
                }
            }
        }
        void GridView_DataBindingComplete(object sender, GridViewBindingCompleteEventArgs e)
        {
            RefreshStatus();
        }
        public FormIdOriginEnum FormNameSource { get; set; }

        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public List<DS_Configuration.wms_configuration_user_settingsRow> CurrentLayouts { get; set; }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public long? DefaultLayout { get; set; }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool HasLayout { get; set; }

        public string RecordCount
        {
            get { return LblRegistros.Text; }
            set { LblRegistros.Text = value; }
        }

        public event SerializationEventHandler LayoutApplied;
        public event GetGridsEventHandler GetGrids;
        public event SerializationEventHandler BeforeSaveLayout;
        public event GetGridsEventHandler GetGridToExport;

        public event PrintOrExportEventHandler PrintOrExport;

        public string AppName { get; set; } = "FormsApp";

        private string FormName { get; set; }

        public LayoutControl()
        {
            InitializeComponent();
            LblRegistros.Text = "";

            bool designMode = (LicenseManager.UsageMode == LicenseUsageMode.Designtime);
            if (designMode) return;
            // this.Manager = CUserSettingsFactory.GetInstance();

            try
            {
                // sIMIConnStr = System.Configuration.ConfigurationManager.ConnectionStrings["ExternalConnectionString"].ConnectionString;
            }
            catch { }
        }
        private RadGridView GetGridElementToExport()
        {
            if (GetGridToExport != null)
            {
                var parameter = new LayoutGetGridListEventParameter();
                GetGridToExport(this, parameter);
                if (parameter.GridViews.Count > 1)
                {
                    throw new InvalidOperationException(" No se puede exportar más de un grid simultáneamente.");
                }
                var gridToExport = parameter.GridViews.FirstOrDefault();
                return gridToExport;
            }

            var grids = OnGetGrids();
            return grids.FirstOrDefault();
        }
        public virtual List<RadGridView> OnGetGrids()
        {
            if (GetGrids != null)
            {
                var arguments = new LayoutGetGridListEventParameter();
                GetGrids(this, arguments);
                if (arguments.Handled) return arguments.GridViews;
            }
            var result = GridView != null ? new List<RadGridView>() { GridView } : new List<RadGridView>();
            foreach (var gridView in result)
            {
                gridView.XmlSerializationInfo.SerializationMetadata.Add(typeof(GridViewTemplate), SummaryRowGroupHeaders, DesignerSerializationVisibilityAttribute.Content);
            }
            return result;
        }

        private void BtnPrintOrExport_DropDownOpening(object sender, EventArgs e)
        {

        }
        private void BtnExportar_Click(object sender, EventArgs e)
        {
            DoBasicExport();
        }
        public void DoBasicExport()
        {
            try
            {
                var grid = GetGridElementToExport();
                if (grid == null) return;

                saveFileDialog1.Filter = "Excel (*.xls)|*.xls";

                if (saveFileDialog1.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                if (saveFileDialog1.FileName.Equals(String.Empty))
                {
                    RadMessageBox.SetThemeName(this.GridView.ThemeName);
                    RadMessageBox.Show("Por favor, indique un nombre de archivo.");
                    return;
                }

                var fileName = this.saveFileDialog1.FileName;
                var openExportFile = false;

                Cursor.Current = Cursors.WaitCursor;

                RunExportToExcelML(fileName);
                OpenExportedFile(fileName);

                Cursor.Current = Cursors.Default;

                if (openExportFile)
                {
                    try
                    {
                        System.Diagnostics.Process.Start(fileName);
                    }
                    catch (Exception ex)
                    {
                        var message = String.Format(" El archivo no puede  abrirse" + "\nError: {0}", ex.Message);
                        RadMessageBox.Show(message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }

                }
            }
            catch (Exception ex)
            {
                // ControlsCommon.ShowException("Layout", "Layout", ex);
            }
        }
        private void FixColumnsExportFormat()
        {
            try
            {
                var grids = OnGetGrids();
                foreach (var grid in grids)
                {
                    var dateColumns = grid.Columns
                        .Where(x => x.FieldName.EndsWith("_datetime") ||
                                    x.FieldName.Contains("fecha") ||
                                    x.FieldName.Contains("hora"));
                    //.Where(x => x.ExcelExportType == DisplayFormatType.Text);

                    foreach (var column in dateColumns)
                    {
                        if (column.FieldName.Contains("fecha"))
                            column.ExcelExportType = DisplayFormatType.ShortDate;
                        else if (column.FieldName.Contains("hora"))
                        {

                            column.ExcelExportType = DisplayFormatType.ShortTime;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                // ControlsCommon.ShowException("Layout", "Layout", ex);
            }
        }

        private void RunExportToExcelML(string fileName)
        {
            try
            {
                var grid = GetGridElementToExport();
                if (grid == null) return;

                FixColumnsExportFormat();

                var excelExporter = new ExportToExcelML(grid)
                {
                    SheetName = string.IsNullOrWhiteSpace(this.SheetTitle) ? this.FindForm().Text : this.SheetTitle,
                    SummariesExportOption = SummariesOption.ExportAll,
                    HiddenColumnOption = HiddenOption.DoNotExport,
                    ExportVisualSettings = this.ExcelExportVisualSettings,
                    ChildViewExportMode = ChildViewExportMode.SelectViewToExport,
                    ExportHierarchy = true
                };

                //set exporting visual settings             
                //opciones provenientes de FrmReceiptConsulting.
                //Si no valen para todos, habrá que configurarlas por propiedades o un evento.

                try
                {
                    excelExporter.RunExport(fileName);
                }
                catch (IOException ex)
                {
                    RadMessageBox.SetThemeName(this.GridView.ThemeName);
                    RadMessageBox.Show(this, ex.Message, "I/O Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
            catch (Exception ex)
            {
                //   ControlsCommon.ShowException("Layout", "Layout", ex);
            }
        }

        private void MnuPrintAll_Click(object sender, EventArgs e)
        {
            OnPrintOrExport(ExportModeEnum.Print, ExportSelectionEnum.All);
        }

        private void MnuPrintSelection_Click(object sender, EventArgs e)
        {
            OnPrintOrExport(ExportModeEnum.Print, ExportSelectionEnum.Selection);
        }

        private void MnuExportAll_Click(object sender, EventArgs e)
        {
            if (ExportBehaviour == ExportBehaviourEnum.BasicExport)
            {
                BtnExportar_Click(sender, e);
                return;
            }
        }

        private void MnuExportSelection_Click(object sender, EventArgs e)
        {
            OnPrintOrExport(ExportModeEnum.Excel, ExportSelectionEnum.Selection);
        }

        protected virtual void OnPrintOrExport(ExportModeEnum mode, ExportSelectionEnum selection)
        {
            PrintOrExport?.Invoke(mode, selection);
        }


        private void MnuExportGroups_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog1.Filter = "Excel (*.xls)|*.xls";
                if (saveFileDialog1.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                var grid = GetGridElementToExport();
                if (grid == null) return;

                if (saveFileDialog1.FileName.Equals(string.Empty))
                {
                    RadMessageBox.SetThemeName(this.GridView.ThemeName);
                    RadMessageBox.Show("Por favor, indique un nombre de archivo");
                    return;
                }

                var fileName = this.saveFileDialog1.FileName;
                var openExportFile = false;

                Cursor.Current = Cursors.WaitCursor;

                var exporter = new GridGroupToExcel(grid, SheetTitle);
                exporter.Export(fileName);
                OpenExportedFile(fileName);
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                // ControlsCommon.ShowException("Layout", "Layout", ex);
            }
        }


        public void OpenExportedFile(string fileName)
        {
            Cursor.Current = Cursors.Default;
            RadMessageBox.SetThemeName(GetGridElementToExport().ThemeName);
            var openExportedFile = RadMessageBox.Show("Los datos se han exportado correctamente. ¿Quiere abrir el archivo?", "Export", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes;
            if (openExportedFile)
            {
                try
                {
                    System.Diagnostics.Process.Start(fileName);
                }
                catch (Exception ex)
                {
                    var message = string.Format("El archivo no puede  abrirse" + "\nError: {0}", ex.Message);
                    RadMessageBox.Show(message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }
    }
}
