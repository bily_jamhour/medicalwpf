﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MD_Controls.Layout
{
    class DropItem
    {
        public long LayoutId { get; set; }
        public string LayoutCode { get; set; }
        public string Description { get; set; }
        //public DS_Configuration.wms_configuration_user_settingsRow DataRow { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}", this.LayoutCode, this.Description);
        }
    }
}
