﻿namespace MD_Controls.Layout
{
    partial class LayoutControl
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LayoutControl));
            this.LblRegistros = new Telerik.WinControls.UI.RadLabel();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.PnlExportDropdown = new Telerik.WinControls.UI.RadPanel();
            this.BtnPrintOrExport = new Telerik.WinControls.UI.RadSplitButton();
            this.radMenuHeaderItem1 = new Telerik.WinControls.UI.RadMenuHeaderItem();
            this.MnuPrintAll = new Telerik.WinControls.UI.RadMenuItem();
            this.MnuPrintSelection = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.MnuExportAll = new Telerik.WinControls.UI.RadMenuItem();
            this.MnuExportSelection = new Telerik.WinControls.UI.RadMenuItem();
            this.MnuExportGroups = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.LblRegistros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PnlExportDropdown)).BeginInit();
            this.PnlExportDropdown.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BtnPrintOrExport)).BeginInit();
            this.SuspendLayout();
            // 
            // LblRegistros
            // 
            this.LblRegistros.AutoSize = false;
            this.LblRegistros.Dock = System.Windows.Forms.DockStyle.Left;
            this.LblRegistros.Location = new System.Drawing.Point(0, 0);
            this.LblRegistros.Name = "LblRegistros";
            this.LblRegistros.Size = new System.Drawing.Size(96, 32);
            this.LblRegistros.TabIndex = 8;
            this.LblRegistros.Text = "[RecNum]";
            // 
            // PnlExportDropdown
            // 
            this.PnlExportDropdown.Controls.Add(this.BtnPrintOrExport);
            this.PnlExportDropdown.Dock = System.Windows.Forms.DockStyle.Right;
            this.PnlExportDropdown.Location = new System.Drawing.Point(218, 0);
            this.PnlExportDropdown.Name = "PnlExportDropdown";
            this.PnlExportDropdown.Size = new System.Drawing.Size(158, 32);
            this.PnlExportDropdown.TabIndex = 13;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PnlExportDropdown.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // BtnPrintOrExport
            // 
            this.BtnPrintOrExport.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuHeaderItem1,
            this.MnuPrintAll,
            this.MnuPrintSelection,
            this.radMenuSeparatorItem1,
            this.MnuExportAll,
            this.MnuExportSelection,
            this.MnuExportGroups});
            this.BtnPrintOrExport.Location = new System.Drawing.Point(7, 2);
            this.BtnPrintOrExport.Name = "BtnPrintOrExport";
            this.BtnPrintOrExport.Size = new System.Drawing.Size(151, 24);
            this.BtnPrintOrExport.TabIndex = 0;
            this.BtnPrintOrExport.Text = "Imprimir o exportar";
            // 
            // radMenuHeaderItem1
            // 
            this.radMenuHeaderItem1.Name = "radMenuHeaderItem1";
            this.radMenuHeaderItem1.Text = "Exportar o imprimir";
            this.radMenuHeaderItem1.UseCompatibleTextRendering = false;
            // 
            // MnuPrintAll
            // 
            this.MnuPrintAll.Name = "MnuPrintAll";
            this.MnuPrintAll.Text = "[Imprimir todo]";
            this.MnuPrintAll.UseCompatibleTextRendering = false;
            this.MnuPrintAll.Click += new System.EventHandler(this.MnuPrintAll_Click);
            // 
            // MnuPrintSelection
            // 
            this.MnuPrintSelection.Name = "MnuPrintSelection";
            this.MnuPrintSelection.Text = "[Imprimir selección]";
            this.MnuPrintSelection.UseCompatibleTextRendering = false;
            this.MnuPrintSelection.Click += new System.EventHandler(this.MnuPrintSelection_Click);
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radMenuSeparatorItem1.UseCompatibleTextRendering = false;
            // 
            // MnuExportAll
            // 
            this.MnuExportAll.Image = ((System.Drawing.Image)(resources.GetObject("MnuExportAll.Image")));
            this.MnuExportAll.Name = "MnuExportAll";
            this.MnuExportAll.Text = "[Exportar todo]";
            this.MnuExportAll.UseCompatibleTextRendering = false;
            this.MnuExportAll.Click += new System.EventHandler(this.MnuExportAll_Click);
            // 
            // MnuExportSelection
            // 
            this.MnuExportSelection.Image = ((System.Drawing.Image)(resources.GetObject("MnuExportSelection.Image")));
            this.MnuExportSelection.Name = "MnuExportSelection";
            this.MnuExportSelection.Text = "Exportar selección]";
            this.MnuExportSelection.UseCompatibleTextRendering = false;
            this.MnuExportSelection.Click += new System.EventHandler(this.MnuExportSelection_Click);
            // 
            // MnuExportGroups
            // 
            this.MnuExportGroups.Image = ((System.Drawing.Image)(resources.GetObject("MnuExportGroups.Image")));
            this.MnuExportGroups.Name = "MnuExportGroups";
            this.MnuExportGroups.Text = "Exportar acumulados";
            this.MnuExportGroups.UseCompatibleTextRendering = false;
            this.MnuExportGroups.Click += new System.EventHandler(this.MnuExportGroups_Click);
            // 
            // LayoutControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.LblRegistros);
            this.Controls.Add(this.PnlExportDropdown);
            this.Name = "LayoutControl";
            this.Size = new System.Drawing.Size(376, 32);
            ((System.ComponentModel.ISupportInitialize)(this.LblRegistros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PnlExportDropdown)).EndInit();
            this.PnlExportDropdown.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BtnPrintOrExport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel LblRegistros;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Telerik.WinControls.UI.RadPanel PnlExportDropdown;
        private Telerik.WinControls.UI.RadSplitButton BtnPrintOrExport;
        private Telerik.WinControls.UI.RadMenuHeaderItem radMenuHeaderItem1;
        private Telerik.WinControls.UI.RadMenuItem MnuPrintAll;
        private Telerik.WinControls.UI.RadMenuItem MnuPrintSelection;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
        private Telerik.WinControls.UI.RadMenuItem MnuExportAll;
        private Telerik.WinControls.UI.RadMenuItem MnuExportSelection;
        private Telerik.WinControls.UI.RadMenuItem MnuExportGroups;
    }
}
