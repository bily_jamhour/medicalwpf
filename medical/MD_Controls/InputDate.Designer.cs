﻿namespace MD_Controls
{
    partial class InputDate
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.chkEnabled = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnabled)).BeginInit();
            this.SuspendLayout();
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radDateTimePicker1.Enabled = false;
            this.radDateTimePicker1.Location = new System.Drawing.Point(111, 4);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            this.radDateTimePicker1.Size = new System.Drawing.Size(164, 20);
            this.radDateTimePicker1.TabIndex = 4;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "sábado, 07 de octubre de 2017";
            this.radDateTimePicker1.Value = new System.DateTime(2017, 10, 7, 19, 54, 28, 0);
            // 
            // chkEnabled
            // 
            this.chkEnabled.Location = new System.Drawing.Point(3, 5);
            this.chkEnabled.Name = "chkEnabled";
            this.chkEnabled.Size = new System.Drawing.Size(77, 18);
            this.chkEnabled.TabIndex = 7;
            this.chkEnabled.Text = "chkEnabled";
            this.chkEnabled.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkEnabled_ToggleStateChanged);
            // 
            // InputDate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkEnabled);
            this.Controls.Add(this.radDateTimePicker1);
            this.Name = "InputDate";
            this.Size = new System.Drawing.Size(278, 30);
            this.Load += new System.EventHandler(this.InputDate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnabled)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private Telerik.WinControls.UI.RadCheckBox chkEnabled;
    }
}
