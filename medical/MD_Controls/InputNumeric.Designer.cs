﻿namespace MD_Controls
{
    partial class InputNumeric
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNumeric = new Telerik.WinControls.UI.RadTextBoxControl();
            this.lblNumeric = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNumeric
            // 
            this.txtNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumeric.Location = new System.Drawing.Point(169, 3);
            this.txtNumeric.Name = "txtNumeric";
            this.txtNumeric.Size = new System.Drawing.Size(100, 20);
            this.txtNumeric.TabIndex = 1;
            this.txtNumeric.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.radTextBoxControl1_TextChanging);
            this.txtNumeric.Leave += new System.EventHandler(this.txtNumeric_Leave);
            // 
            // lblNumeric
            // 
            this.lblNumeric.Location = new System.Drawing.Point(3, 5);
            this.lblNumeric.Name = "lblNumeric";
            this.lblNumeric.Size = new System.Drawing.Size(55, 18);
            this.lblNumeric.TabIndex = 2;
            this.lblNumeric.Text = "radLabel1";
            // 
            // InputNumeric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblNumeric);
            this.Controls.Add(this.txtNumeric);
            this.Name = "InputNumeric";
            this.Size = new System.Drawing.Size(278, 30);
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBoxControl txtNumeric;
        private Telerik.WinControls.UI.RadLabel lblNumeric;
    }
}
