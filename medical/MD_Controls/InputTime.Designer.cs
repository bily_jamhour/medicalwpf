﻿namespace MD_Controls
{
    partial class InputTime
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDate = new Telerik.WinControls.UI.RadLabel();
            this.radTimePicker1 = new Telerik.WinControls.UI.RadTimePicker();
            this.chkEnabled = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnabled)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDate
            // 
            this.lblDate.Location = new System.Drawing.Point(24, 5);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(55, 18);
            this.lblDate.TabIndex = 4;
            this.lblDate.Text = "radLabel1";
            // 
            // radTimePicker1
            // 
            this.radTimePicker1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radTimePicker1.Enabled = false;
            this.radTimePicker1.Location = new System.Drawing.Point(166, 4);
            this.radTimePicker1.MaxValue = new System.DateTime(9999, 12, 31, 23, 59, 59, 0);
            this.radTimePicker1.MinValue = new System.DateTime(((long)(0)));
            this.radTimePicker1.Name = "radTimePicker1";
            this.radTimePicker1.Size = new System.Drawing.Size(100, 20);
            this.radTimePicker1.TabIndex = 5;
            this.radTimePicker1.TabStop = false;
            this.radTimePicker1.Text = "radTimePicker1";
            this.radTimePicker1.Value = new System.DateTime(2017, 10, 7, 20, 5, 37, 262);
            // 
            // chkEnabled
            // 
            this.chkEnabled.Location = new System.Drawing.Point(3, 5);
            this.chkEnabled.Name = "chkEnabled";
            this.chkEnabled.Size = new System.Drawing.Size(15, 15);
            this.chkEnabled.TabIndex = 6;
            this.chkEnabled.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkEnabled_ToggleStateChanged);
            // 
            // InputTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkEnabled);
            this.Controls.Add(this.radTimePicker1);
            this.Controls.Add(this.lblDate);
            this.Name = "InputTime";
            this.Size = new System.Drawing.Size(278, 30);
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnabled)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblDate;
        private Telerik.WinControls.UI.RadTimePicker radTimePicker1;
        private Telerik.WinControls.UI.RadCheckBox chkEnabled;
    }
}
