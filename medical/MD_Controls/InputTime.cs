﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MD_Controls
{
    public partial class InputTime : UserControl
    {
        public InputTime()
        {
            InitializeComponent();
        }
        public bool enabled = false;
        public void SetTitle(string sTitle)
        {
            lblDate.Text = sTitle;
        }
        public void SetValue(DateTime date)
        {
            radTimePicker1.Value = date;
            chkEnabled.Checked = true;
        }
        public void Clear()
        {
            radTimePicker1.Value = null;
        }
        public DateTime? GetTime()
        {
            if (radTimePicker1.Enabled == false)
                return null;
            return radTimePicker1.Value;
        }

        private void chkEnabled_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            radTimePicker1.Enabled = chkEnabled.Checked;
            enabled = radTimePicker1.Enabled;
        }
    }
}
