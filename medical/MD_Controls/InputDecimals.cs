﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MD_Controls
{
    public partial class InputDecimals : UserControl
    {
        private int _numDecimals;
        public InputDecimals()
        {

            InitializeComponent();
        }
        public void Clear()
        {

            txtDecimal.Clear();
            txtDecimal.Refresh();
        }
        public decimal getValue()
        {
            if (txtDecimal.Text.Contains("_"))
                txtDecimal.Value = txtDecimal.Text.Replace('_', '0');
            /* if (Convert.ToDecimal(txtDecimal.Value) <= 0)
                 return 0;*/
            return Convert.ToDecimal(txtDecimal.Value);
        }
        public void setDecimals(int iNumDecimals = 0)
        {
            _numDecimals = (iNumDecimals < 0) ? 0 : iNumDecimals;
            if (_numDecimals == 0)
            {
                txtDecimal.Mask = "##,##";
            }
            else
            {
                txtDecimal.Mask = "##,";
                for (int x = 0; x < _numDecimals; x++)
                {
                    txtDecimal.Mask += "#";
                }
            }
        }
        private void InputDecimals_Load(object sender, EventArgs e)
        {
            setDecimals();
        }
        public void SetTitle(string sTitle)
        {
            lblDecimal.Text = sTitle;
        }
        public void setValue(object dValue)
        {
            try
            {
                txtDecimal.Value = Convert.ToDecimal(dValue);
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
