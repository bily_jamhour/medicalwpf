﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MD_Controls
{
    public partial class InputDate : UserControl
    {
        public InputDate()
        {
            InitializeComponent();
        }

        private void InputDate_Load(object sender, EventArgs e)
        {
            radDateTimePicker1.MaxDate = DateTime.Now;
            radDateTimePicker1.Value = DateTime.Now.AddMinutes(-10);
        }
        public bool enabled = false;
        public void SetTitle(string sTitle)
        {
            chkEnabled.Text = sTitle;
        }
        public void Clear()
        {
            chkEnabled.Checked = false;
            radDateTimePicker1.Value = DateTime.Now;
        }
        public void SetValue(DateTime? date)
        {
            DateTime dateToSet = DateTime.Now;
            if (date != null)
            {
                dateToSet = Convert.ToDateTime(date);
                chkEnabled.Checked = true;
            }
            else
                chkEnabled.Checked = false;
            radDateTimePicker1.Value = dateToSet;

        }
        public DateTime? getDate()
        {
            if (radDateTimePicker1.Enabled == false)
                return null;
            return radDateTimePicker1.Value;
        }
        public String getDateFormat()
        {
            if (radDateTimePicker1.Enabled == false)
                return null;
            return radDateTimePicker1.Value.ToString("MM/DD/YYYY");
        }
        public int? getHours()
        {
            if (radDateTimePicker1.Enabled == false)
                return null;
            return radDateTimePicker1.Value.Hour;
        }
        public int? getMin()
        {
            if (radDateTimePicker1.Enabled == false)
                return null;
            return radDateTimePicker1.Value.Minute;
        }

        private void chkEnabled_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            radDateTimePicker1.Enabled = chkEnabled.Checked;
            enabled = radDateTimePicker1.Enabled;
        }
    }
}
