﻿namespace MD_Controls
{
    partial class DropDownMedical
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddMedical = new Telerik.WinControls.UI.RadDropDownList();
            this.lblDropdownMedical = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ddMedical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDropdownMedical)).BeginInit();
            this.SuspendLayout();
            // 
            // ddMedical
            // 
            this.ddMedical.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddMedical.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddMedical.Location = new System.Drawing.Point(169, 3);
            this.ddMedical.Name = "ddMedical";
            this.ddMedical.Size = new System.Drawing.Size(100, 20);
            this.ddMedical.TabIndex = 0;
            // 
            // lblDropdownMedical
            // 
            this.lblDropdownMedical.Location = new System.Drawing.Point(3, 5);
            this.lblDropdownMedical.Name = "lblDropdownMedical";
            this.lblDropdownMedical.Size = new System.Drawing.Size(117, 18);
            this.lblDropdownMedical.TabIndex = 1;
            this.lblDropdownMedical.Text = "[lblDropdownMedical]";
            // 
            // DropDownMedical
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblDropdownMedical);
            this.Controls.Add(this.ddMedical);
            this.MinimumSize = new System.Drawing.Size(278, 30);
            this.Name = "DropDownMedical";
            this.Size = new System.Drawing.Size(278, 30);
            ((System.ComponentModel.ISupportInitialize)(this.ddMedical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDropdownMedical)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadDropDownList ddMedical;
        private Telerik.WinControls.UI.RadLabel lblDropdownMedical;
    }
}
