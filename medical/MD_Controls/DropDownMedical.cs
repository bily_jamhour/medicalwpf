﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace MD_Controls
{
    public partial class DropDownMedical : UserControl
    {
        public DropDownMedical()
        {
            InitializeComponent();
        }
        private string ddValueSelected = "";
        private string _sTypeData = "";
        public void SelectionChanged(EventHandler eventToSet)
        {
            ddMedical.SelectedValueChanged += eventToSet;
        }
        public void SetTypeDropDownList(string sTypeData = "")
        {
            if (sTypeData.Contains("_"))
                _sTypeData = sTypeData.Split('_').FirstOrDefault(); //Para  que se puedan generar dropdownsmedicals duplicadas asignar nombre ddhta_x donde x es un numero
            else
                _sTypeData = sTypeData;
            ddMedical.Items.Clear();

            if (_sTypeData == "SEX" || _sTypeData == "")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sin Seleccionar", Value = "-1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Hombre", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Mujer", Value = "1" });
                lblDropdownMedical.Text = "Sexo";
            }
            else if (_sTypeData == "HTA")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Si(Previamente diagnosticada antes del ingreso)", Value = "1" });
                lblDropdownMedical.Text = "HTA";
            }
            else if (_sTypeData == "DIABETES")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Si a tratamiento con ADO/Control dietético", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Insulinizados", Value = "2" });

                lblDropdownMedical.Text = "Diabetes Mellitus";
            }
            else if (_sTypeData == "DISLIPEMIA")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Si", Value = "1" });

                lblDropdownMedical.Text = "Dislipemia";
            }
            else if (_sTypeData == "CARDIOPATIA")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Diagnóstico clínico", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Revascularizada percutáneamente", Value = "2" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Revascularizada quirúrgicamente", Value = "3" });
                lblDropdownMedical.Text = "Cardiopatía Isquémica";
            }
            else if (_sTypeData == "SINCOPES")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sospecha clara de síncope neuromediado", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Síncope NO neuromediado", Value = "2" });
                lblDropdownMedical.Text = "Historial SINCOPES";
            }
            else if (_sTypeData == "ICC")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Ingresos previos en planta de hospitalización", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Ingresos previos en UCI", Value = "2" });
                lblDropdownMedical.Text = "Ingresos previos ICC";
            }
            else if (_sTypeData == "FA")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "FA paroxística", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "FA Persistente", Value = "2" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "FA crónica", Value = "3" });

                lblDropdownMedical.Text = "Antecedentes FA";
            }
            else if (_sTypeData == "ANTICOAGULACION")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Antiagregación", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sintrom crónico", Value = "2" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Nuevos anticoagulantes", Value = "3" });

                lblDropdownMedical.Text = "Anticoagulación previa ingreso";
            }
            else if (_sTypeData == "ECOCARDIOGRAMA")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí.FEVI conservada o disfunción leve", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí.Disfunción ventricular de moderada a severa", Value = "2" });

                lblDropdownMedical.Text = "Ecocardiograma previo";
            }
            else if (_sTypeData == "BASAL" || _sTypeData == "NYHA")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "Normal", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "I-II (NYHA leve)", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "III-IV (NYHA avanzada)", Value = "2" });

                lblDropdownMedical.Text = (_sTypeData == "BASAL") ? "Clase funcional basal" : "NYHA al ingreso";
            }
            else if (_sTypeData == "TA")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "Menor a 110 mmHg", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Entre 100 y 150 mmHg", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Mayor a 150 mmHg", Value = "2" });

                lblDropdownMedical.Text = "TAs al ingreso";
            }
            else if (_sTypeData == "ECO")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí", Value = "1" });

                lblDropdownMedical.Text = "Eco en ingreso";
            }
            else if (_sTypeData == "FEVI")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "Normal o levemente deprimida", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Moderada o severamente deprimida", Value = "1" });

                lblDropdownMedical.Text = "FEVI";
            }
            else if (_sTypeData == "VALVULOPATIA")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Valvulopatía aóritca en grado moderado a severo", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Valvulopatía mitral en grado moderado a severo", Value = "2" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Combinación de ambas", Value = "3" });


                lblDropdownMedical.Text = "Valvulopatía significativa";
            }
            else if (_sTypeData == "HTAP")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No consta", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "PAP dentro de la normalidad", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "HTAp leve < 40 mmHg", Value = "2" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "HTAp moderada > 40 mmHg", Value = "3" });

                lblDropdownMedical.Text = "Valvulopatía significativa";
            }
            else if (_sTypeData == "FC")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí, FC menor a 100 lpm", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Bradicardia significativa < 50 lpm", Value = "2" });

                lblDropdownMedical.Text = "Fc controlada a las 24h";
            }
            else if (_sTypeData == "RITMO")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "Ritmo sinusal", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Se mantiente en FA", Value = "1" });

                lblDropdownMedical.Text = "Ritmo al alta";
            }
            else if (_sTypeData == "BETABLOQUEANTESPRE" || _sTypeData == "BETABLOQUEANTESPOST")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí", Value = "1" });

                lblDropdownMedical.Text = (_sTypeData == "BETABLOQUEANTESPRE") ? "Betabloqueantes pre" : "Betabloqueantes post";
            }
            else if (_sTypeData == "IECAPRE" || _sTypeData == "IECAPOST")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí", Value = "1" });

                lblDropdownMedical.Text = (_sTypeData == "IECAPRE") ? "IECAs pre" : "IECAs post";
            }
            /*else if (_sTypeData == "IECAPRE" || _sTypeData == "IECAPOST")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí", Value = "1" });

                lblDropdownMedical.Text = (_sTypeData == "IECAPRE") ? "IECAs pre" : "IECAs post";
            }*/
            else if (_sTypeData == "AMIODARONAPRE" || _sTypeData == "AMIODARONAPOST")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí", Value = "1" });

                lblDropdownMedical.Text = (_sTypeData == "AMIODARONAPRE") ? "Amiodarona pre" : "Amiodarona post";
            }
            else if (_sTypeData == "DIGOXINAPRE" || _sTypeData == "DIGOXINAPOST")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí", Value = "1" });

                lblDropdownMedical.Text = (_sTypeData == "DIGOXINAPRE") ? "Digoxina pre" : "Digoxina post";
            }
            else if (_sTypeData == "ANTIALDOSTERONICOSPRE" || _sTypeData == "ANTIALDOSTERONICOSPOST")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí", Value = "1" });

                lblDropdownMedical.Text = (_sTypeData == "ANTIALDOSTERONICOSPRE") ? "Antialdosteronicos pre" : "Antialdosteronicos post";
            }
            else if (_sTypeData == "MAPA")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "Retirado antes de las 12 horas por intolerancia", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Finaliza las 24 horas de registro", Value = "1" });

                lblDropdownMedical.Text = "Mapa";
            }
            else if (_sTypeData == "REGISTRO")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No interpretable", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Interceptable parcialmente", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "De buena calidad", Value = "2" });

                lblDropdownMedical.Text = "Registro";
            }
            else if (_sTypeData == "PATRON")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No definido", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Dipper", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "No dipper", Value = "2" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Riser", Value = "3" });

                lblDropdownMedical.Text = "Patrón respuesta tensional";
            }
            else if (_sTypeData == "INGRESOS6")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí por nueva descompensación de ICC y FA", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Sí por ICC pero sin influencia de la FA", Value = "2" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Otras causas", Value = "3" });

                lblDropdownMedical.Text = "Ingresos a los 6 meses";
            }
            else if (_sTypeData == "STATUS")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "Fallecido", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Vivo", Value = "1" });

                lblDropdownMedical.Text = "Status al año";
            }
            else if (_sTypeData == "EXITUS")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Causa cardiovascular", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "No causa cardiovascular", Value = "2" });

                lblDropdownMedical.Text = "Exitus";
            }
            else if (_sTypeData == "ANYODM")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No conocido, diagnosticado en este ingreso", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Menos de 5 años", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Más de 5 años", Value = "2" });

                lblDropdownMedical.Text = "Años diagnóstico DM";
            }
            else if (_sTypeData == "REVASCULARIZACION")
            {
                ddMedical.Items.Add(new RadListDataItem() { Text = "No", Value = "0" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Revascularización percutánea", Value = "1" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Revascularización quirúrgica", Value = "2" });
                ddMedical.Items.Add(new RadListDataItem() { Text = "Ambas", Value = "3" });

                lblDropdownMedical.Text = "Revasculariación previa";
            }
            ddMedical.SelectedItem = ddMedical.Items.FirstOrDefault();
        }
        private void DropDownSexo_Load(object sender, EventArgs e)
        {
           
        }

        public void Clear()
        {
            ddMedical.SelectedValue = ddMedical.Items.FirstOrDefault().Value;
        }

        public string GetValue()
        {
            ddValueSelected = Convert.ToString(ddMedical.SelectedValue);
            return ddValueSelected;
        }
        public void SetValue(string value)
        {
            ddMedical.SelectedValue = value;
        }

        public void SetTitle(string sTitle)
        {
            lblDropdownMedical.Text = sTitle;
        }
    }
}
