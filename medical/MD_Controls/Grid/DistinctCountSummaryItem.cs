﻿using medical_Infrastructure.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI;

namespace MD_Controls.Grid
{
    public class DistinctCountSummaryItem : GridViewSummaryItem
    {
        public string FieldName { get; set; }
        private readonly bool _countNulls;
        Dictionary<object, GridViewRowInfo> _countDictionary;

        public DistinctCountSummaryItem() : base()
        {

        }

        public DistinctCountSummaryItem(string name, string formatString, string fieldName, bool countNulls = false) : base(name, formatString, null)
        {
            FieldName = fieldName;
            _countNulls = countNulls;
            AggregateExpression = "Unique";
        }

        public override object Evaluate(IHierarchicalRow row)
        {
            _countDictionary = new Dictionary<object, GridViewRowInfo>();
            try
            {
                ProcessRow(row);
                var result = _countDictionary.Count;
                _countDictionary.Clear();
                return result;
            }
            catch (Exception ex)
            {
                _countDictionary.Clear();
                return "Error";
            }
        }

        private void ProcessRow(IHierarchicalRow row)
        {

            var groupRow = row as GridViewGroupRowInfo;
            if (groupRow != null)
            {
                if (groupRow.Group.Groups.Count > 0)
                {
                    var count = 0;
                    foreach (var childRow in (row as GridViewGroupRowInfo).ChildRows)
                    {
                        ProcessRow(childRow);
                    }
                    return;
                }

                var cellName = FieldName ?? (Name ?? "");
                groupRow.Group.GetItems()
                    .Where(x => x.Cells[cellName].Value != null
                            && x.Cells[cellName].Value != DBNull.Value
                            && (x.Cells[cellName].Value.GetType() != typeof(string) || (!string.IsNullOrWhiteSpace(x.Cells[cellName].Value as string))))
                    .SafeAdd(_countDictionary, x => x.Cells[cellName].Value);
                return;
            }

            if (row is MasterGridViewTemplate)
            {
                var masterRow = row as MasterGridViewTemplate;
                var cell = FieldName ?? (Name ?? "");
                masterRow.Rows
                    .Where(x => x.Cells[cell].Value != null
                            && x.Cells[cell].Value != DBNull.Value
                            && (x.Cells[cell].Value.GetType() != typeof(string) || (!string.IsNullOrWhiteSpace(x.Cells[cell].Value as string))))
                    .SafeAdd(_countDictionary, x => x.Cells[cell].Value);
            }
        }

    }
}
