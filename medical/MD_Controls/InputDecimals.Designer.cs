﻿namespace MD_Controls
{
    partial class InputDecimals
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDecimal = new Telerik.WinControls.UI.RadLabel();
            this.txtDecimal = new Telerik.WinControls.UI.RadMaskedEditBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblDecimal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDecimal)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDecimal
            // 
            this.lblDecimal.Location = new System.Drawing.Point(3, 5);
            this.lblDecimal.Name = "lblDecimal";
            this.lblDecimal.Size = new System.Drawing.Size(55, 18);
            this.lblDecimal.TabIndex = 3;
            this.lblDecimal.Text = "radLabel1";
            // 
            // txtDecimal
            // 
            this.txtDecimal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDecimal.Location = new System.Drawing.Point(169, 3);
            this.txtDecimal.Mask = "##.##";
            this.txtDecimal.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.txtDecimal.Name = "txtDecimal";
            this.txtDecimal.Size = new System.Drawing.Size(100, 20);
            this.txtDecimal.TabIndex = 36;
            this.txtDecimal.TabStop = false;
            this.txtDecimal.Text = "__,__";
            // 
            // InputDecimals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtDecimal);
            this.Controls.Add(this.lblDecimal);
            this.Name = "InputDecimals";
            this.Size = new System.Drawing.Size(278, 30);
            this.Load += new System.EventHandler(this.InputDecimals_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblDecimal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDecimal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblDecimal;
        private Telerik.WinControls.UI.RadMaskedEditBox txtDecimal;
    }
}
