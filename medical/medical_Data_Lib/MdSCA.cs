﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace medical_Data_Lib
{
    public class MdSCA
    {
        public static string AddSCA(int iPacienteId, md_sca dataSCA)
        {
            string sMessageValidation = "";
            using (var context = new bbddmedicalEntities())
            {
                try
                {
                    var mdSCAAdd = new md_sca
                    {
                        md_amiodaronapost = dataSCA.md_amiodaronapost,
                        md_amiodaronapre = dataSCA.md_amiodaronapre,
                        md_antecedentesfa = dataSCA.md_antecedentesfa,
                        md_antialdosteronicospost = dataSCA.md_antialdosteronicospost,
                        md_antialdosteronicospre = dataSCA.md_antialdosteronicospre,
                        md_anticoagulacioningreso = dataSCA.md_anticoagulacioningreso,
                        md_anyosdm = dataSCA.md_anyosdm,
                        md_betabloqueantespost = dataSCA.md_betabloqueantespost,
                        md_betabloqueantespre = dataSCA.md_betabloqueantespre,
                        md_cardiopatiaisquemica = dataSCA.md_cardiopatiaisquemica,
                        md_clasefuncionalbasal = dataSCA.md_clasefuncionalbasal,
                        md_creatininaalta = dataSCA.md_creatininaalta,
                        md_creatininaingreso = dataSCA.md_creatininaingreso,
                        md_diabetesmellitus = dataSCA.md_diabetesmellitus,
                        md_digoxinapost = dataSCA.md_digoxinapost,
                        md_digoxinapre = dataSCA.md_digoxinapre,
                        md_dislipemia = dataSCA.md_dislipemia,
                        md_ecocardiograma = dataSCA.md_ecocardiograma,
                        md_ecocardiogramaduranteingreso = dataSCA.md_ecocardiogramaduranteingreso,
                        md_exitus = dataSCA.md_exitus,
                        md_exitusdesc = dataSCA.md_exitusdesc,
                        md_fccontrolada = dataSCA.md_fccontrolada,
                        md_fcingreso = dataSCA.md_fcingreso,
                        md_fevi = dataSCA.md_fevi,
                        md_historialsincopes = dataSCA.md_historialsincopes,
                        md_horapicotensional_diurno = dataSCA.md_horapicotensional_diurno,
                        md_horapicotensional_nocturno = dataSCA.md_horapicotensional_nocturno,
                        md_hta = dataSCA.md_hta,
                        md_htap = dataSCA.md_htap,
                        md_iecapost = dataSCA.md_iecapost,
                        md_iecapre = dataSCA.md_iecapre,
                        md_ingresos = dataSCA.md_ingresos,
                        md_ingresosicc = dataSCA.md_ingresosicc,
                        md_mapa = dataSCA.md_mapa,
                        md_mediafcduranteregistro = dataSCA.md_mediafcduranteregistro,
                        md_mediatensional_diurno = dataSCA.md_mediatensional_diurno,
                        md_mediatensional_nocturno = dataSCA.md_mediatensional_nocturno,
                        md_niveleshba = dataSCA.md_niveleshba,
                        md_nyha = dataSCA.md_nyha,
                        md_record_id = (dataSCA.md_record_id == 0) ? iPacienteId : dataSCA.md_record_id,
                        md_patron = dataSCA.md_patron,
                        md_picotensional_diurno = dataSCA.md_picotensional_diurno,
                        md_picotensional_nocturno = dataSCA.md_picotensional_nocturno,
                        //md_pacientes=dataSCA.md_pacientes,
                        md_registro = dataSCA.md_registro,
                        md_revascularizacion = dataSCA.md_revascularizacion,
                        md_ritmoalta = dataSCA.md_ritmoalta,
                        md_status = dataSCA.md_status,
                        md_sumframinghanmayor = dataSCA.md_sumframinghanmayor,
                        md_sumframinghanmenor = dataSCA.md_sumframinghanmenor,
                        md_taingreso = dataSCA.md_taingreso,
                        md_valvulopatia = dataSCA.md_valvulopatia
                    };

                    context.md_sca.Add(mdSCAAdd);
                    if (context.ChangeTracker.HasChanges())
                    {
                        context.Entry(mdSCAAdd).State = EntityState.Added;
                        context.SaveChanges();
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {


                        /* Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);*/
                        sMessageValidation += "Se han encontrado los siguientes errores:" + Environment.NewLine;
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sMessageValidation += string.Format("- \"{0}\"",
                              ve.ErrorMessage);
                            /* sMessageValidation += string.Format("- El campo: \"{0}\", Error: \"{1}\"",
                                 ve.PropertyName, ve.ErrorMessage);*/
                            /* Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                 ve.PropertyName, ve.ErrorMessage);*/
                        }
                    }

                }
                catch (Exception ex)
                {
                    return sMessageValidation = ex.Message;
                }

            }
            return sMessageValidation;
        }

        public static List<md_sca> GetSCA(Int32 iPacienteId)
        {
            if (iPacienteId == 0)
                return new List<md_sca>();
            using (var context = new bbddmedicalEntities())
            {
                return context.md_sca.Where(x => x.md_record_id == iPacienteId).ToList();
            }
        }

        public static string RemoveSCA(Int32 iPacienteId)
        {
            string sMessageValidation = "";

            if (iPacienteId == 0)
                return "No se puede eliminar un SCA con identificador de paciente 0";
            using (var context = new bbddmedicalEntities())
            {
                try
                {
                    var scaToRemove = context.md_sca.Where(x => x.md_record_id == iPacienteId).FirstOrDefault();
                    if (scaToRemove == null)
                    {
                        sMessageValidation = string.Format("No se ha encontrado los datos del SCA del paciente con identificador [{0}].", iPacienteId);
                        return sMessageValidation;
                    }
                    context.md_sca.Remove(scaToRemove);
                    if (context.ChangeTracker.HasChanges())
                    {
                        context.Entry(scaToRemove).State = EntityState.Deleted;
                        context.SaveChanges();
                    }

                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {


                        sMessageValidation += "Se han encontrado los siguientes errores:" + Environment.NewLine;
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sMessageValidation += string.Format("- \"{0}\"",
                              ve.ErrorMessage);

                        }
                    }

                }
                catch (Exception ex)
                {
                    return sMessageValidation = ex.Message;
                }
            }
            return sMessageValidation;
        }

        public static string UpdateSCA(int iPacienteId, md_sca dataSCA)
        {
            string sMessageValidation = "";
            using (var context = new bbddmedicalEntities())
            {
                try
                {
                    var mdSCAToUpdate = MdSCA.GetSCA(iPacienteId).FirstOrDefault();
                    if (mdSCAToUpdate == null)
                    {
                        sMessageValidation = string.Format("No se ha encontrado los datos del SCA del paciente con identificador [{0}].", iPacienteId);
                    }
                    else
                    {
                        mdSCAToUpdate.md_amiodaronapost = dataSCA.md_amiodaronapost;
                        mdSCAToUpdate.md_amiodaronapre = dataSCA.md_amiodaronapre;
                        mdSCAToUpdate.md_antecedentesfa = dataSCA.md_antecedentesfa;
                        mdSCAToUpdate.md_antialdosteronicospost = dataSCA.md_antialdosteronicospost;
                        mdSCAToUpdate.md_antialdosteronicospre = dataSCA.md_antialdosteronicospre;
                        mdSCAToUpdate.md_anticoagulacioningreso = dataSCA.md_anticoagulacioningreso;
                        mdSCAToUpdate.md_anyosdm = dataSCA.md_anyosdm;
                        mdSCAToUpdate.md_betabloqueantespost = dataSCA.md_betabloqueantespost;
                        mdSCAToUpdate.md_betabloqueantespre = dataSCA.md_betabloqueantespre;
                        mdSCAToUpdate.md_cardiopatiaisquemica = dataSCA.md_cardiopatiaisquemica;
                        mdSCAToUpdate.md_clasefuncionalbasal = dataSCA.md_clasefuncionalbasal;
                        mdSCAToUpdate.md_creatininaalta = dataSCA.md_creatininaalta;
                        mdSCAToUpdate.md_creatininaingreso = dataSCA.md_creatininaingreso;
                        mdSCAToUpdate.md_diabetesmellitus = dataSCA.md_diabetesmellitus;
                        mdSCAToUpdate.md_digoxinapost = dataSCA.md_digoxinapost;
                        mdSCAToUpdate.md_digoxinapre = dataSCA.md_digoxinapre;
                        mdSCAToUpdate.md_dislipemia = dataSCA.md_dislipemia;
                        mdSCAToUpdate.md_ecocardiograma = dataSCA.md_ecocardiograma;
                        mdSCAToUpdate.md_ecocardiogramaduranteingreso = dataSCA.md_ecocardiogramaduranteingreso;
                        mdSCAToUpdate.md_exitus = dataSCA.md_exitus;
                        mdSCAToUpdate.md_exitusdesc = dataSCA.md_exitusdesc;
                        mdSCAToUpdate.md_fccontrolada = dataSCA.md_fccontrolada;
                        mdSCAToUpdate.md_fcingreso = dataSCA.md_fcingreso;
                        mdSCAToUpdate.md_fevi = dataSCA.md_fevi;
                        mdSCAToUpdate.md_historialsincopes = dataSCA.md_historialsincopes;
                        mdSCAToUpdate.md_horapicotensional_diurno = dataSCA.md_horapicotensional_diurno;
                        mdSCAToUpdate.md_horapicotensional_nocturno = dataSCA.md_horapicotensional_nocturno;
                        mdSCAToUpdate.md_hta = dataSCA.md_hta;
                        mdSCAToUpdate.md_htap = dataSCA.md_htap;
                        mdSCAToUpdate.md_iecapost = dataSCA.md_iecapost;
                        mdSCAToUpdate.md_iecapre = dataSCA.md_iecapre;
                        mdSCAToUpdate.md_ingresos = dataSCA.md_ingresos;
                        mdSCAToUpdate.md_ingresosicc = dataSCA.md_ingresosicc;
                        mdSCAToUpdate.md_mapa = dataSCA.md_mapa;
                        mdSCAToUpdate.md_mediafcduranteregistro = dataSCA.md_mediafcduranteregistro;
                        mdSCAToUpdate.md_mediatensional_diurno = dataSCA.md_mediatensional_diurno;
                        mdSCAToUpdate.md_mediatensional_nocturno = dataSCA.md_mediatensional_nocturno;
                        mdSCAToUpdate.md_niveleshba = dataSCA.md_niveleshba;
                        mdSCAToUpdate.md_nyha = dataSCA.md_nyha;
                        mdSCAToUpdate.md_record_id = dataSCA.md_record_id;
                        mdSCAToUpdate.md_patron = dataSCA.md_patron;
                        mdSCAToUpdate.md_picotensional_diurno = dataSCA.md_picotensional_diurno;
                        mdSCAToUpdate.md_picotensional_nocturno = dataSCA.md_picotensional_nocturno;
                        // mdSCAToUpdate.//md_pacientes=dataSCA.md_pacientes;
                        // mdSCAToUpdate.md_registro = dataSCA.md_registro;
                        mdSCAToUpdate.md_revascularizacion = dataSCA.md_revascularizacion;
                        mdSCAToUpdate.md_ritmoalta = dataSCA.md_ritmoalta;
                        mdSCAToUpdate.md_status = dataSCA.md_status;
                        mdSCAToUpdate.md_sumframinghanmayor = dataSCA.md_sumframinghanmayor;
                        mdSCAToUpdate.md_sumframinghanmenor = dataSCA.md_sumframinghanmenor;
                        mdSCAToUpdate.md_taingreso = dataSCA.md_taingreso;
                        mdSCAToUpdate.md_valvulopatia = dataSCA.md_valvulopatia;

                        // if (context.ChangeTracker.HasChanges())
                        // {
                        context.Entry(mdSCAToUpdate).State = EntityState.Modified;
                        context.SaveChanges();
                        //  }
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {


                        /* Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);*/
                        sMessageValidation += "Se han encontrado los siguientes errores:" + Environment.NewLine;
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sMessageValidation += string.Format("- \"{0}\"",
                              ve.ErrorMessage);
                        }
                    }

                }
                catch (Exception ex)
                {
                    return sMessageValidation = ex.Message;
                }

            }
            return sMessageValidation;
        }

        public static List<md_sca> GetSCAFromFiltering(md_sca mdDataFromFiltering = null)
        {
            List<md_sca> conjuntoSCA = new List<md_sca>();

            /* using (var context = new bbddmedicalEntities())
             {
                 if (mdDataFromFiltering != null)
                      conjuntoPacientes = context.md_pacientes.Where(x =>
                       x.md_fechaalta == mdDataFromFiltering.md_fechaalta ||
                       x.md_fechaingreso == mdDataFromFiltering.md_fechaingreso ||
                       x.md_fechanacimiento == mdDataFromFiltering.md_fechanacimiento ||
                       x.md_nombre.Contains(mdDataFromFiltering.md_nombre) ||
                       x.md_sexo == mdDataFromFiltering.md_sexo
                      ).ToList();
                 else
                     conjuntoPacientes = context.md_pacientes.ToList();
             }*/
            if (mdDataFromFiltering == null)
            {
                using (var context = new bbddmedicalEntities())
                {
                    conjuntoSCA = context.md_sca.ToList();
                }
            }
            else
            {
                using (var context = new bbddmedicalEntities())
                {
                    conjuntoSCA = context.md_sca.ToList();
                    if (/*mdDataFromFiltering.md_record_id != null &&*/ mdDataFromFiltering.md_record_id != 0)
                    {
                        conjuntoSCA = conjuntoSCA.Where(x => x.md_record_id == mdDataFromFiltering.md_record_id).ToList();
                    }
                    /* if (mdDataFromFiltering.md_sexo != -1)
                     {
                         conjuntoSCA = conjuntoSCA.Where(x => x.md_sexo == mdDataFromFiltering.md_sexo).ToList();
                     }
                     if (mdDataFromFiltering.md_nhc != 0 && mdDataFromFiltering.md_nhc != null)
                     {
                         conjuntoSCA = conjuntoSCA.Where(x => x.md_nhc == mdDataFromFiltering.md_nhc).ToList();
                     }
                     if (mdDataFromFiltering.md_fechaalta != null)
                     {
                         conjuntoSCA = conjuntoSCA.Where(x => Convert.ToDateTime(x.md_fechaalta).Date <= Convert.ToDateTime(mdDataFromFiltering.md_fechaalta).Date && x.md_fechaalta != null).ToList();
                     }
                     if (mdDataFromFiltering.md_fechanacimiento != null)
                     {
                         conjuntoSCA = conjuntoSCA.Where(x => Convert.ToDateTime(x.md_fechanacimiento).Date <= Convert.ToDateTime(mdDataFromFiltering.md_fechanacimiento).Date && x.md_fechanacimiento != null).ToList();
                     }
                     if (mdDataFromFiltering.md_fechaingreso != null)
                         conjuntoSCA = conjuntoSCA.Where(z => Convert.ToDateTime(z.md_fechaingreso).Date <= Convert.ToDateTime(mdDataFromFiltering.md_fechaingreso).Date && z.md_fechaingreso != null).ToList();
                 */
                }
            }
            /* else
             {
                 using (var conn = new MedicalConnection())
                 {
                     var command = conn.CreateCommand();
                     command.Transaction = conn.Transaction;

                     command.CommandText += "SELECT * FROM md_pacientes WHERE 1=1 ";
                     if (mdDataFromFiltering.md_nombre != "" && !mdDataFromFiltering.md_nombre.Contains(","))
                     {
                         command.CommandText += "AND md_nombre like @md_nombre";
                         command.AddParameter("md_nombre", DbType.String, "%" + mdDataFromFiltering.md_nombre + "%");
                     }
                     else if (mdDataFromFiltering.md_nombre.Contains(","))
                     {
                         command.AddInParameter("md_nombre", DbType.String, mdDataFromFiltering.md_nombre);
                     }

                 }
             }*/

            return conjuntoSCA.OrderBy(x => x.md_record_id).ToList();

        }

    }
}
