﻿using medical_Infrastructure.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace medical_Data_Lib
{
    public class MdPacientes
    {
        public static string AddPaciente(md_pacientes dataPaciente, ref int iNewRecordIdPaciente)
        {
            string sMessageValidation = "";
            using (var context = new bbddmedicalEntities())
            {
                try
                {
                    var pacienteAdd = new md_pacientes
                    {
                        md_nombre = dataPaciente.md_nombre,
                        md_fechaingreso = dataPaciente.md_fechaingreso,
                        md_sexo = dataPaciente.md_sexo,
                        md_nhc = dataPaciente.md_nhc,
                        md_fechanacimiento = dataPaciente.md_fechanacimiento,
                        md_fechaalta = dataPaciente.md_fechaalta

                    };
                    //context.Entry(pacientes).State = EntityState.Added;
                    context.md_pacientes.Add(pacienteAdd);

                    if (context.ChangeTracker.HasChanges())
                    {
                        context.Entry(pacienteAdd).State = EntityState.Added;
                        //context.SaveChanges();
                        int x = context.SaveChanges();
                        iNewRecordIdPaciente = pacienteAdd.md_record_id;
                    }
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {


                        /* Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);*/
                        sMessageValidation += "Se han encontrado los siguientes errores:" + Environment.NewLine;
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sMessageValidation += string.Format("- \"{0}\"",
                              ve.ErrorMessage);
                            /* sMessageValidation += string.Format("- El campo: \"{0}\", Error: \"{1}\"",
                                 ve.PropertyName, ve.ErrorMessage);*/
                            /* Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                 ve.PropertyName, ve.ErrorMessage);*/
                        }
                    }
                }
                /*using (var medconn = new MedicalConnection())
                {
                    var command = medconn.CreateCommand();
                    command.CommandText += "SELECT * FROM md_icc where 1=1 ";
                    command.AddParameter("md_record_id", DbType.Int64, 35);
                    command.CommandText += "AND md_record_id=@md_record_id";

                    command.ExecuteReader();
                    command.Prepare();
                    command.FillDataset();

                }*/
            }
            return sMessageValidation;
        }

        public static string UpdatePaciente(Int32 iPacienteId, md_pacientes dataPaciente)
        {
            string sMessageValidation = "";

            using (var context = new bbddmedicalEntities())
            {
                try
                {


                    var pacienteToUpdate = context.md_pacientes.Where(x => x.md_record_id == iPacienteId).FirstOrDefault();
                    if (pacienteToUpdate == null)
                        throw new Exception(string.Format("No se ha encontrado el paciente con identificador [{0}].", iPacienteId));

                    pacienteToUpdate.md_nombre = dataPaciente.md_nombre;
                    pacienteToUpdate.md_fechaingreso = dataPaciente.md_fechaingreso;
                    pacienteToUpdate.md_fechaalta = dataPaciente.md_fechaalta;
                    pacienteToUpdate.md_sexo = dataPaciente.md_sexo;
                    pacienteToUpdate.md_fechanacimiento = dataPaciente.md_fechanacimiento;
                    pacienteToUpdate.md_nhc = dataPaciente.md_nhc;
                    if (context.ChangeTracker.HasChanges())
                    {
                        context.Entry(pacienteToUpdate).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {


                        /* Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
eve.Entry.Entity.GetType().Name, eve.Entry.State);*/
                        sMessageValidation += "Se han encontrado los siguientes errores:" + Environment.NewLine;
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sMessageValidation += string.Format("- \"{0}\"",
                              ve.ErrorMessage);
                            /* sMessageValidation += string.Format("- El campo: \"{0}\", Error: \"{1}\"",
                                 ve.PropertyName, ve.ErrorMessage);*/
                            /* Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                 ve.PropertyName, ve.ErrorMessage);*/
                        }
                    }
                }
                return sMessageValidation;
            }
        }

        public static void RemovePaciente(Int32 iPacienteId)
        {
            using (var context = new bbddmedicalEntities())
            {

                var pacienteToRemove = context.md_pacientes.Where(x => x.md_record_id == iPacienteId).FirstOrDefault();
                if (pacienteToRemove == null)
                    throw new Exception(string.Format("No se ha encontrado el paciente con identificador [{0}].", iPacienteId));

                context.md_pacientes.Remove(pacienteToRemove);
                if (context.ChangeTracker.HasChanges())
                {
                    context.Entry(pacienteToRemove).State = EntityState.Deleted;
                    context.SaveChanges();
                }

            }
        }

        public static List<md_pacientes> GetPacientes(List<Int32> lPacientesIds = null)
        {
            // List<md_pacientes> conjuntoPacientes = new List<md_pacientes>();
            string sPacientesIds = string.Empty;
            if (lPacientesIds != null && lPacientesIds.Any())
                sPacientesIds = lPacientesIds.Select(x => x.ToString()).JoinAsParameterWithoutQuotes();
            using (var context = new bbddmedicalEntities())
            {
                if (sPacientesIds != "" && sPacientesIds.Contains(","))
                    return context.md_pacientes.Where(x => x.md_record_id.ToString().Contains(sPacientesIds)).OrderBy(x => x.md_record_id).ToList();
                else if (sPacientesIds != "")
                    return context.md_pacientes.Where(p => p.md_record_id.ToString() == sPacientesIds).ToList();
                else
                    return context.md_pacientes.OrderBy(x => x.md_record_id).ToList();

            }

            /* using (var context = new MedicalConnection())
             {
                 var command = context.CreateCommand();

                 command.CommandText = "Select * from md_pacientes WHERE true ";
                 if (sPacientesIds != string.Empty)
                 {
                     if (sPacientesIds.Contains(','))
                         command.AddInParameter("md_record_id", System.Data.DbType.String, sPacientesIds);
                     else
                     {
                         command.CommandText += " AND md_record_id = @md_pacienteid";
                         command.AddParameter("md_pacienteid", System.Data.DbType.Int32, lPacientesIds.FirstOrDefault());
                     }
                 }

                 return command.FillDataset<List<MdPacientes>>();*/
        }

        public static List<md_pacientes> GetPacientesFromFiltering(md_pacientes mdDataFromFiltering = null)
        {
            List<md_pacientes> conjuntoPacientes = new List<md_pacientes>();

            /* using (var context = new bbddmedicalEntities())
             {
                 if (mdDataFromFiltering != null)
                      conjuntoPacientes = context.md_pacientes.Where(x =>
                       x.md_fechaalta == mdDataFromFiltering.md_fechaalta ||
                       x.md_fechaingreso == mdDataFromFiltering.md_fechaingreso ||
                       x.md_fechanacimiento == mdDataFromFiltering.md_fechanacimiento ||
                       x.md_nombre.Contains(mdDataFromFiltering.md_nombre) ||
                       x.md_sexo == mdDataFromFiltering.md_sexo
                      ).ToList();
                 else
                     conjuntoPacientes = context.md_pacientes.ToList();
             }*/
            if (mdDataFromFiltering == null)
            {
                using (var context = new bbddmedicalEntities())
                {
                    conjuntoPacientes = context.md_pacientes.ToList();
                }
            }
            else
            {
                using (var context = new bbddmedicalEntities())
                {
                    conjuntoPacientes = context.md_pacientes.ToList();
                    if (mdDataFromFiltering.md_nombre != null && mdDataFromFiltering.md_nombre != "" && !mdDataFromFiltering.md_nombre.Contains(","))
                    {
                        conjuntoPacientes = conjuntoPacientes.Where(x => x.md_nombre.Contains(mdDataFromFiltering.md_nombre)).ToList();
                    }
                    if (mdDataFromFiltering.md_sexo != -1)
                    {
                        conjuntoPacientes = conjuntoPacientes.Where(x => x.md_sexo == mdDataFromFiltering.md_sexo).ToList();
                    }
                    if (mdDataFromFiltering.md_nhc != 0 && mdDataFromFiltering.md_nhc != null)
                    {
                        conjuntoPacientes = conjuntoPacientes.Where(x => x.md_nhc == mdDataFromFiltering.md_nhc).ToList();
                    }
                    if (mdDataFromFiltering.md_fechaalta != null)
                    {
                        conjuntoPacientes = conjuntoPacientes.Where(x => Convert.ToDateTime(x.md_fechaalta).Date <= Convert.ToDateTime(mdDataFromFiltering.md_fechaalta).Date && x.md_fechaalta != null).ToList();
                    }
                    if (mdDataFromFiltering.md_fechanacimiento != null)
                    {
                        conjuntoPacientes = conjuntoPacientes.Where(x => Convert.ToDateTime(x.md_fechanacimiento).Date <= Convert.ToDateTime(mdDataFromFiltering.md_fechanacimiento).Date && x.md_fechanacimiento != null).ToList();
                    }
                    if (mdDataFromFiltering.md_fechaingreso != null)
                        conjuntoPacientes = conjuntoPacientes.Where(z => Convert.ToDateTime(z.md_fechaingreso).Date <= Convert.ToDateTime(mdDataFromFiltering.md_fechaingreso).Date && z.md_fechaingreso != null).ToList();
                }
            }
            /* else
             {
                 using (var conn = new MedicalConnection())
                 {
                     var command = conn.CreateCommand();
                     command.Transaction = conn.Transaction;

                     command.CommandText += "SELECT * FROM md_pacientes WHERE 1=1 ";
                     if (mdDataFromFiltering.md_nombre != "" && !mdDataFromFiltering.md_nombre.Contains(","))
                     {
                         command.CommandText += "AND md_nombre like @md_nombre";
                         command.AddParameter("md_nombre", DbType.String, "%" + mdDataFromFiltering.md_nombre + "%");
                     }
                     else if (mdDataFromFiltering.md_nombre.Contains(","))
                     {
                         command.AddInParameter("md_nombre", DbType.String, mdDataFromFiltering.md_nombre);
                     }

                 }
             }*/

            return conjuntoPacientes.OrderBy(x => x.md_record_id).ToList();

        }

        public static md_pacientes GetPacienteFromRecordId(Int32 lPacientesIds)
        {
            // List<md_pacientes> conjuntoPacientes = new List<md_pacientes>();
            string sPacientesIds = string.Empty;
            if (lPacientesIds == 0)
                return null;
            using (var context = new bbddmedicalEntities())
            {
                return context.md_pacientes.Where(x => x.md_record_id == lPacientesIds).FirstOrDefault();
            }

            /* using (var context = new MedicalConnection())
             {
                 var command = context.CreateCommand();

                 command.CommandText = "Select * from md_pacientes WHERE true ";
                 if (sPacientesIds != string.Empty)
                 {
                     if (sPacientesIds.Contains(','))
                         command.AddInParameter("md_record_id", System.Data.DbType.String, sPacientesIds);
                     else
                     {
                         command.CommandText += " AND md_record_id = @md_pacienteid";
                         command.AddParameter("md_pacienteid", System.Data.DbType.Int32, lPacientesIds.FirstOrDefault());
                     }
                 }

                 return command.FillDataset<List<MdPacientes>>();*/
        }

    }
}

