﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace medical_Data_Lib.Excel
{
    public static class ExcelExtensions
    {
        public static CellPointer CellPointer(this ExcelWorksheet sheet, int x, int y, int startColumn)
        {
            return new CellPointer(x, y, startColumn) { Sheet = sheet };
        }
    }

    public class CellPointer
    {
        public ExcelWorksheet Sheet { get; set; }
        public int StartingColum { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public ExcelRange CurrentCell => this.Sheet.Cells[Y, X];

        public CellPointer()
        {
            this.StartingColum = 1;
            this.X = 1;
            this.Y = 1;
        }

        public CellPointer(int x, int y)
            : this()
        {
            if (x == 0) x = 1;
            if (y == 0) y = 1;

            this.X = x;
            this.Y = y;
        }

        public CellPointer(int x, int y, int startCol)
            : this(x, y)
        {
            if (startCol == 0) startCol = 1;
            this.StartingColum = startCol;
        }

        public CellPointer SetX(int x)
        {
            this.X = x;
            return this;
        }

        public CellPointer SetY(int y)
        {
            this.Y = y;
            return this;
        }


        public CellPointer NewLine()
        {
            return NewLine(this.StartingColum);
        }

        public CellPointer NewLine(int startCol)
        {
            this.X = StartingColum;
            this.Y = this.Y + 1;
            return this;
        }

        public CellPointer RestartLine()
        {
            this.X = StartingColum;
            return this;
        }


        public CellPointer Advance(int columns)
        {
            this.X = this.X + columns;
            return this;
        }

        public CellPointer AdvanceRow()
        {
            return AdvanceRows(1);
        }

        public CellPointer AdvanceRows(int rows)
        {
            this.Y = this.Y + rows;
            return this;
        }


        public CellPointer AdvanceOne()
        {
            return this.Advance(1);
        }

        public CellPointer BackColor(Color color)
        {
            CurrentCell.Style.Fill.PatternType = ExcelFillStyle.Solid;
            CurrentCell.Style.Fill.BackgroundColor.SetColor(color);
            return this;
        }

        public CellPointer ForeColor(Color color)
        {
            CurrentCell.Style.Font.Color.SetColor(color);
            return this;
        }

        public CellPointer Bold()
        {
            CurrentCell.Style.Font.Bold = true;
            return this;
        }

        public CellPointer Italic()
        {
            CurrentCell.Style.Font.Italic = true;
            return this;
        }

        public CellPointer FontSize(float size)
        {
            CurrentCell.Style.Font.Size = size;
            return this;
        }

        /// <summary>
        /// Writes text to the current cell and advances one column
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public CellPointer WriteText(string text)
        {
            CurrentCell.Value = text;
            this.AdvanceOne();
            return this;
        }

        public CellPointer WriteDate(DateTime? date)
        {
            CurrentCell.Value = date;
            CurrentCell.Style.Numberformat.Format = "mm-dd-yyyy";
            this.AdvanceOne();
            return this;
        }

        public CellPointer WriteDateTime(DateTime? date)
        {
            CurrentCell.Value = date;
            CurrentCell.Style.Numberformat.Format = "mm-dd-yyyy hh:mm:ss";
            this.AdvanceOne();
            return this;
        }


        public CellPointer WriteObject(object value)
        {
            if (value is DateTime)
            {
                return WriteDate((DateTime)value);
            }
            CurrentCell.Value = value;
            this.AdvanceOne();
            return this;
        }


        public CellPointer WriteObjects(params object[] values)
        {
            foreach (var o in values)
            {
                WriteObject(o);
            }
            return this;
        }

        public CellPointer WriteMoney(decimal? money)
        {
            CurrentCell.Value = money;
            CurrentCell.Style.Numberformat.Format = "0.00"; //"0.00 %";
            this.AdvanceOne();
            return this;
        }

        public CellPointer WriteInt(int? number)
        {
            CurrentCell.Value = number;
            CurrentCell.Style.Numberformat.Format = "0"; //"0.00 %";
            this.AdvanceOne();
            return this;
        }


        public CellPointer WriteInt(long? number)
        {
            CurrentCell.Value = number;
            CurrentCell.Style.Numberformat.Format = "0"; //"0.00 %";
            this.AdvanceOne();
            return this;
        }

        public CellPointer WriteSize(decimal money)
        {
            CurrentCell.Value = money;
            CurrentCell.Style.Numberformat.Format = "0.00"; //"0.00 %";
            this.AdvanceOne();
            return this;
        }


        public bool HasValue()
        {
            return !string.IsNullOrWhiteSpace(CurrentCell.Text);
        }

        public decimal? ReadDecimal()
        {
            decimal? result = null;
            if (!string.IsNullOrWhiteSpace(CurrentCell.Text))
            {
                result = Convert.ToDecimal(CurrentCell.Value);
            }
            AdvanceOne();
            return result;
        }

        public decimal? ReadDecimal(decimal? defaultReturn)
        {
            decimal? result = defaultReturn;
            if (!string.IsNullOrWhiteSpace(CurrentCell.Text))
            {
                try
                {
                    result = Convert.ToDecimal(CurrentCell.Value);
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch (Exception)
                {
                }
            }
            AdvanceOne();
            return result;
        }


        public object ReadObject()
        {
            var result = CurrentCell.Value;
            AdvanceOne();
            return result;
        }

        public string ReadText()
        {
            var result = Convert.ToString(CurrentCell.Value);
            AdvanceOne();
            return result;
        }

        public double? ReadDouble()
        {
            double? result = null;
            if (!string.IsNullOrWhiteSpace(CurrentCell.Text))
            {
                result = Convert.ToDouble(CurrentCell.Value);
            }
            AdvanceOne();
            return result;
        }

        public int? ReadInt32()
        {
            int? result = null;
            if (!string.IsNullOrWhiteSpace(CurrentCell.Text))
            {
                result = Convert.ToInt32(CurrentCell.Value);
            }
            AdvanceOne();
            return result;
        }

        public CellPointer Clone()
        {
            var pointer = new CellPointer(this.X, this.Y, this.StartingColum);
            pointer.Sheet = this.Sheet;
            return pointer;
        }
    }
}
