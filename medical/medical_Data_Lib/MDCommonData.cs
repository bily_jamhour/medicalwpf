﻿using medical_Infrastructure.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace medical_Data_Lib
{
    public class MDCommonData
    {

        public static DataTable getIdAndNamePacientes(bool bAddEmptyValue = true)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("md_record_id");
            dt.Columns.Add("md_nombre");


            using (var conn = new MedicalConnection())
            {
                var command = conn.CreateCommand();
                command.Transaction = conn.Transaction;

                command.CommandText += "SELECT md_record_id , md_nombre FROM md_pacientes WHERE 1=1 ";
                command.Prepare();

                dt = command.FillDataset();
            }

            if (bAddEmptyValue)
            {
                DataRow rEmtpy = dt.NewRow();
                rEmtpy["md_record_id"] = 0;
                rEmtpy["md_nombre"] = "Sin Seleccionar";
                dt.Rows.InsertAt(rEmtpy, 0);
            }

            return dt;
        }

        public static DataTable getIdAndNamePacientesFromICC()
        {
            DataTable dt = new DataTable();

            using (var conn = new MedicalConnection())
            {
                var command = conn.CreateCommand();
                command.Transaction = conn.Transaction;

                command.CommandText += "SELECT md_record_id , md_nombre FROM md_pacientes WHERE 1=1 ";
                command.Prepare();
                dt = command.FillDataset();
            }

            return dt;
        }

        public static DataTable getIdAndNamePacientesInNotSCA()
        {
            DataTable dt = new DataTable();

            using (var conn = new MedicalConnection())
            {
                var command = conn.CreateCommand();
                command.Transaction = conn.Transaction;

                command.CommandText += "SELECT md_pacientes.md_record_id , md_pacientes.md_nombre from md_pacientes where md_pacientes.md_record_id NOT IN (SELECT md_pacientes.md_record_id FROM md_pacientes,md_sca WHERE 1=1  AND md_sca.md_record_id = md_pacientes.md_record_id)";
                command.Prepare();
                dt = command.FillDataset();
            }

            return dt;
        }

        public static DataTable getIdAndNamePacientesInNotICC()
        {
            DataTable dt = new DataTable();

            using (var conn = new MedicalConnection())
            {
                var command = conn.CreateCommand();
                command.Transaction = conn.Transaction;

                command.CommandText += "SELECT md_pacientes.md_record_id , md_pacientes.md_nombre from md_pacientes where md_pacientes.md_record_id NOT IN (SELECT md_pacientes.md_record_id FROM md_pacientes,md_icc WHERE 1=1  AND md_icc.md_record_id = md_pacientes.md_record_id)";
                command.Prepare();
                dt = command.FillDataset();
            }

            return dt;
        }


        public static DataTable getSCAToReport(md_sca dataSCAToFiltering = null, md_pacientes dataPacienteToFiltering = null)
        {
            DataTable dt = new DataTable();

            using (var conn = new MedicalConnection())
            {
                var command = conn.CreateCommand();
                command.Transaction = conn.Transaction;

                command.CommandText += "SELECT md_nombre,md_nhc, md_sexo,md_fechanacimiento,md_fechaingreso,md_fechaalta, md_sca.* from md_pacientes, md_sca WHERE md_pacientes.md_record_id = md_sca.md_record_id ";
                command.Prepare();
                if (dataSCAToFiltering != null)
                {

                }
                if (dataPacienteToFiltering != null)
                {
                    if (dataPacienteToFiltering.md_record_id != 0)
                    {
                        command.CommandText += " AND md_pacientes.md_record_id=@record_id";
                        command.AddParameter("record_id", DbType.Int32, dataPacienteToFiltering.md_record_id);
                    }
                    if (dataPacienteToFiltering.md_sexo >= 0)
                    {
                        command.CommandText += " AND md_pacientes.md_sexo =@sexo ";
                        command.AddParameter("sexo", DbType.Int32, dataPacienteToFiltering.md_sexo);
                    }
                    if (dataPacienteToFiltering.md_fechaalta != null)
                    {
                        command.CommandText += "AND md_pacientes.md_fechaalta <=@fechaAlta";
                        command.AddParameter("fechaAlta", DbType.DateTime, Convert.ToDateTime(dataPacienteToFiltering.md_fechaalta).Date.AddDays(1).AddSeconds(-1));
                    }
                    if (dataPacienteToFiltering.md_fechanacimiento != null)
                    {
                        command.CommandText += "AND md_pacientes.md_fechanacimiento <=@fechaNacimiento";
                        command.AddParameter("fechaNacimiento", DbType.DateTime, Convert.ToDateTime(dataPacienteToFiltering.md_fechanacimiento.Value.AddDays(1).AddSeconds(-1)));
                    }
                    if (dataPacienteToFiltering.md_fechaingreso != null)
                    {
                        command.CommandText += "AND md_pacientes.md_fechaingreso <=@fechaIngreso";
                        command.AddParameter("fechaIngreso", DbType.DateTime, Convert.ToDateTime(dataPacienteToFiltering.md_fechaingreso).Date.AddDays(1).AddSeconds(-1));
                    }
                }
                dt = command.FillDataset();
            }

            return dt;
        }

        public static DataTable getICCToReport(md_icc dataICCToFiltering = null, md_pacientes dataPacienteToFiltering = null)
        {
            DataTable dt = new DataTable();

            using (var conn = new MedicalConnection())
            {
                var command = conn.CreateCommand();
                command.Transaction = conn.Transaction;

                command.CommandText += "SELECT md_nombre,md_nhc, md_sexo,md_fechanacimiento,md_fechaingreso,md_fechaalta, md_icc.* from md_pacientes, md_icc WHERE md_pacientes.md_record_id = md_icc.md_record_id ";
                command.Prepare();
                if (dataICCToFiltering != null)
                {

                }
                if (dataPacienteToFiltering != null)
                {
                    if (dataPacienteToFiltering.md_record_id != 0)
                    {
                        command.CommandText += " AND md_pacientes.md_record_id=@record_id";
                        command.AddParameter("record_id", DbType.Int32, dataPacienteToFiltering.md_record_id);
                    }
                    if (dataPacienteToFiltering.md_sexo >= 0)
                    {
                        command.CommandText += " AND md_pacientes.md_sexo =@sexo ";
                        command.AddParameter("sexo", DbType.Int32, dataPacienteToFiltering.md_sexo);
                    }
                    if (dataPacienteToFiltering.md_fechaalta != null)
                    {
                        command.CommandText += "AND md_pacientes.md_fechaalta <=@fechaAlta";
                        command.AddParameter("fechaAlta", DbType.DateTime, Convert.ToDateTime(dataPacienteToFiltering.md_fechaalta).Date);
                    }
                    if (dataPacienteToFiltering.md_fechanacimiento != null)
                    {
                        command.CommandText += "AND md_pacientes.md_fechanacimiento <=@fechaNacimiento";
                        command.AddParameter("fechaNacimiento", DbType.DateTime, Convert.ToDateTime(dataPacienteToFiltering.md_fechanacimiento).Date);
                    }
                    if (dataPacienteToFiltering.md_fechaingreso != null)
                    {
                        command.CommandText += "AND md_pacientes.md_fechaingreso <=@fechaIngreso";
                        command.AddParameter("fechaIngreso", DbType.DateTime, Convert.ToDateTime(dataPacienteToFiltering.md_fechaingreso).Date);
                    }
                }
                dt = command.FillDataset();
            }

            return dt;
        }
    }
}
