﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace medical_Data_Lib
{
    public class MdICC
    {
        public static string AddICC(int iPacienteId, md_icc dataICC)
        {
            string sMessageValidation = "";
            using (var context = new bbddmedicalEntities())
            {
                try
                {
                    var mdICCAdd = new md_icc
                    {
                        md_amiodaronapost = dataICC.md_amiodaronapost,
                        md_amiodaronapre = dataICC.md_amiodaronapre,
                        md_antecedentesfa = dataICC.md_antecedentesfa,
                        md_antialdosteronicospost = dataICC.md_antialdosteronicospost,
                        md_antialdosteronicospre = dataICC.md_antialdosteronicospre,
                        md_anticoagulacioningreso = dataICC.md_anticoagulacioningreso,
                        md_betabloqueantespost = dataICC.md_betabloqueantespost,
                        md_betabloqueantespre = dataICC.md_betabloqueantespre,
                        md_cardiopatiaisquemica = dataICC.md_cardiopatiaisquemica,
                        md_clasefuncionalbasal = dataICC.md_clasefuncionalbasal,
                        md_creatininaalta = dataICC.md_creatininaalta,
                        md_creatininaingreso = dataICC.md_creatininaingreso,
                        md_diabetesmellitus = dataICC.md_diabetesmellitus,
                        md_digoxinapost = dataICC.md_digoxinapost,
                        md_digoxinapre = dataICC.md_digoxinapre,
                        md_dislipemia = dataICC.md_dislipemia,
                        md_ecocardiograma = dataICC.md_ecocardiograma,
                        md_ecocardiogramaduranteingreso = dataICC.md_ecocardiogramaduranteingreso,
                        md_exitus = dataICC.md_exitus,
                        md_exitusdesc = dataICC.md_exitusdesc,
                        md_fccontrolada = dataICC.md_fccontrolada,
                        md_fcingreso = dataICC.md_fcingreso,
                        md_fevi = dataICC.md_fevi,
                        md_historialsincopes = dataICC.md_historialsincopes,
                        md_horapicotensional_diurno = dataICC.md_horapicotensional_diurno,
                        md_horapicotensional_nocturno = dataICC.md_horapicotensional_nocturno,
                        md_hta = dataICC.md_hta,
                        md_htap = dataICC.md_htap,
                        md_iecapost = dataICC.md_iecapost,
                        md_iecapre = dataICC.md_iecapre,
                        md_ingresos = dataICC.md_ingresos,
                        md_ingresosicc = dataICC.md_ingresosicc,
                        md_mapa = dataICC.md_mapa,
                        md_mediafcduranteregistro = dataICC.md_mediafcduranteregistro,
                        md_mediatensional_diurno = dataICC.md_mediatensional_diurno,
                        md_mediatensional_nocturno = dataICC.md_mediatensional_nocturno,
                        md_nyha = dataICC.md_nyha,
                        md_record_id = (dataICC.md_record_id == 0) ? iPacienteId : dataICC.md_record_id,
                        md_patron = dataICC.md_patron,
                        md_picotensional_diurno = dataICC.md_picotensional_diurno,
                        md_picotensional_nocturno = dataICC.md_picotensional_nocturno,
                        //md_pacientes=dataICC.md_pacientes,
                        md_registro = dataICC.md_registro,
                        md_ritmoalta = dataICC.md_ritmoalta,
                        md_status = dataICC.md_status,
                        md_sumframinghanmayor = dataICC.md_sumframinghanmayor,
                        md_sumframinghanmenor = dataICC.md_sumframinghanmenor,
                        md_taingreso = dataICC.md_taingreso,
                        md_valvulopatia = dataICC.md_valvulopatia
                    };

                    context.md_icc.Add(mdICCAdd);
                    if (context.ChangeTracker.HasChanges())
                    {
                        context.Entry(mdICCAdd).State = EntityState.Added;
                        context.SaveChanges();
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {


                        /* Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);*/
                        sMessageValidation += "Se han encontrado los siguientes errores:" + Environment.NewLine;
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sMessageValidation += string.Format("- \"{0}\"",
                              ve.ErrorMessage);
                            /* sMessageValidation += string.Format("- El campo: \"{0}\", Error: \"{1}\"",
                                 ve.PropertyName, ve.ErrorMessage);*/
                            /* Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                 ve.PropertyName, ve.ErrorMessage);*/
                        }
                    }

                }
                catch (Exception ex)
                {
                    return sMessageValidation = ex.Message;
                }

            }
            return sMessageValidation;
        }

        public static List<md_icc> GetICC(Int32 iPacienteId)
        {
            if (iPacienteId == 0)
                return new List<md_icc>();
            using (var context = new bbddmedicalEntities())
            {
                return context.md_icc.Where(x => x.md_record_id == iPacienteId).ToList();
            }
        }

        public static string RemoveICC(Int32 iPacienteId)
        {
            string sMessageValidation = "";

            if (iPacienteId == 0)
                return "No se puede eliminar un SCA con identificador de paciente 0";
            using (var context = new bbddmedicalEntities())
            {
                try
                {
                    var scaToRemove = context.md_icc.Where(x => x.md_record_id == iPacienteId).FirstOrDefault();
                    if (scaToRemove == null)
                    {
                        sMessageValidation = string.Format("No se ha encontrado los datos del ICC del paciente con identificador [{0}].", iPacienteId);
                        return sMessageValidation;
                    }
                    context.md_icc.Remove(scaToRemove);
                    if (context.ChangeTracker.HasChanges())
                    {
                        context.Entry(scaToRemove).State = EntityState.Deleted;
                        context.SaveChanges();
                    }

                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {


                        sMessageValidation += "Se han encontrado los siguientes errores:" + Environment.NewLine;
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sMessageValidation += string.Format("- \"{0}\"",
                              ve.ErrorMessage);

                        }
                    }

                }
                catch (Exception ex)
                {
                    return sMessageValidation = ex.Message;
                }
            }
            return sMessageValidation;
        }

        public static string UpdateICC(int iPacienteId, md_icc dataICC)
        {
            string sMessageValidation = "";
            using (var context = new bbddmedicalEntities())
            {
                try
                {
                    var mdICCoUpdate = MdICC.GetICC(iPacienteId).FirstOrDefault();
                    if (mdICCoUpdate == null)
                    {
                        sMessageValidation = string.Format("No se ha encontrado los datos del ICC del paciente con identificador [{0}].", iPacienteId);
                        return sMessageValidation;
                    }
                    else
                    {
                        mdICCoUpdate.md_amiodaronapost = dataICC.md_amiodaronapost;
                        mdICCoUpdate.md_amiodaronapre = dataICC.md_amiodaronapre;
                        mdICCoUpdate.md_antecedentesfa = dataICC.md_antecedentesfa;
                        mdICCoUpdate.md_antialdosteronicospost = dataICC.md_antialdosteronicospost;
                        mdICCoUpdate.md_antialdosteronicospre = dataICC.md_antialdosteronicospre;
                        mdICCoUpdate.md_anticoagulacioningreso = dataICC.md_anticoagulacioningreso;
                        mdICCoUpdate.md_betabloqueantespost = dataICC.md_betabloqueantespost;
                        mdICCoUpdate.md_betabloqueantespre = dataICC.md_betabloqueantespre;
                        mdICCoUpdate.md_cardiopatiaisquemica = dataICC.md_cardiopatiaisquemica;
                        mdICCoUpdate.md_clasefuncionalbasal = dataICC.md_clasefuncionalbasal;
                        mdICCoUpdate.md_creatininaalta = dataICC.md_creatininaalta;
                        mdICCoUpdate.md_creatininaingreso = dataICC.md_creatininaingreso;
                        mdICCoUpdate.md_diabetesmellitus = dataICC.md_diabetesmellitus;
                        mdICCoUpdate.md_digoxinapost = dataICC.md_digoxinapost;
                        mdICCoUpdate.md_digoxinapre = dataICC.md_digoxinapre;
                        mdICCoUpdate.md_dislipemia = dataICC.md_dislipemia;
                        mdICCoUpdate.md_ecocardiograma = dataICC.md_ecocardiograma;
                        mdICCoUpdate.md_ecocardiogramaduranteingreso = dataICC.md_ecocardiogramaduranteingreso;
                        mdICCoUpdate.md_exitus = dataICC.md_exitus;
                        mdICCoUpdate.md_exitusdesc = dataICC.md_exitusdesc;
                        mdICCoUpdate.md_fccontrolada = dataICC.md_fccontrolada;
                        mdICCoUpdate.md_fcingreso = dataICC.md_fcingreso;
                        mdICCoUpdate.md_fevi = dataICC.md_fevi;
                        mdICCoUpdate.md_historialsincopes = dataICC.md_historialsincopes;
                        mdICCoUpdate.md_horapicotensional_diurno = dataICC.md_horapicotensional_diurno;
                        mdICCoUpdate.md_horapicotensional_nocturno = dataICC.md_horapicotensional_nocturno;
                        mdICCoUpdate.md_hta = dataICC.md_hta;
                        mdICCoUpdate.md_htap = dataICC.md_htap;
                        mdICCoUpdate.md_iecapost = dataICC.md_iecapost;
                        mdICCoUpdate.md_iecapre = dataICC.md_iecapre;
                        mdICCoUpdate.md_ingresos = dataICC.md_ingresos;
                        mdICCoUpdate.md_ingresosicc = dataICC.md_ingresosicc;
                        mdICCoUpdate.md_mapa = dataICC.md_mapa;
                        mdICCoUpdate.md_mediafcduranteregistro = dataICC.md_mediafcduranteregistro;
                        mdICCoUpdate.md_mediatensional_diurno = dataICC.md_mediatensional_diurno;
                        mdICCoUpdate.md_mediatensional_nocturno = dataICC.md_mediatensional_nocturno;
                        mdICCoUpdate.md_nyha = dataICC.md_nyha;
                        mdICCoUpdate.md_record_id = dataICC.md_record_id;
                        mdICCoUpdate.md_patron = dataICC.md_patron;
                        mdICCoUpdate.md_picotensional_diurno = dataICC.md_picotensional_diurno;
                        mdICCoUpdate.md_picotensional_nocturno = dataICC.md_picotensional_nocturno;
                        // mdSCAToUpdate.//md_pacientes=dataSCA.md_pacientes;
                        mdICCoUpdate.md_registro = dataICC.md_registro;
                        mdICCoUpdate.md_ritmoalta = dataICC.md_ritmoalta;
                        mdICCoUpdate.md_status = dataICC.md_status;
                        mdICCoUpdate.md_sumframinghanmayor = dataICC.md_sumframinghanmayor;
                        mdICCoUpdate.md_sumframinghanmenor = dataICC.md_sumframinghanmenor;
                        mdICCoUpdate.md_taingreso = dataICC.md_taingreso;
                        mdICCoUpdate.md_valvulopatia = dataICC.md_valvulopatia;

                        //if (context.ChangeTracker.HasChanges())
                        //{
                            context.Entry(mdICCoUpdate).State = EntityState.Modified;
                            context.SaveChanges();
                        //}
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {


                        /* Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);*/
                        sMessageValidation += "Se han encontrado los siguientes errores:" + Environment.NewLine;
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sMessageValidation += string.Format("- \"{0}\"",
                              ve.ErrorMessage);
                            /* sMessageValidation += string.Format("- El campo: \"{0}\", Error: \"{1}\"",
                                 ve.PropertyName, ve.ErrorMessage);*/
                            /* Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                 ve.PropertyName, ve.ErrorMessage);*/
                        }
                    }

                }
                catch (Exception ex)
                {
                    return sMessageValidation = ex.Message;
                }

            }
            return sMessageValidation;
        }

        public static List<md_icc> GetICCFromFiltering(md_icc mdDataFromFiltering = null)
        {
            List<md_icc> conjuntoICC = new List<md_icc>();

            /* using (var context = new bbddmedicalEntities())
             {
                 if (mdDataFromFiltering != null)
                      conjuntoPacientes = context.md_pacientes.Where(x =>
                       x.md_fechaalta == mdDataFromFiltering.md_fechaalta ||
                       x.md_fechaingreso == mdDataFromFiltering.md_fechaingreso ||
                       x.md_fechanacimiento == mdDataFromFiltering.md_fechanacimiento ||
                       x.md_nombre.Contains(mdDataFromFiltering.md_nombre) ||
                       x.md_sexo == mdDataFromFiltering.md_sexo
                      ).ToList();
                 else
                     conjuntoPacientes = context.md_pacientes.ToList();
             }*/
            if (mdDataFromFiltering == null)
            {
                using (var context = new bbddmedicalEntities())
                {
                    conjuntoICC = context.md_icc.ToList();
                }
            }
            else
            {
                using (var context = new bbddmedicalEntities())
                {
                    conjuntoICC = context.md_icc.ToList();
                    if (/*mdDataFromFiltering.md_record_id != null &&*/ mdDataFromFiltering.md_record_id != 0)
                    {
                        conjuntoICC = conjuntoICC.Where(x => x.md_record_id == mdDataFromFiltering.md_record_id).ToList();
                    }
                    /* if (mdDataFromFiltering.md_sexo != -1)
                     {
                         conjuntoICC = conjuntoICC.Where(x => x.md_sexo == mdDataFromFiltering.md_sexo).ToList();
                     }
                     if (mdDataFromFiltering.md_nhc != 0 && mdDataFromFiltering.md_nhc != null)
                     {
                         conjuntoICC = conjuntoICC.Where(x => x.md_nhc == mdDataFromFiltering.md_nhc).ToList();
                     }
                     if (mdDataFromFiltering.md_fechaalta != null)
                     {
                         conjuntoICC = conjuntoICC.Where(x => Convert.ToDateTime(x.md_fechaalta).Date <= Convert.ToDateTime(mdDataFromFiltering.md_fechaalta).Date && x.md_fechaalta != null).ToList();
                     }
                     if (mdDataFromFiltering.md_fechanacimiento != null)
                     {
                         conjuntoICC = conjuntoICC.Where(x => Convert.ToDateTime(x.md_fechanacimiento).Date <= Convert.ToDateTime(mdDataFromFiltering.md_fechanacimiento).Date && x.md_fechanacimiento != null).ToList();
                     }
                     if (mdDataFromFiltering.md_fechaingreso != null)
                         conjuntoICC = conjuntoICC.Where(z => Convert.ToDateTime(z.md_fechaingreso).Date <= Convert.ToDateTime(mdDataFromFiltering.md_fechaingreso).Date && z.md_fechaingreso != null).ToList();
                 */
                }
            }
            /* else
             {
                 using (var conn = new MedicalConnection())
                 {
                     var command = conn.CreateCommand();
                     command.Transaction = conn.Transaction;

                     command.CommandText += "SELECT * FROM md_pacientes WHERE 1=1 ";
                     if (mdDataFromFiltering.md_nombre != "" && !mdDataFromFiltering.md_nombre.Contains(","))
                     {
                         command.CommandText += "AND md_nombre like @md_nombre";
                         command.AddParameter("md_nombre", DbType.String, "%" + mdDataFromFiltering.md_nombre + "%");
                     }
                     else if (mdDataFromFiltering.md_nombre.Contains(","))
                     {
                         command.AddInParameter("md_nombre", DbType.String, mdDataFromFiltering.md_nombre);
                     }

                 }
             }*/

            return conjuntoICC.OrderBy(x => x.md_record_id).ToList();

        }

    }
}
