﻿using medical_Infrastructure.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace medical_Infrastructure.Instrumentation
{
    public class SqlLogger : IDisposable
    {
        private bool Disposed { get; set; }
        public static Dictionary<Thread, SqlTraceLog> Traces { get; } = new Dictionary<Thread, SqlTraceLog>();
        public string Name { get; set; }
        public Thread CurrentTread { get; set; }

        private static long m_fileCount;

        public SqlLogger(string name, bool autosave = true)
        {
            CurrentTread = Thread.CurrentThread;
            Name = name;

            lock (Traces)
            {
                var trace = Traces.TryGetOrDefault(Thread.CurrentThread);
                if (trace == null)
                {
                    trace = new SqlTraceLog();
                    Traces[CurrentTread] = trace;
                    trace.Autosave = autosave;
                }
                else
                {
                    trace.InstanceCount++;
                    trace.Autosave |= autosave;
                }
            }
        }

        public static void LogSql(DbConnection con, string sql)
        {
            SqlTraceLog sqlTrace;
            lock (Traces)
            {
                sqlTrace = Traces.TryGetOrDefault(Thread.CurrentThread);
                if (sqlTrace == null) return;
            }

            sqlTrace.SqlList.Add("");
            bool firstTime;
            var id = sqlTrace.IdGenerator.GetId(con, out firstTime);
            sqlTrace.SqlList.Add("Conexión #" + id + (firstTime ? "(nueva)" : ""));
            sqlTrace.Connections[con] = id;

            sqlTrace.SqlList.Add(DateTime.Now.ToLongTimeString());
            sqlTrace.SqlList.Add(sql);
        }

        private void Dump(SqlTraceLog sqlTrace)
        {
            if (Disposed) return;
            if (sqlTrace == null) return;

            sqlTrace.SqlList.Add("");
            sqlTrace.SqlList.Add("================================");
            sqlTrace.SqlList.Add("Connections");
            sqlTrace.SqlList.Add("================================");
            foreach (var remainingConnection in sqlTrace.Connections.Keys)
            {
                bool firstTime;
                var identity = sqlTrace.IdGenerator.GetId(remainingConnection, out firstTime);
                sqlTrace.SqlList.Add("Connection #" + identity + " " + remainingConnection.State);
            }

            var hasOpenConnections = sqlTrace.Connections.Keys.Any(x => x.State != ConnectionState.Closed);
            if (hasOpenConnections)
            {
                Name += ".WARNING";
            }

            if (!sqlTrace.Autosave && !hasOpenConnections) return;

            Interlocked.Increment(ref m_fileCount);
            var path = Path.Combine(ConfigurationManager.AppSettings["tempDir"], Name + "." + DateTime.Now.Ticks + "." + m_fileCount.ToString("0000000000") + ".txt");
            File.WriteAllLines(path, sqlTrace.SqlList);
        }

        public void Dispose()
        {
            if (Disposed) return;


            SqlTraceLog sqlTrace;
            lock (Traces)
            {
                var thread = CurrentTread;
                CurrentTread = null; //the sooner the better
                sqlTrace = Traces.TryGetOrDefault(thread);

                if (sqlTrace == null) return;

                if (--sqlTrace.InstanceCount == 0)
                {
                    Traces.Remove(thread);
                }
                else
                {
                    sqlTrace = null;
                }
            }

            if (sqlTrace == null) return;
            Dump(sqlTrace);
            Disposed = true;
            sqlTrace.IdGenerator = null;
            sqlTrace.Connections = null;
        }
    }

    public class SqlTraceLog : IDisposable
    {
        public DateTime StartTime { get; set; } = DateTime.Now;
        public List<string> SqlList { get; set; } = new List<string>();
        public int InstanceCount { get; internal set; } = 1;
        public ObjectIDGenerator IdGenerator { get; set; } = new ObjectIDGenerator();
        public Dictionary<DbConnection, long> Connections { get; set; } = new Dictionary<DbConnection, long>();
        public bool Autosave { get; set; }

        public void Dispose()
        {
            IdGenerator = null;
        }
    }
}
