﻿using medical_Infrastructure.Instrumentation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace medical_Infrastructure.Utilities
{
    public static class ExtensionsData
    {
        public static int ExecuteNonQueryEx(this DbCommand command)
        {
            SqlLogger.LogSql(command.Connection, command.CommandText);
            return command.ExecuteNonQuery();
        }

        public static void FillEx(this DbDataAdapter adapter, DataTable dt)
        {
            SqlLogger.LogSql(adapter.SelectCommand.Connection, adapter.SelectCommand.CommandText);
            adapter.Fill(dt);
        }

        public static void FillEx(this DbDataAdapter adapter, DataSet dt, string name)
        {
            SqlLogger.LogSql(adapter.SelectCommand.Connection, adapter.SelectCommand.CommandText);
            adapter.Fill(dt, name);
        }

        public static DbDataReader ExecuteReaderEx(this DbCommand command)
        {
            SqlLogger.LogSql(command.Connection, command.CommandText);
            return command.ExecuteReader();
        }
    }
}
