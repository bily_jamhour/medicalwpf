﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace medical_Infrastructure.Utilities
{
   public class MedicalConnection : IDisposable
    {
        private readonly string _connectionString;

        public DbConnection Connection { get; private set; }
        public DbTransaction Transaction { get; set; }
        public bool InTransaction { get; set; }
        public bool UserProvidedConnection { get; private set; }
        public DbProviderFactory Factory { get; set; }

        public MedicalConnection()
        {
            DoWiring();
        }

        public MedicalConnection(string connectionString)
        {
            _connectionString = connectionString;
            DoWiring();
        }

        private void DoWiring()
        {
            var driverName = ConfigurationManager.AppSettings["dataBaseDriverName"] ?? "System.Data.SqlClient";
            Factory = DbProviderFactories.GetFactory(driverName);
            Connection = this.Factory.CreateConnection();

            if (!string.IsNullOrWhiteSpace(_connectionString))
            {
                Connection.ConnectionString = _connectionString;
            }
            else
            {
                Connection.ConnectionString = ConfigurationManager.ConnectionStrings["MedicalConnectionString"].ConnectionString;
            }
            Connection.Open();
            Transaction = this.Connection.BeginTransaction();
            InTransaction = true;
            UserProvidedConnection = false; //just to be explicit
        }

        public MedicalConnection(DbConnection userConnection)
        {
            if (userConnection == null)
            {
                DoWiring();
            }
            else
            {
                var driverName = ConfigurationManager.AppSettings["dataBaseDriverName"] ?? "System.Data.SqlClient";
                Factory = DbProviderFactories.GetFactory(driverName);
                this.Connection = userConnection;
                this.InTransaction = false; // wether it is or not, it is not ours to handle
                this.UserProvidedConnection = true;
            }
        }

        public MedicalConnection(MedicalConnection connection) : this(connection?.Connection)
        {
        }

        public MedicalConnection(string connectionString, MedicalConnection conn)
        {
            if (conn == null)
            {
                if (!string.IsNullOrWhiteSpace(connectionString))
                {
                    _connectionString = connectionString;
                }
                DoWiring();
            }
            else
            {
                var driverName = ConfigurationManager.AppSettings["dataBaseDriverName"] ?? "System.Data.SqlClient";
                Factory = DbProviderFactories.GetFactory(driverName);
                this.Connection = conn.Connection;
                this.InTransaction = false; // wether it is or not, it is not ours to handle
                this.UserProvidedConnection = true;
            }
        }

        public void BeginTransaction()
        {
            if (InTransaction) throw new InvalidOperationException("Transacción ya iniciada");
            Transaction = Connection.BeginTransaction();
            InTransaction = true;
        }

        public void Commit()
        {
            Transaction.Commit();
            Transaction = null;
            InTransaction = false;
        }

        public void Rollback()
        {
            Transaction.Rollback();
            Transaction = null;
            InTransaction = false;
        }

        public DbCommand CreateCommand()
        {
            return this.Connection.CreateCommand();
        }

        public DbCommand CreateCommand(string query)
        {
            var command = this.Connection.CreateCommand();
            command.CommandText = query;
            return command;
        }

        public DbParameter CreateParameter(string name, DbType type, object value)
        {
            var parameter = this.Factory.CreateParameter();
            parameter.ParameterName = name;
            parameter.DbType = type;
            parameter.Value = value;
            return parameter;
        }

        public void Dispose()
        {
            if (!UserProvidedConnection)
            {
                if (this.InTransaction)
                {
                    try
                    {
                        this.Transaction.Rollback();
                    }
                    catch
                    {
                        //gently continue
                    }
                }
                this.Connection.Close();
            }
        }
    }
}
