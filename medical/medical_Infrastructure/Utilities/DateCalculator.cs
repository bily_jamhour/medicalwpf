﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace medical_Infrastructure.Utilities
{
    public class DateCalculator
    {
        private readonly DateTime _today;
        private List<string> _elements;
        private List<char> _operations;

        public DateCalculator(DateTime today)
        {
            _today = today.Date;
        }

        public DateTime ParseDate(string dateText)
        {
            _elements = dateText.Split('+', '-').ToList();
            _operations = dateText.Where(x => x == '+' || x == '-').ToList();
            var currentDate = GetReferenceDate();
            _elements.RemoveAt(0);

            for (var i = 0; i < _elements.Count; i++)
            {
                currentDate = DoAddition(currentDate, _operations[i], _elements[i]);
            }
            return currentDate;
        }

        private DateTime DoAddition(DateTime currentDate, char operation, string value)
        {
            value = value.Trim();
            var valueParts = new string[] { };

            var firstNonNumeric = value.ToList().FindIndex(x => x >= 'A');
            if (firstNonNumeric >= 0)
            {
                valueParts = new[] { value.Substring(0, firstNonNumeric), value.Substring(firstNonNumeric, value.Length - firstNonNumeric) };
            }

            if (valueParts.Length != 2)
            {
                throw new InvalidOperationException($"Error en \"{value}\". Es necesario indicar un número y un identificador de tiempo");
            }

            int delta;
            if (!int.TryParse(valueParts[0], out delta)) throw new InvalidOperationException($"Error al calcular el desplazamiento. \"{valueParts[0]}\" no es un número válido");

            var sign = 1;
            if (operation == '-') sign = -1;

            switch (valueParts[1].ToLower().Trim())
            {
                case "day":
                case "d":
                    currentDate = currentDate.AddDays(sign * delta);
                    break;
                case "week":
                case "w":
                    currentDate = currentDate.AddDays(sign * 7 * delta);
                    break;
                case "month":
                case "m":
                    currentDate = currentDate.AddMonths(sign * delta);
                    break;
                case "quarter":
                case "q":
                    currentDate = currentDate.AddMonths(sign * 3 * delta);
                    break;
                case "semester":
                case "s":
                    currentDate = currentDate.AddMonths(sign * 6 * delta);
                    break;
                case "year":
                case "y":
                    currentDate = currentDate.AddYears(sign * delta);
                    break;
                default:
                    throw new InvalidOperationException(string.Format("Nombre de período inválido: {0}", valueParts[1].ToLower()));
            }
            return currentDate;
        }

        private DateTime GetReferenceDate()
        {
            if (_elements == null || !_elements.Any()) throw new InvalidOperationException("Date expression is empty");
            var current = _today;
            switch (_elements.First().ToLower().Trim())
            {
                case "@today":
                    return current;
                case "@yesterday":
                    return current.AddDays(-1);

                case "@week":
                    while (current.DayOfWeek != DayOfWeek.Monday) current = current.AddDays(-1);
                    return current;
                case "@lastweek":
                    current = current.AddDays(-7);
                    while (current.DayOfWeek != DayOfWeek.Monday) current = current.AddDays(-1);
                    return current;

                case "@month":
                    return current.AddDays(-1 * (current.Day - 1));
                case "@lastmonth":
                    return current.AddDays(-1 * (current.Day - 1)).AddMonths(-1);

                case "@quarter":
                    var quarterNumber = (current.Month - 1) / 3 + 1;
                    return new DateTime(current.Year, (quarterNumber - 1) * 3 + 1, 1);
                case "@lastquarter":
                    var quarterNumber1 = (current.Month - 1) / 3 + 1;
                    current = new DateTime(current.Year, (quarterNumber1 - 1) * 3 + 1, 1);
                    current = current.AddMonths(-3);
                    return current;

                case "@semester":
                    var semesternumber = (current.Month - 1) / 6 + 1;
                    return new DateTime(current.Year, (semesternumber - 1) * 6 + 1, 1);
                case "@lastsemester":
                    var semesternumber1 = (current.Month - 1) / 6 + 1;
                    current = new DateTime(current.Year, (semesternumber1 - 1) * 6 + 1, 1);
                    current = current.AddMonths(-6);
                    return current;

                case "@year":
                    return new DateTime(current.Year);
                case "@lastyear":
                    return new DateTime(current.Year - 1);
                default:
                    throw new InvalidOperationException("Período de referencia inválido");
            }
        }

        public List<string> ApplyDates(object dto)
        {
            var result = new List<string>();
            if (dto == null) throw new ArgumentNullException();
            var type = dto.GetType();
            var props = type.GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(DataTypeAttribute)));

            foreach (var propertyInfo in props)
            {
                var value = Convert.ToString(propertyInfo.GetValue(dto, null));
                if (!value.StartsWith("@")) continue;

                var parsedValue = ParseDate(value);
                var newValue = parsedValue.ToString(CultureInfo.InvariantCulture);
                propertyInfo.SetValue(dto, newValue, null);

                result.Add(string.Format("{0} = {1}", propertyInfo.Name, newValue));
            }
            return result;
        }
    }
}
