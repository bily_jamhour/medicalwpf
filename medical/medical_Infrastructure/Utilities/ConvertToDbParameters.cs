﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace medical_Infrastructure.Utilities
{
    public static class DbParameterTool
    {
        public static void AddParametersFromTypedDataRow(this DbCommand command, DataRow dataObject)
        {
            var type = dataObject.GetType();
            foreach (var field in type.GetProperties())
            {
                if (field.Name.StartsWith("md_"))
                {
                    if (command.CommandText.Contains(field.Name) == false)
                        continue;

                    var param = command.CreateParameter();
                    param.ParameterName = field.Name;

                    param.Value = dataObject.IsNull(field.Name) ? DBNull.Value : field.GetValue(dataObject, null);

                    if (field.PropertyType == typeof(string))
                    {
                        param.DbType = DbType.String;
                    }
                    else if (field.PropertyType == typeof(DateTime))
                    {
                        param.DbType = DbType.DateTime;
                    }
                    else if (field.PropertyType == typeof(int))
                    {
                        param.DbType = DbType.Int32;
                    }
                    else if (field.PropertyType == typeof(long))
                    {
                        param.DbType = DbType.Int64;
                    }
                    else if (field.PropertyType == typeof(double))
                    {
                        param.DbType = DbType.Double;
                    }
                    else if (field.PropertyType == typeof(bool))
                    {
                        param.DbType = DbType.Boolean;
                    }
                    else if (field.PropertyType == typeof(decimal))
                    {
                        param.DbType = DbType.Decimal;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine(field.PropertyType.ToString());
                    }
                    command.Parameters.Add(param);
                }
            }
        }

        public static void AddParametersFromUntypedDataRowUnsafe(this DbCommand command, DataRow dataObject)
        {
            var type = dataObject.GetType();
            var table = dataObject.Table;
            for (var i = 0; i < table.Columns.Count; i++)
            {
                var field = table.Columns[i];

                var param = command.CreateParameter();
                param.ParameterName = field.ColumnName;

                param.Value = dataObject.IsNull(i) ? DBNull.Value : dataObject[i];

                if (field.DataType == typeof(string))
                {
                    param.DbType = DbType.String;
                }
                else if (field.DataType == typeof(DateTime))
                {
                    param.DbType = DbType.DateTime;
                }
                else if (field.DataType == typeof(int))
                {
                    param.DbType = DbType.Int32;
                }
                else if (field.DataType == typeof(long))
                {
                    param.DbType = DbType.Int64;
                }
                else if (field.DataType == typeof(double))
                {
                    param.DbType = DbType.Double;
                }
                else if (field.DataType == typeof(bool))
                {
                    param.DbType = DbType.Boolean;
                }
                else if (field.DataType == typeof(decimal))
                {
                    param.DbType = DbType.Decimal;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine(field.DataType.ToString());
                }
                command.Parameters.Add(param);
            }
        }
    }
}
