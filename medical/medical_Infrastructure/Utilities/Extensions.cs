﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace medical_Infrastructure.Utilities
{
    public static class Extensions
    {
        public static bool IsSimpleType(this Type type)
        {
            return type.IsSubclassOf(typeof(System.ValueType)) || type == typeof(string);
        }

        public static void AppendTo(this string text, StringBuilder builder)
        {
            builder.Append(text);
        }
        public static void AppendLineTo(this string text, StringBuilder builder)
        {
            builder.AppendLine(text);
        }

        public static string CleanSqlString(this string sqlString)
        {
            return (sqlString ?? "").Replace("'", "''");
        }

        public static string JoinAsParameter(this IEnumerable<string> list)
        {
            if (list == null || !list.Any())
            {
                return "";
            }
            return string.Join(",", list.Select(x => "'" + x.CleanSqlString() + "'"));
        }

        public static string JoinAsParameterWithoutQuotes(this IEnumerable<string> list)
        {
            if (list == null || !list.Any())
            {
                return "";
            }
            return string.Join(",", list.Select(x => x.CleanSqlString()));
        }

        public static string JoinAsParameter(this IEnumerable<int> list)
        {
            if (list == null || !list.Any())
            {
                return "";
            }
            return string.Join(",", list);
        }

        public static string JoinAsParameter(this IEnumerable<long> list)
        {
            if (list == null || !list.Any())
            {
                return "";
            }
            return string.Join(",", list);
        }

        public static string JoinLines(this IEnumerable<string> list)
        {
            if (list == null || !list.Any())
            {
                return "";
            }
            return string.Join("\r\n", list);
        }

        public static bool HasValue(this string str)
        {
            return !string.IsNullOrWhiteSpace(str);
        }

        public static DbCommand AddParameter(this DbCommand command, string name, DbType type, object value)
        {
            var factory = DbProviderFactories.GetFactory(ConfigurationManager.AppSettings["dataBaseDriverName"]);
            var parameter = factory.CreateParameter();
            parameter.ParameterName = name;
            parameter.DbType = type;
            parameter.Value = value;
            command.Parameters.Add(parameter);
            return command;
        }

        public static DbCommand AddInParameter(this DbCommand command, string fieldName, DbType type, string valueList, bool condition = true)
        {
            return command.AddInParameter(fieldName, fieldName, type, valueList, condition);
        }

        public static DbCommand AddInParameter(this DbCommand command, string fieldName, string parameterName, DbType type, string valueList, bool condition = true)
        {
            if (!condition) return command;

            if (string.IsNullOrWhiteSpace(valueList)) return command;

            bool isAny;
            if (valueList.StartsWith("\""))
            {
                isAny = valueList.Contains("\",\"");
            }
            else
            {
                isAny = valueList.Contains(",");
            }

            if (isAny)
            {
                command.CommandText += " AND " + fieldName + " IN (" + valueList + ") ";
            }
            else
            {
                if (valueList.StartsWith("\""))
                {
                    valueList = valueList.Substring(1, valueList.Length - 2);
                }
                command.AddParameter(parameterName, type, valueList, " AND " + fieldName + " = @" + parameterName + " ");
            }
            return command;
        }

        public static DbCommand AddParameter(this DbCommand command, string name, DbType type, object value, string appendToQuery, bool onlyIfSatisfies = true)
        {
            if (!onlyIfSatisfies) return command;

            var condition = string.Format("{0}={0}", 1);

            if (command.CommandText.Contains(condition))
                command.CommandText = command.CommandText.Replace(condition, condition + " " + appendToQuery);
            else
                command.CommandText += " " + appendToQuery.Replace("@", ConfigurationManager.AppSettings["preparedStatementParamPrefix"]) + " ";
            command.AddParameter(name, type, value);
            return command;
        }



        public static DbCommand AddCondition(this DbCommand command, string appendToQuery, int conditionKey = 1)
        {
            var condition = string.Format("{0}={0}", conditionKey);
            if (command.CommandText.Contains(condition))
                command.CommandText = command.CommandText.Replace(condition, condition + " " + appendToQuery);
            else
                command.CommandText += appendToQuery;

            return command;
        }

        public static DataTable FillDataset(this MedicalConnection connection, string query, params DbParameter[] parameters)
        {
            return connection.FillDataset<DataTable>(query, parameters);
        }

        public static T FillDataset<T>(this MedicalConnection connection, string query, params DbParameter[] parameters)
            where T : DataTable, new()
        {
            var dataset = new T();
            var command = GetCommand(connection, query, parameters);
            var adapter = connection.Factory.CreateDataAdapter();
            adapter.SelectCommand = command;
            adapter.FillEx(dataset);

            adapter.Dispose();
            command.Dispose();
            return dataset;
        }

        public static T FillDataset<T>(this DbCommand command, params DbParameter[] parameters)
           where T : DataTable, new()
        {
            var dataTable = new T();
            return command.FillDataset(dataTable, parameters);
        }

        public static DataTable FillDataset(this DbCommand command, params DbParameter[] parameters)
        {
            var dataTable = new DataTable();
            return command.FillDataset(dataTable, parameters);
        }

        public static T FillDataset<T>(this DbCommand command, T reuseDatatable, params DbParameter[] parameters)
            where T : DataTable, new()
        {
            return command.FillDataset(reuseDatatable, false, parameters);
        }

        public static T FillDataset<T>(this DbCommand command, T reuseDatatable, bool preserveData, params DbParameter[] parameters)
            where T : DataTable, new()
        {
            if (reuseDatatable != null && !preserveData) reuseDatatable.Clear();
            var dataTable = reuseDatatable ?? new T();
            var factory = DbProviderFactories.GetFactory(ConfigurationManager.AppSettings["dataBaseDriverName"]);
            var adapter = factory.CreateDataAdapter();
            adapter.SelectCommand = command;
            adapter.FillEx(dataTable);
            adapter.Dispose();
            command.Dispose();
            return dataTable;
        }

        public static DbCommand GetCommand(this MedicalConnection connection, string query, params DbParameter[] parameters)
        {
            var command = connection.CreateCommand();
            command.CommandText = query.Replace("@", ConfigurationManager.AppSettings["preparedStatementParamPrefix"]);
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            }
            return command;
        }

        public static DbCommand AddParameters(this DbCommand command, object param)
        {
            if (param == null) return command;
            foreach (var property in param.GetType().GetProperties())
            {
                AddPropertyParameter(command, property.Name, property.GetValue(param, null), property.PropertyType, EnumParameterMode.Default);
            }
            return command;
        }

        public static DbCommand AddParameter<T, TR>(this DbCommand command, T row, Expression<Func<T, TR>> memberLamda, EnumParameterMode mode = EnumParameterMode.Default)
        {
            var memberSelectorExpression = memberLamda.Body as MemberExpression;
            if (row is DataRow)
            {
                var property = memberSelectorExpression.Member as PropertyInfo;
                if (property != null)
                {
                    AddRowColumnParameters(command, row as DataRow, property, mode);
                }
            }
            else
            {
                var value = memberLamda.Compile()(row);
                var type = typeof(TR);
                var name = memberSelectorExpression.Member.Name;
                AddPropertyParameter(command, name, value, type, mode);
            }
            return command;
        }

        public static Dictionary<string, DataColumn> GetDataColumns(this DataTable table)
        {
            var result = new Dictionary<string, DataColumn>();
            foreach (DataColumn column in table.Columns)
            {
                result[column.ColumnName] = column;
            }
            return result;
        }

        public static TR Get<T, TR>(this T row, Expression<Func<T, TR>> memberLamda, TR defaultValue = default(TR)) where T : DataRow
        {
            if (row == null) return defaultValue;

            var memberSelectorExpression = memberLamda.Body as MemberExpression;
            var property = memberSelectorExpression.Member as PropertyInfo;
            if (property == null) return defaultValue;

            var columnName = property.Name;
            var column = row.Table.Columns[columnName];

            var value = defaultValue;
            if (!row.IsNull(column))
                value = (TR)property.GetValue(row, null);
            return value;
        }

        public static bool HasValue<T, TR>(this T row, Expression<Func<T, TR>> memberLamda) where T : DataRow
        {
            if (row == null) return false;

            var memberSelectorExpression = memberLamda.Body as MemberExpression;
            var property = memberSelectorExpression.Member as PropertyInfo;
            if (property == null) return false;

            var columnName = property.Name;
            var column = row.Table.Columns[columnName];

            object value = null;
            if (!row.IsNull(column))
                value = property.GetValue(row, null);
            return value != null;
        }
        public static bool IsEmpty<T, TR>(this T row, Expression<Func<T, TR>> memberLamda) where T : DataRow
        {
            if (row == null) return false;

            var memberSelectorExpression = memberLamda.Body as MemberExpression;
            var property = memberSelectorExpression.Member as PropertyInfo;
            if (property == null) return false;

            var columnName = property.Name;
            var column = row.Table.Columns[columnName];

            object value = null;
            if (!row.IsNull(column))
                value = property.GetValue(row, null);

            if (value is string)
            {
                return string.IsNullOrWhiteSpace(value as string);
            }

            return value != null;
        }



        public static DbCommand AddParameter(this DbCommand command, object parameter)
        {
            if (parameter == null) return command;
            foreach (var property in parameter.GetType().GetProperties())
            {
                var name = property.Name;
                var value = property.GetValue(parameter, null);
                AddPropertyParameter(command, name, value, property.PropertyType, EnumParameterMode.Default);
            }
            return command;
        }

        private static void AddPropertyParameter(DbCommand command, string name, object value, Type type, EnumParameterMode converToString)
        {


            var param = command.CreateParameter();
            if (converToString == EnumParameterMode.ConvertToString)
            {
                if (value is IEnumerable)
                {
                    value = (value as IEnumerable).Cast<object>().Select(Convert.ToString).JoinAsParameter();
                }
                else
                {
                    value = Convert.ToString(value);
                }
                param.DbType = DbType.String;
            }
            else
            {
                if (converToString == EnumParameterMode.ConvertToString) value = Convert.ToString(value);
                //type = typeof(string);
                if (type == typeof(string))
                {
                    param.DbType = DbType.String;
                }
                else if (type == typeof(DateTime))
                {
                    param.DbType = DbType.DateTime;
                }
                else if (type == typeof(int))
                {
                    param.DbType = DbType.Int32;
                }
                else if (type == typeof(long))
                {
                    param.DbType = DbType.Int64;
                }
                else if (type == typeof(double))
                {
                    param.DbType = DbType.Double;
                }
                else if (type == typeof(bool))
                {
                    param.DbType = DbType.Boolean;
                }
                else if (type == typeof(decimal))
                {
                    param.DbType = DbType.Decimal;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine(type.ToString());
                    param.DbType = DbType.String;
                    if (value != null && !type.IsSimpleType())
                    {
                        value = value.XmlSerialize();
                    }
                }
            }
            param.ParameterName = name;
            param.Value = value ?? DBNull.Value;
            command.Parameters.Add(param);
        }

        public static DbType MapType(this Type type)
        {

            if (type == typeof(string))
            {
                return DbType.String;
            }
            else if (type == typeof(DateTime))
            {
                return DbType.DateTime;
            }
            else if (type == typeof(int))
            {
                return DbType.Int32;
            }
            else if (type == typeof(long))
            {
                return DbType.Int64;
            }
            else if (type == typeof(double))
            {
                return DbType.Double;
            }
            else if (type == typeof(bool))
            {
                return DbType.Boolean;
            }
            else if (type == typeof(decimal))
            {
                return DbType.Decimal;
            }
            throw new InvalidOperationException("Unsupported type mapping");
        }

        public static void AddParameter(this DbCommand command, DataRow row, string columnName, string paramName = null, EnumParameterMode converToString = EnumParameterMode.Default)
        {
            var column = row.Table.Columns[columnName];
            var type = column.DataType;

            object value = null;
            if (!row.IsNull(column))
                value = row[columnName];

            var param = command.CreateParameter();
            if (converToString == EnumParameterMode.ConvertToString)
            {
                value = Convert.ToString(value);
                param.DbType = DbType.String;
            }
            else
            {
                if (type == typeof(string))
                {
                    param.DbType = DbType.String;
                }
                else if (type == typeof(DateTime))
                {
                    param.DbType = DbType.DateTime;
                }
                else if (type == typeof(int))
                {
                    param.DbType = DbType.Int32;
                }
                else if (type == typeof(long))
                {
                    param.DbType = DbType.Int64;
                }
                else if (type == typeof(double))
                {
                    param.DbType = DbType.Double;
                }
                else if (type == typeof(bool))
                {
                    param.DbType = DbType.Boolean;
                }
                else if (type == typeof(decimal))
                {
                    param.DbType = DbType.Decimal;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine(type.ToString());
                }
            }

            param.ParameterName = paramName ?? column.ColumnName;
            param.Value = row.IsNull(column) ? DBNull.Value : value;
            command.Parameters.Add(param);
        }

        public static string AddParameterValue(DataRow row, string columnName, string paramName = null, EnumParameterMode converToString = EnumParameterMode.Default)
        {
            var column = row.Table.Columns[columnName];
            var type = column.DataType;

            object value = null;
            if (!row.IsNull(column))
                value = row[columnName];

            if (converToString == EnumParameterMode.ConvertToString)
            {
                return Convert.ToString(value, CultureInfo.InvariantCulture);
            }
            else
            {
                if (type == typeof(string))
                {
                    return "'" + value.ToString().Replace("'", "''") + "'";
                }
                else if (type == typeof(DateTime))
                {
                    return string.Format("DATE '{0}'", Convert.ToDateTime(value));
                }
                else if (type == typeof(int))
                {
                    return Convert.ToString(value, CultureInfo.InvariantCulture);
                }
                else if (type == typeof(long))
                {
                    return Convert.ToString(value, CultureInfo.InvariantCulture);
                }
                else if (type == typeof(double))
                {
                    return Convert.ToString(value, CultureInfo.InvariantCulture);
                }
                else if (type == typeof(bool))
                {
                    return Convert.ToString(value, CultureInfo.InvariantCulture);
                }
                else if (type == typeof(decimal))
                {
                    return Convert.ToString(value, CultureInfo.InvariantCulture);
                }
                else
                {
                    throw new NotImplementedException("No hay soporte para el tipo " + type);
                }
            }
        }

        private static void AddRowColumnParameters<T>(DbCommand command, T row, PropertyInfo property, EnumParameterMode converToString) where T : DataRow
        {
            var columnName = property.Name;
            var column = row.Table.Columns[columnName];
            var type = column.DataType;

            object value = null;
            if (!row.IsNull(column))
                value = property.GetValue(row, null);

            var param = command.CreateParameter();
            if (converToString == EnumParameterMode.ConvertToString)
            {
                value = Convert.ToString(value);
                param.DbType = DbType.String;
            }
            else
            {
                if (type == typeof(string))
                {
                    param.DbType = DbType.String;
                }
                else if (type == typeof(DateTime))
                {
                    param.DbType = DbType.DateTime;
                }
                else if (type == typeof(int))
                {
                    param.DbType = DbType.Int32;
                }
                else if (type == typeof(long))
                {
                    param.DbType = DbType.Int64;
                }
                else if (type == typeof(double))
                {
                    param.DbType = DbType.Double;
                }
                else if (type == typeof(bool))
                {
                    param.DbType = DbType.Boolean;
                }
                else if (type == typeof(decimal))
                {
                    param.DbType = DbType.Decimal;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine(type.ToString());
                }
            }

            param.ParameterName = column.ColumnName;
            param.Value = row.IsNull(column) ? DBNull.Value : value;
            command.Parameters.Add(param);
        }

        public static T ExecuteScalar<T>(this MedicalConnection connection, string query, params DbParameter[] parameters)
        {
            var command = GetCommand(connection, query, parameters);
            return (T)command.ExecuteScalar();
        }

        public static int ExecuteNonQuery(this MedicalConnection connection, string query, params DbParameter[] parameters)
        {
            var command = GetCommand(connection, query, parameters);
            return command.ExecuteNonQueryEx();
        }


        public static int ExecuteNonQueryEx(this MedicalConnection connection, string query, params DbParameter[] parameters)
        {
            var command = GetCommand(connection, query, parameters);
            return command.ExecuteNonQueryEx();
        }


        public static TValue TryGetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key) where TValue : class
        {
            TValue value;
            if (key == null) return default(TValue);
            dictionary.TryGetValue(key, out value);
            return value;
        }

        public static TValue TryGet<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            TValue value;
            if (key == null) return defaultValue;
            if (dictionary.TryGetValue(key, out value))
                return value;

            return defaultValue;
        }

        public static Dictionary<TY, List<TX>> ToMultiBag<TX, TY>(this IEnumerable<TX> list, Func<TX, TY> func)
        {
            var result = new Dictionary<TY, List<TX>>();
            foreach (var item in list)
            {
                var key = func(item);
                var bag = result.TryGetOrDefault(key);
                if (bag == null)
                {
                    bag = new List<TX>();
                    result[key] = bag;
                }
                bag.Add(item);
            }
            return result;
        }
        public static Dictionary<TY, List<TZ>> ToMultiBag<TX, TY, TZ>(this IEnumerable<TX> list, Func<TX, TY> func, Func<TX, TZ> funcVal)
        {
            var result = new Dictionary<TY, List<TZ>>();
            foreach (var item in list)
            {
                var key = func(item);
                var bag = result.TryGetOrDefault(key);
                if (bag == null)
                {
                    bag = new List<TZ>();
                    result[key] = bag;
                }
                bag.Add(funcVal(item));
            }
            return result;
        }
        public static Dictionary<TY, TX> ToDictionarySafe<TX, TY>(this IEnumerable<TX> list, Func<TX, TY> func)
        {
            var result = new Dictionary<TY, TX>();
            foreach (var item in list)
            {
                var key = func(item);
                result[key] = item;
            }
            return result;
        }

        public static Dictionary<TY, TZ> ToDictionarySafe<TX, TY, TZ>(this IEnumerable<TX> list, Func<TX, TY> funcKey, Func<TX, TZ> funcVal)
        {
            var result = new Dictionary<TY, TZ>();
            foreach (var item in list)
            {
                var key = funcKey(item);
                result[key] = funcVal(item);
            }
            return result;
        }

        public static void SafeAdd<TX, TY>(this IEnumerable<TX> list, IDictionary<TY, TX> dictionary, Func<TX, TY> func)
        {
            dictionary.SafeAdd(list, func);
        }

        public static void SafeAdd<TX, TY>(this IDictionary<TY, TX> dictionary, IEnumerable<TX> list, Func<TX, TY> func)
        {
            foreach (var item in list)
            {
                var key = func(item);
                dictionary[key] = item;
            }
        }

        public static DateTime SetDaySafe(this DateTime date, int dayOfMonth)
        {
            if (dayOfMonth > 31) dayOfMonth = 31;
            var newDate = new DateTime(date.Year, date.Month, 1);
            var dateWithDay = newDate.AddDays(dayOfMonth);
            while (dateWithDay.Month != newDate.Month) dateWithDay = dateWithDay.AddDays(-1);
            return dateWithDay;
        }

        /// <summary>
        /// Soporte para cadenas sigular/plural
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="unaOcurrencia">Cadena a mostrar cuando solo hay un dato</param>
        /// <param name="ceroOMultipleOcurrencias">Cadena a mostrar cuando hay más de un dato. Usar {0} para posicionar la cuenta</param>
        /// <returns></returns>
        public static string Pluralize(this int cantidad, string unaOcurrencia, string ceroOMultipleOcurrencias)
        {
            if (cantidad == 1)
                return unaOcurrencia;

            return string.Format(ceroOMultipleOcurrencias, cantidad);
        }

        /// <summary>
        /// Soporte para cadenas sigular/plural
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="unaOcurrencia">Cadena a mostrar cuando solo hay un dato</param>
        /// <param name="ceroOMultipleOcurrencias">Cadena a mostrar cuando hay más de un dato. Usar {0} para posicionar la cuenta</param>
        /// <returns></returns>
        public static string Pluralize(this int cantidad, string unaOcurrencia, string ceroOMultipleOcurrencias, string ceroOcurrencias)
        {
            switch (cantidad)
            {
                case 0:
                    return ceroOcurrencias;
                case 1:
                    return unaOcurrencia;
                default:
                    return string.Format(ceroOMultipleOcurrencias, cantidad);
            }
        }
    }

    public enum EnumParameterMode
    {
        Default = 0,
        ConvertToString = 1
    }

}
