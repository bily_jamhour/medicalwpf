﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace medical_Infrastructure.Utilities
{
    public static class XmlExtensions
    {
        public static string XmlSerialize(this object obj, params Type[] extraTypes)
        {
            if (obj == null)
                return string.Empty;

            var namespaces = new XmlQualifiedName[1];
            namespaces[0] = new XmlQualifiedName("", "");

            var emptyNs = new XmlSerializerNamespaces(namespaces);
            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = true
            };

            var serializer = new XmlSerializer(obj.GetType(), null, extraTypes, null, "");

            using (var stream = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    serializer.Serialize(writer, obj, emptyNs);
                }

                //return stream.ToArray();
                stream.Flush();
                stream.Position = 0;

                using (TextReader reader = new StreamReader(stream))
                {
                    var txt = reader.ReadToEnd();
                    return txt;
                }
            }
        }

        public static T XmlDeserialize<T>(this string xml, params Type[] extraTypes)
        {
            if (string.IsNullOrWhiteSpace(xml))
                return default(T);

            var settings = new XmlReaderSettings()
            {
                ConformanceLevel = ConformanceLevel.Fragment
            };

            var str = new StringBuilder(xml);
            if (!xml.StartsWith("<?xml "))
                str.Insert(0, "<?xml version=\"1.0\"?> ");

            var serializer = new XmlSerializer(typeof(T), null, extraTypes, null, "");

            using (var stream = new MemoryStream())
            {
                var bytes = Encoding.UTF8.GetBytes(xml);
                stream.Write(bytes, 0, bytes.Length);
                stream.Position = 0;
                using (var reader = XmlReader.Create(stream, settings))
                {
                    var obj = serializer.Deserialize(reader);
                    return (T)obj;
                }
            }
        }
    }
}
