﻿namespace medicalWPF.MD_Forms.Reports
{
    partial class FrmExportSCAICC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn57 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn58 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn59 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn60 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn61 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn62 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn63 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn64 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn65 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn66 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn67 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn68 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn69 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn70 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn71 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn72 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn73 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn74 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn75 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn76 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn77 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn78 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn79 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn80 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn81 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn82 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn83 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn84 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn85 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn86 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn87 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn88 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn89 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn90 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn91 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn92 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn93 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn94 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn95 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn96 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn97 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn98 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn99 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn100 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn101 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn102 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn103 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn104 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn105 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn106 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn107 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn108 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn109 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn41 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn42 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn43 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn44 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn45 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn46 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn47 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn48 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn49 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn50 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn51 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn52 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn53 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn54 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn55 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn56 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.lblPaciente = new Telerik.WinControls.UI.RadLabel();
            this.ddPacientes = new Telerik.WinControls.UI.RadDropDownList();
            this.btnClear = new Telerik.WinControls.UI.RadButton();
            this.btnFilter = new Telerik.WinControls.UI.RadButton();
            this.divBotonera = new Telerik.WinControls.UI.RadPanel();
            this.btnExit = new Telerik.WinControls.UI.RadButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pnlTitleSCA = new Telerik.WinControls.UI.RadPanel();
            this.gridICC = new Telerik.WinControls.UI.RadGridView();
            this.pnlTitleICC = new Telerik.WinControls.UI.RadPanel();
            this.bbddmedicalDataSet = new medicalWPF.bbddmedicalDataSet();
            this.mdICCReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridSCA = new Telerik.WinControls.UI.RadGridView();
            this.layoutControl2 = new MD_Controls.Layout.LayoutControl();
            this.layoutControl1 = new MD_Controls.Layout.LayoutControl();
            this.ddSexo = new MD_Controls.DropDownMedical();
            this.inputFechaAlta = new MD_Controls.InputDate();
            this.inputFechaIngreso = new MD_Controls.InputDate();
            this.inputFechaNacimiento = new MD_Controls.InputDate();
            this.inputNHC = new MD_Controls.InputNumeric();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.divBotonera)).BeginInit();
            this.divBotonera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTitleSCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridICC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridICC.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTitleICC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bbddmedicalDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mdICCReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSCA.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel1.EnableAnimation = false;
            this.radCollapsiblePanel1.HeaderText = "Filtros";
            this.radCollapsiblePanel1.HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Right;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 992, 200);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.ddSexo);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.inputFechaAlta);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.inputFechaIngreso);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.inputFechaNacimiento);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.lblPaciente);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.ddPacientes);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.btnClear);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.btnFilter);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.inputNHC);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(930, 104);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(932, 132);
            this.radCollapsiblePanel1.TabIndex = 5;
            this.radCollapsiblePanel1.Text = "radCollapsiblePanel1";
            // 
            // lblPaciente
            // 
            this.lblPaciente.Location = new System.Drawing.Point(20, 18);
            this.lblPaciente.Name = "lblPaciente";
            this.lblPaciente.Size = new System.Drawing.Size(55, 18);
            this.lblPaciente.TabIndex = 18;
            this.lblPaciente.Text = "radLabel1";
            // 
            // ddPacientes
            // 
            this.ddPacientes.DisplayMember = "md_nombre";
            this.ddPacientes.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddPacientes.Location = new System.Drawing.Point(186, 16);
            this.ddPacientes.Name = "ddPacientes";
            this.ddPacientes.Size = new System.Drawing.Size(99, 20);
            this.ddPacientes.TabIndex = 17;
            this.ddPacientes.Text = "radDropDownList1";
            this.ddPacientes.ValueMember = "md_record_id";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Image = global::medicalWPF.Properties.Resources.Clear;
            this.btnClear.Location = new System.Drawing.Point(818, 42);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(110, 33);
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "[btnClear]";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnFilter
            // 
            this.btnFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilter.Image = global::medicalWPF.Properties.Resources.Find1;
            this.btnFilter.Location = new System.Drawing.Point(818, 3);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(110, 33);
            this.btnFilter.TabIndex = 15;
            this.btnFilter.Text = "[btnFilter]";
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // divBotonera
            // 
            this.divBotonera.Controls.Add(this.btnExit);
            this.divBotonera.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.divBotonera.Location = new System.Drawing.Point(0, 715);
            this.divBotonera.Name = "divBotonera";
            this.divBotonera.Size = new System.Drawing.Size(932, 55);
            this.divBotonera.TabIndex = 8;
            ((Telerik.WinControls.UI.RadPanelElement)(this.divBotonera.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.divBotonera.GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Image = global::medicalWPF.Properties.Resources.Exit1;
            this.btnExit.Location = new System.Drawing.Point(819, 10);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 33);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "[btnExit]";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click_1);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 132);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridSCA);
            this.splitContainer1.Panel1.Controls.Add(this.pnlTitleSCA);
            this.splitContainer1.Panel1.Controls.Add(this.layoutControl2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gridICC);
            this.splitContainer1.Panel2.Controls.Add(this.pnlTitleICC);
            this.splitContainer1.Panel2.Controls.Add(this.layoutControl1);
            this.splitContainer1.Size = new System.Drawing.Size(932, 583);
            this.splitContainer1.SplitterDistance = 279;
            this.splitContainer1.TabIndex = 9;
            // 
            // pnlTitleSCA
            // 
            this.pnlTitleSCA.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleSCA.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlTitleSCA.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.pnlTitleSCA.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleSCA.Name = "pnlTitleSCA";
            this.pnlTitleSCA.Size = new System.Drawing.Size(279, 32);
            this.pnlTitleSCA.TabIndex = 6;
            this.pnlTitleSCA.Text = "Sindrome coronario aguda";
            ((Telerik.WinControls.UI.RadPanelElement)(this.pnlTitleSCA.GetChildAt(0))).Text = "Sindrome coronario aguda";
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pnlTitleSCA.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // gridICC
            // 
            this.gridICC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridICC.Location = new System.Drawing.Point(0, 32);
            // 
            // 
            // 
            this.gridICC.MasterTemplate.AllowAddNewRow = false;
            this.gridICC.MasterTemplate.AllowDeleteRow = false;
            this.gridICC.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn57.FieldName = "md_nombre";
            gridViewTextBoxColumn57.HeaderText = "md_nombre";
            gridViewTextBoxColumn57.Name = "md_nombre";
            gridViewTextBoxColumn58.DataType = typeof(int);
            gridViewTextBoxColumn58.FieldName = "md_nhc";
            gridViewTextBoxColumn58.HeaderText = "md_nhc";
            gridViewTextBoxColumn58.Name = "md_nhc";
            gridViewTextBoxColumn59.DataType = typeof(int);
            gridViewTextBoxColumn59.FieldName = "md_sexo";
            gridViewTextBoxColumn59.HeaderText = "column3";
            gridViewTextBoxColumn59.Name = "md_sexo";
            gridViewTextBoxColumn60.FieldName = "md_fechanacimiento";
            gridViewTextBoxColumn60.HeaderText = "column4";
            gridViewTextBoxColumn60.Name = "md_fechanacimiento";
            gridViewTextBoxColumn61.FieldName = "md_fechaingreso";
            gridViewTextBoxColumn61.HeaderText = "column5";
            gridViewTextBoxColumn61.Name = "md_fechaingreso";
            gridViewTextBoxColumn62.FieldName = "md_fechaalta";
            gridViewTextBoxColumn62.HeaderText = "column6";
            gridViewTextBoxColumn62.Name = "md_fechaalta";
            gridViewTextBoxColumn63.DataType = typeof(int);
            gridViewTextBoxColumn63.FieldName = "md_hta";
            gridViewTextBoxColumn63.HeaderText = "md_hta";
            gridViewTextBoxColumn63.Name = "md_hta";
            gridViewTextBoxColumn64.DataType = typeof(int);
            gridViewTextBoxColumn64.FieldName = "md_diabetesmellitus";
            gridViewTextBoxColumn64.HeaderText = "column1";
            gridViewTextBoxColumn64.Name = "md_diabetesmellitus";
            gridViewTextBoxColumn65.DataType = typeof(int);
            gridViewTextBoxColumn65.FieldName = "md_dislipemia";
            gridViewTextBoxColumn65.HeaderText = "column1";
            gridViewTextBoxColumn65.Name = "md_dislipemia";
            gridViewTextBoxColumn66.DataType = typeof(int);
            gridViewTextBoxColumn66.FieldName = "md_cardiopatiaisquemica";
            gridViewTextBoxColumn66.HeaderText = "column1";
            gridViewTextBoxColumn66.Name = "md_cardiopatiaisquemica";
            gridViewTextBoxColumn67.DataType = typeof(int);
            gridViewTextBoxColumn67.FieldName = "md_historialsincopes";
            gridViewTextBoxColumn67.HeaderText = "column1";
            gridViewTextBoxColumn67.Name = "md_historialsincopes";
            gridViewTextBoxColumn68.DataType = typeof(int);
            gridViewTextBoxColumn68.FieldName = "md_ingresosicc";
            gridViewTextBoxColumn68.HeaderText = "column1";
            gridViewTextBoxColumn68.Name = "md_ingresosicc";
            gridViewTextBoxColumn69.DataType = typeof(int);
            gridViewTextBoxColumn69.FieldName = "md_antecedentesfa";
            gridViewTextBoxColumn69.HeaderText = "column1";
            gridViewTextBoxColumn69.Name = "md_antecedentesfa";
            gridViewTextBoxColumn70.DataType = typeof(int);
            gridViewTextBoxColumn70.FieldName = "md_anticoagulacioningreso";
            gridViewTextBoxColumn70.HeaderText = "column1";
            gridViewTextBoxColumn70.Name = "md_anticoagulacioningreso";
            gridViewTextBoxColumn71.DataType = typeof(int);
            gridViewTextBoxColumn71.FieldName = "md_ecocardiograma";
            gridViewTextBoxColumn71.HeaderText = "column1";
            gridViewTextBoxColumn71.Name = "md_ecocardiograma";
            gridViewTextBoxColumn72.DataType = typeof(int);
            gridViewTextBoxColumn72.FieldName = "md_clasefuncionalbasal";
            gridViewTextBoxColumn72.HeaderText = "column1";
            gridViewTextBoxColumn72.Name = "md_clasefuncionalbasal";
            gridViewTextBoxColumn73.DataType = typeof(int);
            gridViewTextBoxColumn73.FieldName = "md_nyha";
            gridViewTextBoxColumn73.HeaderText = "column1";
            gridViewTextBoxColumn73.Name = "md_nyha";
            gridViewTextBoxColumn74.DataType = typeof(uint);
            gridViewTextBoxColumn74.FieldName = "md_sumframinghanmayor";
            gridViewTextBoxColumn74.HeaderText = "column1";
            gridViewTextBoxColumn74.Name = "md_sumframinghanmayor";
            gridViewTextBoxColumn75.DataType = typeof(int);
            gridViewTextBoxColumn75.FieldName = "md_sumframinghanmenor";
            gridViewTextBoxColumn75.HeaderText = "column1";
            gridViewTextBoxColumn75.Name = "md_sumframinghanmenor";
            gridViewTextBoxColumn76.DataType = typeof(int);
            gridViewTextBoxColumn76.FieldName = "md_fcingreso";
            gridViewTextBoxColumn76.HeaderText = "column1";
            gridViewTextBoxColumn76.Name = "md_fcingreso";
            gridViewTextBoxColumn77.DataType = typeof(int);
            gridViewTextBoxColumn77.FieldName = "md_taingreso";
            gridViewTextBoxColumn77.HeaderText = "column1";
            gridViewTextBoxColumn77.Name = "md_taingreso";
            gridViewTextBoxColumn78.DataType = typeof(int);
            gridViewTextBoxColumn78.FieldName = "md_ecocardiogramaduranteingreso";
            gridViewTextBoxColumn78.HeaderText = "column1";
            gridViewTextBoxColumn78.Name = "md_ecocardiogramaduranteingreso";
            gridViewTextBoxColumn79.DataType = typeof(int);
            gridViewTextBoxColumn79.Expression = "";
            gridViewTextBoxColumn79.FieldName = "md_fevi";
            gridViewTextBoxColumn79.HeaderText = "column1";
            gridViewTextBoxColumn79.Name = "md_fevi";
            gridViewTextBoxColumn80.DataType = typeof(int);
            gridViewTextBoxColumn80.FieldName = "md_valvulopatia";
            gridViewTextBoxColumn80.HeaderText = "column1";
            gridViewTextBoxColumn80.Name = "md_valvulopatia";
            gridViewTextBoxColumn81.DataType = typeof(int);
            gridViewTextBoxColumn81.FieldName = "md_htap";
            gridViewTextBoxColumn81.HeaderText = "column1";
            gridViewTextBoxColumn81.Name = "md_htap";
            gridViewTextBoxColumn82.DataType = typeof(int);
            gridViewTextBoxColumn82.FieldName = "md_fccontrolada";
            gridViewTextBoxColumn82.HeaderText = "column1";
            gridViewTextBoxColumn82.Name = "md_fccontrolada";
            gridViewTextBoxColumn83.DataType = typeof(int);
            gridViewTextBoxColumn83.FieldName = "md_ritmoalta";
            gridViewTextBoxColumn83.HeaderText = "column1";
            gridViewTextBoxColumn83.Name = "md_ritmoalta";
            gridViewTextBoxColumn84.FieldName = "md_creatininaingreso";
            gridViewTextBoxColumn84.HeaderText = "column2";
            gridViewTextBoxColumn84.Name = "md_creatininaingreso";
            gridViewTextBoxColumn85.FieldName = "md_creatininaalta";
            gridViewTextBoxColumn85.HeaderText = "column2";
            gridViewTextBoxColumn85.Name = "md_creatininaalta";
            gridViewTextBoxColumn86.DataType = typeof(int);
            gridViewTextBoxColumn86.FieldName = "md_betabloqueantespre";
            gridViewTextBoxColumn86.HeaderText = "column2";
            gridViewTextBoxColumn86.Name = "md_betabloqueantespre";
            gridViewTextBoxColumn87.DataType = typeof(int);
            gridViewTextBoxColumn87.FieldName = "md_betabloqueantespost";
            gridViewTextBoxColumn87.HeaderText = "column1";
            gridViewTextBoxColumn87.Name = "md_betabloqueantespost";
            gridViewTextBoxColumn88.DataType = typeof(int);
            gridViewTextBoxColumn88.FieldName = "md_iecapre";
            gridViewTextBoxColumn88.HeaderText = "column1";
            gridViewTextBoxColumn88.Name = "md_iecapre";
            gridViewTextBoxColumn89.DataType = typeof(int);
            gridViewTextBoxColumn89.Expression = "";
            gridViewTextBoxColumn89.FieldName = "md_iecapost";
            gridViewTextBoxColumn89.HeaderText = "column1";
            gridViewTextBoxColumn89.Name = "md_iecapost";
            gridViewTextBoxColumn90.DataType = typeof(int);
            gridViewTextBoxColumn90.FieldName = "md_amiodaronapre";
            gridViewTextBoxColumn90.HeaderText = "column1";
            gridViewTextBoxColumn90.Name = "md_amiodaronapre";
            gridViewTextBoxColumn91.DataType = typeof(int);
            gridViewTextBoxColumn91.FieldName = "md_amiodaronapost";
            gridViewTextBoxColumn91.HeaderText = "column1";
            gridViewTextBoxColumn91.Name = "md_amiodaronapost";
            gridViewTextBoxColumn92.DataType = typeof(int);
            gridViewTextBoxColumn92.FieldName = "md_digoxinapre";
            gridViewTextBoxColumn92.HeaderText = "column1";
            gridViewTextBoxColumn92.Name = "md_digoxinapre";
            gridViewTextBoxColumn93.DataType = typeof(int);
            gridViewTextBoxColumn93.FieldName = "md_digoxinapost";
            gridViewTextBoxColumn93.HeaderText = "column1";
            gridViewTextBoxColumn93.Name = "md_digoxinapost";
            gridViewTextBoxColumn94.DataType = typeof(int);
            gridViewTextBoxColumn94.FieldName = "md_antialdosteronicospre";
            gridViewTextBoxColumn94.HeaderText = "column1";
            gridViewTextBoxColumn94.Name = "md_antialdosteronicospre";
            gridViewTextBoxColumn95.DataType = typeof(int);
            gridViewTextBoxColumn95.FieldName = "md_antialdosteronicospost";
            gridViewTextBoxColumn95.HeaderText = "column1";
            gridViewTextBoxColumn95.Name = "md_antialdosteronicospost";
            gridViewTextBoxColumn96.DataType = typeof(int);
            gridViewTextBoxColumn96.FieldName = "md_mapa";
            gridViewTextBoxColumn96.HeaderText = "column1";
            gridViewTextBoxColumn96.Name = "md_mapa";
            gridViewTextBoxColumn97.DataType = typeof(int);
            gridViewTextBoxColumn97.FieldName = "md_registro";
            gridViewTextBoxColumn97.HeaderText = "column1";
            gridViewTextBoxColumn97.Name = "md_registro";
            gridViewTextBoxColumn98.FieldName = "md_picotensional_diurno";
            gridViewTextBoxColumn98.HeaderText = "column1";
            gridViewTextBoxColumn98.Name = "md_picotensional_diurno";
            gridViewTextBoxColumn99.FieldName = "md_horapicotensional_diurno";
            gridViewTextBoxColumn99.HeaderText = "column1";
            gridViewTextBoxColumn99.Name = "md_horapicotensional_diurno";
            gridViewTextBoxColumn100.FieldName = "md_mediatensional_diurno";
            gridViewTextBoxColumn100.HeaderText = "column1";
            gridViewTextBoxColumn100.Name = "md_mediatensional_diurno";
            gridViewTextBoxColumn101.FieldName = "md_picotensional_nocturno";
            gridViewTextBoxColumn101.HeaderText = "column1";
            gridViewTextBoxColumn101.Name = "md_picotensional_nocturno";
            gridViewTextBoxColumn102.FieldName = "md_horapicotensional_nocturno";
            gridViewTextBoxColumn102.HeaderText = "column1";
            gridViewTextBoxColumn102.Name = "md_horapicotensional_nocturno";
            gridViewTextBoxColumn103.FieldName = "md_mediatensional_nocturno";
            gridViewTextBoxColumn103.HeaderText = "column1";
            gridViewTextBoxColumn103.Name = "md_mediatensional_nocturno";
            gridViewTextBoxColumn104.FieldName = "md_mediafcduranteregistro";
            gridViewTextBoxColumn104.HeaderText = "column1";
            gridViewTextBoxColumn104.Name = "md_mediafcduranteregistro";
            gridViewTextBoxColumn105.DataType = typeof(int);
            gridViewTextBoxColumn105.FieldName = "md_patron";
            gridViewTextBoxColumn105.HeaderText = "column1";
            gridViewTextBoxColumn105.Name = "md_patron";
            gridViewTextBoxColumn106.DataType = typeof(int);
            gridViewTextBoxColumn106.FieldName = "md_ingresos";
            gridViewTextBoxColumn106.HeaderText = "column1";
            gridViewTextBoxColumn106.Name = "md_ingresos";
            gridViewTextBoxColumn107.DataType = typeof(int);
            gridViewTextBoxColumn107.FieldName = "md_status";
            gridViewTextBoxColumn107.HeaderText = "column1";
            gridViewTextBoxColumn107.Name = "md_status";
            gridViewTextBoxColumn108.DataType = typeof(int);
            gridViewTextBoxColumn108.FieldName = "md_exitus";
            gridViewTextBoxColumn108.HeaderText = "column1";
            gridViewTextBoxColumn108.Name = "md_exitus";
            gridViewTextBoxColumn109.FieldName = "md_exitusdesc";
            gridViewTextBoxColumn109.HeaderText = "column1";
            gridViewTextBoxColumn109.Name = "md_exitusdesc";
            this.gridICC.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn57,
            gridViewTextBoxColumn58,
            gridViewTextBoxColumn59,
            gridViewTextBoxColumn60,
            gridViewTextBoxColumn61,
            gridViewTextBoxColumn62,
            gridViewTextBoxColumn63,
            gridViewTextBoxColumn64,
            gridViewTextBoxColumn65,
            gridViewTextBoxColumn66,
            gridViewTextBoxColumn67,
            gridViewTextBoxColumn68,
            gridViewTextBoxColumn69,
            gridViewTextBoxColumn70,
            gridViewTextBoxColumn71,
            gridViewTextBoxColumn72,
            gridViewTextBoxColumn73,
            gridViewTextBoxColumn74,
            gridViewTextBoxColumn75,
            gridViewTextBoxColumn76,
            gridViewTextBoxColumn77,
            gridViewTextBoxColumn78,
            gridViewTextBoxColumn79,
            gridViewTextBoxColumn80,
            gridViewTextBoxColumn81,
            gridViewTextBoxColumn82,
            gridViewTextBoxColumn83,
            gridViewTextBoxColumn84,
            gridViewTextBoxColumn85,
            gridViewTextBoxColumn86,
            gridViewTextBoxColumn87,
            gridViewTextBoxColumn88,
            gridViewTextBoxColumn89,
            gridViewTextBoxColumn90,
            gridViewTextBoxColumn91,
            gridViewTextBoxColumn92,
            gridViewTextBoxColumn93,
            gridViewTextBoxColumn94,
            gridViewTextBoxColumn95,
            gridViewTextBoxColumn96,
            gridViewTextBoxColumn97,
            gridViewTextBoxColumn98,
            gridViewTextBoxColumn99,
            gridViewTextBoxColumn100,
            gridViewTextBoxColumn101,
            gridViewTextBoxColumn102,
            gridViewTextBoxColumn103,
            gridViewTextBoxColumn104,
            gridViewTextBoxColumn105,
            gridViewTextBoxColumn106,
            gridViewTextBoxColumn107,
            gridViewTextBoxColumn108,
            gridViewTextBoxColumn109});
            this.gridICC.MasterTemplate.ShowRowHeaderColumn = false;
            this.gridICC.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridICC.Name = "gridICC";
            this.gridICC.Size = new System.Drawing.Size(649, 519);
            this.gridICC.TabIndex = 8;
            this.gridICC.Text = "radGridView1";
            this.gridICC.ThemeName = "Office2010Blue";
            this.gridICC.DataBindingComplete += new Telerik.WinControls.UI.GridViewBindingCompleteEventHandler(this.gridICC_DataBindingComplete);
            // 
            // pnlTitleICC
            // 
            this.pnlTitleICC.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleICC.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlTitleICC.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.pnlTitleICC.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleICC.Name = "pnlTitleICC";
            this.pnlTitleICC.Size = new System.Drawing.Size(649, 32);
            this.pnlTitleICC.TabIndex = 7;
            this.pnlTitleICC.Text = "Insuficiencia cardiaca congestiva";
            ((Telerik.WinControls.UI.RadPanelElement)(this.pnlTitleICC.GetChildAt(0))).Text = "Insuficiencia cardiaca congestiva";
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pnlTitleICC.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // bbddmedicalDataSet
            // 
            this.bbddmedicalDataSet.DataSetName = "bbddmedicalDataSet";
            this.bbddmedicalDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // mdICCReportBindingSource
            // 
            this.mdICCReportBindingSource.DataMember = "md_ICC_Report";
            this.mdICCReportBindingSource.DataSource = this.bbddmedicalDataSet;
            // 
            // gridSCA
            // 
            this.gridSCA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSCA.Location = new System.Drawing.Point(0, 32);
            // 
            // 
            // 
            this.gridSCA.MasterTemplate.AllowAddNewRow = false;
            this.gridSCA.MasterTemplate.AllowDeleteRow = false;
            this.gridSCA.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn1.FieldName = "md_nombre";
            gridViewTextBoxColumn1.HeaderText = "md_nombre";
            gridViewTextBoxColumn1.Name = "md_nombre";
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "md_nhc";
            gridViewTextBoxColumn2.HeaderText = "md_nhc";
            gridViewTextBoxColumn2.Name = "md_nhc";
            gridViewTextBoxColumn3.DataType = typeof(int);
            gridViewTextBoxColumn3.FieldName = "md_sexo";
            gridViewTextBoxColumn3.HeaderText = "column3";
            gridViewTextBoxColumn3.Name = "md_sexo";
            gridViewTextBoxColumn4.FieldName = "md_fechanacimiento";
            gridViewTextBoxColumn4.HeaderText = "column4";
            gridViewTextBoxColumn4.Name = "md_fechanacimiento";
            gridViewTextBoxColumn5.FieldName = "md_fechaingreso";
            gridViewTextBoxColumn5.HeaderText = "column5";
            gridViewTextBoxColumn5.Name = "md_fechaingreso";
            gridViewTextBoxColumn6.FieldName = "md_fechaalta";
            gridViewTextBoxColumn6.HeaderText = "column6";
            gridViewTextBoxColumn6.Name = "md_fechaalta";
            gridViewTextBoxColumn7.DataType = typeof(int);
            gridViewTextBoxColumn7.FieldName = "md_hta";
            gridViewTextBoxColumn7.HeaderText = "md_hta";
            gridViewTextBoxColumn7.Name = "md_hta";
            gridViewTextBoxColumn8.DataType = typeof(int);
            gridViewTextBoxColumn8.FieldName = "md_diabetesmellitus";
            gridViewTextBoxColumn8.HeaderText = "md_diabetesmellitus";
            gridViewTextBoxColumn8.Name = "md_diabetesmellitus";
            gridViewTextBoxColumn9.DataType = typeof(int);
            gridViewTextBoxColumn9.FieldName = "md_anyosdm";
            gridViewTextBoxColumn9.HeaderText = "column1";
            gridViewTextBoxColumn9.Name = "md_anyosdm";
            gridViewTextBoxColumn10.DataType = typeof(int);
            gridViewTextBoxColumn10.FieldName = "md_niveleshba";
            gridViewTextBoxColumn10.HeaderText = "column1";
            gridViewTextBoxColumn10.Name = "md_niveleshba";
            gridViewTextBoxColumn11.DataType = typeof(int);
            gridViewTextBoxColumn11.FieldName = "md_dislipemia";
            gridViewTextBoxColumn11.HeaderText = "column1";
            gridViewTextBoxColumn11.Name = "md_dislipemia";
            gridViewTextBoxColumn12.DataType = typeof(int);
            gridViewTextBoxColumn12.FieldName = "md_cardiopatiaisquemica";
            gridViewTextBoxColumn12.HeaderText = "column1";
            gridViewTextBoxColumn12.Name = "md_cardiopatiaisquemica";
            gridViewTextBoxColumn13.DataType = typeof(int);
            gridViewTextBoxColumn13.FieldName = "md_revascularizacion";
            gridViewTextBoxColumn13.HeaderText = "column1";
            gridViewTextBoxColumn13.Name = "md_revascularizacion";
            gridViewTextBoxColumn14.DataType = typeof(int);
            gridViewTextBoxColumn14.FieldName = "md_historialsincopes";
            gridViewTextBoxColumn14.HeaderText = "column1";
            gridViewTextBoxColumn14.Name = "md_historialsincopes";
            gridViewTextBoxColumn15.DataType = typeof(int);
            gridViewTextBoxColumn15.FieldName = "md_ingresosicc";
            gridViewTextBoxColumn15.HeaderText = "column1";
            gridViewTextBoxColumn15.Name = "md_ingresosicc";
            gridViewTextBoxColumn16.DataType = typeof(int);
            gridViewTextBoxColumn16.FieldName = "md_antecedentesfa";
            gridViewTextBoxColumn16.HeaderText = "column1";
            gridViewTextBoxColumn16.Name = "md_antecedentesfa";
            gridViewTextBoxColumn17.DataType = typeof(int);
            gridViewTextBoxColumn17.FieldName = "md_anticoagulacioningreso";
            gridViewTextBoxColumn17.HeaderText = "column1";
            gridViewTextBoxColumn17.Name = "md_anticoagulacioningreso";
            gridViewTextBoxColumn18.DataType = typeof(int);
            gridViewTextBoxColumn18.FieldName = "md_ecocardiograma";
            gridViewTextBoxColumn18.HeaderText = "column1";
            gridViewTextBoxColumn18.Name = "md_ecocardiograma";
            gridViewTextBoxColumn19.DataType = typeof(int);
            gridViewTextBoxColumn19.FieldName = "md_clasefuncionalbasal";
            gridViewTextBoxColumn19.HeaderText = "column1";
            gridViewTextBoxColumn19.Name = "md_clasefuncionalbasal";
            gridViewTextBoxColumn20.DataType = typeof(int);
            gridViewTextBoxColumn20.FieldName = "md_nyha";
            gridViewTextBoxColumn20.HeaderText = "column1";
            gridViewTextBoxColumn20.Name = "md_nyha";
            gridViewTextBoxColumn21.DataType = typeof(uint);
            gridViewTextBoxColumn21.FieldName = "md_sumframinghanmayor";
            gridViewTextBoxColumn21.HeaderText = "column1";
            gridViewTextBoxColumn21.Name = "md_sumframinghanmayor";
            gridViewTextBoxColumn22.DataType = typeof(int);
            gridViewTextBoxColumn22.FieldName = "md_sumframinghanmenor";
            gridViewTextBoxColumn22.HeaderText = "column1";
            gridViewTextBoxColumn22.Name = "md_sumframinghanmenor";
            gridViewTextBoxColumn23.DataType = typeof(int);
            gridViewTextBoxColumn23.FieldName = "md_fcingreso";
            gridViewTextBoxColumn23.HeaderText = "column1";
            gridViewTextBoxColumn23.Name = "md_fcingreso";
            gridViewTextBoxColumn24.DataType = typeof(int);
            gridViewTextBoxColumn24.FieldName = "md_taingreso";
            gridViewTextBoxColumn24.HeaderText = "column1";
            gridViewTextBoxColumn24.Name = "md_taingreso";
            gridViewTextBoxColumn25.DataType = typeof(int);
            gridViewTextBoxColumn25.FieldName = "md_ecocardiogramaduranteingreso";
            gridViewTextBoxColumn25.HeaderText = "column1";
            gridViewTextBoxColumn25.Name = "md_ecocardiogramaduranteingreso";
            gridViewTextBoxColumn26.DataType = typeof(int);
            gridViewTextBoxColumn26.Expression = "";
            gridViewTextBoxColumn26.FieldName = "md_fevi";
            gridViewTextBoxColumn26.HeaderText = "column1";
            gridViewTextBoxColumn26.Name = "md_fevi";
            gridViewTextBoxColumn27.DataType = typeof(int);
            gridViewTextBoxColumn27.FieldName = "md_valvulopatia";
            gridViewTextBoxColumn27.HeaderText = "column1";
            gridViewTextBoxColumn27.Name = "md_valvulopatia";
            gridViewTextBoxColumn28.DataType = typeof(int);
            gridViewTextBoxColumn28.FieldName = "md_htap";
            gridViewTextBoxColumn28.HeaderText = "column1";
            gridViewTextBoxColumn28.Name = "md_htap";
            gridViewTextBoxColumn29.DataType = typeof(int);
            gridViewTextBoxColumn29.FieldName = "md_fccontrolada";
            gridViewTextBoxColumn29.HeaderText = "column1";
            gridViewTextBoxColumn29.Name = "md_fccontrolada";
            gridViewTextBoxColumn30.DataType = typeof(int);
            gridViewTextBoxColumn30.FieldName = "md_ritmoalta";
            gridViewTextBoxColumn30.HeaderText = "column1";
            gridViewTextBoxColumn30.Name = "md_ritmoalta";
            gridViewTextBoxColumn31.FieldName = "md_creatininaingreso";
            gridViewTextBoxColumn31.HeaderText = "column2";
            gridViewTextBoxColumn31.Name = "md_creatininaingreso";
            gridViewTextBoxColumn32.FieldName = "md_creatininaalta";
            gridViewTextBoxColumn32.HeaderText = "column2";
            gridViewTextBoxColumn32.Name = "md_creatininaalta";
            gridViewTextBoxColumn33.DataType = typeof(int);
            gridViewTextBoxColumn33.FieldName = "md_betabloqueantespre";
            gridViewTextBoxColumn33.HeaderText = "column2";
            gridViewTextBoxColumn33.Name = "md_betabloqueantespre";
            gridViewTextBoxColumn34.DataType = typeof(int);
            gridViewTextBoxColumn34.FieldName = "md_betabloqueantespost";
            gridViewTextBoxColumn34.HeaderText = "column1";
            gridViewTextBoxColumn34.Name = "md_betabloqueantespost";
            gridViewTextBoxColumn35.DataType = typeof(int);
            gridViewTextBoxColumn35.FieldName = "md_iecapre";
            gridViewTextBoxColumn35.HeaderText = "column1";
            gridViewTextBoxColumn35.Name = "md_iecapre";
            gridViewTextBoxColumn36.DataType = typeof(int);
            gridViewTextBoxColumn36.Expression = "";
            gridViewTextBoxColumn36.FieldName = "md_iecapost";
            gridViewTextBoxColumn36.HeaderText = "column1";
            gridViewTextBoxColumn36.Name = "md_iecapost";
            gridViewTextBoxColumn37.DataType = typeof(int);
            gridViewTextBoxColumn37.FieldName = "md_amiodaronapre";
            gridViewTextBoxColumn37.HeaderText = "column1";
            gridViewTextBoxColumn37.Name = "md_amiodaronapre";
            gridViewTextBoxColumn38.DataType = typeof(int);
            gridViewTextBoxColumn38.FieldName = "md_amiodaronapost";
            gridViewTextBoxColumn38.HeaderText = "column1";
            gridViewTextBoxColumn38.Name = "md_amiodaronapost";
            gridViewTextBoxColumn39.DataType = typeof(int);
            gridViewTextBoxColumn39.FieldName = "md_digoxinapre";
            gridViewTextBoxColumn39.HeaderText = "column1";
            gridViewTextBoxColumn39.Name = "md_digoxinapre";
            gridViewTextBoxColumn40.DataType = typeof(int);
            gridViewTextBoxColumn40.FieldName = "md_digoxinapost";
            gridViewTextBoxColumn40.HeaderText = "column1";
            gridViewTextBoxColumn40.Name = "md_digoxinapost";
            gridViewTextBoxColumn41.DataType = typeof(int);
            gridViewTextBoxColumn41.FieldName = "md_antialdosteronicospre";
            gridViewTextBoxColumn41.HeaderText = "column1";
            gridViewTextBoxColumn41.Name = "md_antialdosteronicospre";
            gridViewTextBoxColumn42.DataType = typeof(int);
            gridViewTextBoxColumn42.FieldName = "md_antialdosteronicospost";
            gridViewTextBoxColumn42.HeaderText = "column1";
            gridViewTextBoxColumn42.Name = "md_antialdosteronicospost";
            gridViewTextBoxColumn43.DataType = typeof(int);
            gridViewTextBoxColumn43.FieldName = "md_mapa";
            gridViewTextBoxColumn43.HeaderText = "column1";
            gridViewTextBoxColumn43.Name = "md_mapa";
            gridViewTextBoxColumn44.DataType = typeof(int);
            gridViewTextBoxColumn44.FieldName = "md_registro";
            gridViewTextBoxColumn44.HeaderText = "column1";
            gridViewTextBoxColumn44.Name = "md_registro";
            gridViewTextBoxColumn45.FieldName = "md_picotensional_diurno";
            gridViewTextBoxColumn45.HeaderText = "column1";
            gridViewTextBoxColumn45.Name = "md_picotensional_diurno";
            gridViewTextBoxColumn46.FieldName = "md_horapicotensional_diurno";
            gridViewTextBoxColumn46.HeaderText = "column1";
            gridViewTextBoxColumn46.Name = "md_horapicotensional_diurno";
            gridViewTextBoxColumn47.FieldName = "md_mediatensional_diurno";
            gridViewTextBoxColumn47.HeaderText = "column1";
            gridViewTextBoxColumn47.Name = "md_mediatensional_diurno";
            gridViewTextBoxColumn48.FieldName = "md_picotensional_nocturno";
            gridViewTextBoxColumn48.HeaderText = "column1";
            gridViewTextBoxColumn48.Name = "md_picotensional_nocturno";
            gridViewTextBoxColumn49.FieldName = "md_horapicotensional_nocturno";
            gridViewTextBoxColumn49.HeaderText = "column1";
            gridViewTextBoxColumn49.Name = "md_horapicotensional_nocturno";
            gridViewTextBoxColumn50.FieldName = "md_mediatensional_nocturno";
            gridViewTextBoxColumn50.HeaderText = "column1";
            gridViewTextBoxColumn50.Name = "md_mediatensional_nocturno";
            gridViewTextBoxColumn51.FieldName = "md_mediafcduranteregistro";
            gridViewTextBoxColumn51.HeaderText = "column1";
            gridViewTextBoxColumn51.Name = "md_mediafcduranteregistro";
            gridViewTextBoxColumn52.DataType = typeof(int);
            gridViewTextBoxColumn52.FieldName = "md_patron";
            gridViewTextBoxColumn52.HeaderText = "column1";
            gridViewTextBoxColumn52.Name = "md_patron";
            gridViewTextBoxColumn53.DataType = typeof(int);
            gridViewTextBoxColumn53.FieldName = "md_ingresos";
            gridViewTextBoxColumn53.HeaderText = "column1";
            gridViewTextBoxColumn53.Name = "md_ingresos";
            gridViewTextBoxColumn54.DataType = typeof(int);
            gridViewTextBoxColumn54.FieldName = "md_status";
            gridViewTextBoxColumn54.HeaderText = "column1";
            gridViewTextBoxColumn54.Name = "md_status";
            gridViewTextBoxColumn55.DataType = typeof(int);
            gridViewTextBoxColumn55.FieldName = "md_exitus";
            gridViewTextBoxColumn55.HeaderText = "column1";
            gridViewTextBoxColumn55.Name = "md_exitus";
            gridViewTextBoxColumn56.FieldName = "md_exitusdesc";
            gridViewTextBoxColumn56.HeaderText = "column1";
            gridViewTextBoxColumn56.Name = "md_exitusdesc";
            this.gridSCA.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38,
            gridViewTextBoxColumn39,
            gridViewTextBoxColumn40,
            gridViewTextBoxColumn41,
            gridViewTextBoxColumn42,
            gridViewTextBoxColumn43,
            gridViewTextBoxColumn44,
            gridViewTextBoxColumn45,
            gridViewTextBoxColumn46,
            gridViewTextBoxColumn47,
            gridViewTextBoxColumn48,
            gridViewTextBoxColumn49,
            gridViewTextBoxColumn50,
            gridViewTextBoxColumn51,
            gridViewTextBoxColumn52,
            gridViewTextBoxColumn53,
            gridViewTextBoxColumn54,
            gridViewTextBoxColumn55,
            gridViewTextBoxColumn56});
            this.gridSCA.MasterTemplate.ShowRowHeaderColumn = false;
            this.gridSCA.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridSCA.Name = "gridSCA";
            this.gridSCA.Size = new System.Drawing.Size(279, 519);
            this.gridSCA.TabIndex = 9;
            this.gridSCA.Text = "radGridView1";
            this.gridSCA.ThemeName = "Office2010Blue";
            this.gridSCA.DataBindingComplete += new Telerik.WinControls.UI.GridViewBindingCompleteEventHandler(this.gridSCA_DataBindingComplete);
            // 
            // layoutControl2
            // 
            this.layoutControl2.AppName = "FormsApp";
            this.layoutControl2.BackColor = System.Drawing.Color.Transparent;
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.layoutControl2.ExportBehaviour = MD_Controls.Layout.LayoutControl.ExportBehaviourEnum.BasicExport;
            this.layoutControl2.FormNameSource = MD_Controls.Layout.LayoutControl.FormIdOriginEnum.Default;
            this.layoutControl2.GridView = this.gridSCA;
            this.layoutControl2.Location = new System.Drawing.Point(0, 551);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.RecordCount = "";
            this.layoutControl2.SheetTitle = null;
            this.layoutControl2.Size = new System.Drawing.Size(279, 32);
            this.layoutControl2.TabIndex = 5;
            // 
            // layoutControl1
            // 
            this.layoutControl1.AppName = "FormsApp";
            this.layoutControl1.BackColor = System.Drawing.Color.Transparent;
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.layoutControl1.ExportBehaviour = MD_Controls.Layout.LayoutControl.ExportBehaviourEnum.BasicExport;
            this.layoutControl1.FormNameSource = MD_Controls.Layout.LayoutControl.FormIdOriginEnum.Default;
            this.layoutControl1.GridView = this.gridICC;
            this.layoutControl1.Location = new System.Drawing.Point(0, 551);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.RecordCount = "";
            this.layoutControl1.SheetTitle = null;
            this.layoutControl1.Size = new System.Drawing.Size(649, 32);
            this.layoutControl1.TabIndex = 4;
            // 
            // ddSexo
            // 
            this.ddSexo.Location = new System.Drawing.Point(16, 51);
            this.ddSexo.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddSexo.Name = "ddSexo";
            this.ddSexo.Size = new System.Drawing.Size(278, 30);
            this.ddSexo.TabIndex = 22;
            // 
            // inputFechaAlta
            // 
            this.inputFechaAlta.Location = new System.Drawing.Point(495, 69);
            this.inputFechaAlta.Name = "inputFechaAlta";
            this.inputFechaAlta.Size = new System.Drawing.Size(313, 30);
            this.inputFechaAlta.TabIndex = 21;
            // 
            // inputFechaIngreso
            // 
            this.inputFechaIngreso.Location = new System.Drawing.Point(495, 33);
            this.inputFechaIngreso.Name = "inputFechaIngreso";
            this.inputFechaIngreso.Size = new System.Drawing.Size(313, 30);
            this.inputFechaIngreso.TabIndex = 20;
            // 
            // inputFechaNacimiento
            // 
            this.inputFechaNacimiento.Location = new System.Drawing.Point(495, -3);
            this.inputFechaNacimiento.Name = "inputFechaNacimiento";
            this.inputFechaNacimiento.Size = new System.Drawing.Size(313, 30);
            this.inputFechaNacimiento.TabIndex = 19;
            // 
            // inputNHC
            // 
            this.inputNHC.Location = new System.Drawing.Point(16, 51);
            this.inputNHC.Name = "inputNHC";
            this.inputNHC.Size = new System.Drawing.Size(278, 30);
            this.inputNHC.TabIndex = 13;
            this.inputNHC.Visible = false;
            // 
            // FrmExportSCAICC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 770);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.divBotonera);
            this.Controls.Add(this.radCollapsiblePanel1);
            this.MinimumSize = new System.Drawing.Size(940, 800);
            this.Name = "FrmExportSCAICC";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "FrmExportSCAICC";
            this.Load += new System.EventHandler(this.FrmExportSCAICC_Load);
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            this.radCollapsiblePanel1.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.divBotonera)).EndInit();
            this.divBotonera.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlTitleSCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridICC.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridICC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTitleICC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bbddmedicalDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mdICCReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSCA.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadLabel lblPaciente;
        private Telerik.WinControls.UI.RadDropDownList ddPacientes;
        private Telerik.WinControls.UI.RadButton btnClear;
        private Telerik.WinControls.UI.RadButton btnFilter;
        private MD_Controls.InputNumeric inputNHC;
        private Telerik.WinControls.UI.RadPanel divBotonera;
        private Telerik.WinControls.UI.RadButton btnExit;
        private MD_Controls.Layout.LayoutControl layoutControl2;
        private MD_Controls.Layout.LayoutControl layoutControl1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Telerik.WinControls.UI.RadPanel pnlTitleSCA;
        private Telerik.WinControls.UI.RadPanel pnlTitleICC;
        private Telerik.WinControls.UI.RadGridView gridICC;
        private bbddmedicalDataSet bbddmedicalDataSet;
        private System.Windows.Forms.BindingSource mdICCReportBindingSource;
        private Telerik.WinControls.UI.RadGridView gridSCA;
        private MD_Controls.DropDownMedical ddSexo;
        private MD_Controls.InputDate inputFechaAlta;
        private MD_Controls.InputDate inputFechaIngreso;
        private MD_Controls.InputDate inputFechaNacimiento;
    }
}
