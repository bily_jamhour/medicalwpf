﻿using medical_Data_Lib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
namespace medicalWPF.MD_Forms.Reports
{
    public partial class FrmExportSCAICC : RadForm
    {
        md_sca dataSCAFiltering = new md_sca();
        md_icc dataICCFiltering = new md_icc();
        bool filtered = false;
        public FrmExportSCAICC()
        {
            InitializeComponent();
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            md_pacientes filterData = new md_pacientes();
            filtered = false;
            if (inputFechaAlta.enabled)
            {
                filterData.md_fechaalta = inputFechaAlta.getDate();
                filtered = true;
            }
            if (inputFechaIngreso.enabled)
            {
                filterData.md_fechaingreso = inputFechaIngreso.getDate();
                filtered = true;
            }
            if (inputFechaNacimiento.enabled)
            {
                filterData.md_fechanacimiento = inputFechaNacimiento.getDate();
                filtered = true;
            }
            if (inputNHC.Value() != 0)
            {
                filterData.md_nhc = inputNHC.Value();
                filtered = true;
            }
            filterData.md_sexo = Convert.ToInt32(ddSexo.GetValue());
            if (filterData.md_sexo >= 0)
                filtered = true;
            if (ddPacientes.SelectedValue.ToString() != "0")
            {
                filterData.md_record_id = Convert.ToInt32(ddPacientes.SelectedValue);
                filtered = true;
            }
            gridSCA.DataSource = MDCommonData.getSCAToReport(dataPacienteToFiltering: (filtered) ? filterData : null);
            // bbddmedicalDataSet.md_ICC_ReportDataTable dtaux = new bbddmedicalDataSet.md_ICC_ReportDataTable();
            gridICC.DataSource = MDCommonData.getICCToReport(dataPacienteToFiltering: (filtered) ? filterData : null);
            layoutControl1.RecordCount = string.Format("{0} Registros", gridICC.RowCount);
            layoutControl2.RecordCount = string.Format("{0} Registros", gridSCA.RowCount);
        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridSCA_DataBindingComplete(object sender, GridViewBindingCompleteEventArgs e)
        {
            gridSCA.Columns.Remove(gridSCA.Columns.FirstOrDefault(x => x.FieldName == "md_pacientes"));

            if (gridSCA.Columns.Count > 0)
            {
                setLangGrid(gridSCA);
            }
        }

        private void setLangGrid(RadGridView radGridToSet)
        {
            foreach (GridViewDataColumn col in radGridToSet.Columns)
            {
                if (col.FieldName == "md_record_id")
                {
                    col.IsVisible = false;
                    col.VisibleInColumnChooser = false;
                }
                else if (col.FieldName == "md_fechanacimiento")
                {
                    col.HeaderText = "Fecha Nacimimento";
                }
                else if (col.FieldName == "md_fechaingreso")
                {
                    col.HeaderText = "Fecha Ingreso";
                }
                else if (col.FieldName == "md_fechaalta")
                {
                    col.HeaderText = "Fecha Alta";
                }
                else if (col.FieldName == "md_nombre")
                {
                    col.HeaderText = "Paciente";
                }
                else if (col.FieldName == "md_nhc")
                {
                    col.HeaderText = "NHC";
                }
                else if (col.FieldName == "md_sexo")
                {
                    col.HeaderText = "Sexo";
                }
                else if (col.FieldName == "md_hta")
                {
                    col.HeaderText = "HTA";
                }
                else if (col.FieldName == "md_diabetesmellitus")
                {
                    col.HeaderText = "Diabetes Mellitus";
                }
                else if (col.FieldName == "md_dislipemia")
                {
                    col.HeaderText = "Dislipemia";
                }
                else if (col.FieldName == "md_cardiopatiaisquemica")
                {
                    col.HeaderText = "Cardiopatia Isquémica";
                }
                else if (col.FieldName == "md_historialsincopes")
                {
                    col.HeaderText = "Historial Sincopes";
                }
                else if (col.FieldName == "md_ingresosicc")
                {
                    col.HeaderText = "Ingresos previos por ICC";
                }
                else if (col.FieldName == "md_antecedentesfa")
                {
                    col.HeaderText = "Antecedentes FA";
                }
                else if (col.FieldName == "md_anticoagulacioningreso")
                {
                    col.HeaderText = "Anticoagulación previa ingreso";
                }
                else if (col.FieldName == "md_ecocardiograma")
                {
                    col.HeaderText = "Ecocardiograma previo";
                }
                else if (col.FieldName == "md_clasefuncionalbasal")
                {
                    col.HeaderText = "Clase funcional basal";
                }
                else if (col.FieldName == "md_nyha")
                {
                    col.HeaderText = "NYHA al ingreso";
                }

                else if (col.FieldName == "md_sumframinghanmayor")
                {
                    col.HeaderText = "Suma Framinghan mayores";
                }

                else if (col.FieldName == "md_sumframinghanmenor")
                {
                    col.HeaderText = "Suma Framinghan menores";
                }

                else if (col.FieldName == "md_fcingreso")
                {
                    col.HeaderText = "FC al ingreso";
                }

                else if (col.FieldName == "md_taingreso")
                {
                    col.HeaderText = "TAs al ingreso";
                }

                else if (col.FieldName == "md_taingreso")
                {
                    col.HeaderText = "TAs al ingreso";
                }

                else if (col.FieldName == "md_ecocardiogramaduranteingreso")
                {
                    col.HeaderText = "Eco en ingreso";
                }

                else if (col.FieldName == "md_fevi")
                {
                    col.HeaderText = "FEVI";
                }
                else if (col.FieldName == "md_valvulopatia")
                {
                    col.HeaderText = "Valvulopatía significativa";
                }
                else if (col.FieldName == "md_htap")
                {
                    col.HeaderText = "HTAp (Eco)";
                }
                else if (col.FieldName == "md_fccontrolada")
                {
                    col.HeaderText = "FC controlada las 24 h";
                }
                else if (col.FieldName == "md_ritmoalta")
                {
                    col.HeaderText = "Ritmo al alta";
                }

                else if (col.FieldName == "md_creatininaingreso")
                {
                    col.HeaderText = "Creatinina al ingreso";
                }

                else if (col.FieldName == "md_creatininaalta")
                {
                    col.HeaderText = "Creatinina al alta";
                }

                else if (col.FieldName == "md_betabloqueantespre")
                {
                    col.HeaderText = "Betabloqueantes Pre";
                }

                else if (col.FieldName == "md_betabloqueantespost")
                {
                    col.HeaderText = "Betabloqueantes Post";
                }

                else if (col.FieldName == "md_iecapre")
                {
                    col.HeaderText = "IECAs Pre";
                }

                else if (col.FieldName == "md_iecapost")
                {
                    col.HeaderText = "IECAs Post";
                }

                else if (col.FieldName == "md_amiodaronapre")
                {
                    col.HeaderText = "Amiodarona Pre";
                }

                else if (col.FieldName == "md_amiodaronapost")
                {
                    col.HeaderText = "Amiodarona Post";
                }

                else if (col.FieldName == "md_digoxinapre")
                {
                    col.HeaderText = "Digoxina Pre";
                }


                else if (col.FieldName == "md_digoxinapost")
                {
                    col.HeaderText = "Digoxina Post";
                }


                else if (col.FieldName == "md_antialdosteronicospre")
                {
                    col.HeaderText = "Antialdosteronicos Pre";
                }


                else if (col.FieldName == "md_antialdosteronicospost")
                {
                    col.HeaderText = "Antialdosteronicos Post";
                }


                else if (col.FieldName == "md_mapa")
                {
                    col.HeaderText = "Mapa";
                }
                else if (col.FieldName == "md_registro")
                {
                    col.HeaderText = "Registro";
                }
                else if (col.FieldName == "md_picotensional_diurno")
                {
                    col.HeaderText = "Pico tensional diurno";
                }
                else if (col.FieldName == "md_horapicotensional_diurno")
                {
                    col.HeaderText = "Hora del pico tensional";
                }
                else if (col.FieldName == "md_mediatensional_diurno")
                {
                    col.HeaderText = "Media tensional diurna";
                }
                else if (col.FieldName == "md_picotensional_nocturno")
                {
                    col.HeaderText = "Pico tensional nocturno";
                }
                else if (col.FieldName == "md_horapicotensional_nocturno")
                {
                    col.HeaderText = "Hora del pico tensional nocturno";
                }
                else if (col.FieldName == "md_mediatensional_nocturno")
                {
                    col.HeaderText = "Media tensional nocturna";
                }
                else if (col.FieldName == "md_mediafcduranteregistro")
                {
                    col.HeaderText = "Media FC durante el registro";
                }
                else if (col.FieldName == "md_patron")
                {
                    col.HeaderText = "Patrón o respuesta tensional";
                }
                else if (col.FieldName == "md_ingresos")
                {
                    col.HeaderText = "Ingresos a los 6 meses";
                }
                else if (col.FieldName == "md_status")
                {
                    col.HeaderText = "Status al año";
                }

                else if (col.FieldName == "md_exitus")
                {
                    col.HeaderText = "Exitus";
                }

                else if (col.FieldName == "md_exitusdesc")
                {
                    col.HeaderText = "Descripción causa Exitus";
                }
                else if (col.FieldName == "md_anyosdm")
                {
                    col.HeaderText = "Años diagnóstico DM";
                }
                else if (col.FieldName == "md_niveleshba")
                {
                    col.HeaderText = "Niveles HbA1(%)";
                }
                else if (col.FieldName == "md_revascularizacion")
                {
                    col.HeaderText = "Revascularización previa";
                }

            }
            try
            {
                gridSCA.Columns.FirstOrDefault(x => x.FieldName == "md_nombre").IsVisible = true;
                gridICC.Columns.FirstOrDefault(x => x.FieldName == "md_nombre").IsVisible = true;
            }
            catch { }

        }

        private void gridICC_DataBindingComplete(object sender, GridViewBindingCompleteEventArgs e)
        {
            gridICC.Columns.Remove(gridICC.Columns.FirstOrDefault(x => x.FieldName == "md_pacientes"));

            if (gridICC.Columns.Count > 0)
            {
                setLangGrid(gridICC);
            }
        }

        private void FrmExportSCAICC_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'bbddmedicalDataSet.md_ICC_Report' Puede moverla o quitarla según sea necesario.
            // this.md_ICC_ReportTableAdapter.Fill(this.bbddmedicalDataSet.md_ICC_Report);
            ddPacientes.ValueMember = "md_record_id";
            ddPacientes.DisplayMember = "md_nombre";
            ddPacientes.DataSource = MDCommonData.getIdAndNamePacientes();
            ddSexo.SetTypeDropDownList("SEX");
            setLang();
        }
        private void setLang()
        {
            this.Text = "Medical Management-Informes de SCA e ICC";
            inputNHC.SetTitle("NHC");
            btnExit.Text = "Salir";
            btnFilter.Text = "Buscar";
            btnClear.Text = "Limpiar";
            lblPaciente.Text = "Paciente";
            inputFechaAlta.SetTitle("Fecha Alta");
            inputFechaIngreso.SetTitle("Fecha Ingreso");
            inputFechaNacimiento.SetTitle("Fecha Nacimiento");
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ddPacientes.SelectedItem = ddPacientes.Items.FirstOrDefault();
            inputNHC.Clear();
            ddSexo.Clear();
            inputFechaAlta.Clear();
            inputFechaIngreso.Clear();
            inputFechaNacimiento.Clear();
        }
    }
}
