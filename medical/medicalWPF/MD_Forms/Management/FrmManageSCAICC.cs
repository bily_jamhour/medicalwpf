﻿using MD_Controls.Layout;
using medical_Data_Lib;
using medical_Infrastructure.Utilities;
using medicalWPF.MD_Forms.Management.ICC;
using medicalWPF.MD_Forms.Management.SCA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using static MD_Controls.Layout.LayoutControl;

namespace medicalWPF.MD_Forms.Management
{
    public partial class FrmManageSCAICC : RadForm
    {
        md_icc mdICCFiltered = new md_icc();
        md_sca mdSCAFiltered = new md_sca();
        public FrmManageSCAICC()
        {
            InitializeComponent();
        }
        bool bIsColumnsChanged = false;
        private void FrmManageSCAICC_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'bbddmedicalDataSet.md_icc' Puede moverla o quitarla según sea necesario.
            //  this.md_iccTableAdapter.Fill(this.bbddmedicalDataSet.md_icc);
            ddPacientes.ValueMember = "md_record_id";
            ddPacientes.DisplayMember = "md_nombre";
            ddPacientes.DataSource = MDCommonData.getIdAndNamePacientes();
            layoutControl2.ExportBehaviour = ExportBehaviourEnum.None;
            layoutControl1.ExportBehaviour = ExportBehaviourEnum.None;

            gridSCA.Columns.FirstOrDefault(x => x.FieldName == "md_nombre").IsVisible = false;
            gridICC.Columns.FirstOrDefault(x => x.FieldName == "md_nombre").IsVisible = false;

            setLang();
        }

        private void setLang()
        {
            this.Text = "Medical Management-Gestión de SCA e ICC";
            inputNHC.SetTitle("NHC");
            btnExit.Text = "Salir";
            btnFilter.Text = "Buscar";
            btnClear.Text = "Limpiar";
            lblPaciente.Text = "Paciente";

        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            mdICCFiltered = new md_icc();
            mdSCAFiltered = new md_sca();
            if (Convert.ToInt32(ddPacientes.SelectedValue) != 0)
            {
                mdICCFiltered.md_record_id = Convert.ToInt32(ddPacientes.SelectedValue);
                mdSCAFiltered.md_record_id = Convert.ToInt32(ddPacientes.SelectedValue);
            }

            LoadGridICC();
            LoadGridSCA();
            bIsColumnsChanged = true;
        }
        private void LoadGridICC()
        {
            gridICC.DataSource = MdICC.GetICCFromFiltering(mdICCFiltered);
            layoutControl1.RecordCount = string.Format("{0} Registros ICC", gridICC.Rows.Count);
        }
        private void LoadGridSCA()
        {
            gridSCA.DataSource = MdSCA.GetSCAFromFiltering(mdSCAFiltered);
            layoutControl2.RecordCount = string.Format("{0} Registros SCA", gridSCA.Rows.Count);
        }
        private void gridSCA_DataError(object sender, GridViewDataErrorEventArgs e)
        {

        }

        private void gridICC_DataError(object sender, GridViewDataErrorEventArgs e)
        {

        }

        private void gridICC_DataBindingComplete(object sender, GridViewBindingCompleteEventArgs e)
        {
            gridICC.Columns.Remove(gridICC.Columns.FirstOrDefault(x => x.FieldName == "md_pacientes"));
            if (bIsColumnsChanged == true)
                return;

            if (gridICC.Columns.Count > 0)
            {
                setLangGrid(gridICC);
            }
        }

        private void setLangGrid(RadGridView radGridToSet)
        {
            foreach (GridViewDataColumn col in radGridToSet.Columns)
            {
                if (col.FieldName == "md_record_id")
                {
                    col.IsVisible = false;
                    col.VisibleInColumnChooser = false;
                }
                else if (col.FieldName == "md_hta")
                {
                    col.HeaderText = "HTA";
                }
                else if (col.FieldName == "md_diabetesmellitus")
                {
                    col.HeaderText = "Diabetes Mellitus";
                }
                else if (col.FieldName == "md_dislipemia")
                {
                    col.HeaderText = "Dislipemia";
                }
                else if (col.FieldName == "md_cardiopatiaisquemica")
                {
                    col.HeaderText = "Cardiopatia Isquémica";
                }
                else if (col.FieldName == "md_historialsincopes")
                {
                    col.HeaderText = "Historial Sincopes";
                }
                else if (col.FieldName == "md_ingresosicc")
                {
                    col.HeaderText = "Ingresos previos por ICC";
                }
                else if (col.FieldName == "md_antecedentesfa")
                {
                    col.HeaderText = "Antecedentes FA";
                }
                else if (col.FieldName == "md_anticoagulacioningreso")
                {
                    col.HeaderText = "Anticoagulación previa ingreso";
                }
                else if (col.FieldName == "md_ecocardiograma")
                {
                    col.HeaderText = "Ecocardiograma previo";
                }
                else if (col.FieldName == "md_clasefuncionalbasal")
                {
                    col.HeaderText = "Clase funcional basal";
                }
                else if (col.FieldName == "md_nyha")
                {
                    col.HeaderText = "NYHA al ingreso";
                }

                else if (col.FieldName == "md_sumframinghanmayor")
                {
                    col.HeaderText = "Suma Framinghan mayores";
                }

                else if (col.FieldName == "md_sumframinghanmenor")
                {
                    col.HeaderText = "Suma Framinghan menores";
                }

                else if (col.FieldName == "md_fcingreso")
                {
                    col.HeaderText = "FC al ingreso";
                }

                else if (col.FieldName == "md_taingreso")
                {
                    col.HeaderText = "TAs al ingreso";
                }

                else if (col.FieldName == "md_taingreso")
                {
                    col.HeaderText = "TAs al ingreso";
                }

                else if (col.FieldName == "md_ecocardiogramaduranteingreso")
                {
                    col.HeaderText = "Eco en ingreso";
                }

                else if (col.FieldName == "md_fevi")
                {
                    col.HeaderText = "FEVI";
                }
                else if (col.FieldName == "md_valvulopatia")
                {
                    col.HeaderText = "Valvulopatía significativa";
                }
                else if (col.FieldName == "md_htap")
                {
                    col.HeaderText = "HTAp (Eco)";
                }
                else if (col.FieldName == "md_fccontrolada")
                {
                    col.HeaderText = "FC controlada las 24 h";
                }
                else if (col.FieldName == "md_ritmoalta")
                {
                    col.HeaderText = "Ritmo al alta";
                }

                else if (col.FieldName == "md_creatininaingreso")
                {
                    col.HeaderText = "Creatinina al ingreso";
                }

                else if (col.FieldName == "md_creatininaalta")
                {
                    col.HeaderText = "Creatinina al alta";
                }

                else if (col.FieldName == "md_betabloqueantespre")
                {
                    col.HeaderText = "Betabloqueantes Pre";
                }

                else if (col.FieldName == "md_betabloqueantespost")
                {
                    col.HeaderText = "Betabloqueantes Post";
                }

                else if (col.FieldName == "md_iecapre")
                {
                    col.HeaderText = "IECAs Pre";
                }

                else if (col.FieldName == "md_iecapost")
                {
                    col.HeaderText = "IECAs Post";
                }

                else if (col.FieldName == "md_amiodaronapre")
                {
                    col.HeaderText = "Amiodarona Pre";
                }

                else if (col.FieldName == "md_amiodaronapost")
                {
                    col.HeaderText = "Amiodarona Post";
                }

                else if (col.FieldName == "md_digoxinapre")
                {
                    col.HeaderText = "Digoxina Pre";
                }


                else if (col.FieldName == "md_digoxinapost")
                {
                    col.HeaderText = "Digoxina Post";
                }


                else if (col.FieldName == "md_antialdosteronicospre")
                {
                    col.HeaderText = "Antialdosteronicos Pre";
                }


                else if (col.FieldName == "md_antialdosteronicospost")
                {
                    col.HeaderText = "Antialdosteronicos Post";
                }


                else if (col.FieldName == "md_mapa")
                {
                    col.HeaderText = "Mapa";
                }
                else if (col.FieldName == "md_registro")
                {
                    col.HeaderText = "Registro";
                }
                else if (col.FieldName == "md_picotensional_diurno")
                {
                    col.HeaderText = "Pico tensional diurno";
                }
                else if (col.FieldName == "md_horapicotensional_diurno")
                {
                    col.HeaderText = "Hora del pico tensional";
                }
                else if (col.FieldName == "md_mediatensional_diurno")
                {
                    col.HeaderText = "Media tensional diurna";
                }
                else if (col.FieldName == "md_picotensional_nocturno")
                {
                    col.HeaderText = "Pico tensional nocturno";
                }
                else if (col.FieldName == "md_horapicotensional_nocturno")
                {
                    col.HeaderText = "Hora del pico tensional nocturno";
                }
                else if (col.FieldName == "md_mediatensional_nocturno")
                {
                    col.HeaderText = "Media tensional nocturna";
                }
                else if (col.FieldName == "md_mediafcduranteregistro")
                {
                    col.HeaderText = "Media FC durante el registro";
                }
                else if (col.FieldName == "md_patron")
                {
                    col.HeaderText = "Patrón o respuesta tensional";
                }
                else if (col.FieldName == "md_ingresos")
                {
                    col.HeaderText = "Ingresos a los 6 meses";
                }
                else if (col.FieldName == "md_status")
                {
                    col.HeaderText = "Status al año";
                }

                else if (col.FieldName == "md_exitus")
                {
                    col.HeaderText = "Exitus";
                }

                else if (col.FieldName == "md_exitusdesc")
                {
                    col.HeaderText = "Descripción causa Exitus";
                }
                else if (col.FieldName == "md_anyosdm")
                {
                    col.HeaderText = "Años diagnóstico DM";
                }
                else if (col.FieldName == "md_niveleshba")
                {
                    col.HeaderText = "Niveles HbA1(%)";
                }
                else if (col.FieldName == "md_revascularizacion")
                {
                    col.HeaderText = "Revascularización previa";
                }

            }
            gridSCA.Columns.FirstOrDefault(x => x.FieldName == "md_nombre").IsVisible = true;
            gridICC.Columns.FirstOrDefault(x => x.FieldName == "md_nombre").IsVisible = true;
        }

        private void gridSCA_DataBindingComplete(object sender, GridViewBindingCompleteEventArgs e)
        {
            gridSCA.Columns.Remove(gridSCA.Columns.FirstOrDefault(x => x.FieldName == "md_pacientes"));
            if (bIsColumnsChanged == true)
                return;
            if (gridSCA.Columns.Count > 0)
            {
                setLangGrid(gridSCA);
            }
        }

        private void layoutControl1_PrintOrExport(ExportModeEnum mode, LayoutControl.ExportSelectionEnum selection)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridSCA_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.Column.Name == "md_nombre")
            {
                e.CellElement.Text = ddPacientes.Items.FirstOrDefault(x => x.Value.ToString() == e.Row.Cells["md_record_id"].Value.ToString()).Text;
                //e.CellElement.Value = e.CellElement.Text;
                //e.CellElement.Value = ddPacientes.Items.FirstOrDefault(x => x.Value.ToString() == e.Row.Cells["md_record_id"].Value.ToString()).Text;
            }
        }

        private void gridICC_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.Column.Name == "md_nombre")
            {
                e.CellElement.Text = MdPacientes.GetPacienteFromRecordId(Convert.ToInt32(e.Row.Cells["md_record_id"].Value)).md_nombre;
                //e.CellElement.Value = e.CellElement.Text;
            }
        }

        private void btnRemoveSCA_Click(object sender, EventArgs e)
        {
            if (!gridSCA.SelectedRows.Any())
            {
                RadMessageBox.Show("No ha seleccionado ningún registro.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            else
            {/*
                 if (DialogResult.No == RadMessageBox.Show("Eliminará del sistema los siguientes SCAs pertenecientes a los usuarios:" + Environment.NewLine +
                      gridSCA.SelectedRows.Select(x => x.Cells["md_nombre"].Value.ToString()).ToList().JoinAsParameterWithoutQuotes() + Environment.NewLine
                      + "¿Desea continuar?", this.Text, MessageBoxButtons.YesNo, RadMessageIcon.Question))
                 */
                if (DialogResult.No == RadMessageBox.Show("Eliminará del sistema los siguientes SCAs pertenecientes a los usuarios seleccionados" + "¿Desea continuar?", this.Text, MessageBoxButtons.YesNo, RadMessageIcon.Question))
                    return;

                foreach (int iRecordId in gridSCA.SelectedRows.Select(x => Convert.ToInt32(x.Cells["md_record_id"].Value)).ToList())
                {
                    MdSCA.RemoveSCA(iRecordId);
                }
                LoadGridSCA();
            }
        }

        private void btnRemoveICC_Click(object sender, EventArgs e)
        {
            if (!gridICC.SelectedRows.Any())
            {
                RadMessageBox.Show("No ha seleccionado ningún registro.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            else
            {
                /*
                 if (DialogResult.No == RadMessageBox.Show("Eliminará del sistema los siguientes ICCs pertenecientes a los usuarios:" + Environment.NewLine +
                      gridICC.SelectedRows.Select(x => x.Cells["md_nombre"].Value.ToString()).ToList().JoinAsParameterWithoutQuotes() + Environment.NewLine
                      + "¿Desea continuar?", this.Text, MessageBoxButtons.YesNo, RadMessageIcon.Question))
                 */
                if (DialogResult.No == RadMessageBox.Show("Eliminará del sistema los siguientes ICCs pertenecientes a los usuarios seleccionados" + "¿Desea continuar?", this.Text, MessageBoxButtons.YesNo, RadMessageIcon.Question))
                    return;

                foreach (int iRecordId in gridICC.SelectedRows.Select(x => Convert.ToInt32(x.Cells["md_record_id"].Value)).ToList())
                {
                    MdICC.RemoveICC(iRecordId);
                }
                LoadGridICC();
            }
        }

        private void btnNewSCA_Click(object sender, EventArgs e)
        {
            FrmCreateOrUpdateSCA frmCreateSCA = new FrmCreateOrUpdateSCA();
            frmCreateSCA.ShowDialog(this);
            LoadGridSCA();
        }

        private void btnNewICC_Click(object sender, EventArgs e)
        {
            FrmCreateOrUpdateICC frmCreateICC = new FrmCreateOrUpdateICC();
            frmCreateICC.ShowDialog(this);
            LoadGridICC();
        }

        private void btnEditICC_Click(object sender, EventArgs e)
        {
            if (!gridICC.SelectedRows.Any())
            {
                RadMessageBox.Show("No ha seleccionado ningún registro.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            else
            {
                FrmCreateOrUpdateICC frmUpdateICC = new FrmCreateOrUpdateICC((md_icc)gridICC.Rows.FirstOrDefault().DataBoundItem);
                frmUpdateICC.ShowDialog(this);
                LoadGridICC();
            }

        }

        private void btnEditSCA_Click(object sender, EventArgs e)
        {
            if (!gridSCA.SelectedRows.Any())
            {
                RadMessageBox.Show("No ha seleccionado ningún registro.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            else
            {
                FrmCreateOrUpdateSCA frmUpdateICC = new FrmCreateOrUpdateSCA((md_sca)gridSCA.Rows.FirstOrDefault().DataBoundItem);
                frmUpdateICC.ShowDialog(this);
                LoadGridSCA();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ddPacientes.SelectedItem = ddPacientes.Items.FirstOrDefault();
        }
        /*
private void ExportAll(bool exportarExcel = false)
{
try
{
DataTable dt = null;
this.Cursor = Cursors.WaitCursor;
if (gridICC.DataSource == null || ((DataTable)gridICC.DataSource).Rows.Count == 0)
{
MessageBox.Show(this, "No hay registros seleccionados", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
return;
}
if (gridICC.FilterDescriptors.Any())
{
dt = (gridICC.DataSource as DataTable).Clone();
foreach (var row in gridICC.ChildRows)
dt.ImportRow(((DataRowView)row.DataBoundItem).Row);
}
else
{
dt = (DataTable)gridICC.DataSource;
}
dt.TableName = "md_icc";
//If the report is parametrized with LST we must choose a printer
string sPrinterSelected = "";
string sPrintDesignId = "";
DS_Liberlogistics_SGA_Master.wms_reportsRow drReport = CReportsFactory.GetInstance().GetReports("wms_inbound_line_details").FirstOrDefault();
if (drReport == null)
{
RadMessageBox.Show(this, "No está establecida la configuración de informes para [wms_inbound_line_details]", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
this.Cursor = Cursors.Default;
return;
}
string sDataset = drReport.wms_report_dataset;
DS_Liberlogistics_SGA_Master.wms_print_design_filesDataTable dtPrintDesignFiles = CReportsFactory.GetInstance().GetPrintDesignFiles(drReport.wms_report_id, "", "", "", drReport.wms_com_id, drReport.wms_wh_id);

if ((drReport.wms_export_type == "XML" && dtPrintDesignFiles.Rows.Count > 0) || drReport.wms_export_type == "LST")
{
var printPreferences = CReportsFactory.GetInstance().GetPrintPreferences("", drReport.wms_report_id, drReport.wms_report_name, "", "", drReport.wms_com_id, drReport.wms_wh_id, Convert.ToString(cmbBiz.SelectedValue));
var printDesign = printPreferences.Any() ? printPreferences.First().wms_design_file_to_print_id : "";
var frm = new FrmLabelSelectionToPrint(drReport.wms_report_id, _bOnlyPrintSelection: true, printDesign: printDesign);
frm.ShowDialog();
sPrinterSelected = frm.sPrinterSelected;
sPrintDesignId = frm.sCurrentReportPrintDesign;
sDataset = frm.SelectedDataset;

if (!string.IsNullOrWhiteSpace(sDataset))
{
dt = _dtInboundLineDetails;
}

if (!frm.bAccept || (drReport.wms_export_type == "LST" && sPrinterSelected == ""))
{
this.Cursor = Cursors.Default;
return;
}
}

if (exportarExcel)
{
CReportsFactory.GetInstance().GenerarExcel(dt, "wms_inbound_line_details", sPrintDesignId);
}
else
{
CReportsFactory.GetInstance().Imprimir(dt, "wms_inbound_line_details", sPrintDesignId, sPrinterSelected);
}
this.Cursor = Cursors.Default;
}
catch (Exception ex)
{
this.Cursor = Cursors.Default;
CCommonForms.ShowException("btPrint_Click", this.Text.ToString(), ex);
Libertis.WMS_Code_Lib.CCommon.RegisterExceptionLog("btPrint_Click", this.Tag.ToString(), ex);
}
}
private void ExportSelection(bool exportarExcel)
{

this.Cursor = Cursors.WaitCursor;

if (gridInbounds.DataSource == null || ((DataTable)gridInbounds.DataSource).Rows.Count == 0)
{
MessageBox.Show(this, MessagesStringsResources.Message_00022, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
return;
}
DataTable dt = ((DataTable)gridInbounds.DataSource).Clone();
gridInbounds.SelectedRows.ToList().ForEach(row => dt.ImportRow(((DataRowView)row.DataBoundItem).Row));


dt.TableName = "wms_inbound_line_details";

//If the report is parametrized with LST we must choose a printer
string sPrinterSelected = "";
string sPrintDesignId = "";
DS_Liberlogistics_SGA_Master.wms_reportsRow drReport = CReportsFactory.GetInstance().GetReports("wms_inbound_line_details").FirstOrDefault();
if (drReport == null)
{
RadMessageBox.Show(this, "No está establecida la configuración de informes para [wms_inbound_line_details]", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
this.Cursor = Cursors.Default;
return;
}
string sDataset = drReport.wms_report_dataset;
DS_Liberlogistics_SGA_Master.wms_print_design_filesDataTable dtPrintDesignFiles = CReportsFactory.GetInstance().GetPrintDesignFiles(drReport.wms_report_id, "", "", "", drReport.wms_com_id, drReport.wms_wh_id);

if ((drReport.wms_export_type == "XML" && dtPrintDesignFiles.Rows.Count > 0) || drReport.wms_export_type == "LST")
{
FrmLabelSelectionToPrint frm = new FrmLabelSelectionToPrint(drReport.wms_report_id, true);
frm.ShowDialog();
sPrinterSelected = frm.sPrinterSelected;
sPrintDesignId = frm.sCurrentReportPrintDesign;
sDataset = frm.SelectedDataset;

if (!string.IsNullOrWhiteSpace(sDataset))
{
dt = _dtInboundLineDetails;
}

if (!frm.bAccept || (drReport.wms_export_type == "LST" && sPrinterSelected == ""))
{
this.Cursor = Cursors.Default;
return;
}
}

if (exportarExcel)
{
CReportsFactory.GetInstance().GenerarExcel(dt, "wms_inbound_line_details", sPrintDesignId);
}
else
{
CReportsFactory.GetInstance().Imprimir(dt, "wms_inbound_line_details", sPrintDesignId, sPrinterSelected);
}
this.Cursor = Cursors.Default;
}
*/
    }
}
