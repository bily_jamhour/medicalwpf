﻿namespace medicalWPF.MD_Forms.Management.ICC
{
    partial class FrmCreateOrUpdateICC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCausaExitus_2 = new Telerik.WinControls.UI.RadPanel();
            this.txtCausaExitus_2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.txtnombre = new Telerik.WinControls.UI.RadLabel();
            this.txtFechaAlta = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaAlta = new Telerik.WinControls.UI.RadLabel();
            this.txtFechaNacimiento = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaNacimiento = new Telerik.WinControls.UI.RadLabel();
            this.txtFechaIngreso = new Telerik.WinControls.UI.RadLabel();
            this.txtnhc = new Telerik.WinControls.UI.RadLabel();
            this.txtSexo = new Telerik.WinControls.UI.RadLabel();
            this.lblfechaingreso = new Telerik.WinControls.UI.RadLabel();
            this.lblNHC = new Telerik.WinControls.UI.RadLabel();
            this.lblSexo = new Telerik.WinControls.UI.RadLabel();
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.lblPacienteToAssing = new Telerik.WinControls.UI.RadLabel();
            this.ddPacientes = new Telerik.WinControls.UI.RadDropDownList();
            this.pageviewICC = new Telerik.WinControls.UI.RadPanel();
            this.pnlBotonera = new Telerik.WinControls.UI.RadPanel();
            this.btnExit = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.ddhta_2 = new MD_Controls.DropDownMedical();
            this.dddiabetes_2 = new MD_Controls.DropDownMedical();
            this.inputTimehorapicotensional_diurno_2 = new MD_Controls.InputTime();
            this.dddislipemia_2 = new MD_Controls.DropDownMedical();
            this.inputTimehorapicotensional_nocturno_2 = new MD_Controls.InputTime();
            this.ddcardiopatia_2 = new MD_Controls.DropDownMedical();
            this.decimalMediaFCDuranteIngreso_2 = new MD_Controls.InputDecimals();
            this.ddsincopes_2 = new MD_Controls.DropDownMedical();
            this.ddfevi_2 = new MD_Controls.DropDownMedical();
            this.ddicc_2 = new MD_Controls.DropDownMedical();
            this.dddigoxinapre_2 = new MD_Controls.DropDownMedical();
            this.ddfa_2 = new MD_Controls.DropDownMedical();
            this.ddeco_2 = new MD_Controls.DropDownMedical();
            this.ddanticoagulacion_2 = new MD_Controls.DropDownMedical();
            this.numericSumFraminghanMenores_2 = new MD_Controls.InputNumeric();
            this.ddecocardiograma_2 = new MD_Controls.DropDownMedical();
            this.numericFCalIngreso_2 = new MD_Controls.InputNumeric();
            this.ddbasal_2 = new MD_Controls.DropDownMedical();
            this.numericSumFraminghanMayores_2 = new MD_Controls.InputNumeric();
            this.ddta_2 = new MD_Controls.DropDownMedical();
            this.ddvalvulopatia_2 = new MD_Controls.DropDownMedical();
            this.decimalMediaTensionalNocturno_2 = new MD_Controls.InputDecimals();
            this.ddhtap_2 = new MD_Controls.DropDownMedical();
            this.decimalPicoTensionalNocturno_2 = new MD_Controls.InputDecimals();
            this.ddfc_2 = new MD_Controls.DropDownMedical();
            this.decimalMediaTensionalDiurno_2 = new MD_Controls.InputDecimals();
            this.ddritmo_2 = new MD_Controls.DropDownMedical();
            this.deciamlPicoTensionalDiurno_2 = new MD_Controls.InputDecimals();
            this.ddbetabloqueantespre_2 = new MD_Controls.DropDownMedical();
            this.ddbetabloqueantespost_2 = new MD_Controls.DropDownMedical();
            this.ddiecapre_2 = new MD_Controls.DropDownMedical();
            this.deciamlCreatininaAlta_2 = new MD_Controls.InputDecimals();
            this.ddiecapost_2 = new MD_Controls.DropDownMedical();
            this.decimalCreatininiIngreso_2 = new MD_Controls.InputDecimals();
            this.ddamiodaronapre_2 = new MD_Controls.DropDownMedical();
            this.ddamiodaronapost_2 = new MD_Controls.DropDownMedical();
            this.ddexitus_2 = new MD_Controls.DropDownMedical();
            this.ddnyha_2 = new MD_Controls.DropDownMedical();
            this.ddstatus_2 = new MD_Controls.DropDownMedical();
            this.dddigoxinapost_2 = new MD_Controls.DropDownMedical();
            this.ddingresos6_2 = new MD_Controls.DropDownMedical();
            this.ddantialdosteronicospre_2 = new MD_Controls.DropDownMedical();
            this.ddpatron_2 = new MD_Controls.DropDownMedical();
            this.ddantialdosteronicospost_2 = new MD_Controls.DropDownMedical();
            this.ddregistro_2 = new MD_Controls.DropDownMedical();
            this.ddmapa_2 = new MD_Controls.DropDownMedical();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCausaExitus_2)).BeginInit();
            this.pnlCausaExitus_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCausaExitus_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtnombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnhc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSexo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfechaingreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNHC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSexo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPacienteToAssing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageviewICC)).BeginInit();
            this.pageviewICC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBotonera)).BeginInit();
            this.pnlBotonera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCausaExitus_2
            // 
            this.pnlCausaExitus_2.AccessibleDescription = "";
            this.pnlCausaExitus_2.Controls.Add(this.txtCausaExitus_2);
            this.pnlCausaExitus_2.Enabled = false;
            this.pnlCausaExitus_2.Location = new System.Drawing.Point(872, 336);
            this.pnlCausaExitus_2.Name = "pnlCausaExitus_2";
            this.pnlCausaExitus_2.Size = new System.Drawing.Size(411, 32);
            this.pnlCausaExitus_2.TabIndex = 134;
            this.pnlCausaExitus_2.Text = "Causa Exitus";
            ((Telerik.WinControls.UI.RadPanelElement)(this.pnlCausaExitus_2.GetChildAt(0))).Text = "Causa Exitus";
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pnlCausaExitus_2.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // txtCausaExitus_2
            // 
            this.txtCausaExitus_2.Location = new System.Drawing.Point(168, 7);
            this.txtCausaExitus_2.Name = "txtCausaExitus_2";
            this.txtCausaExitus_2.Size = new System.Drawing.Size(232, 20);
            this.txtCausaExitus_2.TabIndex = 134;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.txtnombre);
            this.radPanel1.Controls.Add(this.txtFechaAlta);
            this.radPanel1.Controls.Add(this.lblFechaAlta);
            this.radPanel1.Controls.Add(this.txtFechaNacimiento);
            this.radPanel1.Controls.Add(this.lblFechaNacimiento);
            this.radPanel1.Controls.Add(this.txtFechaIngreso);
            this.radPanel1.Controls.Add(this.txtnhc);
            this.radPanel1.Controls.Add(this.txtSexo);
            this.radPanel1.Controls.Add(this.lblfechaingreso);
            this.radPanel1.Controls.Add(this.lblNHC);
            this.radPanel1.Controls.Add(this.lblSexo);
            this.radPanel1.Controls.Add(this.lblNombre);
            this.radPanel1.Controls.Add(this.lblPacienteToAssing);
            this.radPanel1.Controls.Add(this.ddPacientes);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(1292, 95);
            this.radPanel1.TabIndex = 153;
            // 
            // txtnombre
            // 
            this.txtnombre.Location = new System.Drawing.Point(197, 29);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(55, 18);
            this.txtnombre.TabIndex = 35;
            this.txtnombre.Text = "radLabel1";
            // 
            // txtFechaAlta
            // 
            this.txtFechaAlta.Location = new System.Drawing.Point(898, 64);
            this.txtFechaAlta.Name = "txtFechaAlta";
            this.txtFechaAlta.Size = new System.Drawing.Size(55, 18);
            this.txtFechaAlta.TabIndex = 34;
            this.txtFechaAlta.Text = "radLabel3";
            // 
            // lblFechaAlta
            // 
            this.lblFechaAlta.Location = new System.Drawing.Point(796, 64);
            this.lblFechaAlta.Name = "lblFechaAlta";
            this.lblFechaAlta.Size = new System.Drawing.Size(55, 18);
            this.lblFechaAlta.TabIndex = 33;
            this.lblFechaAlta.Text = "radLabel1";
            // 
            // txtFechaNacimiento
            // 
            this.txtFechaNacimiento.Location = new System.Drawing.Point(898, 30);
            this.txtFechaNacimiento.Name = "txtFechaNacimiento";
            this.txtFechaNacimiento.Size = new System.Drawing.Size(55, 18);
            this.txtFechaNacimiento.TabIndex = 32;
            this.txtFechaNacimiento.Text = "radLabel3";
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.Location = new System.Drawing.Point(796, 30);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(55, 18);
            this.lblFechaNacimiento.TabIndex = 31;
            this.lblFechaNacimiento.Text = "radLabel1";
            // 
            // txtFechaIngreso
            // 
            this.txtFechaIngreso.Location = new System.Drawing.Point(578, 66);
            this.txtFechaIngreso.Name = "txtFechaIngreso";
            this.txtFechaIngreso.Size = new System.Drawing.Size(55, 18);
            this.txtFechaIngreso.TabIndex = 30;
            this.txtFechaIngreso.Text = "radLabel1";
            // 
            // txtnhc
            // 
            this.txtnhc.Location = new System.Drawing.Point(578, 30);
            this.txtnhc.Name = "txtnhc";
            this.txtnhc.Size = new System.Drawing.Size(55, 18);
            this.txtnhc.TabIndex = 29;
            this.txtnhc.Text = "radLabel3";
            // 
            // txtSexo
            // 
            this.txtSexo.Location = new System.Drawing.Point(196, 64);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.Size = new System.Drawing.Size(55, 18);
            this.txtSexo.TabIndex = 28;
            this.txtSexo.Text = "radLabel1";
            // 
            // lblfechaingreso
            // 
            this.lblfechaingreso.Location = new System.Drawing.Point(476, 66);
            this.lblfechaingreso.Name = "lblfechaingreso";
            this.lblfechaingreso.Size = new System.Drawing.Size(55, 18);
            this.lblfechaingreso.TabIndex = 27;
            this.lblfechaingreso.Text = "radLabel1";
            // 
            // lblNHC
            // 
            this.lblNHC.Location = new System.Drawing.Point(476, 30);
            this.lblNHC.Name = "lblNHC";
            this.lblNHC.Size = new System.Drawing.Size(55, 18);
            this.lblNHC.TabIndex = 25;
            this.lblNHC.Text = "radLabel1";
            // 
            // lblSexo
            // 
            this.lblSexo.Location = new System.Drawing.Point(84, 64);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(55, 18);
            this.lblSexo.TabIndex = 23;
            this.lblSexo.Text = "radLabel1";
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(84, 30);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(55, 18);
            this.lblNombre.TabIndex = 22;
            this.lblNombre.Text = "radLabel1";
            // 
            // lblPacienteToAssing
            // 
            this.lblPacienteToAssing.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPacienteToAssing.Location = new System.Drawing.Point(3, 3);
            this.lblPacienteToAssing.Name = "lblPacienteToAssing";
            this.lblPacienteToAssing.Size = new System.Drawing.Size(112, 19);
            this.lblPacienteToAssing.TabIndex = 21;
            this.lblPacienteToAssing.Text = "Paciente a asignar";
            // 
            // ddPacientes
            // 
            this.ddPacientes.DisplayMember = "md_nombre";
            this.ddPacientes.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddPacientes.Location = new System.Drawing.Point(196, 28);
            this.ddPacientes.Name = "ddPacientes";
            this.ddPacientes.Size = new System.Drawing.Size(226, 20);
            this.ddPacientes.TabIndex = 19;
            this.ddPacientes.Text = "radDropDownList1";
            this.ddPacientes.ValueMember = "md_record_id";
            this.ddPacientes.SelectedValueChanged += new System.EventHandler(this.ddPacientes_SelectedValueChanged);
            // 
            // pageviewICC
            // 
            this.pageviewICC.Controls.Add(this.ddhta_2);
            this.pageviewICC.Controls.Add(this.dddiabetes_2);
            this.pageviewICC.Controls.Add(this.inputTimehorapicotensional_diurno_2);
            this.pageviewICC.Controls.Add(this.dddislipemia_2);
            this.pageviewICC.Controls.Add(this.inputTimehorapicotensional_nocturno_2);
            this.pageviewICC.Controls.Add(this.ddcardiopatia_2);
            this.pageviewICC.Controls.Add(this.decimalMediaFCDuranteIngreso_2);
            this.pageviewICC.Controls.Add(this.ddsincopes_2);
            this.pageviewICC.Controls.Add(this.ddfevi_2);
            this.pageviewICC.Controls.Add(this.ddicc_2);
            this.pageviewICC.Controls.Add(this.dddigoxinapre_2);
            this.pageviewICC.Controls.Add(this.ddfa_2);
            this.pageviewICC.Controls.Add(this.ddeco_2);
            this.pageviewICC.Controls.Add(this.ddanticoagulacion_2);
            this.pageviewICC.Controls.Add(this.numericSumFraminghanMenores_2);
            this.pageviewICC.Controls.Add(this.ddecocardiograma_2);
            this.pageviewICC.Controls.Add(this.numericFCalIngreso_2);
            this.pageviewICC.Controls.Add(this.ddbasal_2);
            this.pageviewICC.Controls.Add(this.numericSumFraminghanMayores_2);
            this.pageviewICC.Controls.Add(this.ddta_2);
            this.pageviewICC.Controls.Add(this.ddvalvulopatia_2);
            this.pageviewICC.Controls.Add(this.decimalMediaTensionalNocturno_2);
            this.pageviewICC.Controls.Add(this.ddhtap_2);
            this.pageviewICC.Controls.Add(this.decimalPicoTensionalNocturno_2);
            this.pageviewICC.Controls.Add(this.ddfc_2);
            this.pageviewICC.Controls.Add(this.decimalMediaTensionalDiurno_2);
            this.pageviewICC.Controls.Add(this.ddritmo_2);
            this.pageviewICC.Controls.Add(this.deciamlPicoTensionalDiurno_2);
            this.pageviewICC.Controls.Add(this.ddbetabloqueantespre_2);
            this.pageviewICC.Controls.Add(this.ddbetabloqueantespost_2);
            this.pageviewICC.Controls.Add(this.ddiecapre_2);
            this.pageviewICC.Controls.Add(this.deciamlCreatininaAlta_2);
            this.pageviewICC.Controls.Add(this.ddiecapost_2);
            this.pageviewICC.Controls.Add(this.decimalCreatininiIngreso_2);
            this.pageviewICC.Controls.Add(this.ddamiodaronapre_2);
            this.pageviewICC.Controls.Add(this.pnlCausaExitus_2);
            this.pageviewICC.Controls.Add(this.ddamiodaronapost_2);
            this.pageviewICC.Controls.Add(this.ddexitus_2);
            this.pageviewICC.Controls.Add(this.ddnyha_2);
            this.pageviewICC.Controls.Add(this.ddstatus_2);
            this.pageviewICC.Controls.Add(this.dddigoxinapost_2);
            this.pageviewICC.Controls.Add(this.ddingresos6_2);
            this.pageviewICC.Controls.Add(this.ddantialdosteronicospre_2);
            this.pageviewICC.Controls.Add(this.ddpatron_2);
            this.pageviewICC.Controls.Add(this.ddantialdosteronicospost_2);
            this.pageviewICC.Controls.Add(this.ddregistro_2);
            this.pageviewICC.Controls.Add(this.ddmapa_2);
            this.pageviewICC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageviewICC.Location = new System.Drawing.Point(0, 95);
            this.pageviewICC.Name = "pageviewICC";
            this.pageviewICC.Size = new System.Drawing.Size(1292, 931);
            this.pageviewICC.TabIndex = 154;
            // 
            // pnlBotonera
            // 
            this.pnlBotonera.BackColor = System.Drawing.Color.Transparent;
            this.pnlBotonera.Controls.Add(this.btnExit);
            this.pnlBotonera.Controls.Add(this.btnSave);
            this.pnlBotonera.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBotonera.Location = new System.Drawing.Point(0, 968);
            this.pnlBotonera.Name = "pnlBotonera";
            this.pnlBotonera.Size = new System.Drawing.Size(1292, 58);
            this.pnlBotonera.TabIndex = 155;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pnlBotonera.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Image = global::medicalWPF.Properties.Resources.Exit1;
            this.btnExit.Location = new System.Drawing.Point(1179, 10);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 33);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "[btnExit]";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Image = global::medicalWPF.Properties.Resources.Save;
            this.btnSave.Location = new System.Drawing.Point(12, 10);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(110, 33);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "[btnSave]";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ddhta_2
            // 
            this.ddhta_2.Location = new System.Drawing.Point(20, 12);
            this.ddhta_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddhta_2.Name = "ddhta_2";
            this.ddhta_2.Size = new System.Drawing.Size(414, 30);
            this.ddhta_2.TabIndex = 103;
            // 
            // dddiabetes_2
            // 
            this.dddiabetes_2.Location = new System.Drawing.Point(20, 48);
            this.dddiabetes_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddiabetes_2.Name = "dddiabetes_2";
            this.dddiabetes_2.Size = new System.Drawing.Size(414, 30);
            this.dddiabetes_2.TabIndex = 104;
            // 
            // inputTimehorapicotensional_diurno_2
            // 
            this.inputTimehorapicotensional_diurno_2.Location = new System.Drawing.Point(23, 619);
            this.inputTimehorapicotensional_diurno_2.Name = "inputTimehorapicotensional_diurno_2";
            this.inputTimehorapicotensional_diurno_2.Size = new System.Drawing.Size(411, 30);
            this.inputTimehorapicotensional_diurno_2.TabIndex = 146;
            // 
            // dddislipemia_2
            // 
            this.dddislipemia_2.Location = new System.Drawing.Point(20, 84);
            this.dddislipemia_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddislipemia_2.Name = "dddislipemia_2";
            this.dddislipemia_2.Size = new System.Drawing.Size(414, 30);
            this.dddislipemia_2.TabIndex = 105;
            // 
            // inputTimehorapicotensional_nocturno_2
            // 
            this.inputTimehorapicotensional_nocturno_2.Location = new System.Drawing.Point(442, 619);
            this.inputTimehorapicotensional_nocturno_2.Name = "inputTimehorapicotensional_nocturno_2";
            this.inputTimehorapicotensional_nocturno_2.Size = new System.Drawing.Size(411, 30);
            this.inputTimehorapicotensional_nocturno_2.TabIndex = 146;
            // 
            // ddcardiopatia_2
            // 
            this.ddcardiopatia_2.Location = new System.Drawing.Point(20, 120);
            this.ddcardiopatia_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddcardiopatia_2.Name = "ddcardiopatia_2";
            this.ddcardiopatia_2.Size = new System.Drawing.Size(414, 30);
            this.ddcardiopatia_2.TabIndex = 106;
            // 
            // decimalMediaFCDuranteIngreso_2
            // 
            this.decimalMediaFCDuranteIngreso_2.Location = new System.Drawing.Point(20, 547);
            this.decimalMediaFCDuranteIngreso_2.Name = "decimalMediaFCDuranteIngreso_2";
            this.decimalMediaFCDuranteIngreso_2.Size = new System.Drawing.Size(411, 34);
            this.decimalMediaFCDuranteIngreso_2.TabIndex = 138;
            // 
            // ddsincopes_2
            // 
            this.ddsincopes_2.Location = new System.Drawing.Point(20, 156);
            this.ddsincopes_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddsincopes_2.Name = "ddsincopes_2";
            this.ddsincopes_2.Size = new System.Drawing.Size(414, 30);
            this.ddsincopes_2.TabIndex = 107;
            // 
            // ddfevi_2
            // 
            this.ddfevi_2.Location = new System.Drawing.Point(20, 439);
            this.ddfevi_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfevi_2.Name = "ddfevi_2";
            this.ddfevi_2.Size = new System.Drawing.Size(414, 34);
            this.ddfevi_2.TabIndex = 135;
            // 
            // ddicc_2
            // 
            this.ddicc_2.Location = new System.Drawing.Point(20, 192);
            this.ddicc_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddicc_2.Name = "ddicc_2";
            this.ddicc_2.Size = new System.Drawing.Size(414, 30);
            this.ddicc_2.TabIndex = 108;
            // 
            // dddigoxinapre_2
            // 
            this.dddigoxinapre_2.Location = new System.Drawing.Point(441, 403);
            this.dddigoxinapre_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddigoxinapre_2.Name = "dddigoxinapre_2";
            this.dddigoxinapre_2.Size = new System.Drawing.Size(414, 30);
            this.dddigoxinapre_2.TabIndex = 150;
            // 
            // ddfa_2
            // 
            this.ddfa_2.Location = new System.Drawing.Point(20, 228);
            this.ddfa_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfa_2.Name = "ddfa_2";
            this.ddfa_2.Size = new System.Drawing.Size(414, 30);
            this.ddfa_2.TabIndex = 109;
            // 
            // ddeco_2
            // 
            this.ddeco_2.Location = new System.Drawing.Point(20, 403);
            this.ddeco_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddeco_2.Name = "ddeco_2";
            this.ddeco_2.Size = new System.Drawing.Size(414, 30);
            this.ddeco_2.TabIndex = 149;
            // 
            // ddanticoagulacion_2
            // 
            this.ddanticoagulacion_2.Location = new System.Drawing.Point(20, 264);
            this.ddanticoagulacion_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddanticoagulacion_2.Name = "ddanticoagulacion_2";
            this.ddanticoagulacion_2.Size = new System.Drawing.Size(414, 30);
            this.ddanticoagulacion_2.TabIndex = 110;
            // 
            // numericSumFraminghanMenores_2
            // 
            this.numericSumFraminghanMenores_2.Location = new System.Drawing.Point(440, 514);
            this.numericSumFraminghanMenores_2.Name = "numericSumFraminghanMenores_2";
            this.numericSumFraminghanMenores_2.Size = new System.Drawing.Size(411, 30);
            this.numericSumFraminghanMenores_2.TabIndex = 142;
            // 
            // ddecocardiograma_2
            // 
            this.ddecocardiograma_2.Location = new System.Drawing.Point(20, 300);
            this.ddecocardiograma_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddecocardiograma_2.Name = "ddecocardiograma_2";
            this.ddecocardiograma_2.Size = new System.Drawing.Size(414, 30);
            this.ddecocardiograma_2.TabIndex = 111;
            // 
            // numericFCalIngreso_2
            // 
            this.numericFCalIngreso_2.Location = new System.Drawing.Point(872, 443);
            this.numericFCalIngreso_2.Name = "numericFCalIngreso_2";
            this.numericFCalIngreso_2.Size = new System.Drawing.Size(411, 30);
            this.numericFCalIngreso_2.TabIndex = 145;
            // 
            // ddbasal_2
            // 
            this.ddbasal_2.Location = new System.Drawing.Point(20, 336);
            this.ddbasal_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbasal_2.Name = "ddbasal_2";
            this.ddbasal_2.Size = new System.Drawing.Size(414, 30);
            this.ddbasal_2.TabIndex = 112;
            // 
            // numericSumFraminghanMayores_2
            // 
            this.numericSumFraminghanMayores_2.Location = new System.Drawing.Point(440, 479);
            this.numericSumFraminghanMayores_2.Name = "numericSumFraminghanMayores_2";
            this.numericSumFraminghanMayores_2.Size = new System.Drawing.Size(411, 30);
            this.numericSumFraminghanMayores_2.TabIndex = 141;
            // 
            // ddta_2
            // 
            this.ddta_2.Location = new System.Drawing.Point(20, 372);
            this.ddta_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddta_2.Name = "ddta_2";
            this.ddta_2.Size = new System.Drawing.Size(414, 30);
            this.ddta_2.TabIndex = 113;
            // 
            // ddvalvulopatia_2
            // 
            this.ddvalvulopatia_2.Location = new System.Drawing.Point(439, 12);
            this.ddvalvulopatia_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddvalvulopatia_2.Name = "ddvalvulopatia_2";
            this.ddvalvulopatia_2.Size = new System.Drawing.Size(414, 30);
            this.ddvalvulopatia_2.TabIndex = 114;
            // 
            // decimalMediaTensionalNocturno_2
            // 
            this.decimalMediaTensionalNocturno_2.Location = new System.Drawing.Point(440, 583);
            this.decimalMediaTensionalNocturno_2.Name = "decimalMediaTensionalNocturno_2";
            this.decimalMediaTensionalNocturno_2.Size = new System.Drawing.Size(411, 34);
            this.decimalMediaTensionalNocturno_2.TabIndex = 144;
            // 
            // ddhtap_2
            // 
            this.ddhtap_2.Location = new System.Drawing.Point(439, 48);
            this.ddhtap_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddhtap_2.Name = "ddhtap_2";
            this.ddhtap_2.Size = new System.Drawing.Size(414, 30);
            this.ddhtap_2.TabIndex = 115;
            // 
            // decimalPicoTensionalNocturno_2
            // 
            this.decimalPicoTensionalNocturno_2.Location = new System.Drawing.Point(440, 547);
            this.decimalPicoTensionalNocturno_2.Name = "decimalPicoTensionalNocturno_2";
            this.decimalPicoTensionalNocturno_2.Size = new System.Drawing.Size(411, 34);
            this.decimalPicoTensionalNocturno_2.TabIndex = 143;
            // 
            // ddfc_2
            // 
            this.ddfc_2.Location = new System.Drawing.Point(439, 84);
            this.ddfc_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfc_2.Name = "ddfc_2";
            this.ddfc_2.Size = new System.Drawing.Size(414, 30);
            this.ddfc_2.TabIndex = 116;
            // 
            // decimalMediaTensionalDiurno_2
            // 
            this.decimalMediaTensionalDiurno_2.Location = new System.Drawing.Point(440, 443);
            this.decimalMediaTensionalDiurno_2.Name = "decimalMediaTensionalDiurno_2";
            this.decimalMediaTensionalDiurno_2.Size = new System.Drawing.Size(411, 30);
            this.decimalMediaTensionalDiurno_2.TabIndex = 140;
            // 
            // ddritmo_2
            // 
            this.ddritmo_2.Location = new System.Drawing.Point(439, 120);
            this.ddritmo_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddritmo_2.Name = "ddritmo_2";
            this.ddritmo_2.Size = new System.Drawing.Size(414, 30);
            this.ddritmo_2.TabIndex = 117;
            // 
            // deciamlPicoTensionalDiurno_2
            // 
            this.deciamlPicoTensionalDiurno_2.Location = new System.Drawing.Point(20, 588);
            this.deciamlPicoTensionalDiurno_2.Name = "deciamlPicoTensionalDiurno_2";
            this.deciamlPicoTensionalDiurno_2.Size = new System.Drawing.Size(411, 29);
            this.deciamlPicoTensionalDiurno_2.TabIndex = 139;
            // 
            // ddbetabloqueantespre_2
            // 
            this.ddbetabloqueantespre_2.Location = new System.Drawing.Point(439, 156);
            this.ddbetabloqueantespre_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbetabloqueantespre_2.Name = "ddbetabloqueantespre_2";
            this.ddbetabloqueantespre_2.Size = new System.Drawing.Size(414, 30);
            this.ddbetabloqueantespre_2.TabIndex = 118;
            // 
            // ddbetabloqueantespost_2
            // 
            this.ddbetabloqueantespost_2.Location = new System.Drawing.Point(439, 192);
            this.ddbetabloqueantespost_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbetabloqueantespost_2.Name = "ddbetabloqueantespost_2";
            this.ddbetabloqueantespost_2.Size = new System.Drawing.Size(414, 30);
            this.ddbetabloqueantespost_2.TabIndex = 119;
            // 
            // ddiecapre_2
            // 
            this.ddiecapre_2.Location = new System.Drawing.Point(439, 228);
            this.ddiecapre_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddiecapre_2.Name = "ddiecapre_2";
            this.ddiecapre_2.Size = new System.Drawing.Size(414, 30);
            this.ddiecapre_2.TabIndex = 120;
            // 
            // deciamlCreatininaAlta_2
            // 
            this.deciamlCreatininaAlta_2.Location = new System.Drawing.Point(20, 515);
            this.deciamlCreatininaAlta_2.Name = "deciamlCreatininaAlta_2";
            this.deciamlCreatininaAlta_2.Size = new System.Drawing.Size(411, 30);
            this.deciamlCreatininaAlta_2.TabIndex = 137;
            // 
            // ddiecapost_2
            // 
            this.ddiecapost_2.Location = new System.Drawing.Point(439, 264);
            this.ddiecapost_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddiecapost_2.Name = "ddiecapost_2";
            this.ddiecapost_2.Size = new System.Drawing.Size(414, 30);
            this.ddiecapost_2.TabIndex = 121;
            // 
            // decimalCreatininiIngreso_2
            // 
            this.decimalCreatininiIngreso_2.Location = new System.Drawing.Point(20, 479);
            this.decimalCreatininiIngreso_2.Name = "decimalCreatininiIngreso_2";
            this.decimalCreatininiIngreso_2.Size = new System.Drawing.Size(411, 30);
            this.decimalCreatininiIngreso_2.TabIndex = 136;
            // 
            // ddamiodaronapre_2
            // 
            this.ddamiodaronapre_2.Location = new System.Drawing.Point(439, 300);
            this.ddamiodaronapre_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddamiodaronapre_2.Name = "ddamiodaronapre_2";
            this.ddamiodaronapre_2.Size = new System.Drawing.Size(414, 30);
            this.ddamiodaronapre_2.TabIndex = 122;
            // 
            // ddamiodaronapost_2
            // 
            this.ddamiodaronapost_2.Location = new System.Drawing.Point(439, 336);
            this.ddamiodaronapost_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddamiodaronapost_2.Name = "ddamiodaronapost_2";
            this.ddamiodaronapost_2.Size = new System.Drawing.Size(414, 30);
            this.ddamiodaronapost_2.TabIndex = 123;
            // 
            // ddexitus_2
            // 
            this.ddexitus_2.Location = new System.Drawing.Point(872, 300);
            this.ddexitus_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddexitus_2.Name = "ddexitus_2";
            this.ddexitus_2.Size = new System.Drawing.Size(414, 30);
            this.ddexitus_2.TabIndex = 133;
            this.ddexitus_2.Load += new System.EventHandler(this.ddexitus_2_Load);
            // 
            // ddnyha_2
            // 
            this.ddnyha_2.Location = new System.Drawing.Point(439, 372);
            this.ddnyha_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddnyha_2.Name = "ddnyha_2";
            this.ddnyha_2.Size = new System.Drawing.Size(414, 30);
            this.ddnyha_2.TabIndex = 124;
            // 
            // ddstatus_2
            // 
            this.ddstatus_2.Location = new System.Drawing.Point(872, 264);
            this.ddstatus_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddstatus_2.Name = "ddstatus_2";
            this.ddstatus_2.Size = new System.Drawing.Size(414, 30);
            this.ddstatus_2.TabIndex = 132;
            // 
            // dddigoxinapost_2
            // 
            this.dddigoxinapost_2.Location = new System.Drawing.Point(872, 9);
            this.dddigoxinapost_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddigoxinapost_2.Name = "dddigoxinapost_2";
            this.dddigoxinapost_2.Size = new System.Drawing.Size(414, 30);
            this.dddigoxinapost_2.TabIndex = 125;
            // 
            // ddingresos6_2
            // 
            this.ddingresos6_2.Location = new System.Drawing.Point(872, 228);
            this.ddingresos6_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddingresos6_2.Name = "ddingresos6_2";
            this.ddingresos6_2.Size = new System.Drawing.Size(414, 30);
            this.ddingresos6_2.TabIndex = 131;
            // 
            // ddantialdosteronicospre_2
            // 
            this.ddantialdosteronicospre_2.Location = new System.Drawing.Point(872, 45);
            this.ddantialdosteronicospre_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddantialdosteronicospre_2.Name = "ddantialdosteronicospre_2";
            this.ddantialdosteronicospre_2.Size = new System.Drawing.Size(414, 30);
            this.ddantialdosteronicospre_2.TabIndex = 126;
            // 
            // ddpatron_2
            // 
            this.ddpatron_2.Location = new System.Drawing.Point(872, 192);
            this.ddpatron_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddpatron_2.Name = "ddpatron_2";
            this.ddpatron_2.Size = new System.Drawing.Size(414, 30);
            this.ddpatron_2.TabIndex = 130;
            // 
            // ddantialdosteronicospost_2
            // 
            this.ddantialdosteronicospost_2.Location = new System.Drawing.Point(872, 81);
            this.ddantialdosteronicospost_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddantialdosteronicospost_2.Name = "ddantialdosteronicospost_2";
            this.ddantialdosteronicospost_2.Size = new System.Drawing.Size(414, 30);
            this.ddantialdosteronicospost_2.TabIndex = 127;
            // 
            // ddregistro_2
            // 
            this.ddregistro_2.Location = new System.Drawing.Point(872, 156);
            this.ddregistro_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddregistro_2.Name = "ddregistro_2";
            this.ddregistro_2.Size = new System.Drawing.Size(414, 30);
            this.ddregistro_2.TabIndex = 129;
            // 
            // ddmapa_2
            // 
            this.ddmapa_2.Location = new System.Drawing.Point(872, 120);
            this.ddmapa_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddmapa_2.Name = "ddmapa_2";
            this.ddmapa_2.Size = new System.Drawing.Size(414, 30);
            this.ddmapa_2.TabIndex = 128;
            // 
            // FrmCreateOrUpdateICC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1292, 1026);
            this.Controls.Add(this.pnlBotonera);
            this.Controls.Add(this.pageviewICC);
            this.Controls.Add(this.radPanel1);
            this.MinimumSize = new System.Drawing.Size(1300, 858);
            this.Name = "FrmCreateOrUpdateICC";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "FrmCreateOrUpdateICC";
            this.Load += new System.EventHandler(this.FrmCreateOrUpdateICC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnlCausaExitus_2)).EndInit();
            this.pnlCausaExitus_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCausaExitus_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtnombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnhc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSexo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfechaingreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNHC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSexo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPacienteToAssing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageviewICC)).EndInit();
            this.pageviewICC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlBotonera)).EndInit();
            this.pnlBotonera.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MD_Controls.InputTime inputTimehorapicotensional_diurno_2;
        private MD_Controls.InputTime inputTimehorapicotensional_nocturno_2;
        private MD_Controls.InputDecimals decimalMediaFCDuranteIngreso_2;
        private MD_Controls.DropDownMedical ddfevi_2;
        private MD_Controls.DropDownMedical dddigoxinapre_2;
        private MD_Controls.DropDownMedical ddeco_2;
        private MD_Controls.InputNumeric numericSumFraminghanMenores_2;
        private MD_Controls.InputNumeric numericFCalIngreso_2;
        private MD_Controls.InputNumeric numericSumFraminghanMayores_2;
        private MD_Controls.InputDecimals decimalMediaTensionalNocturno_2;
        private MD_Controls.InputDecimals decimalPicoTensionalNocturno_2;
        private MD_Controls.InputDecimals decimalMediaTensionalDiurno_2;
        private MD_Controls.InputDecimals deciamlPicoTensionalDiurno_2;
        private MD_Controls.InputDecimals deciamlCreatininaAlta_2;
        private MD_Controls.InputDecimals decimalCreatininiIngreso_2;
        private Telerik.WinControls.UI.RadPanel pnlCausaExitus_2;
        private Telerik.WinControls.UI.RadTextBoxControl txtCausaExitus_2;
        private MD_Controls.DropDownMedical ddexitus_2;
        private MD_Controls.DropDownMedical ddstatus_2;
        private MD_Controls.DropDownMedical ddingresos6_2;
        private MD_Controls.DropDownMedical ddpatron_2;
        private MD_Controls.DropDownMedical ddregistro_2;
        private MD_Controls.DropDownMedical ddmapa_2;
        private MD_Controls.DropDownMedical ddantialdosteronicospost_2;
        private MD_Controls.DropDownMedical ddantialdosteronicospre_2;
        private MD_Controls.DropDownMedical dddigoxinapost_2;
        private MD_Controls.DropDownMedical ddnyha_2;
        private MD_Controls.DropDownMedical ddamiodaronapost_2;
        private MD_Controls.DropDownMedical ddamiodaronapre_2;
        private MD_Controls.DropDownMedical ddiecapost_2;
        private MD_Controls.DropDownMedical ddiecapre_2;
        private MD_Controls.DropDownMedical ddbetabloqueantespost_2;
        private MD_Controls.DropDownMedical ddbetabloqueantespre_2;
        private MD_Controls.DropDownMedical ddritmo_2;
        private MD_Controls.DropDownMedical ddfc_2;
        private MD_Controls.DropDownMedical ddhtap_2;
        private MD_Controls.DropDownMedical ddvalvulopatia_2;
        private MD_Controls.DropDownMedical ddta_2;
        private MD_Controls.DropDownMedical ddbasal_2;
        private MD_Controls.DropDownMedical ddecocardiograma_2;
        private MD_Controls.DropDownMedical ddanticoagulacion_2;
        private MD_Controls.DropDownMedical ddfa_2;
        private MD_Controls.DropDownMedical ddicc_2;
        private MD_Controls.DropDownMedical ddsincopes_2;
        private MD_Controls.DropDownMedical ddcardiopatia_2;
        private MD_Controls.DropDownMedical dddislipemia_2;
        private MD_Controls.DropDownMedical dddiabetes_2;
        private MD_Controls.DropDownMedical ddhta_2;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel pageviewICC;
        private Telerik.WinControls.UI.RadPanel pnlBotonera;
        private Telerik.WinControls.UI.RadButton btnExit;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadLabel lblPacienteToAssing;
        private Telerik.WinControls.UI.RadDropDownList ddPacientes;
        private Telerik.WinControls.UI.RadLabel lblNombre;
        private Telerik.WinControls.UI.RadLabel lblSexo;
        private Telerik.WinControls.UI.RadLabel txtFechaAlta;
        private Telerik.WinControls.UI.RadLabel lblFechaAlta;
        private Telerik.WinControls.UI.RadLabel txtFechaNacimiento;
        private Telerik.WinControls.UI.RadLabel lblFechaNacimiento;
        private Telerik.WinControls.UI.RadLabel txtFechaIngreso;
        private Telerik.WinControls.UI.RadLabel txtnhc;
        private Telerik.WinControls.UI.RadLabel txtSexo;
        private Telerik.WinControls.UI.RadLabel lblfechaingreso;
        private Telerik.WinControls.UI.RadLabel lblNHC;
        private Telerik.WinControls.UI.RadLabel txtnombre;
    }
}
