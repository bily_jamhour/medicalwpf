﻿using MD_Controls;
using medical_Data_Lib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Linq;
namespace medicalWPF.MD_Forms.Management.ICC
{
    public partial class FrmCreateOrUpdateICC : RadForm
    {
        private md_icc _rowMDICCToEdit = new md_icc();

        public FrmCreateOrUpdateICC(md_icc rmdICCEdit = null)
        {
            InitializeComponent();
            _rowMDICCToEdit = rmdICCEdit;
        }

        private void FrmCreateOrUpdateICC_Load(object sender, EventArgs e)
        {
            ddPacientes.ValueMember = "md_record_id";
            ddPacientes.DisplayMember = "md_nombre";
            ddPacientes.DataSource = MDCommonData.getIdAndNamePacientesInNotICC();
            txtnombre.Visible = false;
            loadDropDownsICC();
            setLang();
            if (_rowMDICCToEdit == null)
                ddPacientes.Visible = true;
            else
                LoadControls();

        }

        private void LoadControls()
        {
            ddPacientes.Visible = false;
            txtnombre.Visible = true;
            loadDataICC();
        }
        private void loadDataPaciente()
        {
            md_pacientes paciente = null;
            if (_rowMDICCToEdit != null)
                paciente = MdPacientes.GetPacienteFromRecordId(_rowMDICCToEdit.md_record_id);
            else
                paciente = MdPacientes.GetPacienteFromRecordId(Convert.ToInt32(ddPacientes.SelectedValue));

            if (paciente != null)
            {
                txtnombre.Text = paciente.md_nombre;
                txtFechaIngreso.Text = Convert.ToString(paciente.md_fechaingreso);
                txtnhc.Text = paciente.md_nhc.ToString();
                txtSexo.Text = (paciente.md_sexo == 0) ? "Hombre" : "Mujer";
                txtFechaNacimiento.Text = Convert.ToString(paciente.md_fechanacimiento);
                txtFechaAlta.Text = Convert.ToString(paciente.md_fechaalta);
            }
        }
        private void loadDataICC()
        {
            loadDataPaciente();
            ddhtap_2.SetValue(_rowMDICCToEdit.md_htap.ToString());
            ddamiodaronapost_2.SetValue(_rowMDICCToEdit.md_amiodaronapost.ToString());
            ddamiodaronapre_2.SetValue(_rowMDICCToEdit.md_amiodaronapre.ToString());
            ddanticoagulacion_2.SetValue(_rowMDICCToEdit.md_anticoagulacioningreso.ToString());
            ddbasal_2.SetValue(_rowMDICCToEdit.md_clasefuncionalbasal.ToString());
            ddbetabloqueantespost_2.SetValue(_rowMDICCToEdit.md_betabloqueantespost.ToString());
            ddbetabloqueantespre_2.SetValue(_rowMDICCToEdit.md_betabloqueantespre.ToString());
            ddcardiopatia_2.SetValue(_rowMDICCToEdit.md_cardiopatiaisquemica.ToString());
            dddiabetes_2.SetValue(_rowMDICCToEdit.md_diabetesmellitus.ToString());
            dddigoxinapost_2.SetValue(_rowMDICCToEdit.md_digoxinapost.ToString());
            dddigoxinapre_2.SetValue(_rowMDICCToEdit.md_digoxinapre.ToString());
            dddislipemia_2.SetValue(_rowMDICCToEdit.md_dislipemia.ToString());
            ddecocardiograma_2.SetValue(_rowMDICCToEdit.md_ecocardiograma.ToString());
            ddeco_2.SetValue(_rowMDICCToEdit.md_ecocardiogramaduranteingreso.ToString());
            ddexitus_2.SetValue(_rowMDICCToEdit.md_exitus.ToString());
            txtCausaExitus_2.Text = _rowMDICCToEdit.md_exitusdesc;
            ddfa_2.SetValue(_rowMDICCToEdit.md_antecedentesfa.ToString());
            ddfc_2.SetValue(_rowMDICCToEdit.md_fccontrolada.ToString());
            ddfevi_2.SetValue(_rowMDICCToEdit.md_fevi.ToString());
            ddhta_2.SetValue(_rowMDICCToEdit.md_hta.ToString());
            ddicc_2.SetValue(_rowMDICCToEdit.md_ingresosicc.ToString());
            ddiecapost_2.SetValue(_rowMDICCToEdit.md_iecapost.ToString());
            ddiecapre_2.SetValue(_rowMDICCToEdit.md_iecapre.ToString());
            ddingresos6_2.SetValue(_rowMDICCToEdit.md_ingresos.ToString());
            ddmapa_2.SetValue(_rowMDICCToEdit.md_mapa.ToString());
            ddnyha_2.SetValue(_rowMDICCToEdit.md_nyha.ToString());
            ddpatron_2.SetValue(_rowMDICCToEdit.md_patron.ToString());
            ddregistro_2.SetValue(_rowMDICCToEdit.md_registro.ToString());
            ddritmo_2.SetValue(_rowMDICCToEdit.md_ritmoalta.ToString());
            ddsincopes_2.SetValue(_rowMDICCToEdit.md_historialsincopes.ToString());
            ddstatus_2.SetValue(_rowMDICCToEdit.md_status.ToString());
            ddta_2.SetValue(_rowMDICCToEdit.md_taingreso.ToString());
            ddvalvulopatia_2.SetValue(_rowMDICCToEdit.md_valvulopatia.ToString());
            deciamlPicoTensionalDiurno_2.setValue(_rowMDICCToEdit.md_picotensional_diurno.ToString());
            deciamlCreatininaAlta_2.setValue(_rowMDICCToEdit.md_creatininaalta.ToString());
            decimalCreatininiIngreso_2.setValue(_rowMDICCToEdit.md_creatininaingreso.ToString());
            decimalMediaFCDuranteIngreso_2.setValue(_rowMDICCToEdit.md_mediafcduranteregistro.ToString());
            decimalMediaTensionalDiurno_2.setValue(_rowMDICCToEdit.md_mediatensional_diurno.ToString());
            decimalMediaTensionalNocturno_2.setValue(_rowMDICCToEdit.md_mediatensional_nocturno.ToString());
            decimalPicoTensionalNocturno_2.setValue(_rowMDICCToEdit.md_picotensional_nocturno.ToString());
            if (_rowMDICCToEdit.md_horapicotensional_diurno != null)
                inputTimehorapicotensional_diurno_2.SetValue(_rowMDICCToEdit.md_horapicotensional_diurno.Value);
            if (_rowMDICCToEdit.md_horapicotensional_nocturno != null)
                inputTimehorapicotensional_nocturno_2.SetValue(_rowMDICCToEdit.md_horapicotensional_nocturno.Value);

            numericSumFraminghanMayores_2.SetValue(_rowMDICCToEdit.md_sumframinghanmayor.ToString());
            numericSumFraminghanMenores_2.SetValue(_rowMDICCToEdit.md_sumframinghanmenor.ToString());
            numericFCalIngreso_2.SetValue(_rowMDICCToEdit.md_fcingreso.ToString());
            ddantialdosteronicospost_2.SetValue(_rowMDICCToEdit.md_antialdosteronicospost.ToString());
            ddantialdosteronicospre_2.SetValue(_rowMDICCToEdit.md_antialdosteronicospre.ToString());
        }

        private void loadDropDownsICC()
        {
            foreach (var ctrl in pageviewICC.Controls)
            {
                if (ctrl is DropDownMedical)
                {
                    string sNameDropdown = (ctrl as DropDownMedical).Name.Replace("dd", "").ToUpper();
                    if (sNameDropdown != "")
                    {
                        (ctrl as DropDownMedical).SetTypeDropDownList(sNameDropdown);
                    }
                }
            }
        }

        private void setLang()
        {
            this.Text = "Medical Management-Gestión de ICC";
            lblPacienteToAssing.Text = "Paciente a asignar";
            lblNombre.Text = "Nombre";
            lblFechaAlta.Text = "Fecha alta";
            lblfechaingreso.Text = "Fecha ingreso";
            lblFechaNacimiento.Text = "Fecha nacimiento";
            lblNHC.Text = "NHC";
            lblSexo.Text = "Sexo";
            btnExit.Text = "Salir";
            btnSave.Text = "Guardar";
            decimalCreatininiIngreso_2.SetTitle("Creatinina en Ingreso");

            deciamlCreatininaAlta_2.SetTitle("Creatinina en Alta");

            deciamlPicoTensionalDiurno_2.SetTitle("Pico Tensional Diurno");



            decimalMediaTensionalDiurno_2.SetTitle("Media Tensional Diurno");

            decimalMediaTensionalNocturno_2.SetTitle("Media Tensional Nocturno");

            decimalPicoTensionalNocturno_2.SetTitle("Pico Tensional Nocturno");

            decimalMediaFCDuranteIngreso_2.SetTitle("Media FC durante Ingreso");
            numericFCalIngreso_2.SetTitle("FC en Ingreso");


            numericSumFraminghanMayores_2.SetTitle("Suma Niv.Framinghan Mayores");

            numericSumFraminghanMenores_2.SetTitle("Suma Niv.Framinghan Menores");


            inputTimehorapicotensional_nocturno_2.SetTitle("Hora Pico Tensional Nocturno");
            inputTimehorapicotensional_diurno_2.SetTitle("Hora Pico Tensional Diurno");


        }

        private void ddexitus_2_Load(object sender, EventArgs e)
        {
            ddexitus_2.SelectionChanged(ddexitus_2_SelectionChanged);
        }
        private void ddexitus_2_SelectionChanged(object sender, EventArgs e)
        {
            if (ddexitus_2.GetValue() == "2")
            {
                pnlCausaExitus_2.Enabled = true;
            }
            else
            {
                pnlCausaExitus_2.Enabled = false;
                txtCausaExitus_2.Text = "";
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == RadMessageBox.Show("Los cambios no almacenados se perderán. ¿Desea salir?", this.Text, MessageBoxButtons.YesNo, RadMessageIcon.Question))
                this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string sMessageValidation = "";
            md_icc mdICCToUpdate = new md_icc();
            mdICCToUpdate = new md_icc
            {
                md_amiodaronapost = Convert.ToInt32(ddamiodaronapost_2.GetValue()),
                md_amiodaronapre = Convert.ToInt32(ddamiodaronapre_2.GetValue()),
                md_antecedentesfa = Convert.ToInt32(ddfa_2.GetValue()),
                md_antialdosteronicospost = Convert.ToInt32(ddantialdosteronicospost_2.GetValue()),
                md_antialdosteronicospre = Convert.ToInt32(ddantialdosteronicospre_2.GetValue()),
                md_anticoagulacioningreso = Convert.ToInt32(ddanticoagulacion_2.GetValue()),
                md_betabloqueantespost = Convert.ToInt32(ddbetabloqueantespost_2.GetValue()),
                md_betabloqueantespre = Convert.ToInt32(ddbetabloqueantespre_2.GetValue()),
                md_cardiopatiaisquemica = Convert.ToInt32(ddcardiopatia_2.GetValue()),
                md_clasefuncionalbasal = Convert.ToInt32(ddbasal_2.GetValue()),
                md_creatininaalta = deciamlCreatininaAlta_2.getValue(),
                md_creatininaingreso = decimalCreatininiIngreso_2.getValue(),
                md_diabetesmellitus = Convert.ToInt32(dddiabetes_2.GetValue()),
                md_digoxinapost = Convert.ToInt32(dddigoxinapost_2.GetValue()),
                md_digoxinapre = Convert.ToInt32(dddigoxinapre_2.GetValue()),
                md_dislipemia = Convert.ToInt32(dddislipemia_2.GetValue()),
                md_ecocardiograma = Convert.ToInt32(ddecocardiograma_2.GetValue()),
                md_ecocardiogramaduranteingreso = Convert.ToInt32(ddeco_2.GetValue()),
                md_exitus = Convert.ToInt32(ddexitus_2.GetValue()),
                md_exitusdesc = txtCausaExitus_2.Text,
                md_fccontrolada = Convert.ToInt32(ddfc_2.GetValue()),
                md_fcingreso = numericFCalIngreso_2.Value(),
                md_fevi = Convert.ToInt32(ddfevi_2.GetValue()),
                md_historialsincopes = Convert.ToInt32(ddsincopes_2.GetValue()),
                md_horapicotensional_diurno = inputTimehorapicotensional_diurno_2.GetTime(),// DateTime.Now,
                md_horapicotensional_nocturno = inputTimehorapicotensional_nocturno_2.GetTime(),
                md_hta = Convert.ToInt32(ddhta_2.GetValue()),
                md_htap = Convert.ToInt32(ddhtap_2.GetValue()),
                md_iecapost = Convert.ToInt32(ddiecapost_2.GetValue()),
                md_iecapre = Convert.ToInt32(ddiecapre_2.GetValue()),
                md_ingresos = Convert.ToInt32(ddingresos6_2.GetValue()),
                md_ingresosicc = Convert.ToInt32(ddicc_2.GetValue()),
                md_mapa = Convert.ToInt32(ddmapa_2.GetValue()),
                md_mediafcduranteregistro = decimalMediaFCDuranteIngreso_2.getValue(),
                md_mediatensional_diurno = decimalMediaTensionalDiurno_2.getValue(),
                md_mediatensional_nocturno = decimalMediaTensionalNocturno_2.getValue(),
                md_nyha = Convert.ToInt32(ddnyha_2.GetValue()),
                md_record_id = (_rowMDICCToEdit == null) ? Convert.ToInt32(ddPacientes.SelectedValue) : _rowMDICCToEdit.md_record_id,
                md_patron = Convert.ToInt32(ddpatron_2.GetValue()),
                md_picotensional_diurno = deciamlPicoTensionalDiurno_2.getValue(),
                md_picotensional_nocturno = decimalPicoTensionalNocturno_2.getValue(),
                md_registro = Convert.ToInt32(ddregistro_2.GetValue()),
                md_ritmoalta = Convert.ToInt32(ddritmo_2.GetValue()),
                md_status = Convert.ToInt32(ddstatus_2.GetValue()),
                md_sumframinghanmayor = numericSumFraminghanMayores_2.Value(),
                md_sumframinghanmenor = numericSumFraminghanMenores_2.Value(),
                md_taingreso = Convert.ToInt32(ddta_2.GetValue()),
                md_valvulopatia = Convert.ToInt32(ddvalvulopatia_2.GetValue())

            };
            if (_rowMDICCToEdit == null)
                sMessageValidation = MdICC.AddICC(Convert.ToInt32(ddPacientes.SelectedValue), mdICCToUpdate);
            else
                sMessageValidation = MdICC.UpdateICC(Convert.ToInt32(_rowMDICCToEdit.md_record_id), mdICCToUpdate);
            if (sMessageValidation != "")
            {
                RadMessageBox.Show(this, sMessageValidation, this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            this.Close();
        }

        private void ddPacientes_SelectedValueChanged(object sender, EventArgs e)
        {
            loadDataPaciente();
        }
    }
}
