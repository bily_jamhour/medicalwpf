﻿
using medical_Data_Lib;
using medical_Infrastructure.Utilities;
using medicalWPF.MD_Forms.Management.Pacientes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
namespace medicalWPF.MD_Forms.Management
{
    public partial class FrmManagePacientes : RadForm
    {
        public FrmManagePacientes()
        {
            InitializeComponent();
        }
        private md_pacientes filterData = null;
        private bool filtered = false;
        private void FrmManagePacientes_Load(object sender, EventArgs e)
        {
            setLang();
            ddSexo.SetTypeDropDownList("SEX");
        }

        private void setLang()
        {
            this.Text = "Medical Management-Gestión de Pacientes";
            inputFechaAlta.SetTitle("Fecha Alta");
            inputFechaIngreso.SetTitle("Fecha Ingreso");
            inputFechaNacimiento.SetTitle("Fecha Nacimiento");
            lblNombre.Text = "Nombre";// "Nombre";
            inputNHC.SetTitle("NHC");
            btnExit.Text = "Salir";
            btnFilter.Text = "Buscar";// "Buscar";
            btnClear.Text = "Limpiar";// "Limpiar";
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (!gridPacientes.SelectedRows.Any())
            {
                RadMessageBox.Show("No ha seleccionado ningún registro.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            else if (gridPacientes.SelectedRows.Count > 1)
            {
                RadMessageBox.Show("Debe seleccionar un único registro.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            int iRecordId = 0;
            int.TryParse(gridPacientes.SelectedRows.FirstOrDefault().Cells["md_record_id"].Value.ToString(), out iRecordId);
            if (iRecordId == 0)
            {
                RadMessageBox.Show("Record Id no permitido.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            else
            {
                FrmUpdatePacientes frmActualizacion = new FrmUpdatePacientes(iRecordId);
                frmActualizacion.ShowDialog(this);
                gridPacientes.DataSource = MdPacientes.GetPacientesFromFiltering((filtered == true) ? filterData : null);

            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            filterData = new md_pacientes();
            filtered = false;
            if (inputFechaAlta.enabled)
            {
                filterData.md_fechaalta = inputFechaAlta.getDate();
                filtered = true;
            }
            if (inputFechaIngreso.enabled)
            {
                filterData.md_fechaingreso = inputFechaIngreso.getDate();
                filtered = true;
            }
            if (inputFechaNacimiento.enabled)
            {
                filterData.md_fechanacimiento = inputFechaNacimiento.getDate();
                filtered = true;
            }
            if (txtNombre.Text != "")
            {
                filterData.md_nombre = txtNombre.Text;
                filtered = true;
            }
            if (inputNHC.Value() != 0)
            {
                filterData.md_nhc = inputNHC.Value();
                filtered = true;
            }
            filterData.md_sexo = Convert.ToInt32(ddSexo.GetValue());
            if (filterData.md_sexo >= 0)
                filtered = true;
            if (!filtered)
            {
                if (DialogResult.No == RadMessageBox.Show(" Si no utiliza el filtro principal, la consulta puede tardar en ejecutarse. ¿Desea continuar?", this.Text, MessageBoxButtons.YesNo, RadMessageIcon.Question))
                    return;

            }
            gridPacientes.DataSource = MdPacientes.GetPacientesFromFiltering((!filtered) ? null : filterData);
            // gridPacientes.CellFormatting += gridPacientes_CellFormatting;
        }

        private void gridPacientes_DataError(object sender, GridViewDataErrorEventArgs e)
        {
            var caca = e.Context;
        }


        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (!gridPacientes.SelectedRows.Any())
            {
                RadMessageBox.Show("No ha seleccionado ningún registro.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            else
            {
                if (DialogResult.No == RadMessageBox.Show("Eliminará del sistema los siguientes usuarios, junto con sus ICC y SCA:" + Environment.NewLine +
                       gridPacientes.SelectedRows.Select(x => x.Cells["md_nombre"].Value.ToString()).ToList().JoinAsParameterWithoutQuotes() + Environment.NewLine
                       + "¿Desea continuar?", this.Text, MessageBoxButtons.YesNo, RadMessageIcon.Question))
                    return;

                foreach (int iRecordId in gridPacientes.SelectedRows.Select(x => Convert.ToInt32(x.Cells["md_record_id"].Value)).ToList())
                {
                    MdSCA.RemoveSCA(iRecordId);
                    MdICC.RemoveICC(iRecordId);
                    MdPacientes.RemovePaciente(iRecordId);
                    
                }
                gridPacientes.DataSource = MdPacientes.GetPacientesFromFiltering((filtered == true) ? filterData : null);

            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmCreatePacientes frCreate = new FrmCreatePacientes();
            frCreate.ShowDialog(this);
            gridPacientes.DataSource = MdPacientes.GetPacientesFromFiltering((filtered == true) ? filterData : null);

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtNombre.Text = "";
            inputNHC.Clear();
            ddSexo.Clear();
            inputFechaAlta.Clear();
            inputFechaIngreso.Clear();
            inputFechaNacimiento.Clear();
        }
    }
}
