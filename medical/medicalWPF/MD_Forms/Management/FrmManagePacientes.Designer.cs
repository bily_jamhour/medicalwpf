﻿namespace medicalWPF.MD_Forms.Management
{
    partial class FrmManagePacientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.btnClear = new Telerik.WinControls.UI.RadButton();
            this.btnFilter = new Telerik.WinControls.UI.RadButton();
            this.ddSexo = new MD_Controls.DropDownMedical();
            this.inputNHC = new MD_Controls.InputNumeric();
            this.inputFechaAlta = new MD_Controls.InputDate();
            this.inputFechaIngreso = new MD_Controls.InputDate();
            this.inputFechaNacimiento = new MD_Controls.InputDate();
            this.txtNombre = new Telerik.WinControls.UI.RadTextBox();
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.divGrid = new Telerik.WinControls.UI.RadPanel();
            this.gridPacientes = new Telerik.WinControls.UI.RadGridView();
            this.divBotonesGrid = new Telerik.WinControls.UI.RadPanel();
            this.btnNew = new Telerik.WinControls.UI.RadButton();
            this.btnEdit = new Telerik.WinControls.UI.RadButton();
            this.btnRemove = new Telerik.WinControls.UI.RadButton();
            this.divBotonera = new Telerik.WinControls.UI.RadPanel();
            this.btnExit = new Telerik.WinControls.UI.RadButton();
            this.office2010BlueTheme1 = new Telerik.WinControls.Themes.Office2010BlueTheme();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.divGrid)).BeginInit();
            this.divGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPacientes.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.divBotonesGrid)).BeginInit();
            this.divBotonesGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.divBotonera)).BeginInit();
            this.divBotonera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel1.EnableAnimation = false;
            this.radCollapsiblePanel1.HeaderText = "Filtros";
            this.radCollapsiblePanel1.HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Right;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 992, 200);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.btnClear);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.btnFilter);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.ddSexo);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.inputNHC);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.inputFechaAlta);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.inputFechaIngreso);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.inputFechaNacimiento);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.txtNombre);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.lblNombre);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(796, 137);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(798, 165);
            this.radCollapsiblePanel1.TabIndex = 0;
            this.radCollapsiblePanel1.Text = "radCollapsiblePanel1";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Image = global::medicalWPF.Properties.Resources.Clear;
            this.btnClear.Location = new System.Drawing.Point(684, 42);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(110, 33);
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "[btnClear]";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnFilter
            // 
            this.btnFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilter.Image = global::medicalWPF.Properties.Resources.Find1;
            this.btnFilter.Location = new System.Drawing.Point(684, 3);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(110, 33);
            this.btnFilter.TabIndex = 15;
            this.btnFilter.Text = "[btnFilter]";
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // ddSexo
            // 
            this.ddSexo.Location = new System.Drawing.Point(16, 87);
            this.ddSexo.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddSexo.Name = "ddSexo";
            this.ddSexo.Size = new System.Drawing.Size(278, 30);
            this.ddSexo.TabIndex = 14;
            // 
            // inputNHC
            // 
            this.inputNHC.Location = new System.Drawing.Point(16, 51);
            this.inputNHC.Name = "inputNHC";
            this.inputNHC.Size = new System.Drawing.Size(278, 30);
            this.inputNHC.TabIndex = 13;
            // 
            // inputFechaAlta
            // 
            this.inputFechaAlta.Location = new System.Drawing.Point(347, 87);
            this.inputFechaAlta.Name = "inputFechaAlta";
            this.inputFechaAlta.Size = new System.Drawing.Size(313, 30);
            this.inputFechaAlta.TabIndex = 12;
            // 
            // inputFechaIngreso
            // 
            this.inputFechaIngreso.Location = new System.Drawing.Point(347, 51);
            this.inputFechaIngreso.Name = "inputFechaIngreso";
            this.inputFechaIngreso.Size = new System.Drawing.Size(313, 30);
            this.inputFechaIngreso.TabIndex = 11;
            // 
            // inputFechaNacimiento
            // 
            this.inputFechaNacimiento.Location = new System.Drawing.Point(347, 15);
            this.inputFechaNacimiento.Name = "inputFechaNacimiento";
            this.inputFechaNacimiento.Size = new System.Drawing.Size(313, 30);
            this.inputFechaNacimiento.TabIndex = 10;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(186, 15);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(99, 20);
            this.txtNombre.TabIndex = 8;
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(16, 15);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(93, 18);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Nombre paciente";
            // 
            // divGrid
            // 
            this.divGrid.Controls.Add(this.gridPacientes);
            this.divGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.divGrid.Location = new System.Drawing.Point(0, 165);
            this.divGrid.Name = "divGrid";
            this.divGrid.Size = new System.Drawing.Size(756, 405);
            this.divGrid.TabIndex = 1;
            this.divGrid.Text = "radPanel1";
            // 
            // gridPacientes
            // 
            this.gridPacientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPacientes.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridPacientes.MasterTemplate.AllowAddNewRow = false;
            this.gridPacientes.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.FieldName = "md_record_id";
            gridViewTextBoxColumn1.HeaderText = "md_record_id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "md_record_id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.FieldName = "md_nombre";
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "md_nombre";
            gridViewTextBoxColumn2.Width = 126;
            gridViewTextBoxColumn3.DataType = typeof(int);
            gridViewTextBoxColumn3.FieldName = "md_nhc";
            gridViewTextBoxColumn3.HeaderText = "NHC";
            gridViewTextBoxColumn3.Name = "md_nhc";
            gridViewTextBoxColumn3.Width = 126;
            gridViewTextBoxColumn4.FieldName = "md_sexo";
            gridViewTextBoxColumn4.HeaderText = "Sexo";
            gridViewTextBoxColumn4.Name = "md_sexo";
            gridViewTextBoxColumn4.VisibleInColumnChooser = false;
            gridViewTextBoxColumn4.Width = 126;
            gridViewTextBoxColumn5.FieldName = "md_fechanacimiento";
            gridViewTextBoxColumn5.HeaderText = "Fecha Nacimiento";
            gridViewTextBoxColumn5.Name = "md_fechanacimiento";
            gridViewTextBoxColumn5.Width = 126;
            gridViewTextBoxColumn6.FieldName = "md_fechaingreso";
            gridViewTextBoxColumn6.HeaderText = "Fecha Ingreso";
            gridViewTextBoxColumn6.Name = "md_fechaingreso";
            gridViewTextBoxColumn6.Width = 126;
            gridViewTextBoxColumn7.FieldName = "md_fechaalta";
            gridViewTextBoxColumn7.HeaderText = "Fecha Alta";
            gridViewTextBoxColumn7.Name = "md_fechaalta";
            gridViewTextBoxColumn7.Width = 129;
            this.gridPacientes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.gridPacientes.MasterTemplate.EnableFiltering = true;
            this.gridPacientes.MasterTemplate.MultiSelect = true;
            this.gridPacientes.MasterTemplate.ShowRowHeaderColumn = false;
            this.gridPacientes.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridPacientes.Name = "gridPacientes";
            this.gridPacientes.ReadOnly = true;
            this.gridPacientes.Size = new System.Drawing.Size(756, 405);
            this.gridPacientes.TabIndex = 0;
            this.gridPacientes.Text = "radGridView1";
            this.gridPacientes.ThemeName = "Office2010Blue";
            this.gridPacientes.DataError += new Telerik.WinControls.UI.GridViewDataErrorEventHandler(this.gridPacientes_DataError);
            // 
            // divBotonesGrid
            // 
            this.divBotonesGrid.Controls.Add(this.btnNew);
            this.divBotonesGrid.Controls.Add(this.btnEdit);
            this.divBotonesGrid.Controls.Add(this.btnRemove);
            this.divBotonesGrid.Dock = System.Windows.Forms.DockStyle.Right;
            this.divBotonesGrid.Location = new System.Drawing.Point(756, 165);
            this.divBotonesGrid.Name = "divBotonesGrid";
            this.divBotonesGrid.Size = new System.Drawing.Size(42, 405);
            this.divBotonesGrid.TabIndex = 2;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.divBotonesGrid.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.Transparent;
            this.btnNew.Image = global::medicalWPF.Properties.Resources.Add;
            this.btnNew.Location = new System.Drawing.Point(6, 26);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(33, 33);
            this.btnNew.TabIndex = 2;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.Transparent;
            this.btnEdit.Image = global::medicalWPF.Properties.Resources.Edit_document;
            this.btnEdit.Location = new System.Drawing.Point(6, 65);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(33, 33);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.Transparent;
            this.btnRemove.Image = global::medicalWPF.Properties.Resources.Delete;
            this.btnRemove.Location = new System.Drawing.Point(6, 104);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(33, 33);
            this.btnRemove.TabIndex = 0;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // divBotonera
            // 
            this.divBotonera.Controls.Add(this.btnExit);
            this.divBotonera.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.divBotonera.Location = new System.Drawing.Point(0, 570);
            this.divBotonera.Name = "divBotonera";
            this.divBotonera.Size = new System.Drawing.Size(798, 55);
            this.divBotonera.TabIndex = 3;
            ((Telerik.WinControls.UI.RadPanelElement)(this.divBotonera.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.divBotonera.GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Image = global::medicalWPF.Properties.Resources.Exit1;
            this.btnExit.Location = new System.Drawing.Point(685, 10);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 33);
            this.btnExit.TabIndex = 1;
            this.btnExit.Text = "[btnExit]";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // FrmManagePacientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 625);
            this.Controls.Add(this.divGrid);
            this.Controls.Add(this.divBotonesGrid);
            this.Controls.Add(this.divBotonera);
            this.Controls.Add(this.radCollapsiblePanel1);
            this.MinimumSize = new System.Drawing.Size(806, 600);
            this.Name = "FrmManagePacientes";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "FrmManagePacientes";
            this.Load += new System.EventHandler(this.FrmManagePacientes_Load);
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            this.radCollapsiblePanel1.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.divGrid)).EndInit();
            this.divGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPacientes.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.divBotonesGrid)).EndInit();
            this.divBotonesGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.divBotonera)).EndInit();
            this.divBotonera.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadPanel divGrid;
        private Telerik.WinControls.UI.RadPanel divBotonesGrid;
        private Telerik.WinControls.UI.RadButton btnNew;
        private Telerik.WinControls.UI.RadButton btnEdit;
        private Telerik.WinControls.UI.RadButton btnRemove;
        private Telerik.WinControls.UI.RadPanel divBotonera;
        private Telerik.WinControls.UI.RadButton btnExit;
        private Telerik.WinControls.UI.RadGridView gridPacientes;
        private Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme1;
        private Telerik.WinControls.UI.RadTextBox txtNombre;
        private Telerik.WinControls.UI.RadLabel lblNombre;
        private MD_Controls.InputDate inputFechaAlta;
        private MD_Controls.InputDate inputFechaIngreso;
        private MD_Controls.InputDate inputFechaNacimiento;
        private MD_Controls.InputNumeric inputNHC;
        private MD_Controls.DropDownMedical ddSexo;
        private Telerik.WinControls.UI.RadButton btnFilter;
        private Telerik.WinControls.UI.RadButton btnClear;
    }
}
