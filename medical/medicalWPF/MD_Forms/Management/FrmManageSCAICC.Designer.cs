﻿namespace medicalWPF.MD_Forms.Management
{
    partial class FrmManageSCAICC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.office2010BlueTheme1 = new Telerik.WinControls.Themes.Office2010BlueTheme();
            this.divBotonera = new Telerik.WinControls.UI.RadPanel();
            this.layoutControl2 = new MD_Controls.Layout.LayoutControl();
            this.layoutControl1 = new MD_Controls.Layout.LayoutControl();
            this.btnExit = new Telerik.WinControls.UI.RadButton();
            this.gridICC = new Telerik.WinControls.UI.RadGridView();
            this.gridSCA = new Telerik.WinControls.UI.RadGridView();
            this.divBotonesGrid = new Telerik.WinControls.UI.RadPanel();
            this.lblICC = new Telerik.WinControls.UI.RadLabel();
            this.lblSCA = new Telerik.WinControls.UI.RadLabel();
            this.btnNewICC = new Telerik.WinControls.UI.RadButton();
            this.btnEditICC = new Telerik.WinControls.UI.RadButton();
            this.btnRemoveICC = new Telerik.WinControls.UI.RadButton();
            this.btnNewSCA = new Telerik.WinControls.UI.RadButton();
            this.btnEditSCA = new Telerik.WinControls.UI.RadButton();
            this.btnRemoveSCA = new Telerik.WinControls.UI.RadButton();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.lblPaciente = new Telerik.WinControls.UI.RadLabel();
            this.ddPacientes = new Telerik.WinControls.UI.RadDropDownList();
            this.btnClear = new Telerik.WinControls.UI.RadButton();
            this.btnFilter = new Telerik.WinControls.UI.RadButton();
            this.inputNHC = new MD_Controls.InputNumeric();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.md_iccTableAdapter = new medicalWPF.bbddmedicalDataSetTableAdapters.md_iccTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.divBotonera)).BeginInit();
            this.divBotonera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridICC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridICC.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSCA.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.divBotonesGrid)).BeginInit();
            this.divBotonesGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblICC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNewICC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditICC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemoveICC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNewSCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditSCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemoveSCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // divBotonera
            // 
            this.divBotonera.Controls.Add(this.layoutControl2);
            this.divBotonera.Controls.Add(this.layoutControl1);
            this.divBotonera.Controls.Add(this.btnExit);
            this.divBotonera.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.divBotonera.Location = new System.Drawing.Point(0, 715);
            this.divBotonera.Name = "divBotonera";
            this.divBotonera.Size = new System.Drawing.Size(1142, 55);
            this.divBotonera.TabIndex = 7;
            ((Telerik.WinControls.UI.RadPanelElement)(this.divBotonera.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.divBotonera.GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl2.AppName = "FormsApp";
            this.layoutControl2.BackColor = System.Drawing.Color.Transparent;
            this.layoutControl2.ExportBehaviour = MD_Controls.Layout.LayoutControl.ExportBehaviourEnum.BasicExport;
            this.layoutControl2.FormNameSource = MD_Controls.Layout.LayoutControl.FormIdOriginEnum.Default;
            this.layoutControl2.GridView = null;
            this.layoutControl2.Location = new System.Drawing.Point(1, 11);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.RecordCount = "";
            this.layoutControl2.SheetTitle = null;
            this.layoutControl2.Size = new System.Drawing.Size(414, 32);
            this.layoutControl2.TabIndex = 5;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.AppName = "FormsApp";
            this.layoutControl1.BackColor = System.Drawing.Color.Transparent;
            this.layoutControl1.ExportBehaviour = MD_Controls.Layout.LayoutControl.ExportBehaviourEnum.BasicExport;
            this.layoutControl1.FormNameSource = MD_Controls.Layout.LayoutControl.FormIdOriginEnum.Default;
            this.layoutControl1.GridView = null;
            this.layoutControl1.Location = new System.Drawing.Point(578, 11);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.RecordCount = "";
            this.layoutControl1.SheetTitle = null;
            this.layoutControl1.Size = new System.Drawing.Size(414, 32);
            this.layoutControl1.TabIndex = 4;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Image = global::medicalWPF.Properties.Resources.Exit1;
            this.btnExit.Location = new System.Drawing.Point(1029, 10);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 33);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "[btnExit]";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // gridICC
            // 
            this.gridICC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridICC.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridICC.MasterTemplate.AllowAddNewRow = false;
            this.gridICC.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn1.FieldName = "md_nombre";
            gridViewTextBoxColumn1.HeaderText = "Paciente";
            gridViewTextBoxColumn1.Name = "md_nombre";
            this.gridICC.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1});
            this.gridICC.MasterTemplate.ShowRowHeaderColumn = false;
            this.gridICC.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridICC.Name = "gridICC";
            this.gridICC.Size = new System.Drawing.Size(1100, 273);
            this.gridICC.TabIndex = 0;
            this.gridICC.Text = "radGridView1";
            this.gridICC.ThemeName = "Office2010Blue";
            this.gridICC.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.gridICC_CellFormatting);
            this.gridICC.DataError += new Telerik.WinControls.UI.GridViewDataErrorEventHandler(this.gridICC_DataError);
            this.gridICC.DataBindingComplete += new Telerik.WinControls.UI.GridViewBindingCompleteEventHandler(this.gridICC_DataBindingComplete);
            // 
            // gridSCA
            // 
            this.gridSCA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSCA.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridSCA.MasterTemplate.AllowAddNewRow = false;
            this.gridSCA.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn2.FieldName = "md_nombre";
            gridViewTextBoxColumn2.HeaderText = "Paciente";
            gridViewTextBoxColumn2.Name = "md_nombre";
            this.gridSCA.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn2});
            this.gridSCA.MasterTemplate.MultiSelect = true;
            this.gridSCA.MasterTemplate.ShowRowHeaderColumn = false;
            this.gridSCA.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridSCA.Name = "gridSCA";
            this.gridSCA.ReadOnly = true;
            this.gridSCA.Size = new System.Drawing.Size(1100, 273);
            this.gridSCA.TabIndex = 0;
            this.gridSCA.Text = "radGridView1";
            this.gridSCA.ThemeName = "Office2010Blue";
            this.gridSCA.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.gridSCA_CellFormatting);
            this.gridSCA.DataError += new Telerik.WinControls.UI.GridViewDataErrorEventHandler(this.gridSCA_DataError);
            this.gridSCA.DataBindingComplete += new Telerik.WinControls.UI.GridViewBindingCompleteEventHandler(this.gridSCA_DataBindingComplete);
            // 
            // divBotonesGrid
            // 
            this.divBotonesGrid.Controls.Add(this.lblICC);
            this.divBotonesGrid.Controls.Add(this.lblSCA);
            this.divBotonesGrid.Controls.Add(this.btnNewICC);
            this.divBotonesGrid.Controls.Add(this.btnEditICC);
            this.divBotonesGrid.Controls.Add(this.btnRemoveICC);
            this.divBotonesGrid.Controls.Add(this.btnNewSCA);
            this.divBotonesGrid.Controls.Add(this.btnEditSCA);
            this.divBotonesGrid.Controls.Add(this.btnRemoveSCA);
            this.divBotonesGrid.Dock = System.Windows.Forms.DockStyle.Right;
            this.divBotonesGrid.Location = new System.Drawing.Point(1100, 165);
            this.divBotonesGrid.Name = "divBotonesGrid";
            this.divBotonesGrid.Size = new System.Drawing.Size(42, 550);
            this.divBotonesGrid.TabIndex = 6;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.divBotonesGrid.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // lblICC
            // 
            this.lblICC.Location = new System.Drawing.Point(0, 282);
            this.lblICC.Name = "lblICC";
            this.lblICC.Size = new System.Drawing.Size(23, 18);
            this.lblICC.TabIndex = 20;
            this.lblICC.Text = "ICC";
            this.lblICC.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblSCA
            // 
            this.lblSCA.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSCA.Location = new System.Drawing.Point(0, 0);
            this.lblSCA.Name = "lblSCA";
            this.lblSCA.Size = new System.Drawing.Size(42, 18);
            this.lblSCA.TabIndex = 19;
            this.lblSCA.Text = "SCA";
            this.lblSCA.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // btnNewICC
            // 
            this.btnNewICC.BackColor = System.Drawing.Color.Transparent;
            this.btnNewICC.Image = global::medicalWPF.Properties.Resources.Add;
            this.btnNewICC.Location = new System.Drawing.Point(6, 306);
            this.btnNewICC.Name = "btnNewICC";
            this.btnNewICC.Size = new System.Drawing.Size(33, 33);
            this.btnNewICC.TabIndex = 5;
            this.btnNewICC.Click += new System.EventHandler(this.btnNewICC_Click);
            // 
            // btnEditICC
            // 
            this.btnEditICC.BackColor = System.Drawing.Color.Transparent;
            this.btnEditICC.Image = global::medicalWPF.Properties.Resources.Edit_document;
            this.btnEditICC.Location = new System.Drawing.Point(6, 345);
            this.btnEditICC.Name = "btnEditICC";
            this.btnEditICC.Size = new System.Drawing.Size(33, 33);
            this.btnEditICC.TabIndex = 4;
            this.btnEditICC.Click += new System.EventHandler(this.btnEditICC_Click);
            // 
            // btnRemoveICC
            // 
            this.btnRemoveICC.BackColor = System.Drawing.Color.Transparent;
            this.btnRemoveICC.Image = global::medicalWPF.Properties.Resources.Delete;
            this.btnRemoveICC.Location = new System.Drawing.Point(6, 384);
            this.btnRemoveICC.Name = "btnRemoveICC";
            this.btnRemoveICC.Size = new System.Drawing.Size(33, 33);
            this.btnRemoveICC.TabIndex = 3;
            this.btnRemoveICC.Click += new System.EventHandler(this.btnRemoveICC_Click);
            // 
            // btnNewSCA
            // 
            this.btnNewSCA.BackColor = System.Drawing.Color.Transparent;
            this.btnNewSCA.Image = global::medicalWPF.Properties.Resources.Add;
            this.btnNewSCA.Location = new System.Drawing.Point(6, 26);
            this.btnNewSCA.Name = "btnNewSCA";
            this.btnNewSCA.Size = new System.Drawing.Size(33, 33);
            this.btnNewSCA.TabIndex = 2;
            this.btnNewSCA.Click += new System.EventHandler(this.btnNewSCA_Click);
            // 
            // btnEditSCA
            // 
            this.btnEditSCA.BackColor = System.Drawing.Color.Transparent;
            this.btnEditSCA.Image = global::medicalWPF.Properties.Resources.Edit_document;
            this.btnEditSCA.Location = new System.Drawing.Point(6, 65);
            this.btnEditSCA.Name = "btnEditSCA";
            this.btnEditSCA.Size = new System.Drawing.Size(33, 33);
            this.btnEditSCA.TabIndex = 1;
            this.btnEditSCA.Click += new System.EventHandler(this.btnEditSCA_Click);
            // 
            // btnRemoveSCA
            // 
            this.btnRemoveSCA.BackColor = System.Drawing.Color.Transparent;
            this.btnRemoveSCA.Image = global::medicalWPF.Properties.Resources.Delete;
            this.btnRemoveSCA.Location = new System.Drawing.Point(6, 104);
            this.btnRemoveSCA.Name = "btnRemoveSCA";
            this.btnRemoveSCA.Size = new System.Drawing.Size(33, 33);
            this.btnRemoveSCA.TabIndex = 0;
            this.btnRemoveSCA.Click += new System.EventHandler(this.btnRemoveSCA_Click);
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel1.EnableAnimation = false;
            this.radCollapsiblePanel1.HeaderText = "Filtros";
            this.radCollapsiblePanel1.HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Right;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 992, 200);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.lblPaciente);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.ddPacientes);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.btnClear);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.btnFilter);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.inputNHC);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(1140, 137);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(1142, 165);
            this.radCollapsiblePanel1.TabIndex = 4;
            this.radCollapsiblePanel1.Text = "radCollapsiblePanel1";
            // 
            // lblPaciente
            // 
            this.lblPaciente.Location = new System.Drawing.Point(20, 18);
            this.lblPaciente.Name = "lblPaciente";
            this.lblPaciente.Size = new System.Drawing.Size(55, 18);
            this.lblPaciente.TabIndex = 18;
            this.lblPaciente.Text = "radLabel1";
            // 
            // ddPacientes
            // 
            this.ddPacientes.DisplayMember = "md_nombre";
            this.ddPacientes.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddPacientes.Location = new System.Drawing.Point(186, 16);
            this.ddPacientes.Name = "ddPacientes";
            this.ddPacientes.Size = new System.Drawing.Size(99, 20);
            this.ddPacientes.TabIndex = 17;
            this.ddPacientes.Text = "radDropDownList1";
            this.ddPacientes.ValueMember = "md_record_id";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Image = global::medicalWPF.Properties.Resources.Clear;
            this.btnClear.Location = new System.Drawing.Point(1028, 42);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(110, 33);
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "[btnClear]";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnFilter
            // 
            this.btnFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilter.Image = global::medicalWPF.Properties.Resources.Find1;
            this.btnFilter.Location = new System.Drawing.Point(1028, 3);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(110, 33);
            this.btnFilter.TabIndex = 15;
            this.btnFilter.Text = "[btnFilter]";
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // inputNHC
            // 
            this.inputNHC.Location = new System.Drawing.Point(16, 51);
            this.inputNHC.Name = "inputNHC";
            this.inputNHC.Size = new System.Drawing.Size(278, 30);
            this.inputNHC.TabIndex = 13;
            this.inputNHC.Visible = false;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 165);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1100, 550);
            this.radSplitContainer1.TabIndex = 1;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.gridSCA);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1100, 273);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.gridICC);
            this.splitPanel2.Location = new System.Drawing.Point(0, 277);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1100, 273);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // md_iccTableAdapter
            // 
            this.md_iccTableAdapter.ClearBeforeFill = true;
            // 
            // FrmManageSCAICC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 770);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.divBotonesGrid);
            this.Controls.Add(this.radCollapsiblePanel1);
            this.Controls.Add(this.divBotonera);
            this.MinimumSize = new System.Drawing.Size(758, 438);
            this.Name = "FrmManageSCAICC";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "FrmManageSCAICC";
            this.Load += new System.EventHandler(this.FrmManageSCAICC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.divBotonera)).EndInit();
            this.divBotonera.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridICC.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridICC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSCA.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.divBotonesGrid)).EndInit();
            this.divBotonesGrid.ResumeLayout(false);
            this.divBotonesGrid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblICC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNewICC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditICC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemoveICC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNewSCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditSCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemoveSCA)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            this.radCollapsiblePanel1.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme1;
        private Telerik.WinControls.UI.RadButton btnClear;
        private Telerik.WinControls.UI.RadButton btnFilter;
        private MD_Controls.InputNumeric inputNHC;
        private Telerik.WinControls.UI.RadPanel divBotonera;
        private Telerik.WinControls.UI.RadButton btnNewSCA;
        private Telerik.WinControls.UI.RadButton btnEditSCA;
        private Telerik.WinControls.UI.RadButton btnRemoveSCA;
        private Telerik.WinControls.UI.RadGridView gridSCA;
        private Telerik.WinControls.UI.RadPanel divBotonesGrid;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadGridView gridICC;
        private bbddmedicalDataSetTableAdapters.md_iccTableAdapter md_iccTableAdapter;
        private Telerik.WinControls.UI.RadButton btnExit;
        private Telerik.WinControls.UI.RadLabel lblPaciente;
        private Telerik.WinControls.UI.RadDropDownList ddPacientes;
        private MD_Controls.Layout.LayoutControl layoutControl1;
        private MD_Controls.Layout.LayoutControl layoutControl2;
        private Telerik.WinControls.UI.RadButton btnNewICC;
        private Telerik.WinControls.UI.RadButton btnEditICC;
        private Telerik.WinControls.UI.RadButton btnRemoveICC;
        private Telerik.WinControls.UI.RadLabel lblICC;
        private Telerik.WinControls.UI.RadLabel lblSCA;
    }
}
