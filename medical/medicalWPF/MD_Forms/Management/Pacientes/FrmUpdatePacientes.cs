﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using medical_Data_Lib;
using System.Linq;

namespace medicalWPF.MD_Forms.Management.Pacientes
{
    public partial class FrmUpdatePacientes : RadForm
    {
        md_pacientes pacienteToEdit = null;
        List<int> lRecordsIdPacientes = new List<int>();
        public FrmUpdatePacientes(int _pacienteToEditID)
        {
            lRecordsIdPacientes.Add(_pacienteToEditID);
            pacienteToEdit = MdPacientes.GetPacientes(lRecordsIdPacientes).FirstOrDefault();
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmUpdatePacientes_Load(object sender, EventArgs e)
        {
            setLang();
            ddSexo.SetTypeDropDownList();
            loadDataPaciente();
        }

        private void loadDataPaciente()
        {
            this.Text = "Medical Management-Actualización de Pacientes";
            txtNombre.Text = pacienteToEdit.md_nombre;
            inputNHC.SetValue(pacienteToEdit.md_nhc.ToString());
            ddSexo.SetValue(pacienteToEdit.md_sexo.ToString());
            fechaAlta.SetValue(pacienteToEdit.md_fechaalta);
            fechaNacimiento.SetValue(pacienteToEdit.md_fechanacimiento);
            fechaIngreso.SetValue(pacienteToEdit.md_fechaingreso);
        }

        private void setLang()
        {
            btnSave.Text = "Guardar";
            btnExit.Text = "Salir";
            this.Text = "Medical Management-Generación de Pacientes";
            lblNombre.Text = "Nombre paciente";
            fechaAlta.SetTitle("Fecha alta");
            fechaIngreso.SetTitle("Fecha ingreso");
            fechaNacimiento.SetTitle("Fecha nacimiento");
            inputNHC.SetTitle("NHC");
            ddSexo.SetTitle("Sexo");
            btnExit.Text = "Salir";
            btnSave.Text = "Guardar";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text == "" || ddSexo.GetValue() == "")
            {
                RadMessageBox.Show("Debe informar el nombre y sexo del paciente.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            if (DialogResult.No == RadMessageBox.Show(string.Format("Va ha modificar los datos del paciente: {0}-{1}. ¿Desea continuar?", pacienteToEdit.md_record_id, pacienteToEdit.md_nombre),
                  this.Text, MessageBoxButtons.YesNo, RadMessageIcon.Question))
                return;

            md_pacientes pacienteToSave = new md_pacientes();
            pacienteToSave.md_nombre = txtNombre.Text;
            pacienteToSave.md_fechaalta = fechaAlta.getDate();
            pacienteToSave.md_fechanacimiento = fechaNacimiento.getDate();
            pacienteToSave.md_fechaingreso = fechaIngreso.getDate();
            pacienteToSave.md_sexo = Convert.ToInt32(ddSexo.GetValue());
            pacienteToSave.md_nhc = inputNHC.Value();
            string sMessageValidation = MdPacientes.UpdatePaciente(pacienteToEdit.md_record_id, pacienteToSave);
            if (sMessageValidation != "")
                RadMessageBox.Show(this, sMessageValidation, this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            else
                this.Close();
        }
    }
}
