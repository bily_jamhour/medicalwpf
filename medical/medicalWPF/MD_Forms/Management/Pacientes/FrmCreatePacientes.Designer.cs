﻿namespace medicalWPF.MD_Forms.Management.Pacientes
{
    partial class FrmCreatePacientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.txtNombre = new Telerik.WinControls.UI.RadTextBox();
            this.tabDataICCSCA = new System.Windows.Forms.TabControl();
            this.tabSCA = new System.Windows.Forms.TabPage();
            this.tabICC = new System.Windows.Forms.TabPage();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.chkMoreDataSCA = new Telerik.WinControls.UI.RadCheckBox();
            this.chkMoreDataICC = new Telerik.WinControls.UI.RadCheckBox();
            this.fechaNacimiento = new MD_Controls.InputDate();
            this.fechaIngreso = new MD_Controls.InputDate();
            this.inputNHC = new MD_Controls.InputNumeric();
            this.fechaAlta = new MD_Controls.InputDate();
            this.ddSexo = new MD_Controls.DropDownMedical();
            this.pnlBotonera = new Telerik.WinControls.UI.RadPanel();
            this.btnExit = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.pageviewSCAICC = new Telerik.WinControls.UI.RadPageView();
            this.pageviewSCA = new Telerik.WinControls.UI.RadPageViewPage();
            this.inputTimehorapicotensional_diurno = new MD_Controls.InputTime();
            this.inputTimehorapicotensional_nocturno = new MD_Controls.InputTime();
            this.decimalMediaFCDuranteIngreso = new MD_Controls.InputDecimals();
            this.ddRevascularizacion = new MD_Controls.DropDownMedical();
            this.ddNyha = new MD_Controls.DropDownMedical();
            this.ddAnyoDM = new MD_Controls.DropDownMedical();
            this.numericSumFraminghanMenores = new MD_Controls.InputNumeric();
            this.numericFCalIngreso = new MD_Controls.InputNumeric();
            this.numericSumFraminghanMayores = new MD_Controls.InputNumeric();
            this.numericNivelesHBA = new MD_Controls.InputNumeric();
            this.decimalMediaTensionalNocturno = new MD_Controls.InputDecimals();
            this.decimalPicoTensionalNocturno = new MD_Controls.InputDecimals();
            this.decimalMediaTensionalDiurno = new MD_Controls.InputDecimals();
            this.deciamlPicoTensionalDiurno = new MD_Controls.InputDecimals();
            this.deciamlCreatininaAlta = new MD_Controls.InputDecimals();
            this.decimalCreatininiIngreso = new MD_Controls.InputDecimals();
            this.pnlCausaExitus = new Telerik.WinControls.UI.RadPanel();
            this.txtCausaExitus = new Telerik.WinControls.UI.RadTextBoxControl();
            this.ddexitus = new MD_Controls.DropDownMedical();
            this.ddstatus = new MD_Controls.DropDownMedical();
            this.ddingresos6 = new MD_Controls.DropDownMedical();
            this.ddpatron = new MD_Controls.DropDownMedical();
            this.ddregistro = new MD_Controls.DropDownMedical();
            this.ddmapa = new MD_Controls.DropDownMedical();
            this.ddantialdosteronicospost = new MD_Controls.DropDownMedical();
            this.ddantialdosteronicospre = new MD_Controls.DropDownMedical();
            this.dddigoxinapost = new MD_Controls.DropDownMedical();
            this.dddigoxinapre = new MD_Controls.DropDownMedical();
            this.ddamiodaronapost = new MD_Controls.DropDownMedical();
            this.ddamiodaronapre = new MD_Controls.DropDownMedical();
            this.ddiecapost = new MD_Controls.DropDownMedical();
            this.ddiecapre = new MD_Controls.DropDownMedical();
            this.ddbetabloqueantespost = new MD_Controls.DropDownMedical();
            this.ddbetabloqueantespre = new MD_Controls.DropDownMedical();
            this.ddritmo = new MD_Controls.DropDownMedical();
            this.ddfc = new MD_Controls.DropDownMedical();
            this.ddhtap = new MD_Controls.DropDownMedical();
            this.ddvalvulopatia = new MD_Controls.DropDownMedical();
            this.ddfevi = new MD_Controls.DropDownMedical();
            this.ddeco = new MD_Controls.DropDownMedical();
            this.ddta = new MD_Controls.DropDownMedical();
            this.ddbasal = new MD_Controls.DropDownMedical();
            this.ddecocardiograma = new MD_Controls.DropDownMedical();
            this.ddanticoagulacion = new MD_Controls.DropDownMedical();
            this.ddfa = new MD_Controls.DropDownMedical();
            this.ddicc = new MD_Controls.DropDownMedical();
            this.ddsincopes = new MD_Controls.DropDownMedical();
            this.ddcardiopatia = new MD_Controls.DropDownMedical();
            this.dddislipemia = new MD_Controls.DropDownMedical();
            this.dddiabetes_1 = new MD_Controls.DropDownMedical();
            this.ddhta_1 = new MD_Controls.DropDownMedical();
            this.pageviewICC = new Telerik.WinControls.UI.RadPageViewPage();
            this.inputTimehorapicotensional_diurno_2 = new MD_Controls.InputTime();
            this.inputTimehorapicotensional_nocturno_2 = new MD_Controls.InputTime();
            this.decimalMediaFCDuranteIngreso_2 = new MD_Controls.InputDecimals();
            this.ddfevi_2 = new MD_Controls.DropDownMedical();
            this.dddigoxinapre_2 = new MD_Controls.DropDownMedical();
            this.ddeco_2 = new MD_Controls.DropDownMedical();
            this.numericSumFraminghanMenores_2 = new MD_Controls.InputNumeric();
            this.numericFCalIngreso_2 = new MD_Controls.InputNumeric();
            this.numericSumFraminghanMayores_2 = new MD_Controls.InputNumeric();
            this.decimalMediaTensionalNocturno_2 = new MD_Controls.InputDecimals();
            this.decimalPicoTensionalNocturno_2 = new MD_Controls.InputDecimals();
            this.decimalMediaTensionalDiurno_2 = new MD_Controls.InputDecimals();
            this.deciamlPicoTensionalDiurno_2 = new MD_Controls.InputDecimals();
            this.deciamlCreatininaAlta_2 = new MD_Controls.InputDecimals();
            this.decimalCreatininiIngreso_2 = new MD_Controls.InputDecimals();
            this.pnlCausaExitus_2 = new Telerik.WinControls.UI.RadPanel();
            this.txtCausaExitus_2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.ddexitus_2 = new MD_Controls.DropDownMedical();
            this.ddstatus_2 = new MD_Controls.DropDownMedical();
            this.ddingresos6_2 = new MD_Controls.DropDownMedical();
            this.ddpatron_2 = new MD_Controls.DropDownMedical();
            this.ddregistro_2 = new MD_Controls.DropDownMedical();
            this.ddmapa_2 = new MD_Controls.DropDownMedical();
            this.ddantialdosteronicospost_2 = new MD_Controls.DropDownMedical();
            this.ddantialdosteronicospre_2 = new MD_Controls.DropDownMedical();
            this.dddigoxinapost_2 = new MD_Controls.DropDownMedical();
            this.ddnyha_2 = new MD_Controls.DropDownMedical();
            this.ddamiodaronapost_2 = new MD_Controls.DropDownMedical();
            this.ddamiodaronapre_2 = new MD_Controls.DropDownMedical();
            this.ddiecapost_2 = new MD_Controls.DropDownMedical();
            this.ddiecapre_2 = new MD_Controls.DropDownMedical();
            this.ddbetabloqueantespost_2 = new MD_Controls.DropDownMedical();
            this.ddbetabloqueantespre_2 = new MD_Controls.DropDownMedical();
            this.ddritmo_2 = new MD_Controls.DropDownMedical();
            this.ddfc_2 = new MD_Controls.DropDownMedical();
            this.ddhtap_2 = new MD_Controls.DropDownMedical();
            this.ddvalvulopatia_2 = new MD_Controls.DropDownMedical();
            this.ddta_2 = new MD_Controls.DropDownMedical();
            this.ddbasal_2 = new MD_Controls.DropDownMedical();
            this.ddecocardiograma_2 = new MD_Controls.DropDownMedical();
            this.ddanticoagulacion_2 = new MD_Controls.DropDownMedical();
            this.ddfa_2 = new MD_Controls.DropDownMedical();
            this.ddicc_2 = new MD_Controls.DropDownMedical();
            this.ddsincopes_2 = new MD_Controls.DropDownMedical();
            this.ddcardiopatia_2 = new MD_Controls.DropDownMedical();
            this.dddislipemia_2 = new MD_Controls.DropDownMedical();
            this.dddiabetes_2 = new MD_Controls.DropDownMedical();
            this.ddhta_2 = new MD_Controls.DropDownMedical();
            this.office2010BlueTheme1 = new Telerik.WinControls.Themes.Office2010BlueTheme();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).BeginInit();
            this.tabDataICCSCA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMoreDataSCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMoreDataICC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBotonera)).BeginInit();
            this.pnlBotonera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pageviewSCAICC)).BeginInit();
            this.pageviewSCAICC.SuspendLayout();
            this.pageviewSCA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCausaExitus)).BeginInit();
            this.pnlCausaExitus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCausaExitus)).BeginInit();
            this.pageviewICC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCausaExitus_2)).BeginInit();
            this.pnlCausaExitus_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCausaExitus_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(22, 22);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(60, 18);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "lblNombre";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(187, 22);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 8;
            // 
            // tabDataICCSCA
            // 
            this.tabDataICCSCA.Controls.Add(this.tabSCA);
            this.tabDataICCSCA.Controls.Add(this.tabICC);
            this.tabDataICCSCA.Location = new System.Drawing.Point(12, 212);
            this.tabDataICCSCA.Name = "tabDataICCSCA";
            this.tabDataICCSCA.SelectedIndex = 0;
            this.tabDataICCSCA.Size = new System.Drawing.Size(905, 455);
            this.tabDataICCSCA.TabIndex = 9;
            this.tabDataICCSCA.Visible = false;
            // 
            // tabSCA
            // 
            this.tabSCA.Location = new System.Drawing.Point(4, 22);
            this.tabSCA.Name = "tabSCA";
            this.tabSCA.Padding = new System.Windows.Forms.Padding(3);
            this.tabSCA.Size = new System.Drawing.Size(897, 429);
            this.tabSCA.TabIndex = 1;
            this.tabSCA.Text = "SCA";
            this.tabSCA.UseVisualStyleBackColor = true;
            // 
            // tabICC
            // 
            this.tabICC.Location = new System.Drawing.Point(4, 22);
            this.tabICC.Name = "tabICC";
            this.tabICC.Padding = new System.Windows.Forms.Padding(3);
            this.tabICC.Size = new System.Drawing.Size(897, 429);
            this.tabICC.TabIndex = 0;
            this.tabICC.Text = "ICC";
            this.tabICC.UseVisualStyleBackColor = true;
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.Transparent;
            this.radPanel1.Controls.Add(this.chkMoreDataSCA);
            this.radPanel1.Controls.Add(this.chkMoreDataICC);
            this.radPanel1.Controls.Add(this.lblNombre);
            this.radPanel1.Controls.Add(this.fechaNacimiento);
            this.radPanel1.Controls.Add(this.txtNombre);
            this.radPanel1.Controls.Add(this.fechaIngreso);
            this.radPanel1.Controls.Add(this.inputNHC);
            this.radPanel1.Controls.Add(this.fechaAlta);
            this.radPanel1.Controls.Add(this.ddSexo);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(1290, 162);
            this.radPanel1.TabIndex = 10;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radPanel1.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radPanel1.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Transparent;
            // 
            // chkMoreDataSCA
            // 
            this.chkMoreDataSCA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkMoreDataSCA.Location = new System.Drawing.Point(1104, 68);
            this.chkMoreDataSCA.Name = "chkMoreDataSCA";
            this.chkMoreDataSCA.Size = new System.Drawing.Size(81, 18);
            this.chkMoreDataSCA.TabIndex = 10;
            this.chkMoreDataSCA.Text = "Asignar SCA";
            this.chkMoreDataSCA.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkMoreDataSCA_ToggleStateChanged);
            // 
            // chkMoreDataICC
            // 
            this.chkMoreDataICC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkMoreDataICC.Location = new System.Drawing.Point(1104, 24);
            this.chkMoreDataICC.Name = "chkMoreDataICC";
            this.chkMoreDataICC.Size = new System.Drawing.Size(78, 18);
            this.chkMoreDataICC.TabIndex = 9;
            this.chkMoreDataICC.Text = "Asignar ICC";
            this.chkMoreDataICC.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkMoreDataICC_ToggleStateChanged);
            // 
            // fechaNacimiento
            // 
            this.fechaNacimiento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fechaNacimiento.Location = new System.Drawing.Point(596, 22);
            this.fechaNacimiento.Name = "fechaNacimiento";
            this.fechaNacimiento.Size = new System.Drawing.Size(278, 30);
            this.fechaNacimiento.TabIndex = 3;
            // 
            // fechaIngreso
            // 
            this.fechaIngreso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fechaIngreso.Location = new System.Drawing.Point(596, 68);
            this.fechaIngreso.Name = "fechaIngreso";
            this.fechaIngreso.Size = new System.Drawing.Size(278, 30);
            this.fechaIngreso.TabIndex = 4;
            // 
            // inputNHC
            // 
            this.inputNHC.Location = new System.Drawing.Point(19, 68);
            this.inputNHC.Name = "inputNHC";
            this.inputNHC.Size = new System.Drawing.Size(278, 30);
            this.inputNHC.TabIndex = 7;
            // 
            // fechaAlta
            // 
            this.fechaAlta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fechaAlta.Location = new System.Drawing.Point(596, 117);
            this.fechaAlta.Name = "fechaAlta";
            this.fechaAlta.Size = new System.Drawing.Size(278, 30);
            this.fechaAlta.TabIndex = 5;
            // 
            // ddSexo
            // 
            this.ddSexo.Location = new System.Drawing.Point(19, 116);
            this.ddSexo.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddSexo.Name = "ddSexo";
            this.ddSexo.Size = new System.Drawing.Size(278, 30);
            this.ddSexo.TabIndex = 6;
            // 
            // pnlBotonera
            // 
            this.pnlBotonera.BackColor = System.Drawing.Color.Transparent;
            this.pnlBotonera.Controls.Add(this.btnExit);
            this.pnlBotonera.Controls.Add(this.btnSave);
            this.pnlBotonera.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBotonera.Location = new System.Drawing.Point(0, 968);
            this.pnlBotonera.Name = "pnlBotonera";
            this.pnlBotonera.Size = new System.Drawing.Size(1292, 58);
            this.pnlBotonera.TabIndex = 11;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Image = global::medicalWPF.Properties.Resources.Exit1;
            this.btnExit.Location = new System.Drawing.Point(1179, 10);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 33);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "[btnExit]";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Image = global::medicalWPF.Properties.Resources.Save;
            this.btnSave.Location = new System.Drawing.Point(12, 10);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(110, 33);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "[btnSave]";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel1.EnableAnimation = false;
            this.radCollapsiblePanel1.HeaderText = "Información básica";
            this.radCollapsiblePanel1.HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Right;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 1292, 190);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radPanel1);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(1290, 162);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(1292, 190);
            this.radCollapsiblePanel1.TabIndex = 0;
            this.radCollapsiblePanel1.Text = "radCollapsiblePanel1";
            // 
            // pageviewSCAICC
            // 
            this.pageviewSCAICC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.pageviewSCAICC.Controls.Add(this.pageviewSCA);
            this.pageviewSCAICC.Controls.Add(this.pageviewICC);
            this.pageviewSCAICC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageviewSCAICC.ItemSizeMode = ((Telerik.WinControls.UI.PageViewItemSizeMode)((Telerik.WinControls.UI.PageViewItemSizeMode.EqualWidth | Telerik.WinControls.UI.PageViewItemSizeMode.EqualHeight)));
            this.pageviewSCAICC.Location = new System.Drawing.Point(0, 190);
            this.pageviewSCAICC.Name = "pageviewSCAICC";
            this.pageviewSCAICC.SelectedPage = this.pageviewSCA;
            this.pageviewSCAICC.Size = new System.Drawing.Size(1292, 778);
            this.pageviewSCAICC.TabIndex = 0;
            this.pageviewSCAICC.Text = "SCA";
            this.pageviewSCAICC.ThemeName = "Office2010Blue";
            this.pageviewSCAICC.ViewMode = Telerik.WinControls.UI.PageViewMode.Outlook;
            ((Telerik.WinControls.UI.RadPageViewOutlookElement)(this.pageviewSCAICC.GetChildAt(0))).ItemSizeMode = ((Telerik.WinControls.UI.PageViewItemSizeMode)((Telerik.WinControls.UI.PageViewItemSizeMode.EqualWidth | Telerik.WinControls.UI.PageViewItemSizeMode.EqualHeight)));
            ((Telerik.WinControls.UI.RadPageViewContentAreaElement)(this.pageviewSCAICC.GetChildAt(0).GetChildAt(0))).BorderColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPageViewContentAreaElement)(this.pageviewSCAICC.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.UI.RadPageViewLabelElement)(this.pageviewSCAICC.GetChildAt(0).GetChildAt(1))).Text = "SCA";
            ((Telerik.WinControls.UI.RadPageViewLabelElement)(this.pageviewSCAICC.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.UI.OverflowItemsContainer)(this.pageviewSCAICC.GetChildAt(0).GetChildAt(4))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.Layouts.StackLayoutPanel)(this.pageviewSCAICC.GetChildAt(0).GetChildAt(4).GetChildAt(0))).Enabled = true;
            ((Telerik.WinControls.UI.RadPageViewOutlookOverflowButton)(this.pageviewSCAICC.GetChildAt(0).GetChildAt(4).GetChildAt(0).GetChildAt(0))).Enabled = true;
            // 
            // pageviewSCA
            // 
            this.pageviewSCA.Controls.Add(this.inputTimehorapicotensional_diurno);
            this.pageviewSCA.Controls.Add(this.inputTimehorapicotensional_nocturno);
            this.pageviewSCA.Controls.Add(this.decimalMediaFCDuranteIngreso);
            this.pageviewSCA.Controls.Add(this.ddRevascularizacion);
            this.pageviewSCA.Controls.Add(this.ddNyha);
            this.pageviewSCA.Controls.Add(this.ddAnyoDM);
            this.pageviewSCA.Controls.Add(this.numericSumFraminghanMenores);
            this.pageviewSCA.Controls.Add(this.numericFCalIngreso);
            this.pageviewSCA.Controls.Add(this.numericSumFraminghanMayores);
            this.pageviewSCA.Controls.Add(this.numericNivelesHBA);
            this.pageviewSCA.Controls.Add(this.decimalMediaTensionalNocturno);
            this.pageviewSCA.Controls.Add(this.decimalPicoTensionalNocturno);
            this.pageviewSCA.Controls.Add(this.decimalMediaTensionalDiurno);
            this.pageviewSCA.Controls.Add(this.deciamlPicoTensionalDiurno);
            this.pageviewSCA.Controls.Add(this.deciamlCreatininaAlta);
            this.pageviewSCA.Controls.Add(this.decimalCreatininiIngreso);
            this.pageviewSCA.Controls.Add(this.pnlCausaExitus);
            this.pageviewSCA.Controls.Add(this.ddexitus);
            this.pageviewSCA.Controls.Add(this.ddstatus);
            this.pageviewSCA.Controls.Add(this.ddingresos6);
            this.pageviewSCA.Controls.Add(this.ddpatron);
            this.pageviewSCA.Controls.Add(this.ddregistro);
            this.pageviewSCA.Controls.Add(this.ddmapa);
            this.pageviewSCA.Controls.Add(this.ddantialdosteronicospost);
            this.pageviewSCA.Controls.Add(this.ddantialdosteronicospre);
            this.pageviewSCA.Controls.Add(this.dddigoxinapost);
            this.pageviewSCA.Controls.Add(this.dddigoxinapre);
            this.pageviewSCA.Controls.Add(this.ddamiodaronapost);
            this.pageviewSCA.Controls.Add(this.ddamiodaronapre);
            this.pageviewSCA.Controls.Add(this.ddiecapost);
            this.pageviewSCA.Controls.Add(this.ddiecapre);
            this.pageviewSCA.Controls.Add(this.ddbetabloqueantespost);
            this.pageviewSCA.Controls.Add(this.ddbetabloqueantespre);
            this.pageviewSCA.Controls.Add(this.ddritmo);
            this.pageviewSCA.Controls.Add(this.ddfc);
            this.pageviewSCA.Controls.Add(this.ddhtap);
            this.pageviewSCA.Controls.Add(this.ddvalvulopatia);
            this.pageviewSCA.Controls.Add(this.ddfevi);
            this.pageviewSCA.Controls.Add(this.ddeco);
            this.pageviewSCA.Controls.Add(this.ddta);
            this.pageviewSCA.Controls.Add(this.ddbasal);
            this.pageviewSCA.Controls.Add(this.ddecocardiograma);
            this.pageviewSCA.Controls.Add(this.ddanticoagulacion);
            this.pageviewSCA.Controls.Add(this.ddfa);
            this.pageviewSCA.Controls.Add(this.ddicc);
            this.pageviewSCA.Controls.Add(this.ddsincopes);
            this.pageviewSCA.Controls.Add(this.ddcardiopatia);
            this.pageviewSCA.Controls.Add(this.dddislipemia);
            this.pageviewSCA.Controls.Add(this.dddiabetes_1);
            this.pageviewSCA.Controls.Add(this.ddhta_1);
            this.pageviewSCA.Enabled = false;
            this.pageviewSCA.ItemSize = new System.Drawing.SizeF(1272F, 28F);
            this.pageviewSCA.Location = new System.Drawing.Point(15, 31);
            this.pageviewSCA.Name = "pageviewSCA";
            this.pageviewSCA.Size = new System.Drawing.Size(1262, 636);
            this.pageviewSCA.Text = "SCA";
            // 
            // inputTimehorapicotensional_diurno
            // 
            this.inputTimehorapicotensional_diurno.Location = new System.Drawing.Point(0, 614);
            this.inputTimehorapicotensional_diurno.Name = "inputTimehorapicotensional_diurno";
            this.inputTimehorapicotensional_diurno.Size = new System.Drawing.Size(411, 30);
            this.inputTimehorapicotensional_diurno.TabIndex = 104;
            // 
            // inputTimehorapicotensional_nocturno
            // 
            this.inputTimehorapicotensional_nocturno.Location = new System.Drawing.Point(413, 614);
            this.inputTimehorapicotensional_nocturno.Name = "inputTimehorapicotensional_nocturno";
            this.inputTimehorapicotensional_nocturno.Size = new System.Drawing.Size(411, 30);
            this.inputTimehorapicotensional_nocturno.TabIndex = 103;
            // 
            // decimalMediaFCDuranteIngreso
            // 
            this.decimalMediaFCDuranteIngreso.Location = new System.Drawing.Point(412, 438);
            this.decimalMediaFCDuranteIngreso.Name = "decimalMediaFCDuranteIngreso";
            this.decimalMediaFCDuranteIngreso.Size = new System.Drawing.Size(411, 34);
            this.decimalMediaFCDuranteIngreso.TabIndex = 39;
            // 
            // ddRevascularizacion
            // 
            this.ddRevascularizacion.Location = new System.Drawing.Point(-3, 438);
            this.ddRevascularizacion.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddRevascularizacion.Name = "ddRevascularizacion";
            this.ddRevascularizacion.Size = new System.Drawing.Size(414, 34);
            this.ddRevascularizacion.TabIndex = 34;
            // 
            // ddNyha
            // 
            this.ddNyha.Location = new System.Drawing.Point(412, 402);
            this.ddNyha.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddNyha.Name = "ddNyha";
            this.ddNyha.Size = new System.Drawing.Size(414, 30);
            this.ddNyha.TabIndex = 50;
            // 
            // ddAnyoDM
            // 
            this.ddAnyoDM.Location = new System.Drawing.Point(-3, 402);
            this.ddAnyoDM.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddAnyoDM.Name = "ddAnyoDM";
            this.ddAnyoDM.Size = new System.Drawing.Size(414, 30);
            this.ddAnyoDM.TabIndex = 49;
            // 
            // numericSumFraminghanMenores
            // 
            this.numericSumFraminghanMenores.Location = new System.Drawing.Point(412, 585);
            this.numericSumFraminghanMenores.Name = "numericSumFraminghanMenores";
            this.numericSumFraminghanMenores.Size = new System.Drawing.Size(411, 30);
            this.numericSumFraminghanMenores.TabIndex = 43;
            // 
            // numericFCalIngreso
            // 
            this.numericFCalIngreso.Location = new System.Drawing.Point(843, 512);
            this.numericFCalIngreso.Name = "numericFCalIngreso";
            this.numericFCalIngreso.Size = new System.Drawing.Size(411, 30);
            this.numericFCalIngreso.TabIndex = 46;
            // 
            // numericSumFraminghanMayores
            // 
            this.numericSumFraminghanMayores.Location = new System.Drawing.Point(412, 550);
            this.numericSumFraminghanMayores.Name = "numericSumFraminghanMayores";
            this.numericSumFraminghanMayores.Size = new System.Drawing.Size(411, 30);
            this.numericSumFraminghanMayores.TabIndex = 42;
            // 
            // numericNivelesHBA
            // 
            this.numericNivelesHBA.Location = new System.Drawing.Point(843, 548);
            this.numericNivelesHBA.Name = "numericNivelesHBA";
            this.numericNivelesHBA.Size = new System.Drawing.Size(411, 32);
            this.numericNivelesHBA.TabIndex = 47;
            // 
            // decimalMediaTensionalNocturno
            // 
            this.decimalMediaTensionalNocturno.Location = new System.Drawing.Point(843, 474);
            this.decimalMediaTensionalNocturno.Name = "decimalMediaTensionalNocturno";
            this.decimalMediaTensionalNocturno.Size = new System.Drawing.Size(411, 34);
            this.decimalMediaTensionalNocturno.TabIndex = 45;
            // 
            // decimalPicoTensionalNocturno
            // 
            this.decimalPicoTensionalNocturno.Location = new System.Drawing.Point(843, 438);
            this.decimalPicoTensionalNocturno.Name = "decimalPicoTensionalNocturno";
            this.decimalPicoTensionalNocturno.Size = new System.Drawing.Size(411, 34);
            this.decimalPicoTensionalNocturno.TabIndex = 44;
            // 
            // decimalMediaTensionalDiurno
            // 
            this.decimalMediaTensionalDiurno.Location = new System.Drawing.Point(412, 514);
            this.decimalMediaTensionalDiurno.Name = "decimalMediaTensionalDiurno";
            this.decimalMediaTensionalDiurno.Size = new System.Drawing.Size(411, 30);
            this.decimalMediaTensionalDiurno.TabIndex = 41;
            // 
            // deciamlPicoTensionalDiurno
            // 
            this.deciamlPicoTensionalDiurno.Location = new System.Drawing.Point(412, 479);
            this.deciamlPicoTensionalDiurno.Name = "deciamlPicoTensionalDiurno";
            this.deciamlPicoTensionalDiurno.Size = new System.Drawing.Size(411, 29);
            this.deciamlPicoTensionalDiurno.TabIndex = 40;
            // 
            // deciamlCreatininaAlta
            // 
            this.deciamlCreatininaAlta.Location = new System.Drawing.Point(-3, 514);
            this.deciamlCreatininaAlta.Name = "deciamlCreatininaAlta";
            this.deciamlCreatininaAlta.Size = new System.Drawing.Size(411, 30);
            this.deciamlCreatininaAlta.TabIndex = 36;
            // 
            // decimalCreatininiIngreso
            // 
            this.decimalCreatininiIngreso.Location = new System.Drawing.Point(-3, 478);
            this.decimalCreatininiIngreso.Name = "decimalCreatininiIngreso";
            this.decimalCreatininiIngreso.Size = new System.Drawing.Size(411, 30);
            this.decimalCreatininiIngreso.TabIndex = 35;
            // 
            // pnlCausaExitus
            // 
            this.pnlCausaExitus.Controls.Add(this.txtCausaExitus);
            this.pnlCausaExitus.Enabled = false;
            this.pnlCausaExitus.Location = new System.Drawing.Point(843, 400);
            this.pnlCausaExitus.Name = "pnlCausaExitus";
            this.pnlCausaExitus.Size = new System.Drawing.Size(411, 32);
            this.pnlCausaExitus.TabIndex = 34;
            this.pnlCausaExitus.Text = "Causa Exitus";
            ((Telerik.WinControls.UI.RadPanelElement)(this.pnlCausaExitus.GetChildAt(0))).Text = "Causa Exitus";
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pnlCausaExitus.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // txtCausaExitus
            // 
            this.txtCausaExitus.Location = new System.Drawing.Point(168, 7);
            this.txtCausaExitus.Name = "txtCausaExitus";
            this.txtCausaExitus.Size = new System.Drawing.Size(232, 20);
            this.txtCausaExitus.TabIndex = 33;
            // 
            // ddexitus
            // 
            this.ddexitus.Location = new System.Drawing.Point(843, 371);
            this.ddexitus.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddexitus.Name = "ddexitus";
            this.ddexitus.Size = new System.Drawing.Size(414, 30);
            this.ddexitus.TabIndex = 32;
            this.ddexitus.Load += new System.EventHandler(this.ddexitus_Load);
            // 
            // ddstatus
            // 
            this.ddstatus.Location = new System.Drawing.Point(843, 335);
            this.ddstatus.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddstatus.Name = "ddstatus";
            this.ddstatus.Size = new System.Drawing.Size(414, 30);
            this.ddstatus.TabIndex = 31;
            // 
            // ddingresos6
            // 
            this.ddingresos6.Location = new System.Drawing.Point(843, 299);
            this.ddingresos6.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddingresos6.Name = "ddingresos6";
            this.ddingresos6.Size = new System.Drawing.Size(414, 30);
            this.ddingresos6.TabIndex = 30;
            // 
            // ddpatron
            // 
            this.ddpatron.Location = new System.Drawing.Point(843, 263);
            this.ddpatron.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddpatron.Name = "ddpatron";
            this.ddpatron.Size = new System.Drawing.Size(414, 30);
            this.ddpatron.TabIndex = 29;
            // 
            // ddregistro
            // 
            this.ddregistro.Location = new System.Drawing.Point(843, 227);
            this.ddregistro.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddregistro.Name = "ddregistro";
            this.ddregistro.Size = new System.Drawing.Size(414, 30);
            this.ddregistro.TabIndex = 28;
            // 
            // ddmapa
            // 
            this.ddmapa.Location = new System.Drawing.Point(843, 191);
            this.ddmapa.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddmapa.Name = "ddmapa";
            this.ddmapa.Size = new System.Drawing.Size(414, 30);
            this.ddmapa.TabIndex = 27;
            // 
            // ddantialdosteronicospost
            // 
            this.ddantialdosteronicospost.Location = new System.Drawing.Point(843, 155);
            this.ddantialdosteronicospost.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddantialdosteronicospost.Name = "ddantialdosteronicospost";
            this.ddantialdosteronicospost.Size = new System.Drawing.Size(414, 30);
            this.ddantialdosteronicospost.TabIndex = 26;
            // 
            // ddantialdosteronicospre
            // 
            this.ddantialdosteronicospre.Location = new System.Drawing.Point(843, 119);
            this.ddantialdosteronicospre.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddantialdosteronicospre.Name = "ddantialdosteronicospre";
            this.ddantialdosteronicospre.Size = new System.Drawing.Size(414, 30);
            this.ddantialdosteronicospre.TabIndex = 25;
            // 
            // dddigoxinapost
            // 
            this.dddigoxinapost.Location = new System.Drawing.Point(843, 80);
            this.dddigoxinapost.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddigoxinapost.Name = "dddigoxinapost";
            this.dddigoxinapost.Size = new System.Drawing.Size(414, 30);
            this.dddigoxinapost.TabIndex = 24;
            // 
            // dddigoxinapre
            // 
            this.dddigoxinapre.Location = new System.Drawing.Point(843, 44);
            this.dddigoxinapre.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddigoxinapre.Name = "dddigoxinapre";
            this.dddigoxinapre.Size = new System.Drawing.Size(414, 30);
            this.dddigoxinapre.TabIndex = 23;
            // 
            // ddamiodaronapost
            // 
            this.ddamiodaronapost.Location = new System.Drawing.Point(843, 8);
            this.ddamiodaronapost.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddamiodaronapost.Name = "ddamiodaronapost";
            this.ddamiodaronapost.Size = new System.Drawing.Size(414, 30);
            this.ddamiodaronapost.TabIndex = 22;
            // 
            // ddamiodaronapre
            // 
            this.ddamiodaronapre.Location = new System.Drawing.Point(410, 371);
            this.ddamiodaronapre.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddamiodaronapre.Name = "ddamiodaronapre";
            this.ddamiodaronapre.Size = new System.Drawing.Size(414, 30);
            this.ddamiodaronapre.TabIndex = 21;
            // 
            // ddiecapost
            // 
            this.ddiecapost.Location = new System.Drawing.Point(410, 335);
            this.ddiecapost.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddiecapost.Name = "ddiecapost";
            this.ddiecapost.Size = new System.Drawing.Size(414, 30);
            this.ddiecapost.TabIndex = 20;
            // 
            // ddiecapre
            // 
            this.ddiecapre.Location = new System.Drawing.Point(410, 299);
            this.ddiecapre.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddiecapre.Name = "ddiecapre";
            this.ddiecapre.Size = new System.Drawing.Size(414, 30);
            this.ddiecapre.TabIndex = 19;
            // 
            // ddbetabloqueantespost
            // 
            this.ddbetabloqueantespost.Location = new System.Drawing.Point(410, 263);
            this.ddbetabloqueantespost.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbetabloqueantespost.Name = "ddbetabloqueantespost";
            this.ddbetabloqueantespost.Size = new System.Drawing.Size(414, 30);
            this.ddbetabloqueantespost.TabIndex = 18;
            // 
            // ddbetabloqueantespre
            // 
            this.ddbetabloqueantespre.Location = new System.Drawing.Point(410, 227);
            this.ddbetabloqueantespre.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbetabloqueantespre.Name = "ddbetabloqueantespre";
            this.ddbetabloqueantespre.Size = new System.Drawing.Size(414, 30);
            this.ddbetabloqueantespre.TabIndex = 17;
            // 
            // ddritmo
            // 
            this.ddritmo.Location = new System.Drawing.Point(410, 191);
            this.ddritmo.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddritmo.Name = "ddritmo";
            this.ddritmo.Size = new System.Drawing.Size(414, 30);
            this.ddritmo.TabIndex = 16;
            // 
            // ddfc
            // 
            this.ddfc.Location = new System.Drawing.Point(410, 155);
            this.ddfc.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfc.Name = "ddfc";
            this.ddfc.Size = new System.Drawing.Size(414, 30);
            this.ddfc.TabIndex = 15;
            // 
            // ddhtap
            // 
            this.ddhtap.Location = new System.Drawing.Point(410, 119);
            this.ddhtap.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddhtap.Name = "ddhtap";
            this.ddhtap.Size = new System.Drawing.Size(414, 30);
            this.ddhtap.TabIndex = 14;
            // 
            // ddvalvulopatia
            // 
            this.ddvalvulopatia.Location = new System.Drawing.Point(410, 83);
            this.ddvalvulopatia.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddvalvulopatia.Name = "ddvalvulopatia";
            this.ddvalvulopatia.Size = new System.Drawing.Size(414, 30);
            this.ddvalvulopatia.TabIndex = 13;
            // 
            // ddfevi
            // 
            this.ddfevi.Location = new System.Drawing.Point(410, 47);
            this.ddfevi.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfevi.Name = "ddfevi";
            this.ddfevi.Size = new System.Drawing.Size(414, 30);
            this.ddfevi.TabIndex = 12;
            // 
            // ddeco
            // 
            this.ddeco.Location = new System.Drawing.Point(410, 11);
            this.ddeco.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddeco.Name = "ddeco";
            this.ddeco.Size = new System.Drawing.Size(414, 30);
            this.ddeco.TabIndex = 11;
            // 
            // ddta
            // 
            this.ddta.Location = new System.Drawing.Point(-3, 371);
            this.ddta.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddta.Name = "ddta";
            this.ddta.Size = new System.Drawing.Size(414, 30);
            this.ddta.TabIndex = 10;
            // 
            // ddbasal
            // 
            this.ddbasal.Location = new System.Drawing.Point(-3, 335);
            this.ddbasal.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbasal.Name = "ddbasal";
            this.ddbasal.Size = new System.Drawing.Size(414, 30);
            this.ddbasal.TabIndex = 9;
            // 
            // ddecocardiograma
            // 
            this.ddecocardiograma.Location = new System.Drawing.Point(-3, 299);
            this.ddecocardiograma.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddecocardiograma.Name = "ddecocardiograma";
            this.ddecocardiograma.Size = new System.Drawing.Size(414, 30);
            this.ddecocardiograma.TabIndex = 8;
            // 
            // ddanticoagulacion
            // 
            this.ddanticoagulacion.Location = new System.Drawing.Point(-3, 263);
            this.ddanticoagulacion.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddanticoagulacion.Name = "ddanticoagulacion";
            this.ddanticoagulacion.Size = new System.Drawing.Size(414, 30);
            this.ddanticoagulacion.TabIndex = 7;
            // 
            // ddfa
            // 
            this.ddfa.Location = new System.Drawing.Point(-3, 227);
            this.ddfa.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfa.Name = "ddfa";
            this.ddfa.Size = new System.Drawing.Size(414, 30);
            this.ddfa.TabIndex = 6;
            // 
            // ddicc
            // 
            this.ddicc.Location = new System.Drawing.Point(-3, 191);
            this.ddicc.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddicc.Name = "ddicc";
            this.ddicc.Size = new System.Drawing.Size(414, 30);
            this.ddicc.TabIndex = 5;
            // 
            // ddsincopes
            // 
            this.ddsincopes.Location = new System.Drawing.Point(-3, 155);
            this.ddsincopes.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddsincopes.Name = "ddsincopes";
            this.ddsincopes.Size = new System.Drawing.Size(414, 30);
            this.ddsincopes.TabIndex = 4;
            // 
            // ddcardiopatia
            // 
            this.ddcardiopatia.Location = new System.Drawing.Point(-2, 119);
            this.ddcardiopatia.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddcardiopatia.Name = "ddcardiopatia";
            this.ddcardiopatia.Size = new System.Drawing.Size(414, 30);
            this.ddcardiopatia.TabIndex = 3;
            // 
            // dddislipemia
            // 
            this.dddislipemia.Location = new System.Drawing.Point(-3, 83);
            this.dddislipemia.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddislipemia.Name = "dddislipemia";
            this.dddislipemia.Size = new System.Drawing.Size(414, 30);
            this.dddislipemia.TabIndex = 2;
            // 
            // dddiabetes_1
            // 
            this.dddiabetes_1.Location = new System.Drawing.Point(-3, 47);
            this.dddiabetes_1.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddiabetes_1.Name = "dddiabetes_1";
            this.dddiabetes_1.Size = new System.Drawing.Size(414, 30);
            this.dddiabetes_1.TabIndex = 1;
            // 
            // ddhta_1
            // 
            this.ddhta_1.Location = new System.Drawing.Point(-3, 11);
            this.ddhta_1.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddhta_1.Name = "ddhta_1";
            this.ddhta_1.Size = new System.Drawing.Size(414, 30);
            this.ddhta_1.TabIndex = 0;
            // 
            // pageviewICC
            // 
            this.pageviewICC.Controls.Add(this.inputTimehorapicotensional_diurno_2);
            this.pageviewICC.Controls.Add(this.inputTimehorapicotensional_nocturno_2);
            this.pageviewICC.Controls.Add(this.decimalMediaFCDuranteIngreso_2);
            this.pageviewICC.Controls.Add(this.ddfevi_2);
            this.pageviewICC.Controls.Add(this.dddigoxinapre_2);
            this.pageviewICC.Controls.Add(this.ddeco_2);
            this.pageviewICC.Controls.Add(this.numericSumFraminghanMenores_2);
            this.pageviewICC.Controls.Add(this.numericFCalIngreso_2);
            this.pageviewICC.Controls.Add(this.numericSumFraminghanMayores_2);
            this.pageviewICC.Controls.Add(this.decimalMediaTensionalNocturno_2);
            this.pageviewICC.Controls.Add(this.decimalPicoTensionalNocturno_2);
            this.pageviewICC.Controls.Add(this.decimalMediaTensionalDiurno_2);
            this.pageviewICC.Controls.Add(this.deciamlPicoTensionalDiurno_2);
            this.pageviewICC.Controls.Add(this.deciamlCreatininaAlta_2);
            this.pageviewICC.Controls.Add(this.decimalCreatininiIngreso_2);
            this.pageviewICC.Controls.Add(this.pnlCausaExitus_2);
            this.pageviewICC.Controls.Add(this.ddexitus_2);
            this.pageviewICC.Controls.Add(this.ddstatus_2);
            this.pageviewICC.Controls.Add(this.ddingresos6_2);
            this.pageviewICC.Controls.Add(this.ddpatron_2);
            this.pageviewICC.Controls.Add(this.ddregistro_2);
            this.pageviewICC.Controls.Add(this.ddmapa_2);
            this.pageviewICC.Controls.Add(this.ddantialdosteronicospost_2);
            this.pageviewICC.Controls.Add(this.ddantialdosteronicospre_2);
            this.pageviewICC.Controls.Add(this.dddigoxinapost_2);
            this.pageviewICC.Controls.Add(this.ddnyha_2);
            this.pageviewICC.Controls.Add(this.ddamiodaronapost_2);
            this.pageviewICC.Controls.Add(this.ddamiodaronapre_2);
            this.pageviewICC.Controls.Add(this.ddiecapost_2);
            this.pageviewICC.Controls.Add(this.ddiecapre_2);
            this.pageviewICC.Controls.Add(this.ddbetabloqueantespost_2);
            this.pageviewICC.Controls.Add(this.ddbetabloqueantespre_2);
            this.pageviewICC.Controls.Add(this.ddritmo_2);
            this.pageviewICC.Controls.Add(this.ddfc_2);
            this.pageviewICC.Controls.Add(this.ddhtap_2);
            this.pageviewICC.Controls.Add(this.ddvalvulopatia_2);
            this.pageviewICC.Controls.Add(this.ddta_2);
            this.pageviewICC.Controls.Add(this.ddbasal_2);
            this.pageviewICC.Controls.Add(this.ddecocardiograma_2);
            this.pageviewICC.Controls.Add(this.ddanticoagulacion_2);
            this.pageviewICC.Controls.Add(this.ddfa_2);
            this.pageviewICC.Controls.Add(this.ddicc_2);
            this.pageviewICC.Controls.Add(this.ddsincopes_2);
            this.pageviewICC.Controls.Add(this.ddcardiopatia_2);
            this.pageviewICC.Controls.Add(this.dddislipemia_2);
            this.pageviewICC.Controls.Add(this.dddiabetes_2);
            this.pageviewICC.Controls.Add(this.ddhta_2);
            this.pageviewICC.Enabled = false;
            this.pageviewICC.ItemSize = new System.Drawing.SizeF(1272F, 28F);
            this.pageviewICC.Location = new System.Drawing.Point(15, 31);
            this.pageviewICC.Name = "pageviewICC";
            this.pageviewICC.Size = new System.Drawing.Size(1262, 636);
            this.pageviewICC.Text = "ICC";
            // 
            // inputTimehorapicotensional_diurno_2
            // 
            this.inputTimehorapicotensional_diurno_2.Location = new System.Drawing.Point(3, 611);
            this.inputTimehorapicotensional_diurno_2.Name = "inputTimehorapicotensional_diurno_2";
            this.inputTimehorapicotensional_diurno_2.Size = new System.Drawing.Size(411, 30);
            this.inputTimehorapicotensional_diurno_2.TabIndex = 102;
            // 
            // inputTimehorapicotensional_nocturno_2
            // 
            this.inputTimehorapicotensional_nocturno_2.Location = new System.Drawing.Point(416, 611);
            this.inputTimehorapicotensional_nocturno_2.Name = "inputTimehorapicotensional_nocturno_2";
            this.inputTimehorapicotensional_nocturno_2.Size = new System.Drawing.Size(411, 30);
            this.inputTimehorapicotensional_nocturno_2.TabIndex = 101;
            // 
            // decimalMediaFCDuranteIngreso_2
            // 
            this.decimalMediaFCDuranteIngreso_2.Location = new System.Drawing.Point(-3, 539);
            this.decimalMediaFCDuranteIngreso_2.Name = "decimalMediaFCDuranteIngreso_2";
            this.decimalMediaFCDuranteIngreso_2.Size = new System.Drawing.Size(411, 34);
            this.decimalMediaFCDuranteIngreso_2.TabIndex = 90;
            // 
            // ddfevi_2
            // 
            this.ddfevi_2.Location = new System.Drawing.Point(0, 431);
            this.ddfevi_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfevi_2.Name = "ddfevi_2";
            this.ddfevi_2.Size = new System.Drawing.Size(414, 34);
            this.ddfevi_2.TabIndex = 85;
            // 
            // dddigoxinapre_2
            // 
            this.dddigoxinapre_2.Location = new System.Drawing.Point(415, 395);
            this.dddigoxinapre_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddigoxinapre_2.Name = "dddigoxinapre_2";
            this.dddigoxinapre_2.Size = new System.Drawing.Size(414, 30);
            this.dddigoxinapre_2.TabIndex = 100;
            // 
            // ddeco_2
            // 
            this.ddeco_2.Location = new System.Drawing.Point(0, 395);
            this.ddeco_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddeco_2.Name = "ddeco_2";
            this.ddeco_2.Size = new System.Drawing.Size(414, 30);
            this.ddeco_2.TabIndex = 99;
            // 
            // numericSumFraminghanMenores_2
            // 
            this.numericSumFraminghanMenores_2.Location = new System.Drawing.Point(413, 502);
            this.numericSumFraminghanMenores_2.Name = "numericSumFraminghanMenores_2";
            this.numericSumFraminghanMenores_2.Size = new System.Drawing.Size(411, 30);
            this.numericSumFraminghanMenores_2.TabIndex = 94;
            // 
            // numericFCalIngreso_2
            // 
            this.numericFCalIngreso_2.Location = new System.Drawing.Point(846, 431);
            this.numericFCalIngreso_2.Name = "numericFCalIngreso_2";
            this.numericFCalIngreso_2.Size = new System.Drawing.Size(411, 30);
            this.numericFCalIngreso_2.TabIndex = 97;
            // 
            // numericSumFraminghanMayores_2
            // 
            this.numericSumFraminghanMayores_2.Location = new System.Drawing.Point(413, 467);
            this.numericSumFraminghanMayores_2.Name = "numericSumFraminghanMayores_2";
            this.numericSumFraminghanMayores_2.Size = new System.Drawing.Size(411, 30);
            this.numericSumFraminghanMayores_2.TabIndex = 93;
            // 
            // decimalMediaTensionalNocturno_2
            // 
            this.decimalMediaTensionalNocturno_2.Location = new System.Drawing.Point(413, 575);
            this.decimalMediaTensionalNocturno_2.Name = "decimalMediaTensionalNocturno_2";
            this.decimalMediaTensionalNocturno_2.Size = new System.Drawing.Size(411, 34);
            this.decimalMediaTensionalNocturno_2.TabIndex = 96;
            // 
            // decimalPicoTensionalNocturno_2
            // 
            this.decimalPicoTensionalNocturno_2.Location = new System.Drawing.Point(413, 539);
            this.decimalPicoTensionalNocturno_2.Name = "decimalPicoTensionalNocturno_2";
            this.decimalPicoTensionalNocturno_2.Size = new System.Drawing.Size(411, 34);
            this.decimalPicoTensionalNocturno_2.TabIndex = 95;
            // 
            // decimalMediaTensionalDiurno_2
            // 
            this.decimalMediaTensionalDiurno_2.Location = new System.Drawing.Point(413, 431);
            this.decimalMediaTensionalDiurno_2.Name = "decimalMediaTensionalDiurno_2";
            this.decimalMediaTensionalDiurno_2.Size = new System.Drawing.Size(411, 30);
            this.decimalMediaTensionalDiurno_2.TabIndex = 92;
            // 
            // deciamlPicoTensionalDiurno_2
            // 
            this.deciamlPicoTensionalDiurno_2.Location = new System.Drawing.Point(-3, 580);
            this.deciamlPicoTensionalDiurno_2.Name = "deciamlPicoTensionalDiurno_2";
            this.deciamlPicoTensionalDiurno_2.Size = new System.Drawing.Size(411, 29);
            this.deciamlPicoTensionalDiurno_2.TabIndex = 91;
            // 
            // deciamlCreatininaAlta_2
            // 
            this.deciamlCreatininaAlta_2.Location = new System.Drawing.Point(0, 507);
            this.deciamlCreatininaAlta_2.Name = "deciamlCreatininaAlta_2";
            this.deciamlCreatininaAlta_2.Size = new System.Drawing.Size(411, 30);
            this.deciamlCreatininaAlta_2.TabIndex = 87;
            // 
            // decimalCreatininiIngreso_2
            // 
            this.decimalCreatininiIngreso_2.Location = new System.Drawing.Point(0, 471);
            this.decimalCreatininiIngreso_2.Name = "decimalCreatininiIngreso_2";
            this.decimalCreatininiIngreso_2.Size = new System.Drawing.Size(411, 30);
            this.decimalCreatininiIngreso_2.TabIndex = 86;
            // 
            // pnlCausaExitus_2
            // 
            this.pnlCausaExitus_2.AccessibleDescription = "";
            this.pnlCausaExitus_2.Controls.Add(this.txtCausaExitus_2);
            this.pnlCausaExitus_2.Enabled = false;
            this.pnlCausaExitus_2.Location = new System.Drawing.Point(846, 328);
            this.pnlCausaExitus_2.Name = "pnlCausaExitus_2";
            this.pnlCausaExitus_2.Size = new System.Drawing.Size(411, 32);
            this.pnlCausaExitus_2.TabIndex = 84;
            this.pnlCausaExitus_2.Text = "Causa Exitus";
            ((Telerik.WinControls.UI.RadPanelElement)(this.pnlCausaExitus_2.GetChildAt(0))).Text = "Causa Exitus";
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pnlCausaExitus_2.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // txtCausaExitus_2
            // 
            this.txtCausaExitus_2.Location = new System.Drawing.Point(169, 7);
            this.txtCausaExitus_2.Name = "txtCausaExitus_2";
            this.txtCausaExitus_2.Size = new System.Drawing.Size(235, 20);
            this.txtCausaExitus_2.TabIndex = 33;
            // 
            // ddexitus_2
            // 
            this.ddexitus_2.Location = new System.Drawing.Point(846, 292);
            this.ddexitus_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddexitus_2.Name = "ddexitus_2";
            this.ddexitus_2.Size = new System.Drawing.Size(414, 30);
            this.ddexitus_2.TabIndex = 81;
            this.ddexitus_2.Load += new System.EventHandler(this.ddexitus_2_Load);
            // 
            // ddstatus_2
            // 
            this.ddstatus_2.Location = new System.Drawing.Point(846, 256);
            this.ddstatus_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddstatus_2.Name = "ddstatus_2";
            this.ddstatus_2.Size = new System.Drawing.Size(414, 30);
            this.ddstatus_2.TabIndex = 80;
            // 
            // ddingresos6_2
            // 
            this.ddingresos6_2.Location = new System.Drawing.Point(846, 220);
            this.ddingresos6_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddingresos6_2.Name = "ddingresos6_2";
            this.ddingresos6_2.Size = new System.Drawing.Size(414, 30);
            this.ddingresos6_2.TabIndex = 79;
            // 
            // ddpatron_2
            // 
            this.ddpatron_2.Location = new System.Drawing.Point(846, 184);
            this.ddpatron_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddpatron_2.Name = "ddpatron_2";
            this.ddpatron_2.Size = new System.Drawing.Size(414, 30);
            this.ddpatron_2.TabIndex = 78;
            // 
            // ddregistro_2
            // 
            this.ddregistro_2.Location = new System.Drawing.Point(846, 148);
            this.ddregistro_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddregistro_2.Name = "ddregistro_2";
            this.ddregistro_2.Size = new System.Drawing.Size(414, 30);
            this.ddregistro_2.TabIndex = 77;
            // 
            // ddmapa_2
            // 
            this.ddmapa_2.Location = new System.Drawing.Point(846, 112);
            this.ddmapa_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddmapa_2.Name = "ddmapa_2";
            this.ddmapa_2.Size = new System.Drawing.Size(414, 30);
            this.ddmapa_2.TabIndex = 76;
            // 
            // ddantialdosteronicospost_2
            // 
            this.ddantialdosteronicospost_2.Location = new System.Drawing.Point(846, 73);
            this.ddantialdosteronicospost_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddantialdosteronicospost_2.Name = "ddantialdosteronicospost_2";
            this.ddantialdosteronicospost_2.Size = new System.Drawing.Size(414, 30);
            this.ddantialdosteronicospost_2.TabIndex = 75;
            // 
            // ddantialdosteronicospre_2
            // 
            this.ddantialdosteronicospre_2.Location = new System.Drawing.Point(846, 37);
            this.ddantialdosteronicospre_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddantialdosteronicospre_2.Name = "ddantialdosteronicospre_2";
            this.ddantialdosteronicospre_2.Size = new System.Drawing.Size(414, 30);
            this.ddantialdosteronicospre_2.TabIndex = 74;
            // 
            // dddigoxinapost_2
            // 
            this.dddigoxinapost_2.Location = new System.Drawing.Point(846, 1);
            this.dddigoxinapost_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddigoxinapost_2.Name = "dddigoxinapost_2";
            this.dddigoxinapost_2.Size = new System.Drawing.Size(414, 30);
            this.dddigoxinapost_2.TabIndex = 73;
            // 
            // ddnyha_2
            // 
            this.ddnyha_2.Location = new System.Drawing.Point(413, 364);
            this.ddnyha_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddnyha_2.Name = "ddnyha_2";
            this.ddnyha_2.Size = new System.Drawing.Size(414, 30);
            this.ddnyha_2.TabIndex = 72;
            // 
            // ddamiodaronapost_2
            // 
            this.ddamiodaronapost_2.Location = new System.Drawing.Point(413, 328);
            this.ddamiodaronapost_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddamiodaronapost_2.Name = "ddamiodaronapost_2";
            this.ddamiodaronapost_2.Size = new System.Drawing.Size(414, 30);
            this.ddamiodaronapost_2.TabIndex = 71;
            // 
            // ddamiodaronapre_2
            // 
            this.ddamiodaronapre_2.Location = new System.Drawing.Point(413, 292);
            this.ddamiodaronapre_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddamiodaronapre_2.Name = "ddamiodaronapre_2";
            this.ddamiodaronapre_2.Size = new System.Drawing.Size(414, 30);
            this.ddamiodaronapre_2.TabIndex = 70;
            // 
            // ddiecapost_2
            // 
            this.ddiecapost_2.Location = new System.Drawing.Point(413, 256);
            this.ddiecapost_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddiecapost_2.Name = "ddiecapost_2";
            this.ddiecapost_2.Size = new System.Drawing.Size(414, 30);
            this.ddiecapost_2.TabIndex = 69;
            // 
            // ddiecapre_2
            // 
            this.ddiecapre_2.Location = new System.Drawing.Point(413, 220);
            this.ddiecapre_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddiecapre_2.Name = "ddiecapre_2";
            this.ddiecapre_2.Size = new System.Drawing.Size(414, 30);
            this.ddiecapre_2.TabIndex = 68;
            // 
            // ddbetabloqueantespost_2
            // 
            this.ddbetabloqueantespost_2.Location = new System.Drawing.Point(413, 184);
            this.ddbetabloqueantespost_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbetabloqueantespost_2.Name = "ddbetabloqueantespost_2";
            this.ddbetabloqueantespost_2.Size = new System.Drawing.Size(414, 30);
            this.ddbetabloqueantespost_2.TabIndex = 67;
            // 
            // ddbetabloqueantespre_2
            // 
            this.ddbetabloqueantespre_2.Location = new System.Drawing.Point(413, 148);
            this.ddbetabloqueantespre_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbetabloqueantespre_2.Name = "ddbetabloqueantespre_2";
            this.ddbetabloqueantespre_2.Size = new System.Drawing.Size(414, 30);
            this.ddbetabloqueantespre_2.TabIndex = 66;
            // 
            // ddritmo_2
            // 
            this.ddritmo_2.Location = new System.Drawing.Point(413, 112);
            this.ddritmo_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddritmo_2.Name = "ddritmo_2";
            this.ddritmo_2.Size = new System.Drawing.Size(414, 30);
            this.ddritmo_2.TabIndex = 65;
            // 
            // ddfc_2
            // 
            this.ddfc_2.Location = new System.Drawing.Point(413, 76);
            this.ddfc_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfc_2.Name = "ddfc_2";
            this.ddfc_2.Size = new System.Drawing.Size(414, 30);
            this.ddfc_2.TabIndex = 64;
            // 
            // ddhtap_2
            // 
            this.ddhtap_2.Location = new System.Drawing.Point(413, 40);
            this.ddhtap_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddhtap_2.Name = "ddhtap_2";
            this.ddhtap_2.Size = new System.Drawing.Size(414, 30);
            this.ddhtap_2.TabIndex = 63;
            // 
            // ddvalvulopatia_2
            // 
            this.ddvalvulopatia_2.Location = new System.Drawing.Point(413, 4);
            this.ddvalvulopatia_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddvalvulopatia_2.Name = "ddvalvulopatia_2";
            this.ddvalvulopatia_2.Size = new System.Drawing.Size(414, 30);
            this.ddvalvulopatia_2.TabIndex = 62;
            // 
            // ddta_2
            // 
            this.ddta_2.Location = new System.Drawing.Point(0, 364);
            this.ddta_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddta_2.Name = "ddta_2";
            this.ddta_2.Size = new System.Drawing.Size(414, 30);
            this.ddta_2.TabIndex = 61;
            // 
            // ddbasal_2
            // 
            this.ddbasal_2.Location = new System.Drawing.Point(0, 328);
            this.ddbasal_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbasal_2.Name = "ddbasal_2";
            this.ddbasal_2.Size = new System.Drawing.Size(414, 30);
            this.ddbasal_2.TabIndex = 60;
            // 
            // ddecocardiograma_2
            // 
            this.ddecocardiograma_2.Location = new System.Drawing.Point(0, 292);
            this.ddecocardiograma_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddecocardiograma_2.Name = "ddecocardiograma_2";
            this.ddecocardiograma_2.Size = new System.Drawing.Size(414, 30);
            this.ddecocardiograma_2.TabIndex = 59;
            // 
            // ddanticoagulacion_2
            // 
            this.ddanticoagulacion_2.Location = new System.Drawing.Point(0, 256);
            this.ddanticoagulacion_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddanticoagulacion_2.Name = "ddanticoagulacion_2";
            this.ddanticoagulacion_2.Size = new System.Drawing.Size(414, 30);
            this.ddanticoagulacion_2.TabIndex = 58;
            // 
            // ddfa_2
            // 
            this.ddfa_2.Location = new System.Drawing.Point(0, 220);
            this.ddfa_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfa_2.Name = "ddfa_2";
            this.ddfa_2.Size = new System.Drawing.Size(414, 30);
            this.ddfa_2.TabIndex = 57;
            // 
            // ddicc_2
            // 
            this.ddicc_2.Location = new System.Drawing.Point(0, 184);
            this.ddicc_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddicc_2.Name = "ddicc_2";
            this.ddicc_2.Size = new System.Drawing.Size(414, 30);
            this.ddicc_2.TabIndex = 56;
            // 
            // ddsincopes_2
            // 
            this.ddsincopes_2.Location = new System.Drawing.Point(0, 148);
            this.ddsincopes_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddsincopes_2.Name = "ddsincopes_2";
            this.ddsincopes_2.Size = new System.Drawing.Size(414, 30);
            this.ddsincopes_2.TabIndex = 55;
            // 
            // ddcardiopatia_2
            // 
            this.ddcardiopatia_2.Location = new System.Drawing.Point(0, 112);
            this.ddcardiopatia_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddcardiopatia_2.Name = "ddcardiopatia_2";
            this.ddcardiopatia_2.Size = new System.Drawing.Size(414, 30);
            this.ddcardiopatia_2.TabIndex = 54;
            // 
            // dddislipemia_2
            // 
            this.dddislipemia_2.Location = new System.Drawing.Point(0, 76);
            this.dddislipemia_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddislipemia_2.Name = "dddislipemia_2";
            this.dddislipemia_2.Size = new System.Drawing.Size(414, 30);
            this.dddislipemia_2.TabIndex = 53;
            // 
            // dddiabetes_2
            // 
            this.dddiabetes_2.Location = new System.Drawing.Point(0, 40);
            this.dddiabetes_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddiabetes_2.Name = "dddiabetes_2";
            this.dddiabetes_2.Size = new System.Drawing.Size(414, 30);
            this.dddiabetes_2.TabIndex = 52;
            // 
            // ddhta_2
            // 
            this.ddhta_2.Location = new System.Drawing.Point(0, 4);
            this.ddhta_2.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddhta_2.Name = "ddhta_2";
            this.ddhta_2.Size = new System.Drawing.Size(414, 30);
            this.ddhta_2.TabIndex = 51;
            // 
            // FrmCreatePacientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1292, 1026);
            this.Controls.Add(this.pageviewSCAICC);
            this.Controls.Add(this.tabDataICCSCA);
            this.Controls.Add(this.radCollapsiblePanel1);
            this.Controls.Add(this.pnlBotonera);
            this.MinimumSize = new System.Drawing.Size(1300, 858);
            this.Name = "FrmCreatePacientes";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "FrmCreatePacientes";
            this.Load += new System.EventHandler(this.FrmCreatePacientes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).EndInit();
            this.tabDataICCSCA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMoreDataSCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMoreDataICC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBotonera)).EndInit();
            this.pnlBotonera.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pageviewSCAICC)).EndInit();
            this.pageviewSCAICC.ResumeLayout(false);
            this.pageviewSCA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlCausaExitus)).EndInit();
            this.pnlCausaExitus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCausaExitus)).EndInit();
            this.pageviewICC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlCausaExitus_2)).EndInit();
            this.pnlCausaExitus_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCausaExitus_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblNombre;
        private MD_Controls.InputDate fechaNacimiento;
        private MD_Controls.InputDate fechaIngreso;
        private MD_Controls.InputDate fechaAlta;
        private MD_Controls.DropDownMedical ddSexo;
        private MD_Controls.InputNumeric inputNHC;
        private Telerik.WinControls.UI.RadTextBox txtNombre;
        private System.Windows.Forms.TabControl tabDataICCSCA;
        private System.Windows.Forms.TabPage tabICC;
        private System.Windows.Forms.TabPage tabSCA;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel pnlBotonera;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadButton btnExit;
        private Telerik.WinControls.UI.RadCheckBox chkMoreDataSCA;
        private Telerik.WinControls.UI.RadCheckBox chkMoreDataICC;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadPageView pageviewSCAICC;
        private Telerik.WinControls.UI.RadPageViewPage pageviewSCA;
        private Telerik.WinControls.UI.RadPageViewPage pageviewICC;
        private Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme1;
        private MD_Controls.DropDownMedical dddiabetes_1;
        private MD_Controls.DropDownMedical ddhta_1;
        private MD_Controls.DropDownMedical ddpatron;
        private MD_Controls.DropDownMedical ddregistro;
        private MD_Controls.DropDownMedical ddmapa;
        private MD_Controls.DropDownMedical ddantialdosteronicospost;
        private MD_Controls.DropDownMedical ddantialdosteronicospre;
        private MD_Controls.DropDownMedical dddigoxinapost;
        private MD_Controls.DropDownMedical dddigoxinapre;
        private MD_Controls.DropDownMedical ddamiodaronapost;
        private MD_Controls.DropDownMedical ddamiodaronapre;
        private MD_Controls.DropDownMedical ddiecapost;
        private MD_Controls.DropDownMedical ddiecapre;
        private MD_Controls.DropDownMedical ddbetabloqueantespost;
        private MD_Controls.DropDownMedical ddbetabloqueantespre;
        private MD_Controls.DropDownMedical ddritmo;
        private MD_Controls.DropDownMedical ddfc;
        private MD_Controls.DropDownMedical ddhtap;
        private MD_Controls.DropDownMedical ddvalvulopatia;
        private MD_Controls.DropDownMedical ddfevi;
        private MD_Controls.DropDownMedical ddeco;
        private MD_Controls.DropDownMedical ddta;
        private MD_Controls.DropDownMedical ddbasal;
        private MD_Controls.DropDownMedical ddecocardiograma;
        private MD_Controls.DropDownMedical ddanticoagulacion;
        private MD_Controls.DropDownMedical ddfa;
        private MD_Controls.DropDownMedical ddicc;
        private MD_Controls.DropDownMedical ddsincopes;
        private MD_Controls.DropDownMedical ddcardiopatia;
        private MD_Controls.DropDownMedical dddislipemia;
        private MD_Controls.DropDownMedical ddexitus;
        private MD_Controls.DropDownMedical ddstatus;
        private MD_Controls.DropDownMedical ddingresos6;
        private Telerik.WinControls.UI.RadPanel pnlCausaExitus;
        private Telerik.WinControls.UI.RadTextBoxControl txtCausaExitus;
        private MD_Controls.InputDecimals decimalMediaTensionalNocturno;
        private MD_Controls.InputDecimals decimalPicoTensionalNocturno;
        private MD_Controls.InputDecimals decimalMediaTensionalDiurno;
        private MD_Controls.InputDecimals deciamlPicoTensionalDiurno;
        private MD_Controls.InputDecimals deciamlCreatininaAlta;
        private MD_Controls.InputDecimals decimalCreatininiIngreso;
        private MD_Controls.InputNumeric numericSumFraminghanMenores;
        private MD_Controls.InputNumeric numericFCalIngreso;
        private MD_Controls.InputNumeric numericSumFraminghanMayores;
        private MD_Controls.InputNumeric numericNivelesHBA;
        private MD_Controls.DropDownMedical ddAnyoDM;
        private MD_Controls.DropDownMedical ddNyha;
        private MD_Controls.DropDownMedical ddRevascularizacion;
        private MD_Controls.InputDecimals decimalMediaFCDuranteIngreso;
        private MD_Controls.InputDecimals decimalMediaFCDuranteIngreso_2;
        private MD_Controls.DropDownMedical ddfevi_2;
        private MD_Controls.DropDownMedical dddigoxinapre_2;
        private MD_Controls.DropDownMedical ddeco_2;
        private MD_Controls.InputNumeric numericSumFraminghanMenores_2;
        private MD_Controls.InputNumeric numericFCalIngreso_2;
        private MD_Controls.InputNumeric numericSumFraminghanMayores_2;
        private MD_Controls.InputDecimals decimalMediaTensionalNocturno_2;
        private MD_Controls.InputDecimals decimalPicoTensionalNocturno_2;
        private MD_Controls.InputDecimals decimalMediaTensionalDiurno_2;
        private MD_Controls.InputDecimals deciamlPicoTensionalDiurno_2;
        private MD_Controls.InputDecimals deciamlCreatininaAlta_2;
        private MD_Controls.InputDecimals decimalCreatininiIngreso_2;
        private Telerik.WinControls.UI.RadPanel pnlCausaExitus_2;
        private Telerik.WinControls.UI.RadTextBoxControl txtCausaExitus_2;
        private MD_Controls.DropDownMedical ddexitus_2;
        private MD_Controls.DropDownMedical ddstatus_2;
        private MD_Controls.DropDownMedical ddingresos6_2;
        private MD_Controls.DropDownMedical ddpatron_2;
        private MD_Controls.DropDownMedical ddregistro_2;
        private MD_Controls.DropDownMedical ddmapa_2;
        private MD_Controls.DropDownMedical ddantialdosteronicospost_2;
        private MD_Controls.DropDownMedical ddantialdosteronicospre_2;
        private MD_Controls.DropDownMedical dddigoxinapost_2;
        private MD_Controls.DropDownMedical ddnyha_2;
        private MD_Controls.DropDownMedical ddamiodaronapost_2;
        private MD_Controls.DropDownMedical ddamiodaronapre_2;
        private MD_Controls.DropDownMedical ddiecapost_2;
        private MD_Controls.DropDownMedical ddiecapre_2;
        private MD_Controls.DropDownMedical ddbetabloqueantespost_2;
        private MD_Controls.DropDownMedical ddbetabloqueantespre_2;
        private MD_Controls.DropDownMedical ddritmo_2;
        private MD_Controls.DropDownMedical ddfc_2;
        private MD_Controls.DropDownMedical ddhtap_2;
        private MD_Controls.DropDownMedical ddvalvulopatia_2;
        private MD_Controls.DropDownMedical ddta_2;
        private MD_Controls.DropDownMedical ddbasal_2;
        private MD_Controls.DropDownMedical ddecocardiograma_2;
        private MD_Controls.DropDownMedical ddanticoagulacion_2;
        private MD_Controls.DropDownMedical ddfa_2;
        private MD_Controls.DropDownMedical ddicc_2;
        private MD_Controls.DropDownMedical ddsincopes_2;
        private MD_Controls.DropDownMedical ddcardiopatia_2;
        private MD_Controls.DropDownMedical dddislipemia_2;
        private MD_Controls.DropDownMedical dddiabetes_2;
        private MD_Controls.DropDownMedical ddhta_2;
        private MD_Controls.InputTime inputTimehorapicotensional_diurno_2;
        private MD_Controls.InputTime inputTimehorapicotensional_nocturno_2;
        private MD_Controls.InputTime inputTimehorapicotensional_diurno;
        private MD_Controls.InputTime inputTimehorapicotensional_nocturno;
    }
}
