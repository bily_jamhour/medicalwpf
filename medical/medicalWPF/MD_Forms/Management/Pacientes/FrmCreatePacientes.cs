﻿using MD_Controls;
using medical_Data_Lib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinForms.Documents.Layout;

namespace medicalWPF.MD_Forms.Management.Pacientes
{
    public partial class FrmCreatePacientes : RadForm
    {
        public FrmCreatePacientes()
        {
            InitializeComponent();
        }
        string sMessageValidation = "";
        private void FrmCreatePacientes_Load(object sender, EventArgs e)
        {
            setLang();
            ddSexo.SetTypeDropDownList();

            loadDropDownsICC();
            loadDropDownsSCA();


        }

        private void loadDropDownsICC()
        {
            foreach (var ctrl in pageviewICC.Controls)
            {
                if (ctrl is DropDownMedical)
                {
                    string sNameDropdown = (ctrl as DropDownMedical).Name.Replace("dd", "").ToUpper();
                    if (sNameDropdown != "")
                    {
                        (ctrl as DropDownMedical).SetTypeDropDownList(sNameDropdown);
                    }
                }
            }
        }

        private void loadDropDownsSCA()
        {
            foreach (var ctrl in pageviewSCA.Controls)
            {
                if (ctrl is DropDownMedical)
                {
                    string sNameDropdown = (ctrl as DropDownMedical).Name.Replace("dd", "").ToUpper();
                    if (sNameDropdown != "")
                    {
                        (ctrl as DropDownMedical).SetTypeDropDownList(sNameDropdown);
                    }

                }
            }
        }

        private void setLang()
        {
            this.Text = "Medical Management-Generación de Pacientes";
            lblNombre.Text = "Nombre";
            fechaAlta.SetTitle("Fecha alta");
            fechaIngreso.SetTitle("Fecha ingreso");
            fechaNacimiento.SetTitle("Fecha nacimiento");
            inputNHC.SetTitle("NHC");
            ddSexo.SetTitle("Sexo");
            btnExit.Text = "Salir";
            btnSave.Text = "Guardar";
            decimalCreatininiIngreso.SetTitle("Creatinina en Ingreso");
            decimalCreatininiIngreso_2.SetTitle("Creatinina en Ingreso");

            deciamlCreatininaAlta.SetTitle("Creatinina en Alta");
            deciamlCreatininaAlta_2.SetTitle("Creatinina en Alta");

            deciamlPicoTensionalDiurno.SetTitle("Pico Tensional Diurno");
            deciamlPicoTensionalDiurno_2.SetTitle("Pico Tensional Diurno");


            decimalMediaTensionalDiurno.SetTitle("Media Tensional Diurno");
            decimalMediaTensionalDiurno_2.SetTitle("Media Tensional Diurno");

            decimalMediaTensionalNocturno.SetTitle("Media Tensional Nocturno");
            decimalMediaTensionalNocturno_2.SetTitle("Media Tensional Nocturno");

            decimalPicoTensionalNocturno.SetTitle("Pico Tensional Nocturno");
            decimalPicoTensionalNocturno_2.SetTitle("Pico Tensional Nocturno");

            decimalMediaFCDuranteIngreso.SetTitle("Media FC durante Ingreso");
            decimalMediaFCDuranteIngreso_2.SetTitle("Media FC durante Ingreso");
            numericFCalIngreso.SetTitle("FC en Ingreso");
            numericFCalIngreso_2.SetTitle("FC en Ingreso");

            numericNivelesHBA.SetTitle("Niveles HbA1(%)");
            // numericNivelesHBA_2.SetTitle("Niveles HbA1(%)");

            numericSumFraminghanMayores.SetTitle("Suma Niv.Framinghan Mayores");
            numericSumFraminghanMayores_2.SetTitle("Suma Niv.Framinghan Mayores");

            numericSumFraminghanMenores.SetTitle("Suma Niv.Framinghan Menores");
            numericSumFraminghanMenores_2.SetTitle("Suma Niv.Framinghan Menores");

            inputTimehorapicotensional_nocturno.SetTitle("Hora Pico Tensional Nocturno");
            inputTimehorapicotensional_diurno.SetTitle("Hora Pico Tensional Diurno");

            inputTimehorapicotensional_nocturno_2.SetTitle("Hora Pico Tensional Nocturno");
            inputTimehorapicotensional_diurno_2.SetTitle("Hora Pico Tensional Diurno");


        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            /*COMPROBACIONES SI HAN INTRODUCIDO DATOS Y NO HAN GUARDADO, AVISAR!*/
            if (txtNombre.Text != "" || inputNHC.Value() != 0 || ddSexo.GetValue() != "-1" ||
                fechaNacimiento.getDate() != null || fechaIngreso.getDate() != null || fechaAlta.getDate() != null)
            {
                if (DialogResult.Yes == RadMessageBox.Show("Hay datos sin guardar. ¿Desea salir?", this.Text, MessageBoxButtons.YesNo, RadMessageIcon.Question))
                    this.Close();
            }
            else
                this.Close();
        }

        private void chkMoreDataICC_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            pageviewSCAICC.Pages.LastOrDefault().Enabled = chkMoreDataICC.Checked;
        }

        private void chkMoreDataSCA_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            pageviewSCAICC.Pages.FirstOrDefault().Enabled = chkMoreDataSCA.Checked;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            md_sca mdSCAToAdd = new md_sca();
            md_icc mdICCToAdd = new md_icc();
            sMessageValidation = "";
            if (txtNombre.Text == "" || ddSexo.GetValue() == "")
            {
                RadMessageBox.Show("Debe informar el nombre y sexo del paciente.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            if (chkMoreDataICC.Checked == false && chkMoreDataSCA.Checked == false)
                RadMessageBox.Show("No se le asignará el SCA e ICC. Para administrarlo posteriormente, dirijase a la opción 'Gestión de SCA e ICC' del menú principal.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Info);

            md_pacientes newPaciente = new md_pacientes();
            newPaciente.md_nombre = txtNombre.Text;
            newPaciente.md_sexo = Convert.ToInt32(ddSexo.GetValue());
            newPaciente.md_nhc = inputNHC.Value();
            newPaciente.md_fechaalta = fechaAlta.getDate();
            newPaciente.md_fechaingreso = fechaIngreso.getDate();
            newPaciente.md_fechanacimiento = fechaNacimiento.getDate();
            int iNewRecordId = 0;
            sMessageValidation = MdPacientes.AddPaciente(newPaciente, ref iNewRecordId);
            if (sMessageValidation != "")
            {
                RadMessageBox.Show(this, sMessageValidation, this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            else
            {
                if (chkMoreDataICC.Checked)
                {
                    mdICCToAdd = new md_icc
                    {
                        md_amiodaronapost = Convert.ToInt32(ddamiodaronapost_2.GetValue()),
                        md_amiodaronapre = Convert.ToInt32(ddamiodaronapre_2.GetValue()),
                        md_antecedentesfa = Convert.ToInt32(ddfa_2.GetValue()),
                        md_antialdosteronicospost = Convert.ToInt32(ddantialdosteronicospost_2.GetValue()),
                        md_antialdosteronicospre = Convert.ToInt32(ddantialdosteronicospre_2.GetValue()),
                        md_anticoagulacioningreso = Convert.ToInt32(ddanticoagulacion_2.GetValue()),
                        md_betabloqueantespost = Convert.ToInt32(ddbetabloqueantespost_2.GetValue()),
                        md_betabloqueantespre = Convert.ToInt32(ddbetabloqueantespre_2.GetValue()),
                        md_cardiopatiaisquemica = Convert.ToInt32(ddcardiopatia_2.GetValue()),
                        md_clasefuncionalbasal = Convert.ToInt32(ddbasal_2.GetValue()),
                        md_creatininaalta = deciamlCreatininaAlta_2.getValue(),
                        md_creatininaingreso = decimalCreatininiIngreso_2.getValue(),
                        md_diabetesmellitus = Convert.ToInt32(dddiabetes_2.GetValue()),
                        md_digoxinapost = Convert.ToInt32(dddigoxinapost_2.GetValue()),
                        md_digoxinapre = Convert.ToInt32(dddigoxinapre_2.GetValue()),
                        md_dislipemia = Convert.ToInt32(dddislipemia_2.GetValue()),
                        md_ecocardiograma = Convert.ToInt32(ddecocardiograma_2.GetValue()),
                        md_ecocardiogramaduranteingreso = Convert.ToInt32(ddeco_2.GetValue()),
                        md_exitus = Convert.ToInt32(ddexitus_2.GetValue()),
                        md_exitusdesc = txtCausaExitus_2.Text,
                        md_fccontrolada = Convert.ToInt32(ddfc_2.GetValue()),
                        md_fcingreso = numericFCalIngreso_2.Value(),
                        md_fevi = Convert.ToInt32(ddfevi_2.GetValue()),
                        md_historialsincopes = Convert.ToInt32(ddsincopes_2.GetValue()),
                        md_horapicotensional_diurno = inputTimehorapicotensional_diurno_2.GetTime(),// DateTime.Now,
                        md_horapicotensional_nocturno = inputTimehorapicotensional_nocturno_2.GetTime(),
                        md_hta = Convert.ToInt32(ddhta_2.GetValue()),
                        md_htap = Convert.ToInt32(ddhtap_2.GetValue()),
                        md_iecapost = Convert.ToInt32(ddiecapost_2.GetValue()),
                        md_iecapre = Convert.ToInt32(ddiecapre_2.GetValue()),
                        md_ingresos = Convert.ToInt32(ddingresos6_2.GetValue()),
                        md_ingresosicc = Convert.ToInt32(ddicc_2.GetValue()),
                        md_mapa = Convert.ToInt32(ddmapa_2.GetValue()),
                        md_mediafcduranteregistro = decimalMediaFCDuranteIngreso_2.getValue(),
                        md_mediatensional_diurno = decimalMediaTensionalDiurno_2.getValue(),
                        md_mediatensional_nocturno = decimalMediaTensionalNocturno_2.getValue(),
                        md_nyha = Convert.ToInt32(ddnyha_2.GetValue()),
                        md_record_id = iNewRecordId,
                        md_patron = Convert.ToInt32(ddpatron_2.GetValue()),
                        md_picotensional_diurno = deciamlPicoTensionalDiurno_2.getValue(),
                        md_picotensional_nocturno = decimalPicoTensionalNocturno_2.getValue(),
                        md_registro = Convert.ToInt32(ddregistro_2.GetValue()),
                        md_ritmoalta = Convert.ToInt32(ddritmo_2.GetValue()),
                        md_status = Convert.ToInt32(ddstatus_2.GetValue()),
                        md_sumframinghanmayor = numericSumFraminghanMayores_2.Value(),
                        md_sumframinghanmenor = numericSumFraminghanMenores_2.Value(),
                        md_taingreso = Convert.ToInt32(ddta_2.GetValue()),
                        md_valvulopatia = Convert.ToInt32(ddvalvulopatia_2.GetValue())

                    };
                    sMessageValidation = MdICC.AddICC(iNewRecordId, mdICCToAdd);
                    if (sMessageValidation != "")
                    {
                        RadMessageBox.Show(this, sMessageValidation, this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                }
                if (chkMoreDataSCA.Checked)
                {
                    mdSCAToAdd = new md_sca
                    {
                        md_amiodaronapost = Convert.ToInt32(ddamiodaronapost.GetValue()),
                        md_amiodaronapre = Convert.ToInt32(ddamiodaronapre.GetValue()),
                        md_antecedentesfa = Convert.ToInt32(ddfa.GetValue()),
                        md_antialdosteronicospost = Convert.ToInt32(ddantialdosteronicospost.GetValue()),
                        md_antialdosteronicospre = Convert.ToInt32(ddantialdosteronicospre.GetValue()),
                        md_anticoagulacioningreso = Convert.ToInt32(ddanticoagulacion.GetValue()),
                        md_anyosdm = Convert.ToInt32(ddAnyoDM.GetValue()),
                        md_betabloqueantespost = Convert.ToInt32(ddbetabloqueantespost.GetValue()),
                        md_betabloqueantespre = Convert.ToInt32(ddbetabloqueantespre.GetValue()),
                        md_cardiopatiaisquemica = Convert.ToInt32(ddcardiopatia.GetValue()),
                        md_clasefuncionalbasal = Convert.ToInt32(ddbasal.GetValue()),
                        md_creatininaalta = deciamlCreatininaAlta.getValue(),
                        md_creatininaingreso = decimalCreatininiIngreso.getValue(),
                        md_diabetesmellitus = Convert.ToInt32(dddiabetes_1.GetValue()),
                        md_digoxinapost = Convert.ToInt32(dddigoxinapost.GetValue()),
                        md_digoxinapre = Convert.ToInt32(dddigoxinapre.GetValue()),
                        md_dislipemia = Convert.ToInt32(dddislipemia.GetValue()),
                        md_ecocardiograma = Convert.ToInt32(ddecocardiograma.GetValue()),
                        md_ecocardiogramaduranteingreso = Convert.ToInt32(ddeco.GetValue()),
                        md_exitus = Convert.ToInt32(ddexitus.GetValue()),
                        md_exitusdesc = txtCausaExitus.Text,
                        md_fccontrolada = Convert.ToInt32(ddfc.GetValue()),
                        md_fcingreso = numericFCalIngreso.Value(),
                        md_fevi = Convert.ToInt32(ddfevi.GetValue()),
                        md_historialsincopes = Convert.ToInt32(ddsincopes.GetValue()),
                        md_horapicotensional_diurno = inputTimehorapicotensional_diurno.GetTime(),// DateTime.Now,
                        md_horapicotensional_nocturno = inputTimehorapicotensional_nocturno.GetTime(),
                        md_hta = Convert.ToInt32(ddhta_1.GetValue()),
                        md_htap = Convert.ToInt32(ddhtap.GetValue()),
                        md_iecapost = Convert.ToInt32(ddiecapost.GetValue()),
                        md_iecapre = Convert.ToInt32(ddiecapre.GetValue()),
                        md_ingresos = Convert.ToInt32(ddingresos6.GetValue()),
                        md_ingresosicc = Convert.ToInt32(ddicc.GetValue()),
                        md_mapa = Convert.ToInt32(ddmapa.GetValue()),
                        md_mediafcduranteregistro = decimalMediaFCDuranteIngreso.getValue(),
                        md_mediatensional_diurno = decimalMediaTensionalDiurno.getValue(),
                        md_mediatensional_nocturno = decimalMediaTensionalNocturno.getValue(),
                        md_niveleshba = numericNivelesHBA.Value(),
                        md_nyha = Convert.ToInt32(ddNyha.GetValue()),
                        md_record_id = iNewRecordId,
                        md_patron = Convert.ToInt32(ddpatron.GetValue()),
                        md_picotensional_diurno = deciamlPicoTensionalDiurno.getValue(),
                        md_picotensional_nocturno = decimalPicoTensionalNocturno.getValue(),

                        //md_pacientes=dataSCA.md_pacientes,
                        md_registro = Convert.ToInt32(ddregistro.GetValue()),
                        md_revascularizacion = Convert.ToInt32(ddRevascularizacion.GetValue()),
                        md_ritmoalta = Convert.ToInt32(ddritmo.GetValue()),
                        md_status = Convert.ToInt32(ddstatus.GetValue()),
                        md_sumframinghanmayor = numericSumFraminghanMayores.Value(),
                        md_sumframinghanmenor = numericSumFraminghanMenores.Value(),
                        md_taingreso = Convert.ToInt32(ddta.GetValue()),
                        md_valvulopatia = Convert.ToInt32(ddvalvulopatia.GetValue())

                    };
                    sMessageValidation = MdSCA.AddSCA(iNewRecordId, mdSCAToAdd);
                    if (sMessageValidation != "")
                    {
                        RadMessageBox.Show(this, sMessageValidation, this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                }
            }
            if (sMessageValidation == "")
                this.Close();



        }

        private void ddexitus_Load(object sender, EventArgs e)
        {
            ddexitus.SelectionChanged(ddexitus_SelectionChanged);
        }

        private void ddexitus_SelectionChanged(object sender, EventArgs e)
        {
            if (ddexitus.GetValue() == "2")
            {
                pnlCausaExitus.Enabled = true;
            }
            else
            {
                pnlCausaExitus.Enabled = false;
                txtCausaExitus.Text = "";
            }
        }
        private void ddexitus_2_Load(object sender, EventArgs e)
        {
            ddexitus_2.SelectionChanged(ddexitus_2_SelectionChanged);
        }
        private void ddexitus_2_SelectionChanged(object sender, EventArgs e)
        {
            if (ddexitus_2.GetValue() == "2")
            {
                pnlCausaExitus_2.Enabled = true;
            }
            else
            {
                pnlCausaExitus_2.Enabled = false;
                txtCausaExitus_2.Text = "";
            }
        }
    }
}
