﻿namespace medicalWPF.MD_Forms.Management.Pacientes
{
    partial class FrmUpdatePacientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBotonera = new Telerik.WinControls.UI.RadPanel();
            this.btnExit = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.chkMoreDataSCA = new Telerik.WinControls.UI.RadCheckBox();
            this.chkMoreDataICC = new Telerik.WinControls.UI.RadCheckBox();
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.fechaNacimiento = new MD_Controls.InputDate();
            this.txtNombre = new Telerik.WinControls.UI.RadTextBox();
            this.fechaIngreso = new MD_Controls.InputDate();
            this.inputNHC = new MD_Controls.InputNumeric();
            this.fechaAlta = new MD_Controls.InputDate();
            this.ddSexo = new MD_Controls.DropDownMedical();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBotonera)).BeginInit();
            this.pnlBotonera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMoreDataSCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMoreDataICC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBotonera
            // 
            this.pnlBotonera.Controls.Add(this.btnExit);
            this.pnlBotonera.Controls.Add(this.btnSave);
            this.pnlBotonera.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBotonera.Location = new System.Drawing.Point(0, 213);
            this.pnlBotonera.Name = "pnlBotonera";
            this.pnlBotonera.Size = new System.Drawing.Size(792, 54);
            this.pnlBotonera.TabIndex = 12;
            ((Telerik.WinControls.UI.RadPanelElement)(this.pnlBotonera.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Image = global::medicalWPF.Properties.Resources.Exit1;
            this.btnExit.Location = new System.Drawing.Point(679, 6);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 33);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "[btnExit]";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Image = global::medicalWPF.Properties.Resources.Save;
            this.btnSave.Location = new System.Drawing.Point(12, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(110, 33);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "[btnSave]";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.chkMoreDataSCA);
            this.radPanel1.Controls.Add(this.chkMoreDataICC);
            this.radPanel1.Controls.Add(this.lblNombre);
            this.radPanel1.Controls.Add(this.fechaNacimiento);
            this.radPanel1.Controls.Add(this.txtNombre);
            this.radPanel1.Controls.Add(this.fechaIngreso);
            this.radPanel1.Controls.Add(this.inputNHC);
            this.radPanel1.Controls.Add(this.fechaAlta);
            this.radPanel1.Controls.Add(this.ddSexo);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(792, 162);
            this.radPanel1.TabIndex = 10;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radPanel1.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radPanel1.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radPanel1.GetChildAt(0).GetChildAt(1))).Enabled = true;
            // 
            // chkMoreDataSCA
            // 
            this.chkMoreDataSCA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkMoreDataSCA.Location = new System.Drawing.Point(606, 68);
            this.chkMoreDataSCA.Name = "chkMoreDataSCA";
            this.chkMoreDataSCA.Size = new System.Drawing.Size(81, 18);
            this.chkMoreDataSCA.TabIndex = 10;
            this.chkMoreDataSCA.Text = "Asignar SCA";
            this.chkMoreDataSCA.Visible = false;
            // 
            // chkMoreDataICC
            // 
            this.chkMoreDataICC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkMoreDataICC.Location = new System.Drawing.Point(606, 24);
            this.chkMoreDataICC.Name = "chkMoreDataICC";
            this.chkMoreDataICC.Size = new System.Drawing.Size(78, 18);
            this.chkMoreDataICC.TabIndex = 9;
            this.chkMoreDataICC.Text = "Asignar ICC";
            this.chkMoreDataICC.Visible = false;
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(22, 22);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(60, 18);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "lblNombre";
            // 
            // fechaNacimiento
            // 
            this.fechaNacimiento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fechaNacimiento.Location = new System.Drawing.Point(448, 22);
            this.fechaNacimiento.Name = "fechaNacimiento";
            this.fechaNacimiento.Size = new System.Drawing.Size(278, 30);
            this.fechaNacimiento.TabIndex = 3;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(187, 22);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 8;
            // 
            // fechaIngreso
            // 
            this.fechaIngreso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fechaIngreso.Location = new System.Drawing.Point(448, 68);
            this.fechaIngreso.Name = "fechaIngreso";
            this.fechaIngreso.Size = new System.Drawing.Size(278, 30);
            this.fechaIngreso.TabIndex = 4;
            // 
            // inputNHC
            // 
            this.inputNHC.Location = new System.Drawing.Point(19, 68);
            this.inputNHC.Name = "inputNHC";
            this.inputNHC.Size = new System.Drawing.Size(278, 30);
            this.inputNHC.TabIndex = 7;
            // 
            // fechaAlta
            // 
            this.fechaAlta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fechaAlta.Location = new System.Drawing.Point(448, 117);
            this.fechaAlta.Name = "fechaAlta";
            this.fechaAlta.Size = new System.Drawing.Size(278, 30);
            this.fechaAlta.TabIndex = 5;
            // 
            // ddSexo
            // 
            this.ddSexo.Location = new System.Drawing.Point(19, 116);
            this.ddSexo.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddSexo.Name = "ddSexo";
            this.ddSexo.Size = new System.Drawing.Size(278, 30);
            this.ddSexo.TabIndex = 6;
            // 
            // FrmUpdatePacientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 267);
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.pnlBotonera);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(800, 297);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(800, 297);
            this.Name = "FrmUpdatePacientes";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(800, 297);
            this.Text = "FrmUpdatePacientes";
            this.Load += new System.EventHandler(this.FrmUpdatePacientes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnlBotonera)).EndInit();
            this.pnlBotonera.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMoreDataSCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMoreDataICC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel pnlBotonera;
        private Telerik.WinControls.UI.RadButton btnExit;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadCheckBox chkMoreDataSCA;
        private Telerik.WinControls.UI.RadCheckBox chkMoreDataICC;
        private Telerik.WinControls.UI.RadLabel lblNombre;
        private MD_Controls.InputDate fechaNacimiento;
        private Telerik.WinControls.UI.RadTextBox txtNombre;
        private MD_Controls.InputDate fechaIngreso;
        private MD_Controls.InputNumeric inputNHC;
        private MD_Controls.InputDate fechaAlta;
        private MD_Controls.DropDownMedical ddSexo;
    }
}
