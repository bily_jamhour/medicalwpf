﻿using MD_Controls;
using medical_Data_Lib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
namespace medicalWPF.MD_Forms.Management.SCA
{
    public partial class FrmCreateOrUpdateSCA : RadForm
    {
        private md_sca _rowMDSCAToEdit = new md_sca();
        int x = 0;
        public FrmCreateOrUpdateSCA(md_sca rmdSCAEdit = null)
        {
            InitializeComponent();
            _rowMDSCAToEdit = rmdSCAEdit;
        }

        private void FrmCreateOrUpdateSCA_Load(object sender, EventArgs e)
        {
            ddPacientes.ValueMember = "md_record_id";
            ddPacientes.DisplayMember = "md_nombre";
            ddPacientes.DataSource = MDCommonData.getIdAndNamePacientesInNotSCA();
            txtnombre.Visible = false;
            loadDropDownsICC();
            setLang();
            if (_rowMDSCAToEdit == null)
                ddPacientes.Visible = true;
            else
                LoadControls();

            
        }

        private void LoadControls()
        {
            ddPacientes.Visible = false;
            txtnombre.Visible = true;
            loadDataICC();
        }
        private void loadDataPaciente()
        {
            md_pacientes paciente = null;
            if (_rowMDSCAToEdit != null)
                paciente = MdPacientes.GetPacienteFromRecordId(_rowMDSCAToEdit.md_record_id);
            else
                paciente = MdPacientes.GetPacienteFromRecordId(Convert.ToInt32(ddPacientes.SelectedValue));

            if (paciente != null)
            {
                txtnombre.Text = paciente.md_nombre;
                txtFechaIngreso.Text = Convert.ToString(paciente.md_fechaingreso);
                txtnhc.Text = paciente.md_nhc.ToString();
                txtSexo.Text = (paciente.md_sexo == 0) ? "Hombre" : "Mujer";
                txtFechaNacimiento.Text = Convert.ToString(paciente.md_fechanacimiento);
                txtFechaAlta.Text = Convert.ToString(paciente.md_fechaalta);
            }
        }
        private void loadDataICC()
        {
            loadDataPaciente();
            ddhtap.SetValue(_rowMDSCAToEdit.md_htap.ToString());
            ddamiodaronapost.SetValue(_rowMDSCAToEdit.md_amiodaronapost.ToString());
            ddamiodaronapre.SetValue(_rowMDSCAToEdit.md_amiodaronapre.ToString());
            ddanticoagulacion.SetValue(_rowMDSCAToEdit.md_anticoagulacioningreso.ToString());
            ddbasal.SetValue(_rowMDSCAToEdit.md_clasefuncionalbasal.ToString());
            ddbetabloqueantespost.SetValue(_rowMDSCAToEdit.md_betabloqueantespost.ToString());
            ddbetabloqueantespre.SetValue(_rowMDSCAToEdit.md_betabloqueantespre.ToString());
            ddcardiopatia.SetValue(_rowMDSCAToEdit.md_cardiopatiaisquemica.ToString());
            dddiabetes_1.SetValue(_rowMDSCAToEdit.md_diabetesmellitus.ToString());
            dddigoxinapost.SetValue(_rowMDSCAToEdit.md_digoxinapost.ToString());
            dddigoxinapre.SetValue(_rowMDSCAToEdit.md_digoxinapre.ToString());
            dddislipemia.SetValue(_rowMDSCAToEdit.md_dislipemia.ToString());
            ddecocardiograma.SetValue(_rowMDSCAToEdit.md_ecocardiograma.ToString());
            ddeco.SetValue(_rowMDSCAToEdit.md_ecocardiogramaduranteingreso.ToString());
            ddexitus.SetValue(_rowMDSCAToEdit.md_exitus.ToString());
            txtCausaExitus.Text = _rowMDSCAToEdit.md_exitusdesc;
            ddfa.SetValue(_rowMDSCAToEdit.md_antecedentesfa.ToString());
            ddfc.SetValue(_rowMDSCAToEdit.md_fccontrolada.ToString());
            ddfevi.SetValue(_rowMDSCAToEdit.md_fevi.ToString());
            ddhta_1.SetValue(_rowMDSCAToEdit.md_hta.ToString());
            ddicc.SetValue(_rowMDSCAToEdit.md_ingresosicc.ToString());
            ddiecapost.SetValue(_rowMDSCAToEdit.md_iecapost.ToString());
            ddiecapre.SetValue(_rowMDSCAToEdit.md_iecapre.ToString());
            ddingresos6.SetValue(_rowMDSCAToEdit.md_ingresos.ToString());
            ddmapa.SetValue(_rowMDSCAToEdit.md_mapa.ToString());
            ddNyha.SetValue(_rowMDSCAToEdit.md_nyha.ToString());
            ddpatron.SetValue(_rowMDSCAToEdit.md_patron.ToString());
            ddregistro.SetValue(_rowMDSCAToEdit.md_registro.ToString());
            ddritmo.SetValue(_rowMDSCAToEdit.md_ritmoalta.ToString());
            ddsincopes.SetValue(_rowMDSCAToEdit.md_historialsincopes.ToString());
            ddstatus.SetValue(_rowMDSCAToEdit.md_status.ToString());
            ddta.SetValue(_rowMDSCAToEdit.md_taingreso.ToString());
            ddvalvulopatia.SetValue(_rowMDSCAToEdit.md_valvulopatia.ToString());
            deciamlPicoTensionalDiurno.setValue(_rowMDSCAToEdit.md_picotensional_diurno.ToString());
            deciamlCreatininaAlta.setValue(_rowMDSCAToEdit.md_creatininaalta.ToString());
            decimalCreatininiIngreso.setValue(_rowMDSCAToEdit.md_creatininaingreso.ToString());
            decimalMediaFCDuranteIngreso.setValue(_rowMDSCAToEdit.md_mediafcduranteregistro.ToString());
            decimalMediaTensionalDiurno.setValue(_rowMDSCAToEdit.md_mediatensional_diurno.ToString());
            decimalMediaTensionalNocturno.setValue(_rowMDSCAToEdit.md_mediatensional_nocturno.ToString());
            decimalPicoTensionalNocturno.setValue(_rowMDSCAToEdit.md_picotensional_nocturno.ToString());
            if (_rowMDSCAToEdit.md_horapicotensional_diurno != null)
                inputTimehorapicotensional_diurno.SetValue(_rowMDSCAToEdit.md_horapicotensional_diurno.Value);
            if (_rowMDSCAToEdit.md_horapicotensional_nocturno != null)
                inputTimehorapicotensional_nocturno.SetValue(_rowMDSCAToEdit.md_horapicotensional_nocturno.Value);

            numericSumFraminghanMayores.SetValue(_rowMDSCAToEdit.md_sumframinghanmayor.ToString());
            numericSumFraminghanMenores.SetValue(_rowMDSCAToEdit.md_sumframinghanmenor.ToString());
            numericFCalIngreso.SetValue(_rowMDSCAToEdit.md_fcingreso.ToString());
            ddantialdosteronicospost.SetValue(_rowMDSCAToEdit.md_antialdosteronicospost.ToString());
            ddantialdosteronicospre.SetValue(_rowMDSCAToEdit.md_antialdosteronicospre.ToString());
            //Campos propios de SCA:
            ddRevascularizacion.SetValue(_rowMDSCAToEdit.md_revascularizacion.ToString());
            ddAnyoDM.SetValue(_rowMDSCAToEdit.md_anyosdm.ToString());
            numericNivelesHBA.SetValue(_rowMDSCAToEdit.md_niveleshba.ToString());
        }

        private void loadDropDownsICC()
        {
            foreach (var ctrl in pageviewICC.Controls)
            {
                if (ctrl is DropDownMedical)
                {
                    string sNameDropdown = (ctrl as DropDownMedical).Name.Replace("dd", "").ToUpper();
                    if (sNameDropdown != "")
                    {
                        (ctrl as DropDownMedical).SetTypeDropDownList(sNameDropdown);
                    }
                }
            }
        }

        private void setLang()
        {
            this.Text = "Medical Management-Gestión de SCA";
            lblPacienteToAssing.Text = "Paciente a asignar";
            lblNombre.Text = "Nombre";
            lblFechaAlta.Text = "Fecha alta";
            lblfechaingreso.Text = "Fecha ingreso";
            lblFechaNacimiento.Text = "Fecha nacimiento";
            lblNHC.Text = "NHC";
            lblSexo.Text = "Sexo";
            btnExit.Text = "Salir";
            btnSave.Text = "Guardar";
            decimalCreatininiIngreso.SetTitle("Creatinina en Ingreso");

            deciamlCreatininaAlta.SetTitle("Creatinina en Alta");

            deciamlPicoTensionalDiurno.SetTitle("Pico Tensional Diurno");



            decimalMediaTensionalDiurno.SetTitle("Media Tensional Diurno");

            decimalMediaTensionalNocturno.SetTitle("Media Tensional Nocturno");

            decimalPicoTensionalNocturno.SetTitle("Pico Tensional Nocturno");

            decimalMediaFCDuranteIngreso.SetTitle("Media FC durante Ingreso");
            numericFCalIngreso.SetTitle("FC en Ingreso");


            numericSumFraminghanMayores.SetTitle("Suma Niv.Framinghan Mayores");

            numericSumFraminghanMenores.SetTitle("Suma Niv.Framinghan Menores");


            inputTimehorapicotensional_nocturno.SetTitle("Hora Pico Tensional Nocturno");
            inputTimehorapicotensional_diurno.SetTitle("Hora Pico Tensional Diurno");

            numericNivelesHBA.SetTitle("Niveles HBA");
        }

        private void ddexitus_2_Load(object sender, EventArgs e)
        {
            ddexitus.SelectionChanged(ddexitus_SelectionChanged);
        }
        private void ddexitus_SelectionChanged(object sender, EventArgs e)
        {
            if (ddexitus.GetValue() == "2")
            {
                pnlCausaExitus.Enabled = true;
            }
            else
            {
                pnlCausaExitus.Enabled = false;
                txtCausaExitus.Text = "";
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == RadMessageBox.Show("Los cambios no almacenados se perderán. ¿Desea salir?", this.Text, MessageBoxButtons.YesNo, RadMessageIcon.Question))
                this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string sMessageValidation = "";
            md_sca mdSCAToUpdate = new md_sca();
            mdSCAToUpdate = new md_sca
            {
                md_amiodaronapost = Convert.ToInt32(ddamiodaronapost.GetValue()),
                md_amiodaronapre = Convert.ToInt32(ddamiodaronapre.GetValue()),
                md_antecedentesfa = Convert.ToInt32(ddfa.GetValue()),
                md_antialdosteronicospost = Convert.ToInt32(ddantialdosteronicospost.GetValue()),
                md_antialdosteronicospre = Convert.ToInt32(ddantialdosteronicospre.GetValue()),
                md_anticoagulacioningreso = Convert.ToInt32(ddanticoagulacion.GetValue()),
                md_betabloqueantespost = Convert.ToInt32(ddbetabloqueantespost.GetValue()),
                md_betabloqueantespre = Convert.ToInt32(ddbetabloqueantespre.GetValue()),
                md_cardiopatiaisquemica = Convert.ToInt32(ddcardiopatia.GetValue()),
                md_clasefuncionalbasal = Convert.ToInt32(ddbasal.GetValue()),
                md_creatininaalta = deciamlCreatininaAlta.getValue(),
                md_creatininaingreso = decimalCreatininiIngreso.getValue(),
                md_diabetesmellitus = Convert.ToInt32(dddiabetes_1.GetValue()),
                md_digoxinapost = Convert.ToInt32(dddigoxinapost.GetValue()),
                md_digoxinapre = Convert.ToInt32(dddigoxinapre.GetValue()),
                md_dislipemia = Convert.ToInt32(dddislipemia.GetValue()),
                md_ecocardiograma = Convert.ToInt32(ddecocardiograma.GetValue()),
                md_ecocardiogramaduranteingreso = Convert.ToInt32(ddeco.GetValue()),
                md_exitus = Convert.ToInt32(ddexitus.GetValue()),
                md_exitusdesc = txtCausaExitus.Text,
                md_fccontrolada = Convert.ToInt32(ddfc.GetValue()),
                md_fcingreso = numericFCalIngreso.Value(),
                md_fevi = Convert.ToInt32(ddfevi.GetValue()),
                md_historialsincopes = Convert.ToInt32(ddsincopes.GetValue()),
                md_horapicotensional_diurno = inputTimehorapicotensional_diurno.GetTime(),// DateTime.Now,
                md_horapicotensional_nocturno = inputTimehorapicotensional_nocturno.GetTime(),
                md_hta = Convert.ToInt32(ddhta_1.GetValue()),
                md_htap = Convert.ToInt32(ddhtap.GetValue()),
                md_iecapost = Convert.ToInt32(ddiecapost.GetValue()),
                md_iecapre = Convert.ToInt32(ddiecapre.GetValue()),
                md_ingresos = Convert.ToInt32(ddingresos6.GetValue()),
                md_ingresosicc = Convert.ToInt32(ddicc.GetValue()),
                md_mapa = Convert.ToInt32(ddmapa.GetValue()),
                md_mediafcduranteregistro = decimalMediaFCDuranteIngreso.getValue(),
                md_mediatensional_diurno = decimalMediaTensionalDiurno.getValue(),
                md_mediatensional_nocturno = decimalMediaTensionalNocturno.getValue(),
                md_nyha = Convert.ToInt32(ddNyha.GetValue()),
                md_record_id = (_rowMDSCAToEdit == null) ? Convert.ToInt32(ddPacientes.SelectedValue) : _rowMDSCAToEdit.md_record_id,
                md_patron = Convert.ToInt32(ddpatron.GetValue()),
                md_picotensional_diurno = deciamlPicoTensionalDiurno.getValue(),
                md_picotensional_nocturno = decimalPicoTensionalNocturno.getValue(),
                md_registro = Convert.ToInt32(ddregistro.GetValue()),
                md_ritmoalta = Convert.ToInt32(ddritmo.GetValue()),
                md_status = Convert.ToInt32(ddstatus.GetValue()),
                md_sumframinghanmayor = numericSumFraminghanMayores.Value(),
                md_sumframinghanmenor = numericSumFraminghanMenores.Value(),
                md_taingreso = Convert.ToInt32(ddta.GetValue()),
                md_valvulopatia = Convert.ToInt32(ddvalvulopatia.GetValue()),
                md_revascularizacion = Convert.ToInt32(ddRevascularizacion.GetValue()),
                md_anyosdm = Convert.ToInt32(ddAnyoDM.GetValue()),
                md_niveleshba = numericNivelesHBA.Value()
            };
            if (_rowMDSCAToEdit == null)
                sMessageValidation = MdSCA.AddSCA(Convert.ToInt32(ddPacientes.SelectedValue), mdSCAToUpdate);
            else
                sMessageValidation = MdSCA.UpdateSCA(Convert.ToInt32(_rowMDSCAToEdit.md_record_id), mdSCAToUpdate);
            if (sMessageValidation != "")
            {
                RadMessageBox.Show(this, sMessageValidation, this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }
            this.Close();
        }

        private void ddPacientes_SelectedValueChanged(object sender, EventArgs e)
        {
            loadDataPaciente();
        }
    }
}
