﻿namespace medicalWPF.MD_Forms.Management.SCA
{
    partial class FrmCreateOrUpdateSCA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pageviewICC = new Telerik.WinControls.UI.RadPanel();
            this.btnExit = new Telerik.WinControls.UI.RadButton();
            this.pnlBotonera = new Telerik.WinControls.UI.RadPanel();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.ddPacientes = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.txtnombre = new Telerik.WinControls.UI.RadLabel();
            this.txtFechaAlta = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaAlta = new Telerik.WinControls.UI.RadLabel();
            this.txtFechaNacimiento = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaNacimiento = new Telerik.WinControls.UI.RadLabel();
            this.txtFechaIngreso = new Telerik.WinControls.UI.RadLabel();
            this.txtnhc = new Telerik.WinControls.UI.RadLabel();
            this.txtSexo = new Telerik.WinControls.UI.RadLabel();
            this.lblfechaingreso = new Telerik.WinControls.UI.RadLabel();
            this.lblNHC = new Telerik.WinControls.UI.RadLabel();
            this.lblSexo = new Telerik.WinControls.UI.RadLabel();
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.lblPacienteToAssing = new Telerik.WinControls.UI.RadLabel();
            this.inputTimehorapicotensional_diurno = new MD_Controls.InputTime();
            this.inputTimehorapicotensional_nocturno = new MD_Controls.InputTime();
            this.decimalMediaFCDuranteIngreso = new MD_Controls.InputDecimals();
            this.ddRevascularizacion = new MD_Controls.DropDownMedical();
            this.ddNyha = new MD_Controls.DropDownMedical();
            this.ddAnyoDM = new MD_Controls.DropDownMedical();
            this.numericSumFraminghanMenores = new MD_Controls.InputNumeric();
            this.numericFCalIngreso = new MD_Controls.InputNumeric();
            this.numericSumFraminghanMayores = new MD_Controls.InputNumeric();
            this.numericNivelesHBA = new MD_Controls.InputNumeric();
            this.decimalMediaTensionalNocturno = new MD_Controls.InputDecimals();
            this.decimalPicoTensionalNocturno = new MD_Controls.InputDecimals();
            this.decimalMediaTensionalDiurno = new MD_Controls.InputDecimals();
            this.deciamlPicoTensionalDiurno = new MD_Controls.InputDecimals();
            this.deciamlCreatininaAlta = new MD_Controls.InputDecimals();
            this.decimalCreatininiIngreso = new MD_Controls.InputDecimals();
            this.pnlCausaExitus = new Telerik.WinControls.UI.RadPanel();
            this.txtCausaExitus = new Telerik.WinControls.UI.RadTextBoxControl();
            this.ddexitus = new MD_Controls.DropDownMedical();
            this.ddstatus = new MD_Controls.DropDownMedical();
            this.ddingresos6 = new MD_Controls.DropDownMedical();
            this.ddpatron = new MD_Controls.DropDownMedical();
            this.ddregistro = new MD_Controls.DropDownMedical();
            this.ddmapa = new MD_Controls.DropDownMedical();
            this.ddantialdosteronicospost = new MD_Controls.DropDownMedical();
            this.ddantialdosteronicospre = new MD_Controls.DropDownMedical();
            this.dddigoxinapost = new MD_Controls.DropDownMedical();
            this.dddigoxinapre = new MD_Controls.DropDownMedical();
            this.ddamiodaronapost = new MD_Controls.DropDownMedical();
            this.ddamiodaronapre = new MD_Controls.DropDownMedical();
            this.ddiecapost = new MD_Controls.DropDownMedical();
            this.ddiecapre = new MD_Controls.DropDownMedical();
            this.ddbetabloqueantespost = new MD_Controls.DropDownMedical();
            this.ddbetabloqueantespre = new MD_Controls.DropDownMedical();
            this.ddritmo = new MD_Controls.DropDownMedical();
            this.ddfc = new MD_Controls.DropDownMedical();
            this.ddhtap = new MD_Controls.DropDownMedical();
            this.ddvalvulopatia = new MD_Controls.DropDownMedical();
            this.ddfevi = new MD_Controls.DropDownMedical();
            this.ddeco = new MD_Controls.DropDownMedical();
            this.ddta = new MD_Controls.DropDownMedical();
            this.ddbasal = new MD_Controls.DropDownMedical();
            this.ddecocardiograma = new MD_Controls.DropDownMedical();
            this.ddanticoagulacion = new MD_Controls.DropDownMedical();
            this.ddfa = new MD_Controls.DropDownMedical();
            this.ddicc = new MD_Controls.DropDownMedical();
            this.ddsincopes = new MD_Controls.DropDownMedical();
            this.ddcardiopatia = new MD_Controls.DropDownMedical();
            this.dddislipemia = new MD_Controls.DropDownMedical();
            this.dddiabetes_1 = new MD_Controls.DropDownMedical();
            this.ddhta_1 = new MD_Controls.DropDownMedical();
            ((System.ComponentModel.ISupportInitialize)(this.pageviewICC)).BeginInit();
            this.pageviewICC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBotonera)).BeginInit();
            this.pnlBotonera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtnombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnhc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSexo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfechaingreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNHC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSexo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPacienteToAssing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCausaExitus)).BeginInit();
            this.pnlCausaExitus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCausaExitus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pageviewICC
            // 
            this.pageviewICC.Controls.Add(this.inputTimehorapicotensional_diurno);
            this.pageviewICC.Controls.Add(this.inputTimehorapicotensional_nocturno);
            this.pageviewICC.Controls.Add(this.decimalMediaFCDuranteIngreso);
            this.pageviewICC.Controls.Add(this.ddRevascularizacion);
            this.pageviewICC.Controls.Add(this.ddNyha);
            this.pageviewICC.Controls.Add(this.ddAnyoDM);
            this.pageviewICC.Controls.Add(this.numericSumFraminghanMenores);
            this.pageviewICC.Controls.Add(this.numericFCalIngreso);
            this.pageviewICC.Controls.Add(this.numericSumFraminghanMayores);
            this.pageviewICC.Controls.Add(this.numericNivelesHBA);
            this.pageviewICC.Controls.Add(this.decimalMediaTensionalNocturno);
            this.pageviewICC.Controls.Add(this.decimalPicoTensionalNocturno);
            this.pageviewICC.Controls.Add(this.decimalMediaTensionalDiurno);
            this.pageviewICC.Controls.Add(this.deciamlPicoTensionalDiurno);
            this.pageviewICC.Controls.Add(this.deciamlCreatininaAlta);
            this.pageviewICC.Controls.Add(this.decimalCreatininiIngreso);
            this.pageviewICC.Controls.Add(this.pnlCausaExitus);
            this.pageviewICC.Controls.Add(this.ddexitus);
            this.pageviewICC.Controls.Add(this.ddstatus);
            this.pageviewICC.Controls.Add(this.ddingresos6);
            this.pageviewICC.Controls.Add(this.ddpatron);
            this.pageviewICC.Controls.Add(this.ddregistro);
            this.pageviewICC.Controls.Add(this.ddmapa);
            this.pageviewICC.Controls.Add(this.ddantialdosteronicospost);
            this.pageviewICC.Controls.Add(this.ddantialdosteronicospre);
            this.pageviewICC.Controls.Add(this.dddigoxinapost);
            this.pageviewICC.Controls.Add(this.dddigoxinapre);
            this.pageviewICC.Controls.Add(this.ddamiodaronapost);
            this.pageviewICC.Controls.Add(this.ddamiodaronapre);
            this.pageviewICC.Controls.Add(this.ddiecapost);
            this.pageviewICC.Controls.Add(this.ddiecapre);
            this.pageviewICC.Controls.Add(this.ddbetabloqueantespost);
            this.pageviewICC.Controls.Add(this.ddbetabloqueantespre);
            this.pageviewICC.Controls.Add(this.ddritmo);
            this.pageviewICC.Controls.Add(this.ddfc);
            this.pageviewICC.Controls.Add(this.ddhtap);
            this.pageviewICC.Controls.Add(this.ddvalvulopatia);
            this.pageviewICC.Controls.Add(this.ddfevi);
            this.pageviewICC.Controls.Add(this.ddeco);
            this.pageviewICC.Controls.Add(this.ddta);
            this.pageviewICC.Controls.Add(this.ddbasal);
            this.pageviewICC.Controls.Add(this.ddecocardiograma);
            this.pageviewICC.Controls.Add(this.ddanticoagulacion);
            this.pageviewICC.Controls.Add(this.ddfa);
            this.pageviewICC.Controls.Add(this.ddicc);
            this.pageviewICC.Controls.Add(this.ddsincopes);
            this.pageviewICC.Controls.Add(this.ddcardiopatia);
            this.pageviewICC.Controls.Add(this.dddislipemia);
            this.pageviewICC.Controls.Add(this.dddiabetes_1);
            this.pageviewICC.Controls.Add(this.ddhta_1);
            this.pageviewICC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageviewICC.Location = new System.Drawing.Point(0, 95);
            this.pageviewICC.Name = "pageviewICC";
            this.pageviewICC.Size = new System.Drawing.Size(1380, 873);
            this.pageviewICC.TabIndex = 157;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Image = global::medicalWPF.Properties.Resources.Exit1;
            this.btnExit.Location = new System.Drawing.Point(1267, 10);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 33);
            this.btnExit.TabIndex = 2;
            this.btnExit.TabStop = false;
            this.btnExit.Text = "[btnExit]";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlBotonera
            // 
            this.pnlBotonera.BackColor = System.Drawing.Color.Transparent;
            this.pnlBotonera.Controls.Add(this.btnExit);
            this.pnlBotonera.Controls.Add(this.btnSave);
            this.pnlBotonera.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBotonera.Location = new System.Drawing.Point(0, 968);
            this.pnlBotonera.Name = "pnlBotonera";
            this.pnlBotonera.Size = new System.Drawing.Size(1380, 58);
            this.pnlBotonera.TabIndex = 158;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pnlBotonera.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Image = global::medicalWPF.Properties.Resources.Save;
            this.btnSave.Location = new System.Drawing.Point(12, 10);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(110, 33);
            this.btnSave.TabIndex = 0;
            this.btnSave.TabStop = false;
            this.btnSave.Text = "[btnSave]";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ddPacientes
            // 
            this.ddPacientes.DisplayMember = "md_nombre";
            this.ddPacientes.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddPacientes.Location = new System.Drawing.Point(196, 28);
            this.ddPacientes.Name = "ddPacientes";
            this.ddPacientes.Size = new System.Drawing.Size(226, 20);
            this.ddPacientes.TabIndex = 19;
            this.ddPacientes.Text = "radDropDownList1";
            this.ddPacientes.ValueMember = "md_record_id";
            this.ddPacientes.SelectedValueChanged += new System.EventHandler(this.ddPacientes_SelectedValueChanged);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.txtnombre);
            this.radPanel1.Controls.Add(this.txtFechaAlta);
            this.radPanel1.Controls.Add(this.lblFechaAlta);
            this.radPanel1.Controls.Add(this.txtFechaNacimiento);
            this.radPanel1.Controls.Add(this.lblFechaNacimiento);
            this.radPanel1.Controls.Add(this.txtFechaIngreso);
            this.radPanel1.Controls.Add(this.txtnhc);
            this.radPanel1.Controls.Add(this.txtSexo);
            this.radPanel1.Controls.Add(this.lblfechaingreso);
            this.radPanel1.Controls.Add(this.lblNHC);
            this.radPanel1.Controls.Add(this.lblSexo);
            this.radPanel1.Controls.Add(this.lblNombre);
            this.radPanel1.Controls.Add(this.lblPacienteToAssing);
            this.radPanel1.Controls.Add(this.ddPacientes);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(1380, 95);
            this.radPanel1.TabIndex = 156;
            // 
            // txtnombre
            // 
            this.txtnombre.Location = new System.Drawing.Point(197, 29);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(55, 18);
            this.txtnombre.TabIndex = 35;
            this.txtnombre.Text = "radLabel1";
            // 
            // txtFechaAlta
            // 
            this.txtFechaAlta.Location = new System.Drawing.Point(898, 64);
            this.txtFechaAlta.Name = "txtFechaAlta";
            this.txtFechaAlta.Size = new System.Drawing.Size(55, 18);
            this.txtFechaAlta.TabIndex = 34;
            this.txtFechaAlta.Text = "radLabel3";
            // 
            // lblFechaAlta
            // 
            this.lblFechaAlta.Location = new System.Drawing.Point(796, 64);
            this.lblFechaAlta.Name = "lblFechaAlta";
            this.lblFechaAlta.Size = new System.Drawing.Size(55, 18);
            this.lblFechaAlta.TabIndex = 33;
            this.lblFechaAlta.Text = "radLabel1";
            // 
            // txtFechaNacimiento
            // 
            this.txtFechaNacimiento.Location = new System.Drawing.Point(898, 30);
            this.txtFechaNacimiento.Name = "txtFechaNacimiento";
            this.txtFechaNacimiento.Size = new System.Drawing.Size(55, 18);
            this.txtFechaNacimiento.TabIndex = 32;
            this.txtFechaNacimiento.Text = "radLabel3";
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.Location = new System.Drawing.Point(796, 30);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(55, 18);
            this.lblFechaNacimiento.TabIndex = 31;
            this.lblFechaNacimiento.Text = "radLabel1";
            // 
            // txtFechaIngreso
            // 
            this.txtFechaIngreso.Location = new System.Drawing.Point(578, 66);
            this.txtFechaIngreso.Name = "txtFechaIngreso";
            this.txtFechaIngreso.Size = new System.Drawing.Size(55, 18);
            this.txtFechaIngreso.TabIndex = 30;
            this.txtFechaIngreso.Text = "radLabel1";
            // 
            // txtnhc
            // 
            this.txtnhc.Location = new System.Drawing.Point(578, 30);
            this.txtnhc.Name = "txtnhc";
            this.txtnhc.Size = new System.Drawing.Size(55, 18);
            this.txtnhc.TabIndex = 29;
            this.txtnhc.Text = "radLabel3";
            // 
            // txtSexo
            // 
            this.txtSexo.Location = new System.Drawing.Point(196, 64);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.Size = new System.Drawing.Size(55, 18);
            this.txtSexo.TabIndex = 28;
            this.txtSexo.Text = "radLabel1";
            // 
            // lblfechaingreso
            // 
            this.lblfechaingreso.Location = new System.Drawing.Point(476, 66);
            this.lblfechaingreso.Name = "lblfechaingreso";
            this.lblfechaingreso.Size = new System.Drawing.Size(55, 18);
            this.lblfechaingreso.TabIndex = 27;
            this.lblfechaingreso.Text = "radLabel1";
            // 
            // lblNHC
            // 
            this.lblNHC.Location = new System.Drawing.Point(476, 30);
            this.lblNHC.Name = "lblNHC";
            this.lblNHC.Size = new System.Drawing.Size(55, 18);
            this.lblNHC.TabIndex = 25;
            this.lblNHC.Text = "radLabel1";
            // 
            // lblSexo
            // 
            this.lblSexo.Location = new System.Drawing.Point(84, 64);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(55, 18);
            this.lblSexo.TabIndex = 23;
            this.lblSexo.Text = "radLabel1";
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(84, 30);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(55, 18);
            this.lblNombre.TabIndex = 22;
            this.lblNombre.Text = "radLabel1";
            // 
            // lblPacienteToAssing
            // 
            this.lblPacienteToAssing.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPacienteToAssing.Location = new System.Drawing.Point(3, 3);
            this.lblPacienteToAssing.Name = "lblPacienteToAssing";
            this.lblPacienteToAssing.Size = new System.Drawing.Size(112, 19);
            this.lblPacienteToAssing.TabIndex = 21;
            this.lblPacienteToAssing.Text = "Paciente a asignar";
            // 
            // inputTimehorapicotensional_diurno
            // 
            this.inputTimehorapicotensional_diurno.Location = new System.Drawing.Point(16, 635);
            this.inputTimehorapicotensional_diurno.Name = "inputTimehorapicotensional_diurno";
            this.inputTimehorapicotensional_diurno.Size = new System.Drawing.Size(411, 30);
            this.inputTimehorapicotensional_diurno.TabIndex = 18;
            // 
            // inputTimehorapicotensional_nocturno
            // 
            this.inputTimehorapicotensional_nocturno.Location = new System.Drawing.Point(473, 682);
            this.inputTimehorapicotensional_nocturno.Name = "inputTimehorapicotensional_nocturno";
            this.inputTimehorapicotensional_nocturno.Size = new System.Drawing.Size(411, 30);
            this.inputTimehorapicotensional_nocturno.TabIndex = 37;
            // 
            // decimalMediaFCDuranteIngreso
            // 
            this.decimalMediaFCDuranteIngreso.Location = new System.Drawing.Point(13, 570);
            this.decimalMediaFCDuranteIngreso.Name = "decimalMediaFCDuranteIngreso";
            this.decimalMediaFCDuranteIngreso.Size = new System.Drawing.Size(411, 34);
            this.decimalMediaFCDuranteIngreso.TabIndex = 16;
            // 
            // ddRevascularizacion
            // 
            this.ddRevascularizacion.Location = new System.Drawing.Point(13, 459);
            this.ddRevascularizacion.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddRevascularizacion.Name = "ddRevascularizacion";
            this.ddRevascularizacion.Size = new System.Drawing.Size(414, 34);
            this.ddRevascularizacion.TabIndex = 13;
            // 
            // ddNyha
            // 
            this.ddNyha.Location = new System.Drawing.Point(475, 459);
            this.ddNyha.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddNyha.Name = "ddNyha";
            this.ddNyha.Size = new System.Drawing.Size(414, 30);
            this.ddNyha.TabIndex = 31;
            // 
            // ddAnyoDM
            // 
            this.ddAnyoDM.Location = new System.Drawing.Point(13, 423);
            this.ddAnyoDM.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddAnyoDM.Name = "ddAnyoDM";
            this.ddAnyoDM.Size = new System.Drawing.Size(414, 30);
            this.ddAnyoDM.TabIndex = 12;
            // 
            // numericSumFraminghanMenores
            // 
            this.numericSumFraminghanMenores.Location = new System.Drawing.Point(476, 566);
            this.numericSumFraminghanMenores.Name = "numericSumFraminghanMenores";
            this.numericSumFraminghanMenores.Size = new System.Drawing.Size(411, 30);
            this.numericSumFraminghanMenores.TabIndex = 34;
            // 
            // numericFCalIngreso
            // 
            this.numericFCalIngreso.Location = new System.Drawing.Point(938, 428);
            this.numericFCalIngreso.Name = "numericFCalIngreso";
            this.numericFCalIngreso.Size = new System.Drawing.Size(411, 30);
            this.numericFCalIngreso.TabIndex = 49;
            // 
            // numericSumFraminghanMayores
            // 
            this.numericSumFraminghanMayores.Location = new System.Drawing.Point(476, 531);
            this.numericSumFraminghanMayores.Name = "numericSumFraminghanMayores";
            this.numericSumFraminghanMayores.Size = new System.Drawing.Size(411, 30);
            this.numericSumFraminghanMayores.TabIndex = 33;
            // 
            // numericNivelesHBA
            // 
            this.numericNivelesHBA.Location = new System.Drawing.Point(938, 464);
            this.numericNivelesHBA.Name = "numericNivelesHBA";
            this.numericNivelesHBA.Size = new System.Drawing.Size(411, 32);
            this.numericNivelesHBA.TabIndex = 50;
            // 
            // decimalMediaTensionalNocturno
            // 
            this.decimalMediaTensionalNocturno.Location = new System.Drawing.Point(477, 642);
            this.decimalMediaTensionalNocturno.Name = "decimalMediaTensionalNocturno";
            this.decimalMediaTensionalNocturno.Size = new System.Drawing.Size(411, 34);
            this.decimalMediaTensionalNocturno.TabIndex = 36;
            // 
            // decimalPicoTensionalNocturno
            // 
            this.decimalPicoTensionalNocturno.Location = new System.Drawing.Point(476, 602);
            this.decimalPicoTensionalNocturno.Name = "decimalPicoTensionalNocturno";
            this.decimalPicoTensionalNocturno.Size = new System.Drawing.Size(411, 34);
            this.decimalPicoTensionalNocturno.TabIndex = 35;
            // 
            // decimalMediaTensionalDiurno
            // 
            this.decimalMediaTensionalDiurno.Location = new System.Drawing.Point(476, 495);
            this.decimalMediaTensionalDiurno.Name = "decimalMediaTensionalDiurno";
            this.decimalMediaTensionalDiurno.Size = new System.Drawing.Size(411, 30);
            this.decimalMediaTensionalDiurno.TabIndex = 32;
            // 
            // deciamlPicoTensionalDiurno
            // 
            this.deciamlPicoTensionalDiurno.Location = new System.Drawing.Point(13, 606);
            this.deciamlPicoTensionalDiurno.Name = "deciamlPicoTensionalDiurno";
            this.deciamlPicoTensionalDiurno.Size = new System.Drawing.Size(411, 29);
            this.deciamlPicoTensionalDiurno.TabIndex = 17;
            // 
            // deciamlCreatininaAlta
            // 
            this.deciamlCreatininaAlta.Location = new System.Drawing.Point(13, 535);
            this.deciamlCreatininaAlta.Name = "deciamlCreatininaAlta";
            this.deciamlCreatininaAlta.Size = new System.Drawing.Size(411, 30);
            this.deciamlCreatininaAlta.TabIndex = 15;
            // 
            // decimalCreatininiIngreso
            // 
            this.decimalCreatininiIngreso.Location = new System.Drawing.Point(13, 499);
            this.decimalCreatininiIngreso.Name = "decimalCreatininiIngreso";
            this.decimalCreatininiIngreso.Size = new System.Drawing.Size(411, 30);
            this.decimalCreatininiIngreso.TabIndex = 14;
            // 
            // pnlCausaExitus
            // 
            this.pnlCausaExitus.Controls.Add(this.txtCausaExitus);
            this.pnlCausaExitus.Enabled = false;
            this.pnlCausaExitus.Location = new System.Drawing.Point(938, 388);
            this.pnlCausaExitus.Name = "pnlCausaExitus";
            this.pnlCausaExitus.Size = new System.Drawing.Size(411, 32);
            this.pnlCausaExitus.TabIndex = 138;
            this.pnlCausaExitus.Text = "Causa Exitus";
            ((Telerik.WinControls.UI.RadPanelElement)(this.pnlCausaExitus.GetChildAt(0))).Text = "Causa Exitus";
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pnlCausaExitus.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // txtCausaExitus
            // 
            this.txtCausaExitus.Location = new System.Drawing.Point(168, 7);
            this.txtCausaExitus.Name = "txtCausaExitus";
            this.txtCausaExitus.Size = new System.Drawing.Size(232, 20);
            this.txtCausaExitus.TabIndex = 48;
            // 
            // ddexitus
            // 
            this.ddexitus.Location = new System.Drawing.Point(938, 359);
            this.ddexitus.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddexitus.Name = "ddexitus";
            this.ddexitus.Size = new System.Drawing.Size(414, 30);
            this.ddexitus.TabIndex = 47;
            this.ddexitus.Load += new System.EventHandler(this.ddexitus_2_Load);
            // 
            // ddstatus
            // 
            this.ddstatus.Location = new System.Drawing.Point(938, 323);
            this.ddstatus.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddstatus.Name = "ddstatus";
            this.ddstatus.Size = new System.Drawing.Size(414, 30);
            this.ddstatus.TabIndex = 46;
            // 
            // ddingresos6
            // 
            this.ddingresos6.Location = new System.Drawing.Point(938, 287);
            this.ddingresos6.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddingresos6.Name = "ddingresos6";
            this.ddingresos6.Size = new System.Drawing.Size(414, 30);
            this.ddingresos6.TabIndex = 45;
            // 
            // ddpatron
            // 
            this.ddpatron.Location = new System.Drawing.Point(938, 251);
            this.ddpatron.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddpatron.Name = "ddpatron";
            this.ddpatron.Size = new System.Drawing.Size(414, 30);
            this.ddpatron.TabIndex = 44;
            // 
            // ddregistro
            // 
            this.ddregistro.Location = new System.Drawing.Point(938, 215);
            this.ddregistro.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddregistro.Name = "ddregistro";
            this.ddregistro.Size = new System.Drawing.Size(414, 30);
            this.ddregistro.TabIndex = 43;
            // 
            // ddmapa
            // 
            this.ddmapa.Location = new System.Drawing.Point(938, 179);
            this.ddmapa.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddmapa.Name = "ddmapa";
            this.ddmapa.Size = new System.Drawing.Size(414, 30);
            this.ddmapa.TabIndex = 42;
            // 
            // ddantialdosteronicospost
            // 
            this.ddantialdosteronicospost.Location = new System.Drawing.Point(938, 143);
            this.ddantialdosteronicospost.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddantialdosteronicospost.Name = "ddantialdosteronicospost";
            this.ddantialdosteronicospost.Size = new System.Drawing.Size(414, 30);
            this.ddantialdosteronicospost.TabIndex = 41;
            // 
            // ddantialdosteronicospre
            // 
            this.ddantialdosteronicospre.Location = new System.Drawing.Point(938, 107);
            this.ddantialdosteronicospre.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddantialdosteronicospre.Name = "ddantialdosteronicospre";
            this.ddantialdosteronicospre.Size = new System.Drawing.Size(414, 30);
            this.ddantialdosteronicospre.TabIndex = 40;
            // 
            // dddigoxinapost
            // 
            this.dddigoxinapost.Location = new System.Drawing.Point(938, 68);
            this.dddigoxinapost.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddigoxinapost.Name = "dddigoxinapost";
            this.dddigoxinapost.Size = new System.Drawing.Size(414, 30);
            this.dddigoxinapost.TabIndex = 39;
            // 
            // dddigoxinapre
            // 
            this.dddigoxinapre.Location = new System.Drawing.Point(938, 32);
            this.dddigoxinapre.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddigoxinapre.Name = "dddigoxinapre";
            this.dddigoxinapre.Size = new System.Drawing.Size(414, 30);
            this.dddigoxinapre.TabIndex = 38;
            // 
            // ddamiodaronapost
            // 
            this.ddamiodaronapost.Location = new System.Drawing.Point(476, 423);
            this.ddamiodaronapost.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddamiodaronapost.Name = "ddamiodaronapost";
            this.ddamiodaronapost.Size = new System.Drawing.Size(414, 30);
            this.ddamiodaronapost.TabIndex = 30;
            // 
            // ddamiodaronapre
            // 
            this.ddamiodaronapre.Location = new System.Drawing.Point(476, 392);
            this.ddamiodaronapre.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddamiodaronapre.Name = "ddamiodaronapre";
            this.ddamiodaronapre.Size = new System.Drawing.Size(414, 30);
            this.ddamiodaronapre.TabIndex = 29;
            // 
            // ddiecapost
            // 
            this.ddiecapost.Location = new System.Drawing.Point(476, 356);
            this.ddiecapost.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddiecapost.Name = "ddiecapost";
            this.ddiecapost.Size = new System.Drawing.Size(414, 30);
            this.ddiecapost.TabIndex = 28;
            // 
            // ddiecapre
            // 
            this.ddiecapre.Location = new System.Drawing.Point(476, 320);
            this.ddiecapre.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddiecapre.Name = "ddiecapre";
            this.ddiecapre.Size = new System.Drawing.Size(414, 30);
            this.ddiecapre.TabIndex = 27;
            // 
            // ddbetabloqueantespost
            // 
            this.ddbetabloqueantespost.Location = new System.Drawing.Point(476, 284);
            this.ddbetabloqueantespost.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbetabloqueantespost.Name = "ddbetabloqueantespost";
            this.ddbetabloqueantespost.Size = new System.Drawing.Size(414, 30);
            this.ddbetabloqueantespost.TabIndex = 26;
            // 
            // ddbetabloqueantespre
            // 
            this.ddbetabloqueantespre.Location = new System.Drawing.Point(476, 248);
            this.ddbetabloqueantespre.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbetabloqueantespre.Name = "ddbetabloqueantespre";
            this.ddbetabloqueantespre.Size = new System.Drawing.Size(414, 30);
            this.ddbetabloqueantespre.TabIndex = 25;
            // 
            // ddritmo
            // 
            this.ddritmo.Location = new System.Drawing.Point(476, 212);
            this.ddritmo.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddritmo.Name = "ddritmo";
            this.ddritmo.Size = new System.Drawing.Size(414, 30);
            this.ddritmo.TabIndex = 24;
            // 
            // ddfc
            // 
            this.ddfc.Location = new System.Drawing.Point(476, 176);
            this.ddfc.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfc.Name = "ddfc";
            this.ddfc.Size = new System.Drawing.Size(414, 30);
            this.ddfc.TabIndex = 23;
            // 
            // ddhtap
            // 
            this.ddhtap.Location = new System.Drawing.Point(476, 140);
            this.ddhtap.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddhtap.Name = "ddhtap";
            this.ddhtap.Size = new System.Drawing.Size(414, 30);
            this.ddhtap.TabIndex = 22;
            // 
            // ddvalvulopatia
            // 
            this.ddvalvulopatia.Location = new System.Drawing.Point(476, 104);
            this.ddvalvulopatia.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddvalvulopatia.Name = "ddvalvulopatia";
            this.ddvalvulopatia.Size = new System.Drawing.Size(414, 30);
            this.ddvalvulopatia.TabIndex = 21;
            // 
            // ddfevi
            // 
            this.ddfevi.Location = new System.Drawing.Point(476, 68);
            this.ddfevi.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfevi.Name = "ddfevi";
            this.ddfevi.Size = new System.Drawing.Size(414, 30);
            this.ddfevi.TabIndex = 20;
            // 
            // ddeco
            // 
            this.ddeco.Location = new System.Drawing.Point(476, 32);
            this.ddeco.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddeco.Name = "ddeco";
            this.ddeco.Size = new System.Drawing.Size(414, 30);
            this.ddeco.TabIndex = 19;
            // 
            // ddta
            // 
            this.ddta.Location = new System.Drawing.Point(13, 392);
            this.ddta.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddta.Name = "ddta";
            this.ddta.Size = new System.Drawing.Size(414, 30);
            this.ddta.TabIndex = 11;
            // 
            // ddbasal
            // 
            this.ddbasal.Location = new System.Drawing.Point(13, 356);
            this.ddbasal.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddbasal.Name = "ddbasal";
            this.ddbasal.Size = new System.Drawing.Size(414, 30);
            this.ddbasal.TabIndex = 10;
            // 
            // ddecocardiograma
            // 
            this.ddecocardiograma.Location = new System.Drawing.Point(13, 320);
            this.ddecocardiograma.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddecocardiograma.Name = "ddecocardiograma";
            this.ddecocardiograma.Size = new System.Drawing.Size(414, 30);
            this.ddecocardiograma.TabIndex = 9;
            // 
            // ddanticoagulacion
            // 
            this.ddanticoagulacion.Location = new System.Drawing.Point(13, 284);
            this.ddanticoagulacion.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddanticoagulacion.Name = "ddanticoagulacion";
            this.ddanticoagulacion.Size = new System.Drawing.Size(414, 30);
            this.ddanticoagulacion.TabIndex = 8;
            // 
            // ddfa
            // 
            this.ddfa.Location = new System.Drawing.Point(13, 248);
            this.ddfa.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddfa.Name = "ddfa";
            this.ddfa.Size = new System.Drawing.Size(414, 30);
            this.ddfa.TabIndex = 7;
            // 
            // ddicc
            // 
            this.ddicc.Location = new System.Drawing.Point(13, 212);
            this.ddicc.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddicc.Name = "ddicc";
            this.ddicc.Size = new System.Drawing.Size(414, 30);
            this.ddicc.TabIndex = 6;
            // 
            // ddsincopes
            // 
            this.ddsincopes.Location = new System.Drawing.Point(13, 176);
            this.ddsincopes.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddsincopes.Name = "ddsincopes";
            this.ddsincopes.Size = new System.Drawing.Size(414, 30);
            this.ddsincopes.TabIndex = 5;
            // 
            // ddcardiopatia
            // 
            this.ddcardiopatia.Location = new System.Drawing.Point(14, 140);
            this.ddcardiopatia.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddcardiopatia.Name = "ddcardiopatia";
            this.ddcardiopatia.Size = new System.Drawing.Size(414, 30);
            this.ddcardiopatia.TabIndex = 4;
            // 
            // dddislipemia
            // 
            this.dddislipemia.Location = new System.Drawing.Point(13, 104);
            this.dddislipemia.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddislipemia.Name = "dddislipemia";
            this.dddislipemia.Size = new System.Drawing.Size(414, 30);
            this.dddislipemia.TabIndex = 3;
            // 
            // dddiabetes_1
            // 
            this.dddiabetes_1.Location = new System.Drawing.Point(13, 68);
            this.dddiabetes_1.MinimumSize = new System.Drawing.Size(278, 30);
            this.dddiabetes_1.Name = "dddiabetes_1";
            this.dddiabetes_1.Size = new System.Drawing.Size(414, 30);
            this.dddiabetes_1.TabIndex = 2;
            // 
            // ddhta_1
            // 
            this.ddhta_1.Location = new System.Drawing.Point(13, 32);
            this.ddhta_1.MinimumSize = new System.Drawing.Size(278, 30);
            this.ddhta_1.Name = "ddhta_1";
            this.ddhta_1.Size = new System.Drawing.Size(414, 30);
            this.ddhta_1.TabIndex = 1;
            // 
            // FrmCreateOrUpdateSCA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1380, 1026);
            this.Controls.Add(this.pageviewICC);
            this.Controls.Add(this.pnlBotonera);
            this.Controls.Add(this.radPanel1);
            this.MinimumSize = new System.Drawing.Size(1300, 858);
            this.Name = "FrmCreateOrUpdateSCA";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "FrmCreateOrUpdateSCA";
            this.Load += new System.EventHandler(this.FrmCreateOrUpdateSCA_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pageviewICC)).EndInit();
            this.pageviewICC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBotonera)).EndInit();
            this.pnlBotonera.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddPacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtnombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFechaIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnhc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSexo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfechaingreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNHC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSexo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPacienteToAssing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlCausaExitus)).EndInit();
            this.pnlCausaExitus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCausaExitus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel pageviewICC;
        private Telerik.WinControls.UI.RadButton btnExit;
        private Telerik.WinControls.UI.RadPanel pnlBotonera;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadDropDownList ddPacientes;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel txtnombre;
        private Telerik.WinControls.UI.RadLabel txtFechaAlta;
        private Telerik.WinControls.UI.RadLabel lblFechaAlta;
        private Telerik.WinControls.UI.RadLabel txtFechaNacimiento;
        private Telerik.WinControls.UI.RadLabel lblFechaNacimiento;
        private Telerik.WinControls.UI.RadLabel txtFechaIngreso;
        private Telerik.WinControls.UI.RadLabel txtnhc;
        private Telerik.WinControls.UI.RadLabel txtSexo;
        private Telerik.WinControls.UI.RadLabel lblfechaingreso;
        private Telerik.WinControls.UI.RadLabel lblNHC;
        private Telerik.WinControls.UI.RadLabel lblSexo;
        private Telerik.WinControls.UI.RadLabel lblNombre;
        private Telerik.WinControls.UI.RadLabel lblPacienteToAssing;
        private MD_Controls.InputTime inputTimehorapicotensional_diurno;
        private MD_Controls.InputTime inputTimehorapicotensional_nocturno;
        private MD_Controls.InputDecimals decimalMediaFCDuranteIngreso;
        private MD_Controls.DropDownMedical ddRevascularizacion;
        private MD_Controls.DropDownMedical ddNyha;
        private MD_Controls.DropDownMedical ddAnyoDM;
        private MD_Controls.InputNumeric numericSumFraminghanMenores;
        private MD_Controls.InputNumeric numericFCalIngreso;
        private MD_Controls.InputNumeric numericSumFraminghanMayores;
        private MD_Controls.InputNumeric numericNivelesHBA;
        private MD_Controls.InputDecimals decimalMediaTensionalNocturno;
        private MD_Controls.InputDecimals decimalPicoTensionalNocturno;
        private MD_Controls.InputDecimals decimalMediaTensionalDiurno;
        private MD_Controls.InputDecimals deciamlPicoTensionalDiurno;
        private MD_Controls.InputDecimals deciamlCreatininaAlta;
        private MD_Controls.InputDecimals decimalCreatininiIngreso;
        private Telerik.WinControls.UI.RadPanel pnlCausaExitus;
        private Telerik.WinControls.UI.RadTextBoxControl txtCausaExitus;
        private MD_Controls.DropDownMedical ddexitus;
        private MD_Controls.DropDownMedical ddstatus;
        private MD_Controls.DropDownMedical ddingresos6;
        private MD_Controls.DropDownMedical ddpatron;
        private MD_Controls.DropDownMedical ddregistro;
        private MD_Controls.DropDownMedical ddmapa;
        private MD_Controls.DropDownMedical ddantialdosteronicospost;
        private MD_Controls.DropDownMedical ddantialdosteronicospre;
        private MD_Controls.DropDownMedical dddigoxinapost;
        private MD_Controls.DropDownMedical dddigoxinapre;
        private MD_Controls.DropDownMedical ddamiodaronapost;
        private MD_Controls.DropDownMedical ddamiodaronapre;
        private MD_Controls.DropDownMedical ddiecapost;
        private MD_Controls.DropDownMedical ddiecapre;
        private MD_Controls.DropDownMedical ddbetabloqueantespost;
        private MD_Controls.DropDownMedical ddbetabloqueantespre;
        private MD_Controls.DropDownMedical ddritmo;
        private MD_Controls.DropDownMedical ddfc;
        private MD_Controls.DropDownMedical ddhtap;
        private MD_Controls.DropDownMedical ddvalvulopatia;
        private MD_Controls.DropDownMedical ddfevi;
        private MD_Controls.DropDownMedical ddeco;
        private MD_Controls.DropDownMedical ddta;
        private MD_Controls.DropDownMedical ddbasal;
        private MD_Controls.DropDownMedical ddecocardiograma;
        private MD_Controls.DropDownMedical ddanticoagulacion;
        private MD_Controls.DropDownMedical ddfa;
        private MD_Controls.DropDownMedical ddicc;
        private MD_Controls.DropDownMedical ddsincopes;
        private MD_Controls.DropDownMedical ddcardiopatia;
        private MD_Controls.DropDownMedical dddislipemia;
        private MD_Controls.DropDownMedical dddiabetes_1;
        private MD_Controls.DropDownMedical ddhta_1;
    }
}
