﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using medical_Data_Lib;
using medicalWPF.MD_Forms.Management;
using Telerik.WinControls.UI;
using Telerik.Charting;
using System.Linq;
using medicalWPF.MD_Forms.Reports;

namespace medicalWPF.MD_Forms.General
{
    public partial class FrmPrincipalMDI : Telerik.WinControls.UI.RadForm
    {
        public FrmPrincipalMDI()
        {
            InitializeComponent();
        }
        Timer timer1 = new Timer();

        private void btnCreatePacientes_Click(object sender, EventArgs e)
        {
            /*  if (DialogResult.No == RadMessageBox.Show(this, "Se va a generar un nuevo paciente. Desea continuar?", "Creación Paciente", MessageBoxButtons.YesNo))
                  return;

              DateTime dateBirth;
              try
              {
                  Random rnd = new Random();
                  dateBirth = new DateTime(1993, 07, rnd.Next(1, 12));
              }
              catch (Exception)
              {

                  dateBirth = DateTime.Now;
              }
              var sMessage = MdPacientes.AddPaciente(dateBirth);
              if (sMessage != "")
                  RadMessageBox.Show(this, sMessage, this.Text, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
              else
              {
                  radGridView1.DataSource = MdPacientes.GetPacientes();
              }


              //  MdPacientes.AddPaciente(dateBirth);
              //  MdPacientes.GetPacientes();*/
        }

        private void FrmPrincipalMDI_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            timer1.Interval = 1000;
            timer1.Tick += timer1_Tick;
            timer1.Start();
            this.WindowState = FormWindowState.Maximized;
            BarSeries bar = new BarSeries();
            bar.BackColor = radChartView1.Series.FirstOrDefault().BackColor;
            bar.DataPoints.Add(new CategoricalDataPoint() { Category = "30-50", Value = 20 });
            bar.DataPoints.Add(new CategoricalDataPoint() { Category = "50-90", Value = 80 });
            radChartView1.Series.Add(bar);



        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            txtDateNow.Text = "Fecha :" + DateTime.Now.ToString();
        }


        private void txtTitleManagementOption_Click(object sender, EventArgs e)
        {
            if (CheckFormOpen("FrmManagePacientes"))
            {
                RadMessageBox.Show("El formulario ya esta actualmente en uso", "Medical Management");
                return;
            }
            FrmManagePacientes frmManagePaciente = new FrmManagePacientes();
            frmManagePaciente.Show();
            // frmManagePaciente.ShowDialog(this);
        }

        private void txtSearchOption_Click(object sender, EventArgs e)
        {
            if (CheckFormOpen("FrmManageSCAICC"))
            {
                RadMessageBox.Show("El formulario ya esta actualmente en uso", "Medical Management");
                return;
            }
            FrmManageSCAICC frmManagePaciente = new FrmManageSCAICC();
            frmManagePaciente.Show();
            // frmManagePaciente.ShowDialog(this);
        }

        private bool CheckFormOpen(string sFormName)
        {
            bool bFound = false;

            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                if (frm.Name == sFormName)
                {
                    bFound = true;
                    break;
                }
            }
            return bFound;
        }

        private void txtReportsOption_Click(object sender, EventArgs e)
        {
            FrmExportSCAICC frmExpSCAICC = new FrmExportSCAICC();
           // frmExpSCAICC.MdiParent = this;
            frmExpSCAICC.Show();
        }
    }
}
