﻿namespace medicalWPF.MD_Forms.General
{
    partial class FrmPrincipalMDI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis1 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis1 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries1 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries2 = new Telerik.WinControls.UI.BarSeries();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint1 = new Telerik.Charting.CategoricalDataPoint();
            Telerik.Charting.CategoricalDataPoint categoricalDataPoint2 = new Telerik.Charting.CategoricalDataPoint();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.txtTitleManagementOption = new Telerik.WinControls.UI.RadLabel();
            this.btnDeletePaciente = new Telerik.WinControls.UI.RadButton();
            this.btnCreatePacientes = new Telerik.WinControls.UI.RadButton();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.txtSearchOption = new Telerik.WinControls.UI.RadLabel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.txtReportsOption = new Telerik.WinControls.UI.RadLabel();
            this.txtAppTitle = new Telerik.WinControls.UI.RadLabel();
            this.txtDateNow = new Telerik.WinControls.UI.RadLabel();
            this.pnlSummary = new Telerik.WinControls.UI.RadPanel();
            this.office2010BlueTheme1 = new Telerik.WinControls.Themes.Office2010BlueTheme();
            this.radChartView1 = new Telerik.WinControls.UI.RadChartView();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleManagementOption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeletePaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreatePacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearchOption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReportsOption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAppTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateNow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSummary)).BeginInit();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.txtTitleManagementOption);
            this.radPanel1.Controls.Add(this.btnDeletePaciente);
            this.radPanel1.Controls.Add(this.btnCreatePacientes);
            this.radPanel1.Location = new System.Drawing.Point(1, 52);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(221, 165);
            this.radPanel1.TabIndex = 0;
            this.radPanel1.Text = "radPanel1";
            // 
            // txtTitleManagementOption
            // 
            this.txtTitleManagementOption.AutoSize = false;
            this.txtTitleManagementOption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTitleManagementOption.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitleManagementOption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtTitleManagementOption.Location = new System.Drawing.Point(0, 0);
            this.txtTitleManagementOption.Name = "txtTitleManagementOption";
            this.txtTitleManagementOption.Size = new System.Drawing.Size(221, 165);
            this.txtTitleManagementOption.TabIndex = 6;
            this.txtTitleManagementOption.Text = "Gestión de Pacientes";
            this.txtTitleManagementOption.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtTitleManagementOption.Click += new System.EventHandler(this.txtTitleManagementOption_Click);
            // 
            // btnDeletePaciente
            // 
            this.btnDeletePaciente.Location = new System.Drawing.Point(11, 108);
            this.btnDeletePaciente.Name = "btnDeletePaciente";
            this.btnDeletePaciente.Size = new System.Drawing.Size(110, 24);
            this.btnDeletePaciente.TabIndex = 7;
            this.btnDeletePaciente.Text = "Eliminar";
            this.btnDeletePaciente.Visible = false;
            // 
            // btnCreatePacientes
            // 
            this.btnCreatePacientes.Location = new System.Drawing.Point(11, 40);
            this.btnCreatePacientes.Name = "btnCreatePacientes";
            this.btnCreatePacientes.Size = new System.Drawing.Size(110, 24);
            this.btnCreatePacientes.TabIndex = 6;
            this.btnCreatePacientes.Text = "Crear";
            this.btnCreatePacientes.Visible = false;
            this.btnCreatePacientes.Click += new System.EventHandler(this.btnCreatePacientes_Click);
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.txtSearchOption);
            this.radPanel2.Location = new System.Drawing.Point(244, 52);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(221, 165);
            this.radPanel2.TabIndex = 1;
            this.radPanel2.Text = "radPanel2";
            // 
            // txtSearchOption
            // 
            this.txtSearchOption.AutoSize = false;
            this.txtSearchOption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSearchOption.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchOption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtSearchOption.Location = new System.Drawing.Point(0, 0);
            this.txtSearchOption.Name = "txtSearchOption";
            this.txtSearchOption.Size = new System.Drawing.Size(221, 165);
            this.txtSearchOption.TabIndex = 7;
            this.txtSearchOption.Text = "Gestión de SCA e ICC";
            this.txtSearchOption.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtSearchOption.Click += new System.EventHandler(this.txtSearchOption_Click);
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.txtReportsOption);
            this.radPanel3.Location = new System.Drawing.Point(489, 52);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(221, 165);
            this.radPanel3.TabIndex = 2;
            this.radPanel3.Text = "radPanel3";
            // 
            // txtReportsOption
            // 
            this.txtReportsOption.AutoSize = false;
            this.txtReportsOption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReportsOption.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReportsOption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtReportsOption.Location = new System.Drawing.Point(0, 0);
            this.txtReportsOption.Name = "txtReportsOption";
            this.txtReportsOption.Size = new System.Drawing.Size(221, 165);
            this.txtReportsOption.TabIndex = 7;
            this.txtReportsOption.Text = "Informes";
            this.txtReportsOption.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtReportsOption.Click += new System.EventHandler(this.txtReportsOption_Click);
            // 
            // txtAppTitle
            // 
            this.txtAppTitle.AutoSize = false;
            this.txtAppTitle.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAppTitle.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtAppTitle.Location = new System.Drawing.Point(3, 4);
            this.txtAppTitle.Name = "txtAppTitle";
            this.txtAppTitle.Size = new System.Drawing.Size(219, 34);
            this.txtAppTitle.TabIndex = 3;
            this.txtAppTitle.Text = "Medical Management";
            // 
            // txtDateNow
            // 
            this.txtDateNow.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDateNow.Location = new System.Drawing.Point(567, 18);
            this.txtDateNow.Name = "txtDateNow";
            this.txtDateNow.Size = new System.Drawing.Size(149, 18);
            this.txtDateNow.TabIndex = 4;
            this.txtDateNow.Text = "Fecha: 24/Julio/2017 12:48";
            // 
            // pnlSummary
            // 
            this.pnlSummary.Controls.Add(this.txtDateNow);
            this.pnlSummary.Controls.Add(this.txtAppTitle);
            this.pnlSummary.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSummary.Location = new System.Drawing.Point(0, 0);
            this.pnlSummary.Name = "pnlSummary";
            this.pnlSummary.Size = new System.Drawing.Size(721, 46);
            this.pnlSummary.TabIndex = 5;
            // 
            // radChartView1
            // 
            this.radChartView1.AreaDesign = cartesianArea1;
            categoricalAxis1.IsPrimary = true;
            categoricalAxis1.LabelRotationAngle = 300D;
            categoricalAxis1.Title = "";
            linearAxis1.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis1.IsPrimary = true;
            linearAxis1.LabelRotationAngle = 300D;
            linearAxis1.MajorStep = 10D;
            linearAxis1.TickOrigin = null;
            linearAxis1.Title = "";
            this.radChartView1.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis1,
            linearAxis1});
            this.radChartView1.Location = new System.Drawing.Point(12, 223);
            this.radChartView1.Name = "radChartView1";
            barSeries1.HorizontalAxis = categoricalAxis1;
            barSeries1.LabelFormat = "adadadaa";
            barSeries1.LabelMode = Telerik.WinControls.UI.BarLabelModes.Center;
            barSeries1.LegendTitle = "sdsd";
            barSeries1.VerticalAxis = linearAxis1;
            categoricalDataPoint1.Category = "< 20 años";
            categoricalDataPoint1.Label = 56D;
            categoricalDataPoint1.Value = 56D;
            categoricalDataPoint2.Category = "20-30 años";
            categoricalDataPoint2.Label = 10D;
            categoricalDataPoint2.Value = 10D;
            barSeries2.DataPoints.AddRange(new Telerik.Charting.DataPoint[] {
            categoricalDataPoint1,
            categoricalDataPoint2});
            barSeries2.HorizontalAxis = categoricalAxis1;
            barSeries2.LabelFormat = "adadad";
            barSeries2.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries2.LegendTitle = "sdsddf";
            barSeries2.VerticalAxis = linearAxis1;
            this.radChartView1.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries1,
            barSeries2});
            this.radChartView1.ShowGrid = false;
            this.radChartView1.ShowTitle = true;
            this.radChartView1.Size = new System.Drawing.Size(698, 258);
            this.radChartView1.TabIndex = 6;
            this.radChartView1.Text = "radChartView1";
            this.radChartView1.ThemeName = "Office2010Blue";
            this.radChartView1.Title = "Pacientes en el sistema";
            ((Telerik.WinControls.UI.RadChartElement)(this.radChartView1.GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadChartElement)(this.radChartView1.GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // FrmPrincipalMDI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 493);
            this.Controls.Add(this.radChartView1);
            this.Controls.Add(this.pnlSummary);
            this.Controls.Add(this.radPanel3);
            this.Controls.Add(this.radPanel2);
            this.Controls.Add(this.radPanel1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(729, 523);
            this.MinimumSize = new System.Drawing.Size(729, 523);
            this.Name = "FrmPrincipalMDI";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(729, 523);
            this.Text = "Medical Management-Menú Principal";
            this.Load += new System.EventHandler(this.FrmPrincipalMDI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTitleManagementOption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeletePaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreatePacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSearchOption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtReportsOption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAppTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateNow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSummary)).EndInit();
            this.pnlSummary.ResumeLayout(false);
            this.pnlSummary.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel txtTitleManagementOption;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadLabel txtSearchOption;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadLabel txtReportsOption;
        private Telerik.WinControls.UI.RadLabel txtAppTitle;
        private Telerik.WinControls.UI.RadLabel txtDateNow;
        private Telerik.WinControls.UI.RadPanel pnlSummary;
        private Telerik.WinControls.UI.RadButton btnDeletePaciente;
        private Telerik.WinControls.UI.RadButton btnCreatePacientes;
        private Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme1;
        private Telerik.WinControls.UI.RadChartView radChartView1;
    }
}
